//
//  HelpCenterVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 31/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import ObjectMapper
class HelpCenterVC: AppViewController, UITableViewDataSource, UITableViewDelegate {
   
    
    //.............MARK: Variables.........................
    var type = String()
    let controller = UserViewModel()
    var userInquiryStatusList : [UserInquiryStatus] = [];
    
    //.............MARK: IBOutlets.........................

    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var tblTickets: UITableView!
    @IBOutlet weak var noTicketFoundView: UIView!
    @IBOutlet weak var noCloseTicketView: UIView!
    @IBOutlet weak var btnCloseTicket: UIButton!
  
    //-------------------------------------------------------
    //MARK: View Life Cycle
    //-------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noTicketFoundView.isHidden = true
        self.noTicketFoundView.isHidden = true
        self.btnCloseTicket.setTitle(AppHelper.localizedtext(key: "kNoTicketAvailable"), for: .normal);
//        self.title = AppHelper.localizedtext(key: "HelpCenter")
        self.title = AppHelper.localizedtext(key: "QRCodeInvite")
        self.type = "open"
        let fontAttribute = [NSAttributedString.Key.font: Font.tagViewFont,
                             NSAttributedString.Key.foregroundColor: UIColor.black]
        segmentControl.setTitleTextAttributes(fontAttribute, for: .normal)
        
        let fontAttribute1 = [NSAttributedString.Key.font: Font.tagViewFont,
                             NSAttributedString.Key.foregroundColor: UIColor.white]
        segmentControl.setTitleTextAttributes(fontAttribute1, for: .selected)
        if #available(iOS 13.0, *) {
            segmentControl.selectedSegmentTintColor = COLOR.textColor
        } else {
            segmentControl.tintColor = COLOR.textColor
            // Fallback on earlier versions
        }
        let add = UIBarButtonItem(image: UIImage(named: "Plus_WithBackground"), style: .plain, target: self, action: #selector(btnAddAction(_:)))
        self.navigationItem.rightBarButtonItem  = add
        self.setTitleView()
        
    }

    @IBAction func btnCreateNewTicket(_ sender: AnyObject) {
        let raiseTicketVC = self.storyboard?.instantiateViewController(withIdentifier: "RaiseTicketVC")as! RaiseTicketVC
        self.navigationController?.pushViewController(raiseTicketVC, animated: true)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       // self.callGetUserInquiryStatusApi(type)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.type = "open"
        self.callGetUserInquiryStatusApi(type)
    }
    //-------------------------------------------------------
    //MARK: segment Control methods
    //-------------------------------------------------------
    @IBAction func indexChangedSegment(_ sender: Any) {
        switch self.segmentControl.selectedSegmentIndex {
        case 0:
            self.type = "open"
            self.callGetUserInquiryStatusApi(type)
            break
        case 1:
            self.type = "closed"
            self.callGetUserInquiryStatusApi(type)
            break
            
        default:
            break
        }
    }
     //-------------------------------------------------------
        //MARK: Navigation item action
     //-------------------------------------------------------
    @objc func btnAddAction(_ sender: Any) {
        let raiseTicketVC = self.storyboard?.instantiateViewController(withIdentifier: "RaiseTicketVC")as! RaiseTicketVC
        self.navigationController?.pushViewController(raiseTicketVC, animated: true)
    }
    //-------------------------------------------------------
    //MARK: UItableview datasource methods
    //-------------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userInquiryStatusList.count
    }
    //-------------------------------------------------------
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "OngoingOrCompletedTableCell", for: indexPath) as! OngoingOrCompletedTableCell
        cell.lblEventName.text = self.userInquiryStatusList[indexPath.row].message ?? ""
        return cell
    }
    
    //-------------------------------------------------------
    //MARK: UItableview delegate methods
    //-------------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let ticketDetailVc = self.storyboard?.instantiateViewController(withIdentifier: "TicketDetailsVC") as! TicketDetailsVC
        ticketDetailVc.userInquiryStatusObj = self.userInquiryStatusList[indexPath.row]
        self.navigationController?.pushViewController(ticketDetailVc, animated: true)
        
    }
    //-------------------------------------------------------
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    //--------------------------------------------------------------------
    //MARK: Web api methods
    //--------------------------------------------------------------------
    func callGetUserInquiryStatusApi(_ type : String)  {
        
        self.showActivityIndicatory()
        controller.getUserInquiryStatus(type: type) { (success, userInquiryObj, message, statusCode) in
            
            if success{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    print(userInquiryObj)
                    self.userInquiryStatusList = userInquiryObj as? [UserInquiryStatus] ?? []
                     self.userInquiryStatusList = self.userInquiryStatusList.sorted { ($0.name as? String ?? "").localizedCaseInsensitiveCompare(($1.name as? String ?? "")) == ComparisonResult.orderedAscending }
                    if(self.type == "open"){
                        if(self.userInquiryStatusList.count <= 0){
                             self.noTicketFoundView.isHidden = false
                            self.noCloseTicketView.isHidden = true
                             self.tblTickets.isHidden = true
                        }else{
                            self.noCloseTicketView.isHidden = true
                            self.noTicketFoundView.isHidden = true
                            self.tblTickets.isHidden = false
                        }
                    }
                    else{
                        if(self.userInquiryStatusList.count <= 0){
                             self.noTicketFoundView.isHidden = true
                            self.noCloseTicketView.isHidden = false
                            self.tblTickets.isHidden = true
                        }else{
                             self.noTicketFoundView.isHidden = true
                            self.noCloseTicketView.isHidden = true
                            self.tblTickets.isHidden = false
                        }
                    }
                    
                    
                    self.tblTickets.reloadData()
                }
            }else{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    self.showAlert(message: message, title: kAppName)
                }
            }
            
        }
    }
}
