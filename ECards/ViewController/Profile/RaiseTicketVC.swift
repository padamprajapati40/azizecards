//
//  RaiseTicketVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 31/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit


class RaiseTicketVC: AppViewController , UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
 
    //MARK: Variables.........................
    var arrSubjectList = [String]()
    let controller = UserViewModel()
    var arrInquirySubjects = [[String:Any]]()
    var isRequestCall = Bool()
    var subject_id = Int()
    var name : String = String()
    var email : String = String()
    var phone_number : String = String()
    //MARK: IBOutlets.........................
    
    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet var lblCommentTitle: UILabel!
    @IBOutlet var toolbarObj: UIToolbar!
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtComment: UITextView!
    @IBOutlet weak var txtTicket: BWTextField!
    
    @IBOutlet weak var btnRequestCall: UIButton!
    //---------------------------------------------------
    //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        let langName = GlobalUtils.getInstance().getApplicationLanguage()
        if(langName == "arabic"){
            self.lblCommentTitle.textAlignment = .right
        
        }else{
            self.lblCommentTitle.textAlignment = .left
            
        }
//        self.title = AppHelper.localizedtext(key: "RaiseTicket")
        self.title = AppHelper.localizedtext(key: "QRCodeInvite")
        self.btnSubmit.titleLabel?.font = Font.tagViewFont
        self.DefalutDialoguesettingOfPopup()
        self.callGetProfileApi()
        self.callGetUserInquirySubjects()
        
        self.txtComment.placeholder = AppHelper.localizedtext(key: "kComments")
        // self.txtComment.insertTextPlaceholder(with: AppHelper.localizedtext(key: "kComments"))
        self.setTitleView()
    }
    
    //-------------------------------------------------------
    //MARK: UITextfield delegateMethods
    //-------------------------------------------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "SecondrayColor")
        if textField == self.txtTicket{
            if self.arrInquirySubjects.count != 0 {
                textField.inputView = self.pickerView
                textField.inputAccessoryView = self.toolbarObj
            }else{
                self.showAlert(message: ValidationAlert.kNoData, title: kAppName)
            }
            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "lightGray")
    }
    
    //---------------------------------------------------
        //MARK: Done selector
    //---------------------------------------------------
    @IBAction func btnDoneSelector(_ sender: Any) {
        self.view.endEditing(true)
    }
    //-------------------------------------------------------
    //MARK: Web api methods
    //-------------------------------------------------------
    func callGetProfileApi()  {
        
        self.showActivityIndicatory()
        controller.getProfileDetails { (success, userObj, message, statusCode) in
            
            if success{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    print(success)
                    print(userObj)
                    
                    var userObj : User = userObj as! User
                    self.setDateOfProfile(userObj)
                }
            }else{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    self.showAlert(message: message, title: kAppName)
                }
            }
        }
        
    }
    //-------------------------------------------------------
    //MARK: Custom methods
    //-------------------------------------------------------
    
    func setDateOfProfile(_ userObj : User)  {
        
        let fname : String = userObj.first_name ?? ""
        let lName : String = userObj.last_name ?? ""
        let email : String = userObj.email ?? ""
        let phone_number : String = userObj.phone_number ?? ""
        let city : String = userObj.city ?? ""
        let country : String = userObj.country ?? ""
        
        
        self.name = fname
        if self.name == ""{
            self.name = "test"
        }
        self.email = email
        self.phone_number = phone_number
  
        
    }
    //---------------------------------------------------
    //MARK: UIbutton Actions
    //---------------------------------------------------
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        if self.txtTicket.text == "" {
            self.showAlert(message: AppHelper.localizedtext(key: "kEmptySubjectType"), title: kAppName)
            
        }else if self.txtComment.text == ""{
            self.showAlert(message: AppHelper.localizedtext(key: "kEmptyComment"), title: kAppName)
        }else{
            
            self.showActivityIndicatory()
            controller.raiseTicket(Name: self.name, Email: self.email, message: self.txtComment.text!, PhoneNumber: self.phone_number, user_inquiry_subject_id: self.subject_id,requestCall: self.isRequestCall) { (success, response, message, statusCode) in
                
                if success{
                    DispatchQueue.main.async {
                        self.hideActivityIndicator()
                        print(response)
                        print(message)
                        
                        
                        self.showAlertWithCallBack(title: kAppName, message: message, callBack: {
                            let successVc = self.storyboard?.instantiateViewController(withIdentifier: "EventSuccessVC") as! EventSuccessVC
                            successVc.isOpenFromTicket = true
                            self.navigationController?.pushViewController(successVc, animated: true)
                        })
                    }
                }else{
                    DispatchQueue.main.async {
                        self.hideActivityIndicator()
                        self.showAlert(message: message, title: kAppName)
                    }
                }
                
            
            }
        }
        
    }
    //---------------------------------------------------
    @IBAction func btnRequestCallAction(_ sender: Any) {
        let imgUnSelected = UIImage(named: "Circle_Gray")
        let imgSelected = UIImage(named: "circle")
        
        if self.isRequestCall{
            self.isRequestCall = false
            self.btnRequestCall.setImage(imgUnSelected, for: .normal)   
        }else{
            self.isRequestCall = true
            self.btnRequestCall.setImage(imgSelected, for: .normal)
        }
    }
    //---------------------------------------------------
    //MARK: UIpickerview datasource Methods
    //---------------------------------------------------
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    //--------------------------------------------------------------------
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrInquirySubjects.count
    }
    
    
    //---------------------------------------------------
    //MARK: UIpickerview delegate Methods
    //---------------------------------------------------
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return self.arrInquirySubjects[row]["name"] as? String ?? ""
        //return self.arrSubjectList[row] ?? ""
    }
    //--------------------------------------------------------------------
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // self.txtTicket.text = self.arrSubjectList[row] ?? ""
        self.txtTicket.text = self.arrInquirySubjects[row]["name"] as? String ?? ""
        self.subject_id = self.arrInquirySubjects[row]["id"] as? Int ?? 0
     }
    
    
    //---------------------------------------------------
    //MARK: WebApi Methods
    //---------------------------------------------------
    func  callGetUserInquirySubjects()  {
        
        self.showActivityIndicatory()
        controller.getUserInquirySubjects { (success, response, message, statusCode) in
            
            if success{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    print(response)
                    print(statusCode)
                    print(message)
                    
                    let arrResponse : [[String:Any]] = response as? [[String:Any]] ?? []
                    self.arrInquirySubjects = arrResponse
                    print(self.arrInquirySubjects)
//
//                    var subjectList : [String] = arrResponse.map { (dictionary) -> String in
//                        return dictionary["name"] as? String ?? ""
//                    }
//                    subjectList = subjectList.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
//
//                    print(subjectList)
//
//                    self.arrSubjectList = subjectList
//
//                    print(self.arrSubjectList)
                   
                    self.arrInquirySubjects = self.arrInquirySubjects.sorted { ($0["name"] as? String ?? "").localizedCaseInsensitiveCompare(($1["name"] as? String ?? "")) == ComparisonResult.orderedAscending }
                    
                     print(self.arrInquirySubjects)
                 
                }
            }else{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    self.showAlert(message: message, title: kAppName)
                }
            }
        }
        
    }

}
