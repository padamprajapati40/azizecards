//
//  ProfileVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 31/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import PopupDialog


class ProfileVC: AppViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    //..........MARK: Variables.....................
    let imagePicker = UIImagePickerController()
    var photoManager : BWPhotoManager!
      let controller = UserViewModel()
    var isCameraOpen : Bool = false
    var userObj : User?
    //..........MARK: IBOutlets.....................
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var btnLanguageSelection: UIButton!
    @IBOutlet weak var btnHelpCenter: UIButton!
    @IBOutlet weak var btnPrivacyPolicy: UIButton!
    @IBOutlet weak var btnTermsAndCondition: UIButton!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnLoginOrsignup: UIButton!
    @IBOutlet weak var viewSignupOrLogin: UIView!
    @IBOutlet weak var lblLogout: UILabel!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var btnCamera : UIButton!
    //-------------------------------------------------------
    //MARK: View Life Cycle
    //-------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        self.DefalutDialoguesettingOfPopup()
        self.imgUser.image = nil
        self.btnCamera.isHidden = true
        self.btnCamera.isEnabled = false
        let langguageName = GlobalUtils.getInstance().getApplicationLanguage()
               if(langguageName == "arabic"){
                   self.btnLanguageSelection.contentHorizontalAlignment = .right
                   self.btnTermsAndCondition.contentHorizontalAlignment = .right
                   self.btnPrivacyPolicy.contentHorizontalAlignment = .right
                   self.btnHelpCenter.contentHorizontalAlignment = .right
                   
               }else{
                                      self.btnLanguageSelection.contentHorizontalAlignment = .left
                                     self.btnTermsAndCondition.contentHorizontalAlignment = .left
                                     self.btnPrivacyPolicy.contentHorizontalAlignment = .left
                                     self.btnHelpCenter.contentHorizontalAlignment = .left
               }
       if GlobalUtils.getInstance().sessionToken() != ""{
            
            self.viewSignupOrLogin.isHidden = true
            self.scrollview.isHidden = false
        
            let share = UIBarButtonItem(image: UIImage(named: "share"), style: .plain, target: self, action: #selector(btnDoneAction(_ :)))
            self.navigationItem.leftBarButtonItem = share
        
            let edit = UIBarButtonItem(image: UIImage(named: "Edit_pen_blue"), style: .plain, target: self, action: #selector(btnEditAction(_ :)))
            self.navigationItem.rightBarButtonItem = edit
         self.title = AppHelper.localizedtext(key: "Profile")
        
        
        let app_language : String = GlobalUtils.getInstance().getApplicationLanguage()
        self.title = AppHelper.localizedtext(key: "Profile")
        self.btnLanguageSelection.setTitle(AppHelper.localizedtext(key: "LanguageSelection"), for: .normal)
        self.btnTermsAndCondition.setTitle(AppHelper.localizedtext(key: "termsButtonTitle"), for: .normal)
        self.btnPrivacyPolicy.setTitle(AppHelper.localizedtext(key: "kPrivacyPolicy"), for: .normal)
        self.btnHelpCenter.setTitle(AppHelper.localizedtext(key: "HelpCenter"), for: .normal)
        self.lblLogout.text = AppHelper.localizedtext(key: "Logout")
        self.callGetProfileApi()
        
       
     
            
        
        }else{
         self.title = AppHelper.localizedtext(key: "QRCodeInvite")
            self.viewSignupOrLogin.isHidden = false
            self.scrollview.isHidden = true
            self.navigationItem.rightBarButtonItem = nil
        }
        self.setTitleView()
        if GlobalUtils.getInstance().sessionToken() == ""{
                   self.title = AppHelper.localizedtext(key: "QRCodeInvite")
                   self.tabBarController?.tabBar.items?[0].title = AppHelper.localizedtext(key: "ManageEvents")
                   self.tabBarController?.tabBar.items?[1].title = AppHelper.localizedtext(key: "Home")
                   self.tabBarController?.tabBar.items?[2].title = AppHelper.localizedtext(key: "Profile")
                   
        }else{
                    self.title = AppHelper.localizedtext(key: "Home")
                    self.tabBarController?.tabBar.items?[0].title = AppHelper.localizedtext(key: "ManageEvents")
                    self.tabBarController?.tabBar.items?[1].title = AppHelper.localizedtext(key: "Home")
                    self.tabBarController?.tabBar.items?[2].title = AppHelper.localizedtext(key: "Profile")
                   
        }
    }
    
    @objc func btnDoneAction(_ sender: Any) {
        
        let shareText = AppHelper.localizedtext(key: "kShareMSG")

        if let image = UIImage(named: "app_logo") {
            let vc = UIActivityViewController(activityItems: [shareText, image], applicationActivities: [])
            present(vc, animated: true)
        }
          
      }
    
    @objc func btnEditAction(_ sender: Any) {
        
        let editProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        editProfileVC.userObj = userObj
        self.navigationController?.pushViewController(editProfileVC, animated: true)
    }
      
     //-------------------------------------------------------
    //MARK: UIbutton methods
     //-------------------------------------------------------
    @IBAction func btnLoginOrsignupAction(_ sender: Any) {
        let welcomeVC = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeVC")as! WelcomeVC
        // welcomeVC.eventObj = self.EventDetailsList;
        welcomeVC.isOpenFromManageEvents = true
        let navVC = UINavigationController(rootViewController:welcomeVC )
        navVC.modalPresentationStyle = .fullScreen
        self.present(navVC, animated: true, completion: nil);
    }
    //-------------------------------------------------------
    @IBAction func btnTermsAndConditionsAction(_ sender: Any) {
    }
     //-------------------------------------------------------
    @IBAction func btnPrivacyPolicyAction(_ sender: Any) {
    }
    //-------------------------------------------------------
    @IBAction func btnLanguageSelectionAction(_ sender: Any) {
        let languageVC = self.storyboard?.instantiateViewController(withIdentifier: "LanguageSelectionVC") as! LanguageSelectionVC
        languageVC.isOpenFromProfile = true
        
         self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.navigationController?.pushViewController(languageVC, animated: true)
    }
     //-------------------------------------------------------
    @IBAction func btnHelpCenterAction(_ sender: Any) {
       let helpVC = self.storyboard?.instantiateViewController(withIdentifier: "HelpCenterSupportVC") as! HelpCenterSupportVC
               self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
              self.navigationController?.pushViewController(helpVC, animated: true)
    }
     //-------------------------------------------------------
    @IBAction func btnCameraAction(_ sender: Any) {
        isCameraOpen = true;
        photoManager = BWPhotoManager(navigationController: self.navigationController ?? UINavigationController(), mediaType: .image, allowEditing: true, isFromEdit: false, callback: { (chooseImage) in
           
            if chooseImage == nil {
                self.imgUser.image = UIImage(named: "User_Placeholder")
            }else{
                
                self.imgUser.image = chooseImage as! UIImage;
                self.showActivityIndicatory()
                let fName : String = self.userObj?.first_name ?? ""
                let lName : String = self.userObj?.last_name ?? ""
                let country_code : String = self.userObj?.country_code ?? ""
                
                self.controller.updateProfile(picture: chooseImage as! UIImage, FirstName: fName, LastName: lName, Email: self.lblEmail.text!, PhoneNumber: self.lblPhoneNumber.text!, Country_code: country_code) { (success, response, message, statusCode) in
                    self.isCameraOpen = false
                    DispatchQueue.main.async {
                        self.hideActivityIndicator()
                      //  self.showAlert(message: message, title: kAppName)
                    }
                }
            }
                   
               });
    }
     //-------------------------------------------------------
    @IBAction func btnLogoutAction(_ sender: Any) {
        let popUp = PopupDialog(title: kAppName, message: AppHelper.localizedtext(key: "logoutMsg"))
        
        let btnOk = CancelButton(title: AppHelper.localizedtext(key: "YES")) {
            self.logOutApiCall()
        }
        let btnCancel = CancelButton(title: AppHelper.localizedtext(key: "NO")) {
            
        }
        popUp.addButtons([btnOk,btnCancel])
        
        // Present dialog
        self.present(popUp, animated: true, completion: nil)
    }
    
    func logOutApiCall(){
            self.showActivityIndicatory()
            
            controller.logOutApi { (success, response, message, statusCode) in
                
                if success{
                    self.hideActivityIndicator()
                    DispatchQueue.main.async {
                        var sessiontoken : String =  GlobalUtils.getInstance().sessionToken() as? String ?? ""
                        sessiontoken = ""
                        GlobalUtils.getInstance().setSessionToken(token: sessiontoken)
                        self.tabBarController?.selectedIndex = 1
                    }
                }else{
                    self.hideActivityIndicator()
                    DispatchQueue.main.async {
                        self.showAlert(message: message, title: kAppName)
                    }
                }
            }
    }
     //-------------------------------------------------------
    //MARK: Web api methods
    //-------------------------------------------------------
    func callGetProfileApi()  {
        
       // self.showActivityIndicatory()
        controller.getProfileDetails { (success, userObj, message, statusCode) in
            
            if success{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    print(success)
                    print(userObj)
                    
                    var userObj : User = userObj as! User
                    self.userObj = userObj
                    self.setDateOfProfile(userObj)
                }
            }else{
                DispatchQueue.main.async {
                            self.hideActivityIndicator()
                            
                            if statusCode == 422 {
                                self.showAlertWithCallBack(title: kAppName, message: message) {
                                    
                                    var sessiontoken : String =  GlobalUtils.getInstance().sessionToken() as? String ?? ""
                                     // GlobalUtils.getInstance().sessionToken().removeAll()
                                    sessiontoken = ""
                                    GlobalUtils.getInstance().setSessionToken(token: sessiontoken)
                                    let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                    let navVC = UINavigationController(rootViewController:loginVC )
                                           self.present(navVC, animated: true, completion: nil);
                                    //self.navigationController?.pushViewController(loginVC, animated: true)
                
                                   // self.navigationController?.popToViewController(loginVC, animated: true)
                                }
                            }else{
                                self.showAlert(message: message, title: kAppName)
                            }
                        }
            }
        }
        
    }
    //-------------------------------------------------------
    //MARK: Custom methods
    //-------------------------------------------------------
    
    func setDateOfProfile(_ userObj : User)  {
        
         let fname : String = userObj.first_name ?? ""
        let lName : String = userObj.last_name ?? ""
        let email : String = userObj.email ?? ""
        let phone_number : String = userObj.phone_number ?? ""
        let city : String = userObj.city ?? ""
        let country : String = userObj.country ?? ""
        var picture : String  = userObj.picture?.url ?? ""
        let country_code : String = userObj.country_code ?? ""
        
        self.lblName.text = "\(fname) \(lName)"
        self.lblEmail.text = email
        self.lblPhoneNumber.text = "\(country_code)\(phone_number)"
       // self.RoundImage(img: self.imgUser)
        
        GlobalUtils.getInstance().setEmail(email: email)
        
        if picture == "" {
//        self.imgUser.image = UIImage(named: "User_Placeholder")
            self.RoundImage(img: self.imgUser)
        }else{
            let url = URL(string: picture  ?? "")!
            self.imgUser.setImageFromURL(url: url,placeholder:UIImage(named:"User_Placeholder"));
            self.RoundImage(img: self.imgUser)
        }
        
        
       // self.imgUser.image = UIImage(named: "User_Placeholder")
        
        
    }
    
}
