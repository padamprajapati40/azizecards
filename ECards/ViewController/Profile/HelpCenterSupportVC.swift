//
//  HelpCenterSupportVC.swift
//  AzizECards
//
//  Created by Dhruva Madhani on 20/03/20.
//  Copyright © 2020 Dhruva Madhani. All rights reserved.
//

import UIKit

class HelpCenterSupportVC: AppViewController {

    
    //...MARK: Variables.............................
    
    
    
    
     //...MARK: IBOutlets.............................
    @IBOutlet weak var btnSupport : UIButton!
    @IBOutlet weak var btnHowTo : UIButton!
    
    
    //---------------------------------------------------------------------
        //MARK: View Life Cycle
    //---------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = AppHelper.localizedtext(key: "HelpCenter")
        self.setTitleView()
    }
    
    //---------------------------------------------------------------------
         //MARK: IBAction Of UIButton
     //---------------------------------------------------------------------
    @IBAction func btnSupportAction(_ sender: Any) {
        let helpVC = self.storyboard?.instantiateViewController(withIdentifier: "HelpCenterVC") as! HelpCenterVC
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.navigationController?.pushViewController(helpVC, animated: true)
    }
    //---------------------------------------------------------------------
    @IBAction func btnHowToAction(_ sender: Any) {
          let webVc = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
          webVc.isOpenForTutorials = true
          self.navigationController?.pushViewController(webVc, animated: true)
      }
    
    //------------------------------------------------
           //MARK: Custom Methods
    //------------------------------------------------

}
