//
//  EditProfileVC.swift
//  AzizECards
//
//  Created by Dhruva Madhani on 19/03/20.
//  Copyright © 2020 Dhruva Madhani. All rights reserved.
//

import UIKit
import PopupDialog
import CountryPickerView
class EditProfileVC: AppViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextFieldDelegate {

    
    //...MARK: Variables.............................
    var userObj : User?
    let controller = UserViewModel()
    let imagePicker = UIImagePickerController()
    var photoManager : BWPhotoManager!
    var isCameraOpen : Bool = false
    let cp = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
    var country_code = String()
    
    //...MARK: IBOutlets.............................
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtNumber : UITextField!
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var btnCamera : UIButton!
    @IBOutlet weak var imgUser : UIImageView!
    @IBOutlet weak var txtPhoneCode : BWTextField!
    @IBOutlet weak var txtFirstName : BWTextField!
    @IBOutlet weak var txtLastName : BWTextField!
  //  @IBOutlet weak var cp : CountryPickerView!
    //---------------------------------------------------------------------
        //MARK: View Life Cycle
    //---------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        self.title = AppHelper.localizedtext(key: "kEditProfile")
        print(userObj)
        
//        self.txtPhoneCode.leftView = cp
//        self.txtPhoneCode.leftViewMode = .always
        //self.viewCountry = cp
        self.cp.dataSource = self
        self.cp.delegate = self
        self.cp.frame = CGRect(x: 10, y: 0, width: self.txtPhoneCode.frame.size.width, height: self.txtPhoneCode.frame.size.height)
       
        self.txtPhoneCode.addSubview(self.cp);
        self.country_code = "+966"
        self.cp.setCountryByCode("+966")
        self.cp.setCountryByPhoneCode("+966")
        self.cp.showCountryCodeInView = false
        self.cp.centerXAnchor.constraint(equalTo: self.txtPhoneCode.centerXAnchor).isActive = true
        self.cp.centerYAnchor.constraint(equalTo: self.txtPhoneCode.centerYAnchor).isActive = true
       // self.cp.center = CGPoint(x: self.txtPhoneCode.bounds.midX, y: self.txtPhoneCode.bounds.midY)
       
//        self.viewCountry.delegate = self
//        self.viewCountry.dataSource = self
//
//        self.viewCountry.setCountryByCode("+966")
//        self.viewCountry.setCountryByPhoneCode("+966")
//        self.viewCountry.showCountryCodeInView = false
//        self.viewCountry.showCountryNameInView = false
//        self.viewCountry.showCountryCodeInView = true
//
//        self.viewCountry.isHidden = true
        
        self.setProfileData(userObj: self.userObj!)
        
        self.setTitleView()
    }
    //------------------------------------------------
        //MARK: Custom Methods
    //------------------------------------------------
    func setProfileData(userObj : User)  {
        self.txtFirstName.placeholder = AppHelper.localizedtext(key: "kFirstName");
        self.txtLastName.placeholder = AppHelper.localizedtext(key: "kLastName");
        let email : String = userObj.email ?? ""
        let phone_number : String = userObj.phone_number ?? ""
        let country_code : String = userObj.country_code ?? ""
        let first_name : String = userObj.first_name ?? ""
        let last_name : String = userObj.last_name ?? ""
        
        self.txtNumber.text = phone_number
        self.txtEmail.text = email
        self.txtFirstName.text = first_name
        self.txtLastName.text = last_name
        
        
         cp.setCountryByPhoneCode(country_code)
     //   self.viewCountry.setCountryByPhoneCode(country_code)
       // self.txtPhoneCode.text = ""
        
        self.country_code = country_code
        
        var picture : String  = userObj.picture?.url ?? ""
        if picture == "" {
        //        self.imgUser.image = UIImage(named: "User_Placeholder")
                    self.RoundImage(img: self.imgUser)
                }else{
                    let url = URL(string: picture  ?? "")!
                    self.imgUser.setImageFromURL(url: url,placeholder:UIImage(named:"user_placholder_withoutBlack"));
                    self.RoundImage(img: self.imgUser)
        }
    }
    
    //---------------------------------------------------
    //MARK: UItextfield Delegate Methods
    //---------------------------------------------------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "SecondrayColor")
    }
    
    //---------------------------------------------------
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "lightGray")
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           if(textField == self.txtNumber){
               let country_code : String = self.userObj?.country_code ?? ""
               guard let textFieldText = textField.text,
                   let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                       return false
               }
               let substringToReplace = textFieldText[rangeOfTextToReplace]
               let count = textFieldText.count - substringToReplace.count + string.count
            let length = self.country_code.length - 1
               if(length == 3){
                   return count <= 9
               }else{
                   return count <= 10
               }
               
           }
           return true
       }
    
    //---------------------------------------------------------------------
        //MARK: IBAction Of UIButton
    //---------------------------------------------------------------------
    @IBAction func btnEditProfileAction(_ sender: Any) {
        
        let country_code : String = self.country_code
        var max_length = 10
        if country_code.length - 1 >= 3{
            max_length = 9
        }
        if self.txtFirstName.text == "" {
               self.showAlert(title: kAppName, message: ValidationAlert.kEmptyFirstName)
        }else if self.txtLastName.text == "" {
            self.showAlert(title: kAppName, message: ValidationAlert.kEmptyLastName)
        }else if self.imgUser.image == nil{
            self.showAlert(message: "", title: kAppName)
        }else if self.txtNumber.text == ""{
            self.showAlert(message: "", title: kAppName)
        }else if self.txtEmail.text == ""{
            self.showAlert(message: "", title: kAppName)
        }else if self.txtNumber.text?.length != max_length {
            self.showAlert(title: kAppName, message: ValidationAlert.kInvalidPhone)
        }else{
            
           // let fName : String = self.userObj?.first_name ?? ""
          //  let lName : String = self.userObj?.last_name ?? ""
           
            
            controller.updateProfile(picture: self.imgUser.image!, FirstName: self.txtFirstName.text!, LastName: self.txtLastName.text!, Email: self.txtEmail.text!, PhoneNumber: self.txtNumber.text!, Country_code: "\(self.country_code)") { (success, response, message, statusCode) in
                
                if success{
                    DispatchQueue.main.async {
                       
                    //     self.showAlert(message: message, title: kAppName)
                        let popUp = PopupDialog(title: kAppName, message: message)
                        
                        let btnOk = CancelButton(title: AppHelper.localizedtext(key: "YES")) {
                            self.navigationController?.popViewController(animated: true)
                        }
                        popUp.addButtons([btnOk])
                        
                        // Present dialog
                        self.present(popUp, animated: true, completion: nil)
                        return
                                      
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(message: message, title: kAppName)
                    }
                }
            }
            
        }
    }
    
    //------------------------------------------------
    @IBAction func btnCameraAction(_ sender: Any) {
           
    
        photoManager = BWPhotoManager(navigationController: self.navigationController ?? UINavigationController(), mediaType: .image, allowEditing: true, isFromEdit: false, callback: { (chooseImage) in
        
         if chooseImage == nil {
             self.imgUser.image = UIImage(named: "user_placholder_withoutBlack")
         }else{
            
            let fName : String = self.userObj?.first_name ?? ""
            let lName : String = self.userObj?.last_name ?? ""
            let country_code : String = self.country_code
             
             self.imgUser.image = chooseImage as! UIImage;
             self.showActivityIndicatory()
             self.controller.updateProfile(picture: chooseImage as! UIImage, FirstName: fName, LastName: lName, Email: self.txtEmail.text!, PhoneNumber: self.txtNumber.text!, Country_code: country_code) { (success, response, message, statusCode) in
                 self.isCameraOpen = false
                 DispatchQueue.main.async {
                     self.hideActivityIndicator()
                     //self.showAlert(message: message, title: kAppName)
                 }
             }
         }
                
            });
        
    }
}

extension EditProfileVC : CountryPickerViewDelegate{
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        // Only countryPickerInternal has it's delegate set
        let title = "Selected Country"
        let message = "Name: \(country.name) \nCode: \(country.code) \nPhone: \(country.phoneCode)"
      //  self.txtPhoneCode.text = "\(country.phoneCode)"
        self.country_code = "\(country.phoneCode)"
        self.txtNumber.becomeFirstResponder()
        self.txtNumber.text = ""
        self.cp.setCountryByName(country.name);
      
    }
}

extension EditProfileVC : CountryPickerViewDataSource{
    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
     //   if countryPickerView.tag == cpvMain.tag && showPreferredCountries.isOn {
            var countries = [Country]()
            ["IN", "QA"].forEach { code in
                if let country = countryPickerView.getCountryByCode(code) {
                    countries.append(country)
                }
            }
            return countries
      //  }
        return []
    }
    
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
//        if countryPickerView.tag == cpvMain.tag && showPreferredCountries.isOn {
//            return "Preferred title"
//        }
        return nil
    }
    
//    func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool {
//      //  return countryPickerView.tag == cpvMain.tag && showOnlyPreferredCountries.isOn
//    }
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select a Country"
    }
        
//    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
////        if countryPickerView.tag == cpvMain.tag {
////            switch searchBarPosition.selectedSegmentIndex {
////            case 0: return .tableViewHeader
////            case 1: return .navigationBar
////            default: return .hidden
////            }
////        }
////        return .tableViewHeader
//    }
    
//    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
//        return countryPickerView.tag == cpvMain.tag && showPhoneCodeInList.isOn
//    }
//
//    func showCountryCodeInList(in countryPickerView: CountryPickerView) -> Bool {
//       return countryPickerView.tag == cpvMain.tag && showCountryCodeInList.isOn
//    }
}

