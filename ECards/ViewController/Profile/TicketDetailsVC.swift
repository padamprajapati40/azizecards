//
//  TicketDetailsVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 02/01/20.
//  Copyright © 2020 Dhruva Madhani. All rights reserved.
//

import UIKit
extension StringProtocol {
    var firstUppercased: String { prefix(1).uppercased() + dropFirst() }
    var firstCapitalized: String { prefix(1).capitalized + dropFirst() }
}


class TicketDetailsVC: AppViewController {

    var userInquiryStatusObj : UserInquiryStatus?
    
    @IBOutlet weak var lblIssueType: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblRequestCall: UILabel!
    @IBOutlet weak var txtViewComment : UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        self.title = AppHelper.localizedtext(key: "QRCodeInvite")
        if self.userInquiryStatusObj != nil{
            self.setDataOfTicket(self.userInquiryStatusObj!)
        }
        self.setTitleView()
        
    }
    
    func setDataOfTicket(_ ticketDetails : UserInquiryStatus){
        
        
        self.lblStatus.isHidden = true
        let message : String = ticketDetails.message ?? ""
        
        let issueType : String = ticketDetails.ticket_type?.name ?? ""
        
        let user_type : Int = ticketDetails.ticket_type_id ?? 0
        self.lblComment.numberOfLines = 0
        self.lblStatus.text = (ticketDetails.status ?? "").firstUppercased
        self.lblIssueType.text = "   \(issueType)"
     
        self.lblComment.text = "   \(message)"
        
    }

}
