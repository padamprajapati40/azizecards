//
//  EventMainVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 02/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import ObjectMapper
import Kingfisher




extension String {
    public var withoutHtml: String {
        guard let data = self.data(using: .utf8) else {
            return self
        }

        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]

        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return self
        }

        return attributedString.string
    }
}

extension UIImageView{
    func setImageFromURL(url:URL){
        self.clipsToBounds = true
        self.contentMode = .scaleAspectFill
        self.cornerRadius = 10.0;
        self.kf.setImage(with: url ,
                                 placeholder: nil,
                                 options: [.transition(ImageTransition.fade(1))],
                                 progressBlock: { receivedSize, totalSize in
                                    
        },
                                 completionHandler: { image, error, cacheType, imageURL in
                                    
        })
    }
    func setImageFromURL(url:URL,placeholder:UIImage?){
        self.clipsToBounds = true
        self.contentMode = .scaleAspectFill
        self.cornerRadius = 10.0;
        
        self.kf.setImage(with: url ,
                                 placeholder: nil,
                                 options: [.transition(ImageTransition.fade(1))],
                                 progressBlock: { receivedSize, totalSize in
                                    
        },
                                 completionHandler: { image, error, cacheType, imageURL in
                                   
        })
    }
}

class EventMainVC: AppViewController {

      //MARK: Variables.........................
    var arrOngoingEvents = [[String:Any]]()
    var arrBrowseByEvents = [[String:Any]]()
    var cardList = [CardTypes]()
    var upcomingEventList : UpcomingEvent?
    
     let controller = OrganizerViewModel()
    
    //MARK: IBOutlets.........................
    
  //  @IBOutlet weak var tblOngoingEvent: UITableView!
    @IBOutlet weak var collcetionEvent: UICollectionView!
    @IBOutlet weak var lblBrowseByEvents : UILabel!
    
    //---------------------------------------------------
    //MARK: View Life Cycle
    //---------------------------------------------------
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(GlobalUtils.getInstance().userID() != 0){
            self.title = AppHelper.localizedtext(key: "Home")
        }else{
            self.title = AppHelper.localizedtext(key: "QRCodeInvite")
        }
        
        
        
       let button = UIButton(type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "app_top"), for: .normal)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItems = [barButton] 
    }
    
    
    //---------------------------------------------------
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.callCardTypeApi()
        
        
        
        if GlobalUtils.getInstance().sessionToken() == ""{
                   self.title = AppHelper.localizedtext(key: "Home")
                   self.tabBarController?.tabBar.items?[0].title = AppHelper.localizedtext(key: "ManageEvents")
                   self.tabBarController?.tabBar.items?[1].title = AppHelper.localizedtext(key: "Home")
                   self.tabBarController?.tabBar.items?[2].title = AppHelper.localizedtext(key: "Profile")
                   
        }else{
                    self.title = AppHelper.localizedtext(key: "Home")
                    self.tabBarController?.tabBar.items?[0].title = AppHelper.localizedtext(key: "ManageEvents")
                    self.tabBarController?.tabBar.items?[1].title = AppHelper.localizedtext(key: "Home")
                    self.tabBarController?.tabBar.items?[2].title = AppHelper.localizedtext(key: "Profile")
                   
        }
        
        self.lblBrowseByEvents.text = AppHelper.localizedtext(key: "kBrowseByEvents")
       
    }

    //---------------------------------------------------
    //MARK: UIButton Actions
    //---------------------------------------------------
    
    
    
    //---------------------------------------------------
    //MARK: WebApi Methods
    //---------------------------------------------------
    func callCardTypeApi()  {
      //  if(self.cardList.count <= 0){
         //   self.showActivityIndicatory()
        //}
        
        controller.getCardTypes { (success, cardList, message, statusCode) in
            if success{
                
                self.hideActivityIndicator()
                self.cardList = cardList as? [CardTypes] ?? []
                
                print(self.cardList)
               
                DispatchQueue.main.async {
                        self.collcetionEvent.reloadData()
                }
                
                print(message)
                print(statusCode)
            }else{
                self.hideActivityIndicator()
                print(message)
                print(statusCode)
            }
        }
        
//        controller.getUpcomingEvents { (success, upcomingEventList, message, statusCode) in
//
//            self.hideActivityIndicator()
//         self.upcomingEventList = upcomingEventList as? UpcomingEvent
//        print(self.upcomingEventList)
//            print(success)
//            DispatchQueue.main.async {
//                self.tblOngoingEvent.reloadData()
//            }
//
//        }
    }

}

//extension EventMainVC:UITableViewDataSource,UITableViewDelegate{
//
//    //---------------------------------------------------
//        //MARK: UItableview Datasource
//    //---------------------------------------------------
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "OngoingEventTableCell", for: indexPath) as! OngoingEventTableCell
//
//        let strDate : String = self.upcomingEventList?.event_date ?? ""
//        if strDate != ""{
//             cell.lblDate.text = convertDateFormater(strDate)
//        }
//
//        cell.lblEventName.text = self.upcomingEventList?.name ?? ""
//
//        return cell
//    }
//
//
//    //---------------------------------------------------
//    //MARK: UItableview Delegate
//    //---------------------------------------------------
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 100.0
//    }
//
//    func convertDateFormater(_ date: String) -> String
//    {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//        let date = dateFormatter.date(from: date)
//        dateFormatter.dateFormat = "yyyy-MM-dd"
//        return  dateFormatter.string(from: date!)
//
//    }
//
//}

extension EventMainVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    
    //---------------------------------------------------
        //MARK: UICollectionview Datasource
    //---------------------------------------------------
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cardList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EventCollectionCell", for: indexPath)as! EventCollectionCell
            cell.lblEventTypeName.text = self.cardList[indexPath.row].name ?? ""
       // cell.lblEventTypeName.textAlignment = .natural
        let strUrl : String = self.cardList[indexPath.row].image?.url ?? ""
        if strUrl != ""{
           let url = URL(string: strUrl)!
            cell.imgEventType.setImageFromURL(url: url);
        }
        
            cell.imgEventType.layer.cornerRadius = 10.0;
            return cell
        }
    
    
    //---------------------------------------------------
        //MARK: UIcollectionview Delegate
    //---------------------------------------------------
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
        let eventVC = self.storyboard?.instantiateViewController(withIdentifier: "SpecificEventVC") as! SpecificEventVC

       eventVC.hidesBottomBarWhenPushed = false;
        eventVC.eventid = self.cardList[indexPath.row].id ?? 0
        
      // let eventVC = self.storyboard?.instantiateViewController(withIdentifier: "CustomizeCardAddVC") as! CustomizeCardAddVC
        self.navigationController?.pushViewController(eventVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var count = self.cardList.count;
        if(count % 2 != 0){
            count = count + 1;
        }
        var height = collectionView.frame.size.height;
        height = (height-30.0)/(CGFloat(count/2))
        return CGSize(width:(collectionView.frame.size.width-10)/2,height: height)
        
    }
}
