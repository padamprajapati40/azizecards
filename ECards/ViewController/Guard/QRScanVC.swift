//
//  QRScanVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 10/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

import ObjectMapper

class QRScanVC: AppViewController {

     //MARK: Variables.........................
   // let scanner = QRCode()
    var guardObj : Guard?
    
    
    //MARK: IBOutlets.........................
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var btnNotScanned: UIButton!
    @IBOutlet weak var lblScanned: UILabel!
    @IBOutlet weak var btnScanQRCode: UIButton!
    @IBOutlet weak var btnScanned: UIButton!
    @IBOutlet weak var lblNotScanned: UILabel!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblEventDate: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var viewScan: UIView!
    //---------------------------------------------------
    //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(guardObj)
        
         let imgCirlce = UIImage(named: "Circle_Gray")

        self.btnScanned.setImage(imgCirlce?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnScanned.tintColor = UIColor(named: "PrimaryColor")
        
        self.btnNotScanned.setImage(imgCirlce?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnNotScanned.tintColor = UIColor(named: "lightGray")
        
        self.title = AppHelper.localizedtext(key: "ScanCode")
        
        let logout = UIBarButtonItem(image: UIImage(named: "logout"), style: .plain, target: self, action: #selector(btnLogoutAction(_:)))
        self.navigationItem.rightBarButtonItem  = logout
        self.setTitleView()
        //For Scan
//        scanner.scanFrame = self.viewScan.bounds
//        scanner.prepareScan(self.viewScan) { (stringValue) -> () in
//            print(stringValue)
//        }
        
        
    }
    //---------------------------------------------------
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // start scan
      //  scanner.startScan()
        
        //// generate qrcode
       // iconView.image = QRCode.generateImage("Hello SwiftQRCode", avatarImage: UIImage(named: "placeholder"), avatarScale: 0.3)
    }
    
    //For Scan
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.iconView.image = QRCode.generateImage("Hello SwiftQRCode", avatarImage: UIImage(named: "placeholder"), avatarScale: 0.3)
//    }
    
    @objc func btnLogoutAction(_ sender: Any) {
        
    }
    
    //---------------------------------------------------
    //MARK: UIButton Actions
    //---------------------------------------------------
    @IBAction func btnScanQRCodeAction(_ sender: Any) {
        
        let errorVC = self.storyboard?.instantiateViewController(withIdentifier: "QRScanErrorVC") as! QRScanErrorVC
        let navVC = UINavigationController(rootViewController:errorVC )
        self.present(navVC, animated: true, completion: nil);
      //  self.navigationController?.pushViewController(failVC, animated: true)
        
    }
    //---------------------------------------------------
    @IBAction func btnScannedAction(_ sender: Any) {
        
    }
    //---------------------------------------------------
    @IBAction func btnNotScannedAction(_ sender: Any) {
        
    }
    //---------------------------------------------------
}
