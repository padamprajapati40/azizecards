//
//  GuestListVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 09/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

class GuestListVC: AppViewController, UITableViewDataSource , UITableViewDelegate {
   
    

    
 
    @IBOutlet weak var lblScannedGuestNumber: UILabel!
    @IBOutlet weak var tblGuests: UITableView!
    
    //---------------------------------------------------
    //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitleView()
        // Do any additional setup after loading the view.
    }
    
    //---------------------------------------------------
        //MARK: Tableview Datasource
    //---------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    //---------------------------------------------------
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GuestListTableCell", for: indexPath) as! GuestListTableCell
        cell.imgSucess.image = UIImage(named: "success")
        return cell
        
    }
    
    //---------------------------------------------------
    //MARK: Tableview Delegate
    //---------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

}
