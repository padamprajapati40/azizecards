//
//  QRScanErrorVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 27/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

class QRScanErrorVC: AppViewController {

    
    //MARK: Variables.........................
    
    
    
    //MARK: IBOutlets.........................
    
     @IBOutlet weak var btnScanQRCode: UIButton!
    
    //---------------------------------------------------
    //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        let close = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(btnCloseAction(_:)))
        self.navigationItem.rightBarButtonItem  = close
        self.setTitleView()
    }
    
    @objc func btnCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //---------------------------------------------------
    //MARK: UIButton Actions
    //---------------------------------------------------
    @IBAction func btnScanQRCodeAction(_ sender: Any) {
        
        
        
    }
}
