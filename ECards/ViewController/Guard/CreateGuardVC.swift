//
//  CreateGuardVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 20/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

import PopupDialog
import CountryPickerView

class CreateGuardVC: AppViewController,UITableViewDataSource, UITableViewDelegate ,ContactEditAndDeleteDelegate, UITextFieldDelegate,UITextViewDelegate{
   
    
    //MARK: Variables.........................
    var arrGuards = [[String:Any]]()
    var isOpenFromEdit = Bool()
    var tag = Int()
    let controller = OrganizerViewModel()
    var eventId  = Int()
    var guardList : [Guard] = [];
    var country_code = String()
    let cp = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
    var guard_id = Int()
    var isAddedNow = Bool()
    //MARK: IBOutlets.........................
    
    @IBOutlet weak var txtDate: BWTextField!
    @IBOutlet weak var txtName: BWTextField!
    @IBOutlet weak var txtPhoneNumber: BWTextField!
    @IBOutlet weak var txtPhoneCode: BWTextField!
    @IBOutlet weak var txtMEssage: UITextView!
    @IBOutlet weak var tblGuests: UITableView!
    @IBOutlet weak var btnAddAnotherGuard: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var toolbarObj: UIToolbar!
    
    //---------------------------------------------------
    //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = AppHelper.localizedtext(key: "CreateGuard")
        self.btnSubmit.titleLabel?.font = Font.tagViewFont
        self.btnAddAnotherGuard.titleLabel?.font = Font.tagViewFont
        self.DefalutDialoguesettingOfPopup()
        
        self.txtPhoneNumber.placeholder = ""
        //for country code
        self.txtPhoneCode.placeholder = ""
        
//               self.txtPhoneCode.leftView = cp
//               self.txtPhoneCode.leftViewMode = .always
        self.cp.frame = CGRect(x: 10, y: 0, width: self.txtPhoneCode.frame.size.width, height: self.txtPhoneCode.frame.size.height)
        self.txtPhoneCode.addSubview(self.cp);
               cp.dataSource = self
               cp.delegate = self
               self.country_code = "+966"
               cp.setCountryByCode("+966")
               cp.setCountryByPhoneCode("+966")
               cp.showCountryCodeInView = false
        self.txtPhoneNumber.delegate = self;
        
        self.callGetGuardListApi("\(eventId)")
        self.cp.centerXAnchor.constraint(equalTo: self.txtPhoneCode.centerXAnchor).isActive = true
        self.cp.centerYAnchor.constraint(equalTo: self.txtPhoneCode.centerYAnchor).isActive = true
        self.setTitleView()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          if(textField == self.txtPhoneNumber){
              guard let textFieldText = textField.text,
                  let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                      return false
              }
              let substringToReplace = textFieldText[rangeOfTextToReplace]
              let count = textFieldText.count - substringToReplace.count + string.count
              let length = self.country_code.length - 1
              if(length == 3){
                  return count <= 9
              }else{
                  return count <= 10
              }
              
          }
          return true
      }
    //---------------------------------------------------
    //MARK: Button Done Toolbar ACtion
    //---------------------------------------------------
    @IBAction func btnDoneAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    //---------------------------------------------------
    //MARK: UIDatePicker ACtion
    //---------------------------------------------------
    @IBAction func datePickerValueChangedAction(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let strDate = dateFormatter.string(from: datePicker.date)
        print(strDate)
        self.txtDate.text = strDate
    }

    //---------------------------------------------------
    //MARK: UIButton Actions
    //---------------------------------------------------
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        if self.arrGuards.count == 0{
            var phoneCode = 10
                   if(country_code.length - 1 == 3){
                       phoneCode = 9
            }
            if self.txtName.text == "" {
                self.showAlert(message: ValidationAlert.kEmptyName, title: kAppName)
            }else if self.country_code  == "" {
                self.showAlert(message: ValidationAlert.kEmptyPhoneCode, title: kAppName)
                
            }else if self.txtPhoneNumber.text == "" {
                self.showAlert(message: ValidationAlert.kEmptyPhoneNumber, title: kAppName)
            }else if self.txtPhoneNumber.text?.length != phoneCode {
                self.showAlert(title: kAppName, message: ValidationAlert.kInvalidPhone)
            }
            else{
                
                var dicGuard : [String:Any] = [String:Any]()
                dicGuard["name"] = self.txtName.text!
                dicGuard["country_code"] = self.country_code
                dicGuard["phone_number"] = self.txtPhoneNumber.text!
                print(dicGuard)
                self.arrGuards.append(dicGuard)
                
                
              
                controller.createGuard(EventId: "\(self.eventId)", arrGuard: self.arrGuards) { (success, response, message, statusCode) in
                    
                    print(response)
                    if success{
                        DispatchQueue.main.async {
                            let successVc = self.storyboard?.instantiateViewController(withIdentifier: "EventSuccessVC") as! EventSuccessVC
                            successVc.isOpenFromaCreateGuard = true
                            self.navigationController?.pushViewController(successVc, animated: true)
                            
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.showAlert(message: message, title: kAppName)
                        }
                    }
                }
            }
        }else{
            var arrGuard : [[String:Any]] = []
            for dictData in self.arrGuards{
                var dictGuard = dictData
                dictGuard["isAddedNow"] = nil
                arrGuard.append(dictGuard)
            }
            
            
            controller.createGuard(EventId: "\(self.eventId)", arrGuard: arrGuard) { (success, response, message, statusCode) in
                
                print(response)
                if success{
                    DispatchQueue.main.async {
                        let successVc = self.storyboard?.instantiateViewController(withIdentifier: "EventSuccessVC") as! EventSuccessVC
                        successVc.isOpenFromaCreateGuard = true
                        self.navigationController?.pushViewController(successVc, animated: true)
                        
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(message: message, title: kAppName)
                    }
                }
            }
        }
    }
    
    //---------------------------------------------------
    @IBAction func btnAddAnotherGuardAction(_ sender: Any) {
        var phoneCode = 10
                          if(country_code.length - 1 == 3){
                              phoneCode = 9
                          }
       // self.txtMEssage.resignFirstResponder()
        if self.txtName.text == "" {
            
            self.showAlert(message: ValidationAlert.kEmptyName, title: kAppName)
        }else if self.country_code  == "" {
            self.showAlert(message: ValidationAlert.kEmptyPhoneCode, title: kAppName)
            
        }else if self.txtPhoneNumber.text == "" {
            self.showAlert(message: ValidationAlert.kEmptyPhoneNumber, title: kAppName)
        }else if self.txtPhoneNumber.text?.length != phoneCode {
            self.showAlert(title: kAppName, message: ValidationAlert.kInvalidPhone)
        }
        else{
            var dicGuard : [String:Any] = [String:Any]()
            if(tag != 0){
                dicGuard["id"] = tag
            }
            
            dicGuard["name"] = self.txtName.text!
            dicGuard["country_code"] = self.country_code
            dicGuard["phone_number"] = "\(self.txtPhoneNumber.text!)"
//            dicGuard["expiry_date"] = self.txtDate.text!
//            dicGuard["instruction"] = self.txtMEssage.text!
            //  dicGuard[""] = self.txt
            print(dicGuard)
            
            if isOpenFromEdit{
                
                
               //self.arrGuards[tag] = dicGuard
                
                
                let isAddNow : Bool = self.arrGuards[tag]["isAddedNow"] as? Bool ?? false
                if isAddNow{
                    //for add
                    
                    self.arrGuards[self.tag] = dicGuard
                    self.tblGuests.reloadData()
                }else{
                    //for edit
                    let id : Int = dicGuard["id"] as? Int ?? 0
                                  
                    dicGuard["id"] = self.guard_id
                        self.controller.updateEventGuardDetails(phone_number: self.txtPhoneNumber.text!, name: self.txtName.text!, event_id: "\(self.eventId)", guard_id: "\(self.guard_id)") { (success, response, message, statusCode) in
                                      
                                if success{
                                          DispatchQueue.main.async {
                                              
                                             // self.showAlert(title: kAppName, message: message)
                                              self.showAlertWithCallBack(title: kAppName, message: message) {
                                                  self.arrGuards[self.tag] = dicGuard
                                                self.tblGuests.reloadData()
                                              }
                                          }
                                          
                                      }else{
                                          DispatchQueue.main.async {
                                              self.showAlert(title: kAppName, message: message)
                                          }
                                      }
                                  }
                }
                
                if self.guardList.count != 0{
//                  let id : Int = dicGuard["id"] as? Int ?? 0
//
//                    self.controller.updateEventGuardDetails(phone_number: self.txtPhoneNumber.text!, name: self.txtName.text!, event_id: "\(self.eventId)", guard_id: "\(self.guard_id)") { (success, response, message, statusCode) in
//
//                        if success{
//                            DispatchQueue.main.async {
//
//                               // self.showAlert(title: kAppName, message: message)
//                                self.showAlertWithCallBack(title: kAppName, message: message) {
//                                    self.arrGuards[self.tag] = dicGuard
//                                }
//                            }
//
//                        }else{
//                            DispatchQueue.main.async {
//                                self.showAlert(title: kAppName, message: message)
//                            }
//                        }
//                    }
                }else{
                    self.arrGuards[self.tag] = dicGuard
                }
                
            }else{
              //  self.isAddedNow = true
                dicGuard["isAddedNow"] = true
                 self.arrGuards.append(dicGuard)
               // self.btnSubmit.backgroundColor = UIColor(named:"SecondrayColor")!
               // self.btnSubmit.isEnabled = true
            }
           
            
            self.tblGuests.reloadData()
            self.txtPhoneCode.text = ""
            self.txtPhoneNumber.text = ""
            self.txtName.text = ""
//            self.txtMEssage.text = ""
//            self.txtDate.text = ""
            
        }
    }
    
    //---------------------------------------------------
    //MARK: UITableview Datasource
    //---------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrGuards.count
    }
    //---------------------------------------------------
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableCell", for: indexPath) as! ContactTableCell
        cell.delegate = self
        cell.btnDelete.tag = indexPath.row
        cell.btnEdit.tag = indexPath.row
        let dicGuard : [String:Any] = self.arrGuards[indexPath.row] as? [String:Any] ?? [:]
        
      cell.lblName.text = dicGuard["name"] as? String ?? ""
        let phoneCode : String = dicGuard["country_code"] as? String ?? ""
        let phoneNumber : String = dicGuard["phone_number"] as? String ?? ""
        cell.lblContact.text = "\(phoneCode)\(phoneNumber)"
     //   cell.lblName.text = ""
        return cell
    }
    
    //---------------------------------------------------
    //MARK: UItextfield Delegate
    //---------------------------------------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "SecondrayColor")
        if textField == self.txtDate{
            
            textField.inputView = self.datePicker
            textField.inputAccessoryView = self.toolbarObj
        }
    }
    //---------------------------------------------------
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "lightGray")
    }
    
    
    //---------------------------------------------------
    //MARK: UItextView Delegate
    //---------------------------------------------------
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.bordrColor = UIColor(named: "SecondrayColor")
    }
    
    //---------------------------------------------------
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.bordrColor = UIColor(named: "lightGray")
    }
    //---------------------------------------------------
    //MARK: Custom Delegate
    //---------------------------------------------------
    func editContact(_ tag: Int) {
        let dicGuard : [String:Any] = self.arrGuards[tag]
        
        self.guard_id = dicGuard["id"] as? Int ?? 0
        let name  : String = dicGuard["name"] as? String ?? ""
        let phoneCode : String = dicGuard["country_code"] as? String ?? ""
        
        let phoneNumber : String = dicGuard["phone_number"] as? String ?? ""
//        let expiryDate : String = dicGuard["expiry_date"] as? String ?? ""
//        let message : String = dicGuard["instruction"] as? String ?? ""
        //dicGuard["instruction"] = self.txtMEssage.text!
        
    //    cp.getCountryByCode(phoneCode)
      //  cp.getCountryByPhoneCode(phoneCode)
        cp.setCountryByPhoneCode(phoneCode)
        self.txtPhoneNumber.text = "\(phoneNumber)"
        //self.txtPhoneCode.text = "\(cp.getCountryByPhoneCode(phoneCode))"
        self.txtName.text = name
//        self.txtDate.text = expiryDate
//        self.txtMEssage.text = message
//        
        self.isOpenFromEdit = true
        self.tag = tag
    }
    
    func deleteContact(_ tag: Int) {
        self.isOpenFromEdit = false
        
//        self.showAlertWithCallBack(title: kAppName, message: "Do you want to delete this contact?") {
//            DispatchQueue.main.async {
//                self.arrGuards.remove(at: tag)
//                self.tblGuests.reloadData()
//            }
//
//        }
        
        
        let popUp = PopupDialog(title: kAppName, message: ValidationAlert.kDeleteConatct)
        let btnOk = CancelButton(title: AppHelper.localizedtext(key: "YES")) {
            DispatchQueue.main.async {
                
                let isAddNow : Bool = self.arrGuards[tag]["isAddedNow"] as? Bool ?? false
                if isAddNow{
                    self.arrGuards.remove(at: tag)
                    self.tblGuests.reloadData()
                }else{
                    let id :Int = self.arrGuards[tag]["id"] as? Int ?? 0
                    self.controller.deleteGuardApi(eventId: "\(self.eventId)", guardId: "\(id)") { (success, response, message, statusCode) in
                        if success{
                            self.arrGuards.remove(at: tag)
                            self.tblGuests.reloadData()
                        }else{
                            self.showAlert(message: message, title: kAppName)
                        }
                    }
                }
            }
        }
        let btnCancel = CancelButton(title: AppHelper.localizedtext(key: "NO")) {
                   
        }
         popUp.addButtons([btnOk,btnCancel])
               
               // Present dialog
        self.present(popUp, animated: true, completion: nil)
        
        
//        let deleteAlert = UIAlertController(title: kAppName, message: ValidationAlert.kDeleteConatct, preferredStyle: .alert)
//        let okAction = UIAlertAction(title: AppHelper.localizedtext(key: "YES"), style: .default) { (action) in
//
//        }
//        let cancelAction = UIAlertAction(title: AppHelper.localizedtext(key: "NO"), style: .destructive, handler: nil)
//        deleteAlert.addAction(okAction)
//        deleteAlert.addAction(cancelAction)
//        self.present(deleteAlert, animated: true, completion: nil)
    }
    //---------------------------------------------------
    //MARK: WebApi Methods
    //---------------------------------------------------
     
    func callGetGuardListApi(_ event_Id : String)  {
        
        self.showActivityIndicatory()
        controller.getGuardList(event_id: event_Id) { (success, guardList, message, statusCode) in
            
            if success{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    
                    print(message)
                    print(statusCode)
                    
                    print(guardList)
                    self.guardList = guardList as? [Guard] ?? []
                    print(self.guardList)
                    
                    if self.guardList.count != 0 {
                                 for i in 0 ... self.guardList.count - 1 {
                                     
                                    let id : Int = self.guardList[i].id ?? 0
                                     let name : String = self.guardList[i].name ?? ""
                                     let phone_number : String = self.guardList[i].phone_number ?? ""
                                    
                                     let country_Code : String = self.guardList[i].country_code ?? ""
                                     
                                     var dicAttendee : [String:Any] = [String:Any]()
                                    dicAttendee["id"] = id
                                           dicAttendee["name"] = name
                                           dicAttendee["phone_number"] = phone_number
                                         
                                           dicAttendee["country_code"] = country_Code
                                    dicAttendee["isAddedNow"] = false
                                           print(dicAttendee)
                                     self.arrGuards.append(dicAttendee)
                                     
                                 }
                             }
                    
                    print(self.arrGuards)
                    if self.arrGuards.count == 0{
                       // self.btnSubmit.isEnabled = false
                      //  self.btnSubmit.backgroundColor = UIColor.lightGray;
                    }
                    self.tblGuests.reloadData()
                    
                }
            }else{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    self.showAlert(message: message, title: kAppName)
                    
                }
            }
            
        }
    }
}

extension CreateGuardVC : CountryPickerViewDelegate{
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        // Only countryPickerInternal has it's delegate set
        let title = "Selected Country"
        let message = "Name: \(country.name) \nCode: \(country.code) \nPhone: \(country.phoneCode)"
      //  self.txtPhoneCode.text = "\(country.phoneCode)"
        self.country_code = "\(country.phoneCode)"
        self.txtPhoneNumber.becomeFirstResponder()
        self.txtPhoneNumber.text = ""
        self.cp.setCountryByName(country.name)
      
    }
}

extension CreateGuardVC : CountryPickerViewDataSource{
    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
     //   if countryPickerView.tag == cpvMain.tag && showPreferredCountries.isOn {
            var countries = [Country]()
            ["IN", "QA"].forEach { code in
                if let country = countryPickerView.getCountryByCode(code) {
                    countries.append(country)
                }
            }
            return countries
      //  }
        return []
    }
    
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
//        if countryPickerView.tag == cpvMain.tag && showPreferredCountries.isOn {
//            return "Preferred title"
//        }
        return nil
    }
    
//    func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool {
//      //  return countryPickerView.tag == cpvMain.tag && showOnlyPreferredCountries.isOn
//    }
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select a Country"
    }
        
//    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
////        if countryPickerView.tag == cpvMain.tag {
////            switch searchBarPosition.selectedSegmentIndex {
////            case 0: return .tableViewHeader
////            case 1: return .navigationBar
////            default: return .hidden
////            }
////        }
////        return .tableViewHeader
//    }
    
//    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
//        return countryPickerView.tag == cpvMain.tag && showPhoneCodeInList.isOn
//    }
//
//    func showCountryCodeInList(in countryPickerView: CountryPickerView) -> Bool {
//       return countryPickerView.tag == cpvMain.tag && showCountryCodeInList.isOn
//    }
}

