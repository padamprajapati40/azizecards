//
//  AddGuestsVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 03/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import ContactsUI
import ObjectMapper
import MobileCoreServices
import Alamofire
import PopupDialog

class AddGuestsVC: AppViewController, CNContactPickerDelegate, UITableViewDataSource, UITableViewDelegate, AddContactDelegate, ContactEditAndDeleteDelegate, PlanSelectDelegate,UISearchBarDelegate {
   
    func planSelected(_ range: String, id: Int, row: Int, amount : Int) {
        print(range)
        self.selectedPlanID = id
        self.selectedRow = row
        DispatchQueue.main.async {
            self.showAlertWithCallBack(title: kAppName, message: "You have selected new plan of \(amount) SR having number of guests are in between \(range)") {
                       self.callUpdateEventApi("\(self.eventId)", numberOfAttendees: range, selectedPlan_id: id)
                   }
        }
    }
    var isSelectedNoInPopUp = Bool()
    var isAddedNow = Bool()
    var isPlanUpgraded = Bool()
    var isFromCreateEvent:Bool = false
    //MARK: Varibales..............
    var arrSameContacts = [[String:Any]]()
    var arrContacts = [[String:Any]]()
    var arrData : [[String:Any]] = []
    let controller = OrganizerViewModel()
     var eventId : Int = Int()
    var eventObj : EventDetails?
    var isOpenFromOngoingList = Bool()
    var maxInvitees = String()
   var selectedPlanID = Int()
    var selectedRow = Int()
    var eventDetailObj : EventDetails?
    var isOpenFromManageEvents = Bool()
    var attendees : [Attendees] = []
    var eventCompletedObj : CompletedEvents?
    var maxAttendees = Int()
    var arrContactPicker = [[String:Any]]()
    var isSearchActive = Bool()
    //MARK: IBOutlets..............
    @IBOutlet weak var btnSelectGuestFromContact: UIButton!
    @IBOutlet weak var btnImportFromExcelFile: UIButton!
    @IBOutlet weak var btnEnterManually: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var tblContacts: UITableView!
    @IBOutlet weak var lblSelectContact : UILabel!
    @IBOutlet weak var btnSampleExcel: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var lblStaticName : UILabel!
    @IBOutlet weak var lblStaticContactNumber : UILabel!
    @IBOutlet weak var btnWhatsapp: UIButton!
    
    //-------------------------------------------------------
    //MARK: View Life Cycle
    //-------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchBar.delegate = self;
        self.title = AppHelper.localizedtext(key: "QRCodeInvite")
        self.lblSelectContact.isHidden = false
        self.tblContacts.isHidden = true
        
        self.btnContinue.titleLabel?.font =  Font.tagViewFont
        self.searchBar.delegate = self
        self.DefalutDialoguesettingOfPopup()

        print(self.eventDetailObj)
        self.eventDetailObj = self.eventObj
        
        if isOpenFromManageEvents{
            self.arrContacts.removeAll()
            self.attendees = self.attendees ?? []
            print(attendees)
            
          
            
            if self.attendees.count != 0 {
                for i in 0 ... self.attendees.count - 1 {
                    
                    let name : String = self.attendees[i].full_name ?? ""
                    let phone_number : String = self.attendees[i].phone_number ?? ""
                    let email : String = self.attendees[i].email ?? ""
                    let country_Code : String = self.attendees[i].country_code ?? ""
                    let id : Int = self.attendees[i].id ?? 0
                    
                    var dicAttendee : [String:Any] = [String:Any]()
                          dicAttendee["full_name"] = name
                          dicAttendee["phone_number"] = phone_number
                          dicAttendee["email"] = email
                          dicAttendee["country_code"] = country_Code
                            dicAttendee["id"] = id
                          print(dicAttendee)
                    self.arrContacts.append(dicAttendee)
                    
                }
            }
            print(self.arrContacts)
            if self.attendees.count == 0 {
                self.tblContacts.isHidden = true
                self.lblSelectContact.isHidden = false
            }else{
                self.tblContacts.isHidden = false
                self.lblSelectContact.isHidden = true
            }
            self.arrContacts = (arrContacts as NSArray).sortedArray(using: [NSSortDescriptor(key: "full_name", ascending: true)]) as! [[String:AnyObject]]
            self.tblContacts.reloadData()
            
         //   var arrAttendees : [[String:Any]] = attendees as? [[String:Any]] ?? []
          //  print(arrAttendees)
        }else{
            if self.arrContacts.count != 0 {
                
                let arrAttendee : [[String:Any]] = UserDefaults.value(forKey: UserdefaultsKeyName.kEventAttendee) as? [[String:Any]] ?? []
                UserDefaults.standard.synchronize()
                self.arrContacts = arrAttendee
                
                print(self.arrContacts)
                
            }
        }
        
        
        self.arrData = self.arrContacts
        self.arrContacts = (arrContacts as NSArray).sortedArray(using: [NSSortDescriptor(key: "full_name", ascending: true)]) as! [[String:AnyObject]]
    
        self.setTitleView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.resignSearchbarAndKeyboard()
        self.callEventDetailApi("\(eventId)")
//        let arrData = UserDefaults.standard.value(forKey: UserdefaultsKeyName.kEventAttendee)
//           print(arrData)
        self.isPlanUpgraded = false
        self.navigationItem.setHidesBackButton(false, animated: true)
        self.btnSampleExcel.setTitle(AppHelper.localizedtext(key: "SampleExcel"), for: .normal)
//        self.arrData = self.arrContacts
//        self.arrContacts = (arrContacts as NSArray).sortedArray(using: [NSSortDescriptor(key: "full_name", ascending: true)]) as! [[String:AnyObject]]
//        self.tblContacts.reloadData()
        
//        self.btnWhatsapp.isHidden = true
//        self.btnWhatsapp.isEnabled = false
        
   
        self.searchBar.delegate = self
    }
    
    //-------------------------------------------------------
    //MARK: UITbleview datasource
    //-------------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrContacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableCell", for: indexPath) as! ContactTableCell
        cell.btnEdit.tag = indexPath.row
        cell.btnDelete.tag = indexPath.row
        cell.delegate = self
        let dicAttendee : [String:Any] = self.arrContacts[indexPath.row] as? [String:Any] ?? [:]
        
        if(self.eventObj?.Invitation_message_deliver == true || self.eventCompletedObj?.invitation_message_deliver == true){
            cell.btnEdit.isHidden = true
            cell.btnDelete.isHidden = true
            
            if dicAttendee.isEmpty{
                
            }else{
                if self.isAddedNow{
                    let isAdded = dicAttendee["isAddedNow"] as? Bool ?? false
                    if isAdded{
                        cell.btnEdit.isHidden = false
                        cell.btnDelete.isHidden = false
                    }else{
                        cell.btnEdit.isHidden = true
                        cell.btnDelete.isHidden = true
                    }
                }
            }
            
        }else{
            
            if self.isOpenFromManageEvents{
                if self.isAddedNow{
                    let isAdded = dicAttendee["isAddedNow"] as? Bool ?? false
                    if isAdded{
                            cell.btnEdit.isHidden = false
                            cell.btnDelete.isHidden = false
                    }else{
                            cell.btnEdit.isHidden = true
                            cell.btnDelete.isHidden = true
                    }
                }else{
                        cell.btnEdit.isHidden = true
                        cell.btnDelete.isHidden = true
                }
            }else{
                cell.btnEdit.isHidden = false
                cell.btnDelete.isHidden = false
            }
            

      
            
        }
        
            
                  if dicAttendee.isEmpty{
                      cell.lblName.text = ""
                      cell.lblContact.text = ""
                      cell.lblSrNumber.text = ""
                  }else{
                    
//                  if self.isAddedNow{
//                      let isAdded = dicAttendee["isAddedNow"] as? Bool ?? false
//                      if isAdded{
//                          cell.btnEdit.isHidden = false
//                          cell.btnDelete.isHidden = false
//                      }
//                  }

                    let name: String = dicAttendee["full_name"] as? String ?? ""
                    cell.lblName.text = name
                    let number: String = dicAttendee["phone_number"] as? String ?? ""
                    let country_code : String = dicAttendee["country_code"] as? String ?? ""
                    cell.lblContact.text =  country_code + number
                    cell.lblSrNumber.text = "\(indexPath.row + 1)"
                  }
      
        return cell
    }

    //-------------------------------------------------------
    //MARK: UITbleview Delegate
    //-------------------------------------------------------
    
    
    //-------------------------------------------------------
    //MARK: UIButton Actions
    //-------------------------------------------------------
    
    @IBAction func btnImportFromExcelFileAction(_ sender: Any) {

        self.resignSearchbarAndKeyboard()
        
        let importMenu = UIDocumentPickerViewController(documentTypes: ["public.item","com.microsoft.excel.xls","org.openxmlformats.spreadsheetml.sheet", kUTTypePDF as String], in: UIDocumentPickerMode.import )
        importMenu.delegate = self
        self.present(importMenu, animated: true, completion: nil)
        
    }
    
    @IBAction func btnSelectGuestsFromContactAction(_ sender: Any) {
        self.resignSearchbarAndKeyboard()
        let cnPicker = CNContactPickerViewController()
        cnPicker.delegate = self
        self.present(cnPicker, animated: true, completion: nil)
    }
    
    @IBAction func btnEnterManuallyAction(_ sender: Any) {
        self.resignSearchbarAndKeyboard()
        let contact = self.storyboard?.instantiateViewController(withIdentifier: "AddContactFormVC")as! AddContactFormVC
        contact.delegate = self
        contact.isOpenFromEdit = false
        self.navigationController?.pushViewController(contact, animated: true)

    }
    
    @IBAction func btnSampleExcelAction(_ sender: Any) {
        let webVc = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
        webVc.isSampleExcel = true
        self.navigationController?.pushViewController(webVc, animated: true)
    }
    
    
    @IBAction func btnWhatsappAction(_ sender: Any) {

        if self.arrContacts.count == 0 {
            self.showAlert(message: ValidationAlert.kEmptyContacts, title: kAppName)
        }else{
            let eventName : String = self.eventDetailObj?.name ?? ""
            let strEventDate : String = self.eventDetailObj?.event_date ?? ""
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
             let eventDate = dateFormatter.date(from:strEventDate)
            dateFormatter.dateFormat = "dd/MM/yyyy"
             let strEventFinalDate  : String = dateFormatter.string(from: eventDate ?? Date())
            
            let strWhatsAppText : String = "Dear Invitees/Guests, We would like to invite you to \(eventName) on \(strEventFinalDate)"
            
            if let urlString = "https://wa.me/?text=\(strWhatsAppText). ".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
                let url = URL(string: urlString),
                UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url)
            }
        }
    }
    
    func checkMaxLimitOfConatctsToBeAdded(){
        
           let planObj = self.eventDetailObj?.plan
           let plan_id : Int = planObj?.id ??  0
               let plan_name : String = planObj?.name ?? ""
               let plan_price : Int = planObj?.price ?? 0
               let plan_description : String = planObj?.description ?? ""
               
           let arrGuest = self.eventDetailObj?.guest_plans as? [Guest_plans] ?? []
           var max_Attendees : Int = 0
           if arrGuest.count != 0{
               let guest_Plan_Obj = arrGuest.last;
               let guest_name : String = guest_Plan_Obj?.name ?? ""
               max_Attendees = guest_Plan_Obj?.maximum_attendees as? Int ?? 0
                    print(guest_name)
                    print(plan_name)
           }else{
               max_Attendees = 0
           }
        self.maxAttendees = max_Attendees
           
           print(arrGuest[0].id)
           print(arrGuest[0].plan_id)
           if self.arrContacts.count > max_Attendees {
          //  self.showAlert(message: "You can not choose more than \(max_Attendees) invitees. To choose more invitees please upgrade your plan.Do you want to upgrade it?", title: kAppName)
               var strAlert : String = self.cutosmMessageOFALert(max_Attendees: self.maxAttendees)
            let alert = UIAlertController(title: kAppName, message: strAlert, preferredStyle: .alert)
            let okAction = UIAlertAction(title: AppHelper.localizedtext(key: "YES"), style: .default) { (action) in
                
                var guest_plan_id : Int = arrGuest.last?.id ?? 0
                
                self.isPlanUpgraded = true
                
                let plan = self.storyboard?.instantiateViewController(withIdentifier: "PlanOfEventsVC") as! PlanOfEventsVC
                plan.isFromCreateEvent = self.isFromCreateEvent
                plan.delegate = self;
                plan.eventDetailObj = self.eventDetailObj
                
                plan.isOpenFromAddGuest = true
                plan.package_id = plan_id ?? 0
             //   plan.isSelected = isSelected
                plan.selectedTag = self.selectedRow
                plan.selectedPlan_id = guest_plan_id
                plan.isOpenFromAddGuest = true
                let navVC = UINavigationController(rootViewController:plan )
                self.present(navVC, animated: true, completion: nil)
               
            }
            let cancelAction = UIAlertAction(title: AppHelper.localizedtext(key: "NO"), style: .destructive) { (action) in
                self.isPlanUpgraded = false
                
            }
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    @IBAction func btnContinueAction(_ sender: Any) {
        
      if self.isPlanUpgraded{
          
            self.callAttendeeAdded()
        }else{
        if self.arrContacts.count == 0 {
                   self.showAlert(message: AppHelper.localizedtext(key: "kEmptyContacts"), title: kAppName)
                   return
               }
        if self.arrContacts.count == self.attendees.count{
            if self.isAddedNow{
                
                if self.isSelectedNoInPopUp{
                     self.navigationController?.popToRootViewController(animated: true)
                }else{
                    self.callAttendeeAdded()
                }
            }else{
               self.navigationController?.popToRootViewController(animated: true)
            }
            
        }else{
            self.callAttendeeAdded()
        }
        
            
        }
    }
    //-------------------------------------------------------
      //MARK:- Web api Method
    //-------------------------------------------------------
    func  callEventDetailApi(_ eventId : String)  {
        
        controller.getEventDetails(cardId: eventId) { (success, eventDetailsList, message, statusCode) in
            if success{
                DispatchQueue.main.async {
                    
                    print(eventDetailsList)
                    print()
                    
                    let eventDetailObj = eventDetailsList as? EventDetails
                    self.eventDetailObj = eventDetailObj
                    print(self.eventDetailObj)

                    let instant_msg : Bool = self.eventDetailObj?.instant_invitation_sent ?? false
//                    if instant_msg{
//                        self.btnWhatsapp.isHidden = false
//                        self.btnWhatsapp.isEnabled = true
//                    }else{
//                        self.btnWhatsapp.isHidden = true
//                        self.btnWhatsapp.isEnabled = false
//                    }
//
//                    self.btnWhatsapp.isHidden = true
//                    self.btnWhatsapp.isEnabled = false
                }
            }else{
                DispatchQueue.main.async {
                    self.showAlert(message: message, title: kAppName)
                }
            }
        }
    }
    func callAttendeeAdded(){
          self.showActivityIndicatory()
        controller.addAttendees(event_id: "\(self.eventId)", arrAttendees: self.arrContacts) { (success, response, message, statusCode) in
                                  if success{
                                     self.hideActivityIndicator()
                                      DispatchQueue.main.async {
                                          if GlobalUtils.getInstance().userLoggedIn(){
                                              if self.isOpenFromOngoingList{
                                                 
                                                let isInviteSent : Bool = self.eventDetailObj?.Instant_Invitation_sent ?? false
                                                if isInviteSent{
                                                     self.callInstantMessageApi("email", send_message: "instant", id: "\(self.eventId)")
                                                }else{
                                                    let successVc = self.storyboard?.instantiateViewController(withIdentifier: "EventSuccessVC") as! EventSuccessVC
                                                                                                              successVc.isOpenFromAttendessAdded = true
                                                                               successVc.eventObj = self.eventObj
                                                                              self.navigationController?.pushViewController(successVc, animated: true)
                                                }
                                                
                                               
                                                
                                              }else{
                                                
                                                  let eventDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailWholeStaticDataVC")as! EventDetailWholeStaticDataVC
                                                  eventDetailVC.eventObj = self.eventObj
                                                  eventDetailVC.eventId = self.eventId
                                                  self.navigationController?.pushViewController(eventDetailVC, animated: true)
                                              }


                                          }else{
                                              let welcomeVC = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeVC")as! WelcomeVC
                                              welcomeVC.eventObj = self.eventObj;
                                              let navVC = UINavigationController(rootViewController:welcomeVC )
                                              self.present(navVC, animated: true, completion: nil);

                                          }

                                      }
                                  }else{
                                    DispatchQueue.main.async {
                                      self.hideActivityIndicator()
                                      self.showAlert(message: message, title: kAppName)
                                    }

                                  }
                              }
    }
    //-------------------------------------------------------
    func callUpdateEventApi( _ eventID : String , numberOfAttendees : String, selectedPlan_id : Int)  {
        
        let name : String = self.eventDetailObj?.name ?? ""
        let date : String = self.eventDetailObj?.event_date ?? ""
        let location : String = self.eventDetailObj?.venue ?? ""
        let language : String = self.eventDetailObj?.language ?? ""
        let to_time : String = self.eventDetailObj?.to_time ?? ""
        let from_time : String = self.eventDetailObj?.from_time ?? ""
        let msgDateTime : String = self.eventDetailObj?.message_date_time ?? ""
        let card_theme_id : Int = self.eventDetailObj?.card_theme_id ?? 0
        let msg : String = self.eventDetailObj?.message ?? ""
        let latiude : Int = self.eventDetailObj?.latitude ?? 0
        let longitude : Int = self.eventDetailObj?.longitude ?? 0
        let planObj = self.eventDetailObj?.plan
               let plan_id : Int = planObj?.id ??  0
        let isInstant = self.eventDetailObj?.instant_invitation_sent ?? false
        
        self.showActivityIndicatory()
        controller.updateEventDetails(event_id: eventID, Name: name, Date: date, PickLocation: location, ChooseLanguage: language, fromDate: from_time, ToTime: to_time, numberOfAttendes: numberOfAttendees, messageDateTime: msgDateTime, cardId: "\(card_theme_id)", EventCardId: "", message: msg, latitude: "\(latiude)", longitude: "\(longitude)", plan_id: "\(plan_id)", packageID: self.selectedPlanID, isInvitationSend: isInstant) { (success, response, message, statusCode) in
            
            self.hideActivityIndicator()
            if success{
                DispatchQueue.main.async {
                    let dictData = response as? [String:Any] ?? [:]
                    let resDict = dictData
                    var cardDetailsList : EventDetails?
                    cardDetailsList = Mapper<EventDetails>().map(JSON: resDict)
                    self.eventDetailObj = cardDetailsList
                    self.arrContacts.append(contentsOf: self.arrContactPicker)
                    self.isAddedNow = true
                    self.arrData = self.arrContacts
                    self.arrContacts = (self.arrContacts as NSArray).sortedArray(using: [NSSortDescriptor(key: "full_name", ascending: true)]) as! [[String:AnyObject]]
                    self.arrData = self.arrContacts;
                    print(self.arrContacts)
                    if self.arrContacts.count == 0 {
                              self.tblContacts.isHidden = true
                              self.lblSelectContact.isHidden = false
                          }else{
                              self.tblContacts.isHidden = false
                              self.lblSelectContact.isHidden = true
                    }
                    UserDefaults.standard.set(self.arrContacts, forKey: UserdefaultsKeyName.kEventAttendee)
                                 UserDefaults.standard.synchronize()
                    self.tblContacts.reloadData()
//                     let eventDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailWholeStaticDataVC")as! EventDetailWholeStaticDataVC
//                                                         eventDetailVC.eventObj = self.eventObj
//                                                         eventDetailVC.eventId = self.eventId
//                                                         self.navigationController?.pushViewController(eventDetailVC, animated: true)
                }
            }else{
                DispatchQueue.main.async {
                    self.showAlert(message: message, title: kAppName)
                }
            }
        }
    }
    
    func cutosmMessageOFALert(max_Attendees : Int) -> String {
        var strTailText = String()
        var strStartText = String()
        var strMiddleText = String()

        strTailText = AppHelper.localizedtext(key: "kMaxContactAlertLastText")
        strMiddleText = AppHelper.localizedtext(key: "kMaxContactAlertMiddleText")
        strStartText = AppHelper.localizedtext(key: "kMaxContactAlertStartText")
        var strFinalString = "\(strStartText) \(max_Attendees) \(strMiddleText) \(strTailText)"
        return strFinalString
    }
    
    
    //-------------------------------------------------------
    //MARK:- Custom Delegate Method
    //-------------------------------------------------------
    func addContactDetails(name: String, phoneCode : String,phoneNumber: String, email : String,country_name:String) {
        print(name)
        print(phoneNumber)
        print(country_name)
        self.arrContactPicker.removeAll()
        
        var dicAttendee : [String:Any] = [String:Any]()
        dicAttendee["full_name"] = name
        dicAttendee["phone_number"] = phoneNumber
        dicAttendee["email"] = email
        if phoneCode.contains("+"){
            dicAttendee["country_code"] = phoneCode
        }else{
            dicAttendee["country_code"] = "+\(phoneCode)"
        }
        dicAttendee["country_name"] = country_name
        dicAttendee["isAddedNow"] = true
        
        print(dicAttendee)
        self.arrContactPicker.append(dicAttendee)
//        let dicSimilarContact = self.arrContacts.filter{ ($0["phone_number"] as? String ?? "").contains(dicAttendee["phone_number"] as? String ?? "") }
//
//        if dicSimilarContact.isEmpty{
//            self.arrContactPicker.append(dicAttendee)
//        }else{
//            self.showAlert(message: AppHelper.localizedtext(key: "kSameContacts"), title: kAppName)
//            return
//        }
//        self.arrContactPicker.append(dicAttendee)
        
        
                  let planObj = self.eventDetailObj?.plan
                  let plan_id : Int = planObj?.id ??  0
                      let plan_name : String = planObj?.name ?? ""
                      let plan_price : Int = planObj?.price ?? 0
                      let plan_description : String = planObj?.description ?? ""
                      
                  let arrGuest = self.eventDetailObj?.guest_plans as? [Guest_plans] ?? []
                  var max_Attendees : Int = 0
                if arrGuest.count != 0{
                      let guest_Plan_Obj = arrGuest.first;
                      let guest_name : String = guest_Plan_Obj?.name ?? ""
                      max_Attendees = guest_Plan_Obj?.maximum_attendees as? Int ?? 0
                           print(guest_name)
                           print(plan_name)
                  }else{
                      max_Attendees = 0
                  }
                    self.maxAttendees = max_Attendees
                  
                  print(arrGuest[0].id)
                  print(arrGuest[0].plan_id)
            if self.arrContacts.count + self.arrContactPicker.count > self.maxAttendees {
                 
               
    
                
                var strAlert : String = self.cutosmMessageOFALert(max_Attendees: self.maxAttendees)
                
                 let popUp = PopupDialog(title: kAppName, message: strAlert)
                let btnOk = CancelButton(title: AppHelper.localizedtext(key: "YES")) {
                    
                    self.isPlanUpgraded = true
                    var guest_plan_id : Int = arrGuest.last?.id ?? 0
                    let plan = self.storyboard?.instantiateViewController(withIdentifier: "PlanOfEventsVC") as! PlanOfEventsVC
                    plan.delegate = self;
                    plan.isFromCreateEvent = self.isFromCreateEvent
                    plan.eventDetailObj = self.eventDetailObj;
                    plan.isOpenFromAddGuest = true
                    let planObj = self.eventDetailObj?.plan
                    let plan_id : Int = planObj?.id ??  0
                    plan.package_id = plan_id ?? 0
                    //   plan.isSelected = isSelected
                    plan.selectedTag = self.selectedRow
                    plan.selectedPlan_id = guest_plan_id
                    plan.isOpenFromAddGuest = true
                    let navVC = UINavigationController(rootViewController:plan )
                    self.present(navVC, animated: true, completion: nil)
                }
                
                let btnCancel = CancelButton(title: AppHelper.localizedtext(key: "NO")) {
                    self.isPlanUpgraded = false
                    self.isSelectedNoInPopUp = true
                    self.arrContactPicker.removeAll()
                }
                
                popUp.addButtons([btnOk,btnCancel])
                              
                // Present dialog
                self.present(popUp, animated: true, completion: nil)
                
                
//                   let alert = UIAlertController(title: kAppName, message: strAlert, preferredStyle: .alert)
//                let okAction = UIAlertAction(title: AppHelper.localizedtext(key: "YES"), style: .default) { (action) in
//
//                        self.isPlanUpgraded = true
//                          var guest_plan_id : Int = arrGuest.last?.id ?? 0
//                          let plan = self.storyboard?.instantiateViewController(withIdentifier: "PlanOfEventsVC") as! PlanOfEventsVC
//                          plan.delegate = self;
//                            plan.isFromCreateEvent = self.isFromCreateEvent
//                          plan.eventDetailObj = self.eventDetailObj;
//                          plan.isOpenFromAddGuest = true
//                          let planObj = self.eventDetailObj?.plan
//                          let plan_id : Int = planObj?.id ??  0
//                          plan.package_id = plan_id ?? 0
//                       //   plan.isSelected = isSelected
//                          plan.selectedTag = self.selectedRow
//                          plan.selectedPlan_id = guest_plan_id
//                          plan.isOpenFromAddGuest = true
//                          let navVC = UINavigationController(rootViewController:plan )
//                          self.present(navVC, animated: true, completion: nil)
//
//                   }
//                    let cancelAction = UIAlertAction(title: AppHelper.localizedtext(key: "NO"), style: .destructive) { (action) in
//                    self.isPlanUpgraded = false
//                        self.isSelectedNoInPopUp = true
//                    self.arrContactPicker.removeAll()
//                   }
//                   alert.addAction(okAction)
//                   alert.addAction(cancelAction)
//                   self.present(alert, animated: true, completion: nil)
       
            }else{
                self.isAddedNow = true
                self.arrContacts.append(dicAttendee)
                 self.arrData = self.arrContacts
                 self.arrContacts = (arrContacts as NSArray).sortedArray(using: [NSSortDescriptor(key: "full_name", ascending: true)]) as! [[String:AnyObject]]
                 self.arrData = self.arrContacts;
                 print(self.arrContacts)
                 if self.arrContacts.count == 0 {
                            self.tblContacts.isHidden = true
                            self.lblSelectContact.isHidden = false
                        }else{
                            self.tblContacts.isHidden = false
                            self.lblSelectContact.isHidden = true
                 }
                 
                // self.checkMaxLimitOfConatctsToBeAdded()
                 UserDefaults.standard.set(self.arrContacts, forKey: UserdefaultsKeyName.kEventAttendee)
                 UserDefaults.standard.synchronize()
                 self.tblContacts.reloadData()
                
        }
    }
     //-------------------------------------------------------
    func editContactDetails(name: String, phoneCode : String,phoneNumber: String, email : String,tag: Int,country_name:String) {
        print(name)
        print(phoneNumber)
        print(tag)
        print(country_name)
        
        var dicAttendee : [String:Any] = [String:Any]()
        dicAttendee["full_name"] = name
        dicAttendee["phone_number"] = phoneNumber
        dicAttendee["email"] = email
        dicAttendee["country_code"] = phoneCode
        dicAttendee["isAddedNow"] = true
        dicAttendee["country_name"] = country_name
        print(dicAttendee)
        
        self.arrContacts[tag] = dicAttendee
        self.arrData = self.arrContacts;
        self.arrContacts = (arrContacts as NSArray).sortedArray(using: [NSSortDescriptor(key: "full_name", ascending: true)]) as! [[String:AnyObject]]
        print(self.arrContacts)
        
        UserDefaults.standard.set(self.arrContacts, forKey: UserdefaultsKeyName.kEventAttendee)
        UserDefaults.standard.synchronize()
        
        self.tblContacts.reloadData()
    }
    
    //-------------------------------------------------------
    func editContact(_ tag: Int) {
        print(tag)
        self.resignSearchbarAndKeyboard()
        
        let dicContact : [String:Any] = self.arrContacts[tag]
        
        let nameContact : String = dicContact["full_name"] as? String ?? ""
        let phoneNumber : String = dicContact["phone_number"] as? String ?? ""
        let email : String = dicContact["email"] as? String ?? ""
        let phoneCode : String = dicContact["country_code"] as? String ?? ""
        let country_name : String = dicContact["country_name"] as? String ?? ""
        
        let contact = self.storyboard?.instantiateViewController(withIdentifier: "AddContactFormVC")as! AddContactFormVC
        contact.delegate = self
        contact.name = nameContact
        contact.phoneNumber = phoneNumber
        contact.email = email
        contact.country_code = phoneCode
        contact.tag = tag
        contact.isOpenFromEdit = true
        contact.selected_country_name = country_name
        self.navigationController?.pushViewController(contact, animated: true)
        
        
    }
    //-------------------------------------------------------
    func deleteContact(_ tag: Int) {
        print(tag)
        print("Delete called")
        
        self.resignSearchbarAndKeyboard()
        
        let dictContact : [String:Any] = self.arrContacts[tag] as? [String:Any] ?? [:]
        var attendee_id : Int = dictContact["id"] as? Int ?? 0
        
        print(attendee_id)
        
        let event_id : Int = self.eventDetailObj?.id ?? 0
        print(self.eventDetailObj?.id)
        
      //  print(self.attendees[tag])
        
        
        let popUp = PopupDialog(title: kAppName, message: ValidationAlert.kDeleteConatct)
        
        let btnOk = CancelButton(title: AppHelper.localizedtext(key: "YES")) {
                       DispatchQueue.main.async {
                 let contactObj = self.arrContacts[tag];
                 let fileterArray = self.attendees.filter { $0.full_name == contactObj["full_name"] as? String ?? "" }
                 if(fileterArray.count>0){
                     self.callDeleteAttendeeApi("\(event_id)", attendee_id: "\(attendee_id)", tag: tag)
                 }else{
                     self.arrContacts.remove(at: tag)
                     self.arrData = self.arrContacts;
                     self.arrContacts = (self.arrContacts as NSArray).sortedArray(using: [NSSortDescriptor(key: "full_name", ascending: true)]) as! [[String:AnyObject]]
                                    
                                    
                                    UserDefaults.standard.set(self.arrContacts, forKey: UserdefaultsKeyName.kEventAttendee)
                                               UserDefaults.standard.synchronize()
                                               self.tblContacts.reloadData()
                                               
                                                if self.arrContacts.count == 0 {
                                                    self.tblContacts.isHidden = true
                                                    self.lblSelectContact.isHidden = false
                                               }else{
                                                    self.tblContacts.isHidden = false
                                                    self.lblSelectContact.isHidden = true
                                             }
                     }
                 }
        }
        let btnCancel = CancelButton(title: AppHelper.localizedtext(key: "NO")) {
            
        }
        popUp.addButtons([btnOk,btnCancel])
        
        // Present dialog
        self.present(popUp, animated: true, completion: nil)
        return;
        
        let alert = UIAlertController(title: kAppName, message: ValidationAlert.kDeleteMessage, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: AppHelper.localizedtext(key: "YES"), style: .default) { (action) in
            DispatchQueue.main.async {
                let contactObj = self.arrContacts[tag];
                let fileterArray = self.attendees.filter { $0.full_name == contactObj["full_name"] as? String ?? "" }
                if(fileterArray.count>0){
                    self.callDeleteAttendeeApi("\(event_id)", attendee_id: "\(attendee_id)", tag: tag)
                }else{
                    self.arrContacts.remove(at: tag)
                    self.arrData = self.arrContacts;
                    self.arrContacts = (self.arrContacts as NSArray).sortedArray(using: [NSSortDescriptor(key: "full_name", ascending: true)]) as! [[String:AnyObject]]
                                   
                                   
                                   UserDefaults.standard.set(self.arrContacts, forKey: UserdefaultsKeyName.kEventAttendee)
                                              UserDefaults.standard.synchronize()
                                              self.tblContacts.reloadData()
                                              
                                               if self.arrContacts.count == 0 {
                                                   self.tblContacts.isHidden = true
                                                   self.lblSelectContact.isHidden = false
                                              }else{
                                                   self.tblContacts.isHidden = false
                                                   self.lblSelectContact.isHidden = true
                                            }
                    }
                }
        }
      
        let cancelAction = UIAlertAction(title: AppHelper.localizedtext(key: "NO"), style: .destructive, handler: nil)
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
         self.present(alert, animated: true, completion: nil)
        
    }
    //-------------------------------------------------------
    //MARK:- CNContactPickerDelegate Method
    //-------------------------------------------------------
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {
        picker.dismiss(animated: true, completion: nil)
        var arrSelectedContacts :[[String:Any]] = []
        self.arrContactPicker.removeAll()
        contacts.forEach { contact in
            var dicAttendee : [String:Any] = [String:Any]()
            dicAttendee["full_name"] = contact.givenName
        
            print(contact)
            print(contact.givenName)
            print(contact.phoneNumbers)
            var countryCode = "";
            
          
            print(contact.postalAddresses)
            let homeAddress : [CNLabeledValue<CNPostalAddress>] = contact.postalAddresses
          //  print(contact.postalAddresses["CNPostalAddressCountryKey"])
            
          //  homeAddress.contains(.country)
            
            for number in contact.phoneNumbers {
                let phoneNumber = number.value
                print("number is = \(phoneNumber)")
               
                let FulMobNumVar  = number.value as! CNPhoneNumber
                 countryCode = FulMobNumVar.value(forKey: "countryCode") as? String ?? ""
                
                if(countryCode == ""){
                    countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String ?? "IN"
                    countryCode = countryCode.uppercased()
                    countryCode = countryCode.getCountryPhonceCode()
                        
                    print(countryCode);
                }else{
                    countryCode = countryCode.uppercased()
                    
                    countryCode = countryCode.getCountryPhonceCode()
                }
                
                
                var strPhoneNumber : String = phoneNumber.stringValue
                strPhoneNumber = strPhoneNumber.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil)
                strPhoneNumber = strPhoneNumber.replacingOccurrences(of: "(", with: "", options: NSString.CompareOptions.literal, range: nil)
                strPhoneNumber = strPhoneNumber.replacingOccurrences(of: ")", with: "", options: NSString.CompareOptions.literal, range: nil)
                strPhoneNumber = strPhoneNumber.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
                strPhoneNumber = strPhoneNumber.replacingOccurrences(of: "+91", with: "", options: NSString.CompareOptions.literal, range: nil)
                
                if(strPhoneNumber.contains("+966")){
                    strPhoneNumber =  strPhoneNumber.replacingOccurrences(of: "+966", with: "")
                }
                else if(strPhoneNumber.contains("+\(countryCode)")){
                       strPhoneNumber =  strPhoneNumber.replacingOccurrences(of: "+\(countryCode)", with: "")
                }
                else if(strPhoneNumber.contains("+1")){
                       strPhoneNumber =  strPhoneNumber.replacingOccurrences(of: "+1", with: "")
                }
                
                    //dicAttendee["phone_number"] = "+" + countryCode +  " " + strPhoneNumber
                dicAttendee["phone_number"] = strPhoneNumber
            }
            
            dicAttendee["last_name"] = ""
            let arrEmailAddress = contact.emailAddresses
            if arrEmailAddress.isEmpty{
                 dicAttendee["email"] = ""
            }else{
                dicAttendee["email"] = ""
            }
           
            if countryCode.contains("+"){
                 dicAttendee["country_code"] = countryCode
            }else{
                 dicAttendee["country_code"] = "+\(countryCode)"
            }
//            dicAttendee["country_code"] = "+" + countryCode
         //   dicAttendee["country_code"] = "+91"
            dicAttendee["isAddedNow"] = true
            print(dicAttendee)
            self.arrContactPicker.append(dicAttendee)
             self.isAddedNow = true
          let test1 =  self.arrContacts.filter { ($0["phone_number"] as? String ?? "").hasPrefix(dicAttendee["phone_number"] as? String ?? "") }
            
//            let dicSimilarContact = self.arrContacts.filter{ ($0["phone_number"] as? String ?? "").contains(dicAttendee["phone_number"] as? String ?? "") }
//
//            if dicSimilarContact.isEmpty{
//                self.isAddedNow = true
//                 self.arrContactPicker.append(dicAttendee)
//            }else{
//                self.showAlert(message: AppHelper.localizedtext(key: "kSameContacts"), title: kAppName)
//            }
            
           
            
           // dicAttendee.filter{ self.arrContactPicker.contains(where: $0["full_name"]) }
            
           // self.arrData = self.arrContactPicker;
            UserDefaults.standard.set(self.arrContacts, forKey: UserdefaultsKeyName.kEventAttendee)
            UserDefaults.standard.synchronize()
            
            self.lblSelectContact.isHidden = true
            self.tblContacts.isHidden = false
            
           
            
        }
         let arrGuest = self.eventDetailObj?.guest_plans as? [Guest_plans] ?? []
            var max_Attendees : Int = 0
            if arrGuest.count != 0{
                let guest_Plan_Obj = arrGuest.first;
                let guest_name : String = guest_Plan_Obj?.name ?? ""
                max_Attendees = guest_Plan_Obj?.maximum_attendees as? Int ?? 0
                     print(guest_name)
                  //   print(plan_name)
            }else{
                max_Attendees = 0
            }
         self.maxAttendees = max_Attendees
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
             if self.arrContacts.count + self.arrContactPicker.count > self.maxAttendees{
                var strAlert : String = self.cutosmMessageOFALert(max_Attendees: self.maxAttendees)
                
                 let popUp = PopupDialog(title: kAppName, message: strAlert)
                let btnOk = CancelButton(title: AppHelper.localizedtext(key: "YES")) {
                        self.isPlanUpgraded = true
                       var guest_plan_id : Int = arrGuest.last?.id ?? 0
                       let plan = self.storyboard?.instantiateViewController(withIdentifier: "PlanOfEventsVC") as! PlanOfEventsVC
                       plan.delegate = self;
                        plan.isFromCreateEvent = self.isFromCreateEvent
                       plan.eventDetailObj = self.eventDetailObj;
                       plan.isOpenFromAddGuest = true
                       let planObj = self.eventDetailObj?.plan
                       let plan_id : Int = planObj?.id ??  0
                       plan.package_id = plan_id ?? 0
                    //   plan.isSelected = isSelected
                       plan.selectedTag = self.selectedRow
                       plan.selectedPlan_id = guest_plan_id
                       plan.isOpenFromAddGuest = true
                       let navVC = UINavigationController(rootViewController:plan )
                       self.present(navVC, animated: true, completion: nil)
                }
                let btnCancel = CancelButton(title: AppHelper.localizedtext(key: "NO")) {
                    self.arrContactPicker.removeAll()
                    self.isPlanUpgraded = false
                    self.isSelectedNoInPopUp = true
                }
                popUp.addButtons([btnOk,btnCancel])
                
                // Present dialog
                self.present(popUp, animated: true, completion: nil)
                return;
                
                let alert = UIAlertController(title: kAppName, message: strAlert, preferredStyle: .alert)
                let okAction = UIAlertAction(title: AppHelper.localizedtext(key: "YES"), style: .default) { (action) in
                                 
                                self.isPlanUpgraded = true
                                 var guest_plan_id : Int = arrGuest.last?.id ?? 0
                                 let plan = self.storyboard?.instantiateViewController(withIdentifier: "PlanOfEventsVC") as! PlanOfEventsVC
                                 plan.delegate = self;
                                plan.isFromCreateEvent = self.isFromCreateEvent
                                 plan.eventDetailObj = self.eventDetailObj;
                                 plan.isOpenFromAddGuest = true
                                 let planObj = self.eventDetailObj?.plan
                                 let plan_id : Int = planObj?.id ??  0
                                 plan.package_id = plan_id ?? 0
                              //   plan.isSelected = isSelected
                                 plan.selectedTag = self.selectedRow
                                 plan.selectedPlan_id = guest_plan_id
                                 plan.isOpenFromAddGuest = true
                                 let navVC = UINavigationController(rootViewController:plan )
                                 self.present(navVC, animated: true, completion: nil)
                 }
                let cancelAction = UIAlertAction(title: AppHelper.localizedtext(key: "NO"), style: .destructive) { (action) in
                                self.arrContactPicker.removeAll()
                                self.isPlanUpgraded = false
                                self.isSelectedNoInPopUp = true
                }
                             alert.addAction(okAction)
                             alert.addAction(cancelAction)
                             self.present(alert, animated: true, completion: nil)
                             return
             }else{
                  self.arrSameContacts = self.arrContactPicker.sorted { ($0["name"] as? String ?? "").localizedCaseInsensitiveCompare(($1["name"] as? String ?? "")) == ComparisonResult.orderedSame }
                    print(self.arrSameContacts)
                
                 self.arrContacts.append(contentsOf: self.arrContactPicker);
                 
                 self.arrContacts = (self.arrContacts as NSArray).sortedArray(using: [NSSortDescriptor(key: "full_name", ascending: true)]) as! [[String:AnyObject]]
                 self.arrData = self.arrContacts;
                 self.arrContactPicker.removeAll()
                 self.tblContacts.reloadData()
             }
         }
    }
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        print("Cancel Contact Picker")
        if self.arrContacts.count == 0 {
            self.tblContacts.isHidden = true
            self.lblSelectContact.isHidden = false
        }else{
            self.tblContacts.isHidden = false
            self.lblSelectContact.isHidden = true
        }
    }
    
    
    func callDeleteAttendeeApi(_ event_id : String , attendee_id : String, tag : Int)  {
        
        
        self.showActivityIndicatory()
        self.view.endEditing(true)
    
        controller.deleteAttendeeApi(eventId: event_id, Attendee_ID: attendee_id) { (success, response, message, statusCode) in
            
            
            if success{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    self.showAlertWithCallBack(title: kAppName, message: message) {
                        
                        if self.searchBar.text == ""{
                            self.arrContacts.remove(at: tag)
                            self.tblContacts.reloadData()
                        }else{
                            
                            
                            let obj  = self.arrContacts.remove(at: tag)
                            let index = self.arrData.index{ $0["id"] as? Int ?? 0 == obj["id"] as? Int ?? 0 }
                            if(index != nil){
                                self.arrData.remove(at:index!)
                            }
                            
                            self.arrContacts = self.arrData;
                            self.searchBar.text = ""
                            self.arrContacts = (self.arrContacts as NSArray).sortedArray(using: [NSSortDescriptor(key: "full_name", ascending: true)]) as! [[String:AnyObject]]
                            self.tblContacts.reloadData()
                        }
                        //self.arrData = self.arrContacts;
                        
                    }
                  //  self.showAlert(message: message, title: kAppName)
                    
                    if self.arrContacts.count != 0{
                        self.lblSelectContact.isHidden = true
                        self.tblContacts.isHidden = false
                    }else{
                        self.lblSelectContact.isHidden = false
                        self.tblContacts.isHidden = true
                    }
                }
            }else{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    self.showAlert(message: message, title: kAppName)
                }
            }
        }
    }
    
    
    
    //---------------------------------------------------
    //MARK: UIsearchbar  Delegate
    //---------------------------------------------------
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.arrData = self.arrContacts
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        searchBar.resignFirstResponder()
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
      //  searchBar.text = ""
        searchBar.endEditing(true)
        self.view.endEditing(true)
       self.searchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
       self.searchBar.endEditing(true)
       
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.characters.count == 0 {
            self.arrContacts = self.arrData
            self.tblContacts.reloadData()
        } else {
            self.arrContacts = self.arrContacts.filter { ($0["full_name"] as? String ?? "").hasPrefix(searchText) }
            self.arrContacts = (arrContacts as NSArray).sortedArray(using: [NSSortDescriptor(key: "full_name", ascending: true)]) as! [[String:AnyObject]]
            self.tblContacts.reloadData()
        }
    }
    
    func resignSearchbarAndKeyboard()  {
        self.searchBar.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    func callInstantMessageApi(_ type : String, send_message : String , id : String){
       
               self.showActivityIndicatory()
       
             // let id: Int = self.eventObj?.id ?? 0
              controller.sendInstantMessage(EventId: id, type: type, send_Message: send_message) { (success, response, message, statusCode) in
       
                  if success{
                       DispatchQueue.main.async {
                           self.hideActivityIndicator()
                            let successVc = self.storyboard?.instantiateViewController(withIdentifier: "EventSuccessVC") as! EventSuccessVC
                                                           successVc.isOpenFromAttendessAdded = true
                            successVc.eventObj = self.eventObj
                           self.navigationController?.pushViewController(successVc, animated: true)
                       }
                   }else{
                       DispatchQueue.main.async {
                           self.hideActivityIndicator()
                          self.showAlert(message: message, title: kAppName)
                       }
                   }
       
               }
           }
}


extension AddGuestsVC : UIDocumentMenuDelegate, UIDocumentPickerDelegate, UINavigationControllerDelegate{
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {

               let cico = url as URL
               print(cico)
               print(url)
        
        
              do {
                  let fileData = try Data.init(contentsOf: url)
                  let fileStream:String = fileData.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0))
                
                 self.fileUpload(fileData: fileData)
                    
                
//                  if(url.pathExtension == "pdf"){
//                      resumeBase64String = "data:application/\(url.pathExtension);base64,(\(fileStream))"
//                  }else{
//                      resumeBase64String = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,(\(fileStream))"
//                  }
                  
              }
              catch {
                  
              }

               print(url.lastPathComponent)

               print(url.pathExtension)

              }

    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
            documentPicker.delegate = self
            present(documentPicker, animated: true, completion: nil)
        }


    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
                print("view was cancelled")
                dismiss(animated: true, completion: nil)
        }
    
    func planUpdateSuccefully(){
        
                
                self.arrContacts.append(contentsOf: self.arrContactPicker)
                self.arrData = self.arrContacts
                self.arrContacts = (arrContacts as NSArray).sortedArray(using: [NSSortDescriptor(key: "full_name", ascending: true)]) as! [[String:AnyObject]]
                self.arrData = self.arrContacts;
                print(self.arrContacts)
                if self.arrContacts.count == 0 {
                          self.tblContacts.isHidden = true
                          self.lblSelectContact.isHidden = false
                      }else{
                          self.tblContacts.isHidden = false
                          self.lblSelectContact.isHidden = true
                }
        
              
               UserDefaults.standard.set(self.arrContacts, forKey: UserdefaultsKeyName.kEventAttendee)
               UserDefaults.standard.synchronize()
               self.tblContacts.reloadData()
    }
    
    func fileUpload(fileData:Data?)  {
        let url = WebServiceUrl.kBASEURL + "organizers/events/" + "\(self.eventId)/" + "attendees/create_in_bulk"
        var token = GlobalUtils.getInstance().sessionToken();
        token = "Bearer " + token
      
        let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Authorization" : token,
                "Content-type" : "multipart/form-data"
            ]
            
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
                if let data = fileData{
                    multipartFormData.append(data, withName: "file", fileName: "file.xls", mimeType: "application/vnd.ms-excel")
                }
                
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print("Succesfully uploaded")
                         DispatchQueue.main.async {
                            if let err = response.error{
                                self.showAlert(message:err.localizedDescription , title: kAppName)
                                print(err.localizedDescription)
                                return
                            }
                            let dictData = response.result.value as? [String:Any]
                            let arrDataList = dictData?["data"] as? [[String:Any]] ?? []
                            if(arrDataList.count > 0){
                                for dictcontact in arrDataList{
                                    var dicAttendee : [String:Any] = [:]
                                    dicAttendee["phone_number"] = dictcontact["phone_number"] as? String ?? ""
                                    dicAttendee["full_name"] = dictcontact["full_name"] as? String ?? ""
                                    dicAttendee["email"] = dictcontact["email"] as? String ?? ""
                                    dicAttendee["country_code"] = dictcontact["country_code"] as? String ?? ""
                                    self.arrContactPicker.append(dicAttendee)
                                }
                                let arrGuest = self.eventDetailObj?.guest_plans as? [Guest_plans] ?? []
                                    var max_Attendees : Int = 0
                                    if arrGuest.count != 0{
                                        let guest_Plan_Obj = arrGuest.last;
                                        let guest_name : String = guest_Plan_Obj?.name ?? ""
                                        max_Attendees = guest_Plan_Obj?.maximum_attendees as? Int ?? 0
                                             print(guest_name)
                                          //   print(plan_name)
                                    }else{
                                        max_Attendees = 0
                                    }
                                 self.maxAttendees = max_Attendees
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                     if self.arrContacts.count + self.arrContactPicker.count > self.maxAttendees{
                                        var strAlert : String = self.cutosmMessageOFALert(max_Attendees: self.maxAttendees)
                                        
                                         let popUp = PopupDialog(title: kAppName, message: strAlert)
                                        let btnOk = CancelButton(title: AppHelper.localizedtext(key: "YES")) {
                                            
                                            
                                            var guest_plan_id : Int = arrGuest.last?.id ?? 0
                                            let plan = self.storyboard?.instantiateViewController(withIdentifier: "PlanOfEventsVC") as! PlanOfEventsVC
                                            plan.delegate = self;
                                            plan.isFromCreateEvent = self.isFromCreateEvent
                                            plan.eventDetailObj = self.eventDetailObj;
                                            plan.isOpenFromAddGuest = true
                                            let planObj = self.eventDetailObj?.plan
                                            let plan_id : Int = planObj?.id ??  0
                                            plan.package_id = plan_id ?? 0
                                            //   plan.isSelected = isSelected
                                            plan.selectedTag = self.selectedRow
                                            plan.selectedPlan_id = guest_plan_id
                                            plan.isOpenFromAddGuest = true
                                            let navVC = UINavigationController(rootViewController:plan )
                                            self.present(navVC, animated: true, completion: nil)
                                        }
                                        let btnCancel = CancelButton(title: AppHelper.localizedtext(key: "NO")) {
                                            self.arrContactPicker.removeAll()
                                            self.isSelectedNoInPopUp = true
                                            self.isPlanUpgraded = false
                                        }
                                        
                                        
                                        popUp.addButtons([btnOk,btnCancel])
                                        
                                        // Present dialog
                                        self.present(popUp, animated: true, completion: nil)
                                        
//                                        let alert = UIAlertController(title: kAppName, message: strAlert, preferredStyle: .alert)
//                                        let okAction = UIAlertAction(title: AppHelper.localizedtext(key: "YES"), style: .default) { (action) in
//
//                                                         var guest_plan_id : Int = arrGuest.last?.id ?? 0
//                                                         let plan = self.storyboard?.instantiateViewController(withIdentifier: "PlanOfEventsVC") as! PlanOfEventsVC
//                                                         plan.delegate = self;
//                                                        plan.isFromCreateEvent = self.isFromCreateEvent
//                                                         plan.eventDetailObj = self.eventDetailObj;
//                                                         plan.isOpenFromAddGuest = true
//                                                         let planObj = self.eventDetailObj?.plan
//                                                         let plan_id : Int = planObj?.id ??  0
//                                                         plan.package_id = plan_id ?? 0
//                                                      //   plan.isSelected = isSelected
//                                                         plan.selectedTag = self.selectedRow
//                                                         plan.selectedPlan_id = guest_plan_id
//                                                         plan.isOpenFromAddGuest = true
//                                                         let navVC = UINavigationController(rootViewController:plan )
//                                                         self.present(navVC, animated: true, completion: nil)
//                                                     }
//                                        let cancelAction = UIAlertAction(title: AppHelper.localizedtext(key: "NO"), style: .destructive) { (action) in
//                                                        self.arrContactPicker.removeAll()
//                                                        self.isSelectedNoInPopUp = true
//                                                        self.isPlanUpgraded = false
//
//                                                     }
//                                                     alert.addAction(okAction)
//                                                     alert.addAction(cancelAction)
//                                                     self.present(alert, animated: true, completion: nil)
                                                     return
                                     }else{
                                         
                                         self.arrContacts.append(contentsOf: self.arrContactPicker);
                                         self.arrContacts = (self.arrContacts as NSArray).sortedArray(using: [NSSortDescriptor(key: "full_name", ascending: true)]) as! [[String:AnyObject]]
                                         self.arrData = self.arrContacts;
                                         self.arrContactPicker.removeAll()
                                         self.tblContacts.reloadData()
                                     }
                                 }
                                
                                self.tblContacts.isHidden = false
                               
                            }else{
                                self.showAlert(message: ValidationAlert.kEmptyConatctToImport, title: kAppName)
                            }
                        }
                        
                        print(response);
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    self.showAlert(message:error.localizedDescription , title: kAppName)
                }
            }
        }
    }
