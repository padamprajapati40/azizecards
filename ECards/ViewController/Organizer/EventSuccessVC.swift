//
//  EventSuccessVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 04/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

extension Range where Bound == String.Index {
    var nsRange:NSRange {
        return NSRange(location: self.lowerBound.encodedOffset,
                   length: self.upperBound.encodedOffset -
                    self.lowerBound.encodedOffset)
    }
}
extension UITapGestureRecognizer {

    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)

        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)

        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize

        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)

        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)

        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }

}

class EventSuccessVC: AppViewController {

     let controller = OrganizerViewModel()
    var eventObj : EventDetails?
    var isInviteSelecetd = Bool()
    var strTitle = String()
    var strMessage = String()
    var isopenFromThankyouMessage = Bool()
    var isOpenScheduled = Bool()
    var isOpenFromaCreateGuard = Bool()
    var isOpenFromAttendessAdded = Bool()
    var isOpenFromTicket = Bool()
    var isopenFromCancelledMessage = Bool()
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
 
    @IBOutlet weak var btnContinue: UIButton!
 
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setData()
        
        print(self.eventObj)
       
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        self.title = AppHelper.localizedtext(key: "QRCodeInvite")
        if(self.eventObj?.instant_invitation_sent == true){
            let event_id : Int = self.eventObj?.id ?? 0
            self.callInstantMessageApi("email", send_message: "instant", id: "\(event_id)")
        }
        self.setTitleView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func setData()  {
        if isopenFromThankyouMessage{
            self.strTitle = AppHelper.localizedtext(key: "MessageSent")
            self.strMessage = AppHelper.localizedtext(key: "MessageSentSuccessMessage")
            self.btnContinue.tag = 100
            self.btnContinue.setTitle(AppHelper.localizedtext(key: "BackToEvents"), for: .normal)
           
            self.btnContinue.isHidden = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                      self.navigationController?.popToRootViewController(animated: true)
                 }
          
        }else if isopenFromCancelledMessage{
            self.strTitle = AppHelper.localizedtext(key: "kCancelledEventTitle")
            self.strMessage = AppHelper.localizedtext(key: "kEventCancel")
            self.btnContinue.tag = 100
            self.btnContinue.setTitle(AppHelper.localizedtext(key: "BackToEvents"), for: .normal)
            self.btnContinue.isHidden = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.navigationController?.popToRootViewController(animated: true)
            }
                     
        }
        else if isOpenFromaCreateGuard{
            self.strTitle = AppHelper.localizedtext(key: "GuardCreated")
            self.strMessage = AppHelper.localizedtext(key: "CreateGuardSuccessMessage")
            self.btnContinue.tag = 200
            self.btnContinue.setTitle(AppHelper.localizedtext(key: "BackToEvents"), for: .normal)
            self.btnContinue.isHidden = true
            self.btnContinue.isEnabled = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                      self.navigationController?.popToRootViewController(animated: true)
                 }
        }else if isOpenFromAttendessAdded{
            self.strTitle = AppHelper.localizedtext(key: "AttendeeAdded")
            self.strMessage = AppHelper.localizedtext(key: "AttendeeAddedSuccessMessage")
            self.btnContinue.tag = 300
            self.btnContinue.setTitle(AppHelper.localizedtext(key: "BackToEvents"), for: .normal)
            self.btnContinue.isHidden = true
            self.btnContinue.isEnabled = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                      self.navigationController?.popToRootViewController(animated: true)
                 }
        }else if isOpenFromTicket{
            self.strTitle = AppHelper.localizedtext(key: "TicketRaised")
            self.strMessage = AppHelper.localizedtext(key: "TicketSuccessMessage")
            self.btnContinue.isHidden = true
            self.btnContinue.isEnabled = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                      self.navigationController?.popToRootViewController(animated: true)
                 }
        }else{
            self.strTitle = AppHelper.localizedtext(key: "EventCreated")
            self.strMessage = AppHelper.localizedtext(key: "EventCreatedSuccessMessage")
            self.btnContinue.tag = 400
            self.btnContinue.isHidden = true
            self.btnContinue.isEnabled = false
                        
//            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
//                self.navigationController?.popToRootViewController(animated: true)
//            }
            
        }
        if(self.isOpenScheduled == true){
            self.strMessage = AppHelper.localizedtext(key: "kEventSchedueld")
        }
        self.lblTitle.text = strTitle
        self.lblMessage.text = strMessage
        let language = GlobalUtils.getInstance().getApplicationLanguage()
        if language == "arabic"{
            self.lblMessage.halfTextColorChange(fullText: strMessage, changeText: "إدارة الحدث")
        }else{
            self.lblMessage.halfTextColorChange(fullText: strMessage, changeText: "Manage Event")
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.numberOfTapsRequired = 1;
        self.lblMessage.isUserInteractionEnabled = true
        self.lblMessage.addGestureRecognizer(tap)
    }
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        var text = "Manage Event"
        let language = GlobalUtils.getInstance().getApplicationLanguage()
        if language == "arabic"{
            text = "إدارة الحدث"
          
        }
        let tap = sender as! UITapGestureRecognizer
        guard let range = self.lblMessage.text?.range(of: text)?.nsRange else {
           return
        }
        if tap.didTapAttributedTextInLabel(label: self.lblMessage, inRange: range) {
               self.navigationController?.popToRootViewController(animated: true)
               self.tabBarController?.selectedIndex = 0;
       }
    }
   //--------------------------------------------------------------------


    @IBAction func btnContinueAction(_ sender: Any) {
//        switch self.btnContinue.tag {
//        case 100,200,300:
//            self.navigationController?.popToRootViewController(animated: true)
//            break
//        case 400:
//            self.navigationController?.popToRootViewController(animated: true)
//            break
//        default:
//            break
//        }
//        
        if self.isInviteSelecetd{
            
           // self.callInstantMessageApi("email", send_message: "instant", id: "\(event_id)")
        }else{
            self.showAlertWithCallBack(title: kAppName, message: ValidationAlert.kEmptyInstantMessage) {
                
            }
             //self.navigationController?.popToRootViewController(animated: true)
        }
        
    }
    
   func callInstantMessageApi(_ type : String, send_message : String , id : String){
         
                 self.showActivityIndicatory()
        
                controller.sendInstantMessage(EventId: id, type: type, send_Message: send_message) { (success, response, message, statusCode) in
         
                    if success{
                         DispatchQueue.main.async {
                             self.hideActivityIndicator()
                             
                           // self.showAlertWithCallBack(title: kAppName, message: message) {
                               
                             
                         //   }
                            
                         }
                     }else{
                         DispatchQueue.main.async {
                             self.hideActivityIndicator()
                            self.showAlert(message: message, title: kAppName)
                         }
                     }
         
                 }
             }
    
    
    
}
