//
//  EventDetailVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 13/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import ObjectMapper
import PopupDialog


class CreateEventVC: AppViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource,MapVCDelegate, PlanSelectDelegate, UITableViewDataSource, UITableViewDelegate , planMessageDelegate{
    
    
    func mainPlanSeletionMethod(_ tag: Int) {
        let dictPlan : [String:Any] = self.packageList[tag] as? [String:Any] ?? [:]
        print(dictPlan)
        
        
        
    }
    
    func mainPlanMethodForMessage(_ tag: Int) {
        let message : String = self.packageList[tag].description ?? ""
         self.popupAndsetMessageOninfoClick(title: kAppName, Message:message)
    }
    
    
    
   
  
    //........MARK: Variables......................
    var eventId : Int = Int()
    var cardId : Int = Int()
    var guestPlanId : Int = Int()
    var planId : Int?
    var arrLanguage = ["English", "Arabic", "Both"]
    let controller = OrganizerViewModel()
    var isInviteSelecetd = Bool()
    var isSelectedPremium = Bool()
    var isSelectedStandard = Bool()
    var isEnglishSelected = Bool()
    var isArabicSelected = Bool()
    var isBothLanguageSelected = Bool()
    var eventName = String()
    var packageList = [PlanOfPackage]()
    var packgae_id = Int()
    var strEnglish = String()
    var strArabic = String()
    var rowSelected = Int()
   var bgImgUrl = String()
     var eventObj : EventDetails?
     var eventCreatedObj : EventDetails?
    var isOpenFromEdit = Bool()
    let tickImage = UIImage(named: "Square_Blue_Tick")
    let unTickImage = UIImage(named: "square")
    var isPlanUpdated : Bool = false
    var selectedContacts = Int()
    //........MARK: IBOutlets......................
    
    @IBOutlet weak var lblInvitationLabelHeight : NSLayoutConstraint!
    @IBOutlet weak var lblInvitationStaticLabel: UILabel!
    @IBOutlet weak var txtInvitationHeight : NSLayoutConstraint!
    @IBOutlet weak var heightTable : NSLayoutConstraint!
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var lblStaticMessage: UILabel!
    @IBOutlet weak var btnArabic: UIButton!
    @IBOutlet weak var btnEnglish: UIButton!
    @IBOutlet weak var btnInfoPremium: UIButton!
    @IBOutlet weak var btnInfoStandard: UIButton!
    @IBOutlet weak var btnBoth: UIButton!
    @IBOutlet weak var txtFromTime: BWTextField!
    @IBOutlet weak var txtToTime: BWTextField!
    @IBOutlet weak var viewStandard: UIView!
    @IBOutlet weak var viewPremium: UIView!
    @IBOutlet weak var txtEventName: BWTextField!
    @IBOutlet weak var txtEventDate: BWTextField!
    @IBOutlet weak var txtEventTime: BWTextField!
    @IBOutlet weak var txtPickLocation: BWTextField!
    @IBOutlet weak var txtPackageOptions: BWTextField!
    @IBOutlet weak var txtNumberOfAttendess: BWTextField!
    @IBOutlet weak var txtChooseLanguage: BWTextField!
    @IBOutlet weak var txtMessageDateAndTime: BWTextField!
    @IBOutlet weak var txtEndTime: BWTextField!
     @IBOutlet weak var txtAmount: BWTextField!
    @IBOutlet weak var btnCreateEvent: UIButton!
    @IBOutlet weak var txtArabicText: UITextView!
    @IBOutlet weak var txtEnglishText: UITextView!
    
    @IBOutlet weak var btnStandard: UIButton!
    
    @IBOutlet weak var lblMessage: PaddingLabel!
    @IBOutlet weak var tblPlan: UITableView!
    @IBOutlet weak var btnPremium: UIButton!
    @IBOutlet var pickerDate: UIDatePicker!
    @IBOutlet var toolbarObj: UIToolbar!
    @IBOutlet var pickerView: UIPickerView!
    var address:String = ""
    var latitude:Double = 0.0
    var longitude:Double = 0.0
    //--------------------------------------------------------------------
    //MARK: View life cycle
    //--------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtPickLocation.delegate = self;
        self.lblStaticMessage.isHidden = true
        self.DefalutDialoguesettingOfPopup()
        self.callPlanOfPackageAPi()
        self.lblMessage.isHidden = true
        

        self.btnCreateEvent.titleLabel?.font = Font.tagViewFont
        self.txtPickLocation.textRect(forBounds: CGRect(x: 0, y: 0, width: self.txtPickLocation.frame.width - 100, height:44.0))
        if isOpenFromEdit{
            self.title = AppHelper.localizedtext(key: "EditEvent")
            print(self.eventObj)
             self.callPlanOfPackageAPi()
           
            
        }else{
            self.title = AppHelper.localizedtext(key: "CreateEvent") 
        }
        
        
        let imgBlueCircle = UIImage(named: "Square_Blue_Tick")
        let imgGrayCirle = UIImage(named: "square")
        self.btnEnglish.setImage(imgGrayCirle, for: .normal)
        self.btnArabic.setImage(imgGrayCirle, for: .normal)
        self.btnBoth.setImage(imgGrayCirle, for: .normal)
        self.btnEnglish.setImage(imgBlueCircle, for: .selected)
        self.btnArabic.setImage(imgBlueCircle, for: .selected)
        self.btnBoth.setImage(imgBlueCircle, for: .selected)
        self.btnInvite.setImage(imgGrayCirle, for: .normal)
        self.btnInvite.setImage(imgBlueCircle, for: .selected)
        
        print("Event id is........")
        
        let appLanguage = GlobalUtils.getInstance().getApplicationLanguage()
        if(appLanguage == "arabic"){
            self.pickerDate.locale = Locale(identifier: "ar");
            
        }else{
            self.pickerDate.locale = Locale(identifier: "en");
        }
        
        
        print(eventId)
        self.setTitleView()
        self.txtEventName.maxLength  = 40;
    }
    //------------------------------
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isOpenFromEdit{
            
        }else{
           if self.eventCreatedObj == nil{
           
            }else{
                self.showAlert(message: AppHelper.localizedtext(key: "kAlreadyCreatedEvent"), title: kAppName)
            }
        }
        
    }
    //--------------------------------------------------------------------
        //MARK: UITableview Datasource
    //--------------------------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.packageList.count
       }
      //--------------------------------------------------------------------
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlanCell", for: indexPath) as! PlanCell
        cell.btnInfo.tag = indexPath.row
        cell.btnPlan.tag = indexPath.row
        cell.lblPlan.text = self.packageList[indexPath.row].name ?? ""
        //cell.btnPlan.isSelected = self.packageList[indexPath.row].isselected ?? false
        let imgBlueCircle = UIImage(named: "Square_Blue_Tick")
        let imgGrayCirle = UIImage(named: "square")
        if(self.packageList[indexPath.row].isselected ?? false == true){
            cell.btnPlan.setImage(imgBlueCircle, for: .normal)
             cell.btnPlan.setImage(imgBlueCircle, for: .selected)
        }else{
             cell.btnPlan.setImage(imgGrayCirle, for: .normal)
            cell.btnPlan.setImage(imgGrayCirle, for: .selected)
        }
        cell.delegate = self
        cell.btnPlan.tag = indexPath.row
        
        cell.viewPlan.backgroundColor = UIColor(named: "PlanOverViewBackgroundColor")
        return cell
       }
    
    //--------------------------------------------------------------------
        //MARK: UItableview Delegate
    //--------------------------------------------------------------------
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    //--------------------------------------------------------------------
    func setDataOfEvents (_ eventObj : EventDetails){
        
        self.lblMessage.isHidden = false
        print(eventObj.attendees?.count)
        self.selectedContacts = eventObj.attendees?.count ?? 0
        
            let strEventDate : String = eventObj.event_date ?? ""
            let strMessageDate : String = eventObj.message_date_time ?? ""
            let strEventEndTime : String = eventObj.to_time ?? ""
        
            let dateFormatter = DateFormatter()
          dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
      let messageDate = dateFormatter.date(from: strMessageDate)
            
            let eventDate = dateFormatter.date(from:strEventDate)
              dateFormatter.dateFormat = "E, dd MMM yyyy HH:mm:ss Z"
            
        
        let eventEndDate = dateFormatter.date(from: strEventEndTime)
            
            //dateFormatter.dateFormat = "dd/MM/yyyy"
            dateFormatter.dateFormat = "dd-MMM-yyyy  h:mm a"
            let strEventFinalDate  : String = dateFormatter.string(from: eventDate ?? Date())
            
           // dateFormatter.dateFormat = "dd-MMM-yyyy  h:mm a"
            let strMessageFinalDate : String = dateFormatter.string(from: messageDate ?? Date())
            
            self.txtEventDate.text = strEventFinalDate
        let strEventEndDateTime : String = dateFormatter.string(from: eventEndDate ?? Date())
        self.txtEndTime.text = strEventEndDateTime
        
            
            self.txtEventName.text = eventObj.name
            self.txtMessageDateAndTime.text = " \(strMessageFinalDate)"
            
    //        self.lblNumberOfAttendess.text = "\(eventDetaliLits.total_number_of_guests)"
            self.txtPickLocation.text = eventObj.venue
           let msg = eventObj.message ?? ""
           // self.lblMessage.text = eventObj.message ?? ""
          self.lblMessage.attributedText = msg.htmlToAttributedString ?? NSAttributedString(string:"")
          
//        let message : String = eventObj.message ?? ""
//       self.lblMessage.text = message.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
        
        
         self.cardId = eventObj.card_theme_id ?? 0
        self.eventId = eventObj.id ?? 0
        
            var language : String = eventObj.language as? String ?? ""
            
            switch language {
            case "both":
                language = "Both"
                self.isBothLanguageSelected = true
                self.isEnglishSelected = false
                self.isArabicSelected = false
                self.lblStaticMessage.isHidden = false
                let msg = self.strEnglish + "\n\n" + self.strArabic
                self.lblMessage.attributedText = msg.htmlToAttributedString ?? NSAttributedString(string:"")
               
                
                self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
                break
            case "english":
                language = "English"
                self.isBothLanguageSelected = false
                self.isEnglishSelected = true
                self.isArabicSelected = false
                self.lblStaticMessage.isHidden = false
                 let msg = self.strEnglish
                self.lblMessage.attributedText = msg.htmlToAttributedString ?? NSAttributedString(string:"")
                self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
                break
            case "arabic":
                language = "Arabic"
                self.isBothLanguageSelected = false
                self.isEnglishSelected = false
                self.isArabicSelected = true
                self.lblStaticMessage.isHidden = false
                let msg = self.strArabic
                self.lblMessage.attributedText = msg.htmlToAttributedString ?? NSAttributedString(string:"")
             //   self.lblMessage.text = self.strArabic
                self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
                break
            default:
                break
            }
        
        
            let imgBlueCircle = UIImage(named: "Square_Blue_Tick")
            let imgGrayCirle = UIImage(named: "square")
            
                   let to_time : String = eventObj.to_time ?? ""
                   let from_time : String = eventObj.from_time ?? ""
                  //  dateFormatter.dateFormat = "h:mm a"
                   let to_Date = dateFormatter.date(from: to_time)
                   let from_Date = dateFormatter.date(from: from_time)
                  

        if to_Date != nil{
            self.txtToTime.text = dateFormatter.string(from:to_Date! )
        }
        
        if from_Date != nil {
            self.txtFromTime.text = dateFormatter.string(from:from_Date! )
        }
        
        //use self.txtEndTime for end date and time of event.
        
        
            
            let planObj = eventObj.plan
            let plan_name : String = planObj?.name ?? ""
            let plan_price : Int = planObj?.price ?? 0
            let plan_description : String = planObj?.description ?? ""
            let arrGuest = eventObj.guest_plans as? [Guest_plans] ?? []
        
        self.planId = planObj?.id ?? 0
        
        var selectedIndex = 0
        for package in self.packageList{
            var pack = package
//            if(selectedIndex == self.planId){
//                pack.isselected = !(package.isselected ?? false);
//            }else{
//                pack.isselected = false
//            }
//            
            if (pack.id == self.planId){
                pack.isselected = !(package.isselected ?? false);
            }else{
                 pack.isselected = false
            }
            self.packageList[selectedIndex] = pack;
            selectedIndex = selectedIndex + 1
        }
        
        
        
//        if planObj?.id == 2 {
//            self.btnPremium.setImage(imgBlueCircle, for: .normal)
//            self.btnStandard.setImage(imgGrayCirle, for: .normal)
//            self.isSelectedPremium = true
//            self.isSelectedStandard = false
//            
//            self.viewPremium.backgroundColor = UIColor(named: "ViewSelectedColor")
//                       self.viewStandard.backgroundColor = UIColor(named: "PlanOverViewBackgroundColor")
//                       
//                       self.viewPremium.layer.borderColor = UIColor(named: "SecondrayColor")?.cgColor
//                       self.viewPremium.layer.borderWidth = 2.0
//                       
//                       self.viewStandard.layer.borderWidth = 0.0
//        }else{
//            self.btnPremium.setImage(imgGrayCirle, for: .normal)
//            self.btnStandard.setImage(imgBlueCircle, for: .normal)
//            self.isSelectedPremium = false
//            self.isSelectedStandard = true
//            
//            
//            self.viewPremium.backgroundColor = UIColor(named: "PlanOverViewBackgroundColor")
//            self.viewStandard.backgroundColor = UIColor(named: "ViewSelectedColor")
//            
//            
//            self.viewStandard.layer.borderColor = UIColor(named: "SecondrayColor")?.cgColor
//            self.viewStandard.layer.borderWidth = 2.0
//            
//            self.viewPremium.layer.borderWidth = 0.0
//        }
        
        self.latitude = Double(eventObj.latitude ?? 0) ?? 0.0
        self.longitude = Double(  eventObj.longitude ?? 0) ?? 0.0
      //  self.planId = eventObj.plan_id ?? 0
      //  eventObj.guest_plan_id ?? 0
        
        
        if arrGuest.count != 0{
            let guest_Plan_Obj = arrGuest[0]
                     let guest_name : String = guest_Plan_Obj.name ?? ""
                     
                   //  self.lblNumberOfAttendess.text = guest_name
                   //  self.btnPriceOfPackage.setTitle("\(plan_price)SR", for: .normal)
                     //self.txt.text = plan_name
                     let minimum_attendees : Int = guest_Plan_Obj.minimum_attendees ?? 0
                     let maximum_attendees : Int = guest_Plan_Obj.maximum_attendees ?? 0
            let amount : Int = guest_Plan_Obj.amount ?? 0
            let strGuest : String  = AppHelper.localizedtext(key: "kGuests")
                     self.txtNumberOfAttendess.text = "\(minimum_attendees) - \(maximum_attendees) \(strGuest) "
            self.txtAmount.text = "\(amount) SR"
            self.guestPlanId = guest_Plan_Obj.id ?? 0
          //  self.planId = guest_Plan_Obj.id ?? 0
            
        }
        
        if eventObj.instant_invitation_sent ?? false {

            
            self.btnInvite.setImage(tickImage, for: .normal)
            
        }else{
            self.btnInvite.setImage(unTickImage, for: .normal)
        }
            
        
        if eventObj.instant_invitation_sent == false{
              self.txtMessageDateAndTime.text = " \(strMessageFinalDate)"
            self.txtMessageDateAndTime.isHidden = false
            self.txtMessageDateAndTime.isEnabled = true
            self.isInviteSelecetd = false
        }else{
              self.txtMessageDateAndTime.text = ""
            self.txtMessageDateAndTime.isHidden = true
            self.txtMessageDateAndTime.isEnabled = false
            self.isInviteSelecetd = true
        }
         
            //self.msg = plan_description
            
//            if planObj?.id == 1{
//                self.lblInstanceMessage.isHidden = true
//                self.btnInvite.isHidden = true
//            }else{
//                self.lblInstanceMessage.isHidden = false
//                self.btnInvite.isHidden = false
//            }
        
        self.tblPlan.reloadData()
           
        }
    
    
    //--------------------------------------------------------------------
    //MARK: UITextfield delegate
    //--------------------------------------------------------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "SecondrayColor")
        if textField == self.txtEventDate {
            self.pickerDate.tag = 1001
          //  self.pickerDate.datePickerMode = .date
            self.pickerDate.minimumDate = Date()
            self.pickerDate.maximumDate = nil
            textField.inputView = self.pickerDate
            textField.inputAccessoryView = self.toolbarObj
        }
        if textField == self.txtEndTime{
            self.pickerDate.tag = 1002
           // self.pickerDate.datePickerMode = .date
            self.pickerDate.maximumDate = nil
            textField.inputView = self.pickerDate
            textField.inputAccessoryView = self.toolbarObj
        }
        
        if textField == self.txtToTime{
          
            self.pickerDate.tag = 3001
       
            self.pickerDate.datePickerMode = .time
            textField.inputView = self.pickerDate
            textField.inputAccessoryView = self.toolbarObj
        }
        
        if textField == self.txtFromTime{
            self.pickerDate.tag = 4001
            self.pickerDate.minimumDate = nil;
            self.pickerDate.datePickerMode = .time
            textField.inputView = self.pickerDate
            textField.inputAccessoryView = self.toolbarObj
        }
        
        if textField == self.txtMessageDateAndTime {
            let dateFormatter = DateFormatter()
            self.pickerDate.minimumDate = Date();
            dateFormatter.dateFormat = "dd-MM-yyyy h:mm a"
            let edate = dateFormatter.date(from: self.txtEventDate.text ?? "")
            let time = self.txtFromTime.text ?? ""
            
            self.pickerDate.maximumDate = edate;
            if(time != ""){
                            let dateFormatter = DateFormatter()
                            dateFormatter.timeStyle = DateFormatter.Style.short
                            dateFormatter.dateFormat = "h:mm a"
                            let date = dateFormatter.date(from: time);
                            var component = Calendar.current.dateComponents(in: TimeZone.current, from: edate!)
                            var component1 = Calendar.current.dateComponents(in: TimeZone.current, from: date!)
                            component.hour = component1.hour;
                            component.minute = component1.minute;
                            component.second = component1.second;
                            let maximumDate = Calendar.current.date(from: component)
                            self.pickerDate.maximumDate = maximumDate;
            }
            self.pickerDate.tag = 2001
            self.pickerDate.datePickerMode = .dateAndTime
            textField.inputView = self.pickerDate
            textField.inputAccessoryView = self.toolbarObj
        }
        
        if textField == self.txtChooseLanguage{
            textField.inputView = self.pickerView
            textField.inputAccessoryView = self.toolbarObj
        }
        
       
    }
    
    
    
    //--------------------------------------------------------------------
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        if(textField == self.txtPickLocation){
            self.view.endEditing(true)
            let mapVC = self.storyboard?.instantiateViewController(withIdentifier: "MapVC") as! MapVC
            mapVC.delegate = self;
            self.navigationController?.pushViewController(mapVC, animated: true)
            return false;
        }
        
        if textField == self.txtNumberOfAttendess{
            var isSelected = Bool()
//            if self.isSelectedPremium{
//                self.planId = self.packageList[1].id ?? 0
//            }
//            if isSelectedStandard{
//                self.planId = self.packageList[0].id ?? 0
//            }
            if self.txtNumberOfAttendess.text == ""{
                isSelected = false
                if self.planId == nil{
                    self.showAlert(message: ErrorMessage.kSelectPackage , title: kAppName)
                }
            }else{
                isSelected = true
            }
            
            if(self.planId != nil){
                let plan = self.storyboard?.instantiateViewController(withIdentifier: "PlanOfEventsVC") as! PlanOfEventsVC
                plan.delegate = self;
                plan.package_id = self.planId!
                plan.isSelected = isSelected
                plan.selectedTag = self.rowSelected
                if self.eventObj != nil {
                    plan.isOpenFromEditDetails = self.isOpenFromEdit
                    plan.selectedPlan_id = self.guestPlanId
                }
                self.navigationController?.pushViewController(plan, animated: true)
            }
            
            return false;
        }
        
        return true;
    }
    //--------------------------------------------------------------------
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "lightGray")
    }
    //--------------------------------------------------------------------
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        
//        
//        if textField != self.txtPickLocation{
//            var maxLenght = 15
//            
//            var currentString = textField.text
//            
//            
//            return currentString!.length <= maxLenght
//        }else{
//            return true
//        }
//      
//        
//    }
    //--------------------------------------------------------------------
    //MARK: Picker Datasource
    //--------------------------------------------------------------------
     func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    //--------------------------------------------------------------------
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrLanguage.count
    }
    
    
    //--------------------------------------------------------------------
    //MARK: Picker delegates
    //--------------------------------------------------------------------

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return self.arrLanguage[row]
    }
    //--------------------------------------------------------------------
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.txtChooseLanguage.text = self.arrLanguage[row]
    }
    
  //--------------------------------------------------------------------
    //MARK: Picker actions
    //--------------------------------------------------------------------
    @IBAction func DateValueChangeActions(_ sender: Any) {
        let dateFormatter = DateFormatter()
        
        if self.pickerDate.tag == 1001{
            
             dateFormatter.dateStyle = DateFormatter.Style.short
            dateFormatter.dateFormat = "dd-MMM-yyyy h:mm a"
            let strDate = dateFormatter.string(from: pickerDate.date)
            print(strDate)
            self.txtEventDate.text = strDate
            
        }else  if self.pickerDate.tag == 1002{
            
             dateFormatter.dateStyle = DateFormatter.Style.short
            dateFormatter.dateFormat = "dd-MMM-yyyy h:mm a"
            let strDate = dateFormatter.string(from: pickerDate.date)
            print(strDate)
                self.txtEndTime.text = strDate
            
        } else if self.pickerDate.tag == 2001{
            dateFormatter.dateStyle = DateFormatter.Style.short
            dateFormatter.timeStyle = DateFormatter.Style.short
            dateFormatter.dateFormat = "dd-MMM-yyyy  h:mm a"
            let strDate = dateFormatter.string(from: pickerDate.date)
            print(strDate)
            self.txtMessageDateAndTime.text = strDate
        }else if self.pickerDate.tag == 3001{
            dateFormatter.timeStyle = DateFormatter.Style.short
            dateFormatter.dateFormat = "h:mm a"
            let strDate = dateFormatter.string(from: pickerDate.date)
            print(strDate)
            self.txtToTime.text = strDate
        }else if self.pickerDate.tag == 4001{
            dateFormatter.timeStyle = DateFormatter.Style.short
            dateFormatter.dateFormat = "h:mm a"
            let strDate = dateFormatter.string(from: pickerDate.date)
            print(strDate)
            self.txtFromTime.text = strDate
        }
        
    }
    //--------------------------------------------------------------------
    //MARK: Toolbar actions
    //--------------------------------------------------------------------
    @IBAction func toolbarAction(_ sender: Any) {
        self.view.endEditing(true)
        if self.txtFromTime.text != "" {
            if self.txtToTime.text != ""{
                 let result = compareDates(self.txtFromTime.text ?? "", time2: self.txtToTime.text ?? "")
                if !result{
                    self.showAlertWithCallBack(title: kAppName, message: ValidationAlert.kEndTime, callBack: {
                        self.txtToTime.text = ""
                        
                    })
                }
            }
        }
    }
    //--------------------------------------------------------------------
    //MARK: Uibutton actions
    //--------------------------------------------------------------------
    
    @IBAction func btnStandardActions(_ sender: Any) {
    
        let imgBlueCircle = UIImage(named: "Square_Blue_Tick")
        let imgGrayCirle = UIImage(named: "square")
        
        self.txtNumberOfAttendess.text = ""
        self.txtAmount.text = ""
        self.isSelectedPremium = false
        self.isSelectedStandard = true
        if self.isSelectedPremium{
           self.btnPremium.setImage(imgBlueCircle, for: .normal)
            self.btnStandard.setImage(imgGrayCirle, for: .normal)
            
            self.viewPremium.backgroundColor = UIColor(named: "ViewSelectedColor")
            self.viewStandard.backgroundColor = UIColor(named: "PlanOverViewBackgroundColor")
            
            self.viewPremium.layer.borderColor = UIColor(named: "SecondrayColor")?.cgColor
            self.viewPremium.layer.borderWidth = 2.0
            
            self.viewStandard.layer.borderWidth = 0.0
            
        }else{
            self.btnPremium.setImage(imgGrayCirle, for: .normal)
            self.btnStandard.setImage(imgBlueCircle, for: .normal)
            
            self.viewPremium.backgroundColor = UIColor(named: "PlanOverViewBackgroundColor")
            self.viewStandard.backgroundColor = UIColor(named: "ViewSelectedColor")
            
            self.viewStandard.layer.borderColor = UIColor(named: "SecondrayColor")?.cgColor
            self.viewStandard.layer.borderWidth = 2.0
            
            self.viewPremium.layer.borderWidth = 0.0
        }
    }
    
 //--------------------------------------------------------------------
    @IBAction func btnPremiumAction(_ sender: Any) {
        let imgBlueCircle = UIImage(named: "Square_Blue_Tick")
        let imgGrayCirle = UIImage(named: "square")
    
        self.txtNumberOfAttendess.text = ""
        
        self.isSelectedPremium = true
        self.isSelectedStandard = false
        if self.isSelectedPremium{
            self.btnPremium.setImage(imgBlueCircle, for: .normal)
            self.btnStandard.setImage(imgGrayCirle, for: .normal)
            
            self.viewPremium.backgroundColor = UIColor(named: "ViewSelectedColor")
            self.viewStandard.backgroundColor = UIColor(named: "PlanOverViewBackgroundColor")
            
            
            self.viewPremium.layer.borderColor = UIColor(named: "SecondrayColor")?.cgColor
            self.viewPremium.layer.borderWidth = 2.0
            
            self.viewStandard.layer.borderWidth = 0.0
        }else{
            self.btnPremium.setImage(imgGrayCirle, for: .normal)
            self.btnStandard.setImage(imgBlueCircle, for: .normal)
            
            self.viewPremium.backgroundColor = UIColor(named: "PlanOverViewBackgroundColor")
            self.viewStandard.backgroundColor = UIColor(named: "ViewSelectedColor")
            
            
            self.viewStandard.layer.borderColor = UIColor(named: "SecondrayColor")?.cgColor
            self.viewStandard.layer.borderWidth = 2.0
            
            self.viewPremium.layer.borderWidth = 0.0
        }
    }
    //--------------------------------------------------------------------
    @IBAction func btnEnglishAction(_ sender: Any) {
        
        self.lblMessage.text = ""
        self.lblStaticMessage.isHidden = false
        self.isEnglishSelected = true
        self.isArabicSelected = false
        self.isBothLanguageSelected = false
        
        
      //  self.lblMessage.text = self.strEnglish
        
        if self.txtEventName.text == ""{
           self.showAlert(message: ValidationAlert.kEmptyEventName, title: kAppName)
            return
        }else if self.txtEventDate.text == ""{
           self.showAlert(message: ValidationAlert.kEmptyEventDate, title: kAppName)
        }else{
            self.lblMessage.isHidden = false
            if self.strEnglish.contains("<EVENT NAME>"){
               self.strEnglish = self.strEnglish.replacingOccurrences(of: "<EVENT NAME>", with: self.txtEventName.text!)
            }
            
            if self.strEnglish.contains("<DATE>"){
                self.strEnglish = self.strEnglish.replacingOccurrences(of: "<DATE>", with: self.txtEventDate.text!)
            }
            let msg = self.strEnglish
            self.lblMessage.attributedText = msg.htmlToAttributedString ?? NSAttributedString(string:"")
           // self.lblMessage.text = self.strEnglish
        }
        
    self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
        
    }
    //--------------------------------------------------------------------
    @IBAction func btnArabicAction(_ sender: Any) {
        self.lblMessage.text = ""
        self.lblStaticMessage.isHidden = false
        self.isEnglishSelected = false
        self.isArabicSelected = true
        self.isBothLanguageSelected = false
       // self.lblMessage.text = self.strArabic
        
        
        if self.txtEventName.text == ""{
            self.showAlert(message: ValidationAlert.kEmptyEventName, title: kAppName)
            return
        }else if self.txtEventDate.text == ""{
            self.showAlert(message: ValidationAlert.kEmptyEventDate, title: kAppName)
        }else{
             self.lblMessage.isHidden = false
            if self.strArabic.contains("<EVENT NAME>"){
                self.strArabic = self.strArabic.replacingOccurrences(of: "<EVENT NAME>", with: self.txtEventName.text!)
            }
            
            if self.strEnglish.contains("<DATE>"){
                self.strArabic = self.strArabic.replacingOccurrences(of: "<DATE>", with: self.txtEventDate.text!)
            }
            self.lblMessage.attributedText = self.strArabic.htmlToAttributedString ?? NSAttributedString(string:"")
        }
        
        
        self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
    }
    //--------------------------------------------------------------------
    @IBAction func btnBothLangugaeAction(_ sender: Any) {
        self.lblMessage.text = ""
        self.lblStaticMessage.isHidden = false
        self.isEnglishSelected = false
        self.isArabicSelected = false
        self.isBothLanguageSelected = true
        if self.txtEventName.text == ""{
            self.showAlert(message: ValidationAlert.kEmptyEventName, title: kAppName)
            return
        }else if self.txtEventDate.text == ""{
            self.showAlert(message: ValidationAlert.kEmptyEventDate, title: kAppName)
        }else{
            
             self.lblMessage.isHidden = false
            if self.strEnglish.contains("<EVENT NAME>"){
                self.strEnglish = self.strEnglish.replacingOccurrences(of: "<EVENT NAME>", with: self.txtEventName.text!)
            }
            
            if self.strEnglish.contains("<DATE>"){
                self.strEnglish = self.strEnglish.replacingOccurrences(of: "<DATE>", with: self.txtEventDate.text!)
            }
            
            if self.strArabic.contains("<EVENT NAME>"){
                self.strArabic = self.strArabic.replacingOccurrences(of: "<EVENT NAME>", with: self.txtEventName.text!)
            }
            
            if self.strEnglish.contains("<DATE>"){
                self.strArabic = self.strArabic.replacingOccurrences(of: "<DATE>", with: self.txtEventDate.text!)
            }
            let text = self.strEnglish + "\n\n" + self.strArabic + "\n"
            
            self.lblMessage.attributedText = text.htmlToAttributedString ?? NSAttributedString(string:"")
           
        }
        
//        self.lblMessage.text = self.strEnglish + "\n\n" + self.strArabic + "\n"
        self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
    }
    //--------------------------------------------------------------------
    
    @IBAction func btnInfoPremiumAction(_ sender: Any) {
        let message: String = "Features included in this packages: \n\n\n-Theme, \n -Multiple Messages(Invitation, Reminder, Cancellation, Thank You), \n -QR Code Generator(Unique for every attendee), \n -Google Maps(Share the location of the event), \n -RSVP(Receive updates on who accepted/rejected your invitation), \n -Create Security Guards(To Scan QR codes at the event entrance, \n -Event Management Dashboard(For organizers)"
        self.popupAndsetMessageOninfoClick(title: kAppName, Message:message)
    }
    //--------------------------------------------------------------------
    
    @IBAction func btnInforStandardAction(_ sender: Any) {
        let message: String = "Features includes in this packages: \n -Theme, \n -Single Messages(Invitation Only), \n -Google Maps(Share the location of the event)"
        self.popupAndsetMessageOninfoClick(title: kAppName, Message:message)
    }
     //--------------------------------------------------------------------
    @IBAction func btnInviteAction(_ sender: Any) {
          
           if self.isInviteSelecetd{
                
                self.isInviteSelecetd = false
                self.btnInvite.setImage(unTickImage, for: .normal)
                self.txtMessageDateAndTime.isHidden = false
                self.txtMessageDateAndTime.isEnabled = true
                
           }else{
               
               self.isInviteSelecetd = true
               self.btnInvite.setImage(tickImage, for: .normal)
               self.txtMessageDateAndTime.isHidden = true
               self.txtMessageDateAndTime.isEnabled = false
           }
           
           
           
       }
    //--------------------------------------------------------------------
    @IBAction func btnCreateEventAction(_ sender: Any) {
       
        let result = compareDates(self.txtToTime.text ?? "", time2: self.txtFromTime.text ?? "")
    
        if self.txtEventName.text == "" {
            self.showAlert(message: ValidationAlert.kEmptyEventName, title: kAppName)
        }else if self.txtEventDate.text == ""{
            self.showAlert(message: ValidationAlert.kEmptyEventDate, title: kAppName)
        }else if self.txtPickLocation.text == "" {
            self.showAlert(message: ValidationAlert.kEmptyEventLocation, title: kAppName)
//        }else if !self.isSelectedStandard && !self.isSelectedPremium {
//            self.showAlert(message: ErrorMessage.kEmptyEventPackageOptions, title: kAppName)
        }else if self.txtNumberOfAttendess.text == "" {
            self.showAlert(message: ValidationAlert.kEmptyNoOfAttendess, title: kAppName)
        }else if !self.isEnglishSelected && !self.isArabicSelected && !self.isBothLanguageSelected {
            self.showAlert(message: ValidationAlert.kChooseLanguage, title: kAppName)
        }else if(self.isInviteSelecetd == false){
            if txtMessageDateAndTime.text == "" {
                self.showAlert(message: ValidationAlert.kEmptyMessageDate, title: kAppName)
            }
            else{
                
                let purchasedPlan : Int = self.planId!;
                var chosenLanguage = String()

                if self.isEnglishSelected{
                        chosenLanguage = "english"
                }else if self.isArabicSelected{
                       chosenLanguage = "arabic"
                }else{
                       chosenLanguage = "both"
                }

                var messageTime : String =  String()

                if let index = self.txtMessageDateAndTime.text!.range(of: ",")?.lowerBound {
                       let substring = self.txtMessageDateAndTime.text![..<index]                 // "ora"

                         let string = String(substring)
                          print(string)  // "ora"
                           messageTime = string
                 }
                if self.isOpenFromEdit{
                   
                        self.showActivityIndicatory()
                        
                        var planId : Int = self.planId ?? 0
                        
                    controller.updateEventDetails(event_id: "\(self.eventId)", Name: self.txtEventName.text!, Date: self.txtEventDate.text!, PickLocation: self.txtPickLocation.text!, ChooseLanguage: chosenLanguage, fromDate: self.txtEventDate.text!, ToTime: self.txtEndTime.text!, numberOfAttendes: self.txtNumberOfAttendess.text!, messageDateTime: self.txtMessageDateAndTime.text!, cardId: "\(self.cardId)", EventCardId: "\(self.eventId)", message: self.lblMessage.text ?? "", latitude: "\(self.latitude)", longitude: "\(self.longitude)", plan_id: "\(planId)", packageID: self.guestPlanId,isInvitationSend:self.isInviteSelecetd) { (success, response, message, statusCode) in
                            
                            if success{
                                DispatchQueue.main.async {
                                    self.hideActivityIndicator()
                                      let dicResponse : [String:Any] = response as? [String:Any] ?? [:]
                                    var eventDetailList : EventDetails?
                                                                 eventDetailList = Mapper<EventDetails>().map(JSON: dicResponse)

                                                                 print(eventDetailList)
                                    
                                    
                                                              if self.isInviteSelecetd{
                                                                  let event_id : Int = eventDetailList?.id ?? 0
                                                                                            self.callInstantMessageApi("email", send_message: "instant", id: "\(event_id)")
                                                              }
                                  //  self.showAlert(message: message, title: kAppName)
                                    self.showAlertWithCallBack(title: kAppName, message: message) {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                                
                                
                            }else{
                                DispatchQueue.main.async {
                                    self.hideActivityIndicator()
                                  //  self.showAlert(message: message, title: kAppName)
                                    
                                    let strMsg : String = message
                                    var arrStrMsg = strMsg.components(separatedBy: ".")
                                    if arrStrMsg.count > 0 {
                                        self.showAlert(message: "\(arrStrMsg[0]) \n \(arrStrMsg[1])", title: kAppName)
                                    }else{
                                        self.showAlert(message: message, title: kAppName)
                                    }

                                }
                            }
                            
                        }
                        
                    
                }else{
                    self.showActivityIndicatory()
                   
                        controller.createEvents(Name: self.txtEventName.text!, Date: self.txtEventDate.text!, PickLocation: self.txtPickLocation.text!, ChooseLanguage: chosenLanguage,fromDate: self.txtEventDate.text ?? "",ToTime: self.txtEndTime.text ?? "" , numberOfAttendes: self.txtNumberOfAttendess.text!, messageDateTime: self.txtMessageDateAndTime.text ?? "", cardId: "\(self.cardId)", EventCardId: "\(self.eventId)", message: self.lblMessage.text ?? "",latitude: "\(self.latitude)",longitude: "\(self.longitude)",plan_id:"\(self.guestPlanId)" ,packageID:purchasedPlan,isInvitationSend:self.isInviteSelecetd) { (success, response, message, statusCode) in
                    
                                           self.hideActivityIndicator()
                                           if success{
                                               print(response)
                                               print(message)
                                               print(statusCode)

                                               let dicResponse : [String:Any] = response as? [String:Any] ?? [:]
                                               let id : Int = dicResponse["id"] as? Int ?? 0


                                               var eventDetailList : EventDetails?
                                               eventDetailList = Mapper<EventDetails>().map(JSON: dicResponse)

                                          
                                             self.eventCreatedObj = eventDetailList
                                        
                                             self.moveToNextScreen( id , eventObj : self.eventCreatedObj!)
                                           }else{

                                               DispatchQueue.main.async {
                                                let strMsg : String = message
                                                var arrStrMsg = strMsg.components(separatedBy: ".")
                                                if arrStrMsg.count > 0 {
                                                    self.showAlert(message: "\(arrStrMsg[0]) \n \(arrStrMsg[1])", title: kAppName)
                                                }else{
                                                    self.showAlert(message: message, title: kAppName)
                                                }
                                                   
                                               }
                                           }
                    }
                }
            }
        }
//        else if(result == false){
//            self.showAlert(message: ErrorMessage.kEndTime, title: kAppName)
//        }
       
         else{
            self.showActivityIndicatory()
            var purchasedPlan : Int = self.planId!;


            var chosenLanguage = String()

            if self.isEnglishSelected{
                chosenLanguage = "english"
            }else if self.isArabicSelected{
                chosenLanguage = "arabic"
            }else{
                chosenLanguage = "both"
            }

            var messageTime : String =  String()

            if let index = self.txtMessageDateAndTime.text!.range(of: ",")?.lowerBound {
                let substring = self.txtMessageDateAndTime.text![..<index]                 // "ora"


                let string = String(substring)
                print(string)  // "ora"
                messageTime = string
            }

            if isOpenFromEdit{
                self.showActivityIndicatory()
                
                var planId : Int = self.planId ?? 0
                
                controller.updateEventDetails(event_id: "\(self.eventId)", Name: self.txtEventName.text!, Date: self.txtEventDate.text!, PickLocation: self.txtPickLocation.text!, ChooseLanguage: chosenLanguage, fromDate: self.txtEventDate.text!, ToTime: self.txtEndTime.text!, numberOfAttendes: self.txtNumberOfAttendess.text!, messageDateTime: self.txtMessageDateAndTime.text!, cardId: "\(self.cardId)", EventCardId: "\(self.eventId)", message: self.lblMessage.text ?? "", latitude: "\(self.latitude)", longitude: "\(self.longitude)", plan_id: "\(planId)", packageID: self.guestPlanId, isInvitationSend: self.isInviteSelecetd) { (success, response, message, statusCode) in
                    
                    if success{
                        DispatchQueue.main.async {
                            self.hideActivityIndicator()
                              let dicResponse : [String:Any] = response as? [String:Any] ?? [:]
                            var eventDetailList : EventDetails?
                                                         eventDetailList = Mapper<EventDetails>().map(JSON: dicResponse)

                                                         print(eventDetailList)
                            
                            
                                                      if self.isInviteSelecetd{
                                                          let event_id : Int = eventDetailList?.id ?? 0
                                                                                    self.callInstantMessageApi("email", send_message: "instant", id: "\(event_id)")
                                                      }
                          //  self.showAlert(message: message, title: kAppName)
                            self.showAlertWithCallBack(title: kAppName, message: message) {
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                        
                        
                    }else{
                        DispatchQueue.main.async {
                            self.hideActivityIndicator()
                           // self.showAlert(message: message, title: kAppName)
                            let strMsg : String = message
                            var arrStrMsg = strMsg.components(separatedBy: ".")
                            if arrStrMsg.count > 0 {
                                self.showAlert(message: "\(arrStrMsg[0]) \n \(arrStrMsg[1])", title: kAppName)
                            }else{
                                self.showAlert(message: message, title: kAppName)
                            }

                        }
                    }
                    
                }
                
            }else{
                self.hideActivityIndicator()
                if self.eventCreatedObj == nil{
                    self.showActivityIndicatory()
                    controller.createEvents(Name: self.txtEventName.text!, Date: self.txtEventDate.text!, PickLocation: self.txtPickLocation.text!, ChooseLanguage: chosenLanguage,fromDate: self.txtEventDate.text ?? "",ToTime: self.txtEndTime.text ?? "" , numberOfAttendes: self.txtNumberOfAttendess.text!, messageDateTime: self.txtMessageDateAndTime.text ?? "", cardId: "\(self.cardId)", EventCardId: "\(self.eventId)", message: self.lblMessage.text ?? "",latitude: "\(self.latitude)",longitude: "\(self.longitude)",plan_id:"\(self.guestPlanId)" ,packageID:purchasedPlan,isInvitationSend:self.isInviteSelecetd) { (success, response, message, statusCode) in
                    
                                           self.hideActivityIndicator()
                                           if success{
                                               print(response)
                                               print(message)
                                               print(statusCode)

                                               let dicResponse : [String:Any] = response as? [String:Any] ?? [:]
                                               let id : Int = dicResponse["id"] as? Int ?? 0


                                               var eventDetailList : EventDetails?
                                               eventDetailList = Mapper<EventDetails>().map(JSON: dicResponse)

                                          
                                             self.eventCreatedObj = eventDetailList
                                        
                                             self.moveToNextScreen( id , eventObj : self.eventCreatedObj!)
                                           }else{

                                               DispatchQueue.main.async {
                                                //   self.showAlert(message: message, title: kAppName)
                                                let strMsg : String = message
                                                var arrStrMsg = strMsg.components(separatedBy: ".")
                                                if arrStrMsg.count > 0 {
                                                    self.showAlert(message: "\(arrStrMsg[0]) \n \(arrStrMsg[1])", title: kAppName)
                                                }else{
                                                    self.showAlert(message: message, title: kAppName)
                                                }

                                               }
                                           }

                                       }
                }else{
                    self.showAlertWithCallBack(title: kAppName, message: AppHelper.localizedtext(key: "kAlreadyCreatedEvent"), callBack: {
                        DispatchQueue.main.async {
                                               let id : Int = self.eventCreatedObj?.id ?? 0
                                                                                             let addGuests = self.storyboard?.instantiateViewController(withIdentifier: "CustomizeCardAddVC")as! CustomizeCardAddVC
                                                                                             addGuests.eventId  = id
                                                                                             addGuests.cardId = self.cardId
                                                                                             addGuests.eventName = self.eventName
                                                                                             addGuests.bgImgUrl = self.bgImgUrl
                                                                                     //        addGuests.msg = self.lblMessage.text!
                                               addGuests.EventDetailsList = self.eventCreatedObj
                                                                                             self.navigationController?.pushViewController(addGuests, animated: true)
                                                                                         }
                    })
                    
                }
            }
        }
//

//        let addGuests = self.storyboard?.instantiateViewController(withIdentifier: "CustomizeCardAddVC")as! CustomizeCardAddVC
//     //  addGuests.eventId  = id
//        addGuests.bgImgUrl = self.bgImgUrl
//      addGuests.cardId = self.cardId
//       addGuests.eventName = self.eventName
////        addGuests.msg = self.lblMessage.text!
//    //    addGuests.EventDetailsList = EventDetails
//        self.navigationController?.pushViewController(addGuests, animated: true)
        
    }
    
    //--------------------------------------------------------------------
    //MARK: Web api Methods
    //--------------------------------------------------------------------
    func callPlanOfPackageAPi()  {
        
        self.showActivityIndicatory()
        controller.getPlanListOfPackageOptions { (success, response, message, statusCode) in
            
            if success{
                DispatchQueue.main.async {
                    
                    self.hideActivityIndicator()
                    
                    let dicResponse : [String:Any] = response as? [String:Any] ?? [:]
                    let arrPlans = dicResponse["plans"] as? [[String:Any]] ?? [[:]]
                    var planList : [PlanOfPackage] = [];
                    
                    planList = Mapper<PlanOfPackage>().mapArray(JSONArray: arrPlans)
                    self.packageList = planList as? [PlanOfPackage] ?? []
                    
                     self.packageList = self.packageList.sorted { ($0.name as? String ?? "").localizedCaseInsensitiveCompare(($1.name as? String ?? "")) == ComparisonResult.orderedAscending }
                    
                    print(self.packageList)
                    self.heightTable.constant = CGFloat(50.0) * CGFloat(self.packageList.count)
                 
                    self.view.layoutIfNeeded()
                    self.tblPlan.reloadData()
                    
                    let arrInvitationList = dicResponse["invitation_messages"] as? [String] ?? []
                    self.strEnglish = arrInvitationList[0] as? String ?? ""
                    self.strArabic = arrInvitationList[1] as? String ?? ""
                    if self.isOpenFromEdit{
                        if self.eventObj != nil {
                                self.setDataOfEvents(self.eventObj!)
                            }
                    }
                    
                }
            }else{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    self.showAlert(message: message, title: kAppName)
                }
            }
        }
    }
    
    
    //--------------------------------------------------------------------
    //MARK: Custom Delegate Methods
    //--------------------------------------------------------------------
    func sendLocationToCreateEventVC(_ address: String?,latitute:Double,longitude:Double){
        self.txtPickLocation.text = address
        self.address = address ?? ""
        self.longitude = longitude
        self.latitude = latitute
    }
    //--------------------------------------------------------------------
      func planSelected(_ range: String, id: Int, row: Int, amount: Int) {

     //   if range != ""{
        var strStaticGuest = String()

        strStaticGuest = AppHelper.localizedtext(key: "kGuests")
        
            self.txtNumberOfAttendess.text = range + " \(strStaticGuest)"
        self.txtAmount.text = "\(amount) SR"
        self.guestPlanId = id
        self.rowSelected = row
        self.isPlanUpdated = true
        
        
       // }
    }
    //--------------------------------------------------------------------
    //MARK: Custom  Methods
    //--------------------------------------------------------------------
    func languageSelectedAction(_ english:Bool,arabic:Bool,both:Bool)   {
        
       let imgBlueCircle = UIImage(named: "Square_Blue_Tick")
       let imgGrayCirle = UIImage(named: "square")
        
        if english{
            self.btnEnglish.isSelected = true
            self.btnArabic.isSelected = false
            self.btnBoth.isSelected = false
            
        }
        
        if arabic{
                    self.btnEnglish.isSelected = false
                    self.btnArabic.isSelected = true
                    self.btnBoth.isSelected = false
        }
        
        if both{
           self.btnEnglish.isSelected = false
            self.btnArabic.isSelected = false
            self.btnBoth.isSelected = true
        }
    }
    
  
    //------------------------------------------------
    func compareDates(_ time1 : String, time2 : String) -> Bool {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.short
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.timeZone = .current
        let date1 = self.txtEventDate.text as? String ?? ""
        let eventDate : Date = dateFormatter.date(from: date1) as? Date ?? Date()
      let date2 =  eventDate.timeIntervalSince1970
        let timezoneOffset =  TimeZone.current.secondsFromGMT()
        let timezoneEpochOffset = (date2 + Double(timezoneOffset))
        let timeZoneOffsetDate = Date(timeIntervalSince1970: timezoneEpochOffset)
        
        let dateTime1 : Date = dateFormatter.date(from: time1) as? Date ?? Date()
        let dateTime2 : Date = dateFormatter.date(from: time2) as? Date ?? Date()
        
        let result : ComparisonResult = dateTime1.compare(dateTime2)
        print(result)
        if result == ComparisonResult.orderedSame{
           return true
        }else if result == ComparisonResult.orderedDescending{
            return false
        }else if result == ComparisonResult.orderedAscending{
            return true
        }else{
            return false
        }
    }
    
    func compareDatesOFEventandInvitationMsg(_ date1 : String, date2 : String) -> Bool{
        
         let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "dd-MMM-yyyy  h:mm a"
        
        let event_date : Date = dateFormatter.date(from: date1) as? Date ?? Date()
        let invitation_date : Date = dateFormatter1.date(from: date2) as? Date ?? Date()
        
        let result : ComparisonResult = event_date.compare(invitation_date)
        print(result)
        if result == ComparisonResult.orderedSame{
            self.showAlert(message: AppHelper.localizedtext(key: "kSameTimeError"), title: kAppName)
            self.txtToTime.text = ""
            self.txtFromTime.text = ""
        return false
        }else if result == ComparisonResult.orderedDescending{
            return true
        }else if result == ComparisonResult.orderedAscending{
            return false
        }
        return false
    }
    
    
    //------------------------------------------------
//    func DefalutDialoguesettingOfPopup(){
//        let dialogAppearance = PopupDialogDefaultView.appearance()
//
//        dialogAppearance.backgroundColor      = UIColor(red:0.23, green:0.23, blue:0.27, alpha:1.00)
//        dialogAppearance.titleFont            = .boldSystemFont(ofSize: 14)
//        dialogAppearance.titleColor           = UIColor(named: "SecondrayColor")
//        dialogAppearance.titleTextAlignment   = .center
//        dialogAppearance.messageFont          = .systemFont(ofSize: 14)
//        dialogAppearance.messageColor         = UIColor(named: "DarkGray")
//        dialogAppearance.messageTextAlignment = .left
//
//        let pcv = PopupDialogContainerView.appearance()
//        pcv.backgroundColor = UIColor(red:0.23, green:0.23, blue:0.27, alpha:1.00)
//     //   pcv.cornerRadius    = 2
//        pcv.shadowEnabled   = true
//        pcv.shadowColor     = .black
//
//
//        // Customize dialog appearance
//        let pv = PopupDialogDefaultView.appearance()
//        pv.titleFont    = UIFont(name: "Zeit-Light", size: 16)!
//      //  pv.titleColor   = .white
//        pv.messageFont  = UIFont(name: "Zeit-Light", size: 14)!
//      //  pv.messageColor = UIColor(white: 0.8, alpha: 1)
//    }
    
    func callInstantMessageApi(_ type : String, send_message : String , id : String){
    
            self.showActivityIndicatory()
    
          // let id: Int = self.eventObj?.id ?? 0
           controller.sendInstantMessage(EventId: id, type: type, send_Message: send_message) { (success, response, message, statusCode) in
    
               if success{
                    DispatchQueue.main.async {
                        self.hideActivityIndicator()
                        self.showAlertWithCallBack(title: kAppName, message: "Invitation message send sucessfully") {
                            let eventId : Int = Int(id) ?? 0
                            self.moveToNextScreen(eventId, eventObj: self.eventCreatedObj!)
                        }
                       // self.showAlert(message: message, title: kAppName)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.hideActivityIndicator()
                       self.showAlert(message: message, title: kAppName)
                    }
                }
    
            }
        }
    
    func mainPlanSeletionMethod (_ tag : Int, isSelectedPlan : Bool, indexPath : IndexPath){
        var packagePlan = self.packageList[tag];
        self.planId = packagePlan.id ?? 0
        var selectedIndex = 0
        for package in self.packageList{
            var pack = package
            if(selectedIndex == tag){
                pack.isselected = !(package.isselected ?? false);
            }else{
                pack.isselected = false
            }
            self.packageList[selectedIndex] = pack;
            selectedIndex = selectedIndex + 1
        }
        
        self.txtNumberOfAttendess.text = ""
        self.txtAmount.text = ""
        self.tblPlan.reloadData()
        
    }
    
    func planUpdateSuccefully() {
        
    }
    
    func moveToNextScreen(_ eventId: Int , eventObj : EventDetails){
        DispatchQueue.main.async {
            
            let addGuests = self.storyboard?.instantiateViewController(withIdentifier: "CustomizeCardAddVC")as! CustomizeCardAddVC
            addGuests.eventId  = eventId
            addGuests.cardId = self.cardId
            addGuests.eventName = self.eventName
            addGuests.bgImgUrl = self.bgImgUrl
            //        addGuests.msg = self.lblMessage.text!
             addGuests.EventDetailsList = eventObj
            self.navigationController?.pushViewController(addGuests, animated: true)
         }
    }
}

