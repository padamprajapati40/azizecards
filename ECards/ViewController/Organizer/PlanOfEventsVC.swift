//
//  PlanOfEventsVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 17/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire

protocol PlanSelectDelegate {
    func planSelected(_ range: String, id: Int, row : Int, amount : Int)
    func planUpdateSuccefully()
}

class PlanOfEventsVC: AppViewController, UITableViewDataSource, UITableViewDelegate,PaymentDelegate {
    
    
    //MARK: Varibales..............
    var planList = [PlansForEvents]()
     let controller = OrganizerViewModel()
    var delegate : PlanSelectDelegate?
    var package_id = Int()
    var isFromCreateEvent : Bool = false
    var selectedTag = Int()
    var isSelected = Bool()
    var isOpenFromAddGuest = Bool()
    var selectedPlan_id : Int = Int()
    var isOpenFromEditDetails = Bool()
    var eventDetailObj:EventDetails?
    //MARK: IBOutlets..............
    
    @IBOutlet weak var tblPlans: UITableView!
    
    //-------------------------------------------------------
    //MARK: View Life Cycle
    //-------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = AppHelper.localizedtext(key: "PlanList") 
        print(self.selectedTag)
        print(self.selectedPlan_id)
        
        if self.isOpenFromAddGuest{
            let close = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(btnDoneAction(_ :)))
            
            self.navigationItem.rightBarButtonItem = close
        }
        
        self.callPlanLIstAPi(self.package_id)
        self.setTitleView()
        
    }
    
    @objc func btnDoneAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //-------------------------------------------------------
    //MARK: UITbleview datasource
    //-------------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.planList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlanTableCell", for: indexPath) as! PlanTableCell
       
        cell.lblName.text = self.planList[indexPath.row].name ?? ""
        cell.lblAmount.text = "\(self.planList[indexPath.row].amount ?? 0) SR"
        cell.lblRange.text = self.planList[indexPath.row].range ?? ""
        
       
        if isSelected {
            if selectedTag == indexPath.row{
                cell.accessoryType = .checkmark
            }
        }else{
            cell.accessoryType = .none
        }
       
        
        if self.isOpenFromEditDetails{
            let id = self.planList[indexPath.row].id ?? 0
            if self.selectedPlan_id == id {
                cell.accessoryType = .checkmark
            }else{
                cell.accessoryType = .none
            }
        }
            
     
        return cell
    }
    
    
    
    //-------------------------------------------------------
    //MARK: UITbleview Delegate
    //-------------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

            
            
            let id = self.planList[indexPath.row].id ?? 0
            if self.selectedPlan_id == id {
                
                self.showAlert(message: AppHelper.localizedtext(key: "kAlreadySelectedPlan"), title: kAppName)
            }else{
                if self.isFromCreateEvent{
                    self.delegate?.planSelected(self.planList[indexPath.row].range ?? "", id: self.planList[indexPath.row].id ?? 0, row: indexPath.row, amount: self.planList[indexPath.row].amount ?? 0)
                    self.dismiss(animated: true, completion: nil)
                }else if !self.isOpenFromAddGuest{
                    self.delegate?.planSelected(self.planList[indexPath.row].range ?? "", id: self.planList[indexPath.row].id ?? 0, row: indexPath.row, amount: self.planList[indexPath.row].amount ?? 0)
                    self.navigationController?.popViewController(animated: true)
                }
                
                else{
                    self.callUpdateEventApi("\(self.eventDetailObj?.id)", numberOfAttendees: self.planList[indexPath.row].range!, selectedPlan_id: self.planList[indexPath.row].id!)
                }
            }
           
    }

    func callUpdateEventApi( _ eventID : String , numberOfAttendees : String, selectedPlan_id : Int)  {
        
        let name : String = self.eventDetailObj?.name ?? ""
        let date : String = self.eventDetailObj?.event_date ?? ""
        let location : String = self.eventDetailObj?.venue ?? ""
        let language : String = self.eventDetailObj?.language ?? ""
        let to_time : String = self.eventDetailObj?.to_time ?? ""
        let from_time : String = self.eventDetailObj?.from_time ?? ""
        let msgDateTime : String = self.eventDetailObj?.message_date_time ?? ""
        let card_theme_id : Int = self.eventDetailObj?.card_theme_id ?? 0
        let msg : String = self.eventDetailObj?.message ?? ""
        let latiude : Int = self.eventDetailObj?.latitude ?? 0
        let longitude : Int = self.eventDetailObj?.longitude ?? 0
        let planObj = self.eventDetailObj?.plan
        let plan_id : Int = planObj?.id ??  0

        
        self.showActivityIndicatory()
        let isInstant : Bool = self.eventDetailObj?.instant_invitation_sent ?? false
        controller.updateEventDetails(event_id: "\(self.eventDetailObj?.id ?? 0)", Name: name, Date: date, PickLocation: location, ChooseLanguage: language, fromDate: from_time, ToTime: to_time, numberOfAttendes: numberOfAttendees, messageDateTime: msgDateTime, cardId: "\(card_theme_id)", EventCardId: "", message: msg, latitude: "\(latiude)", longitude: "\(longitude)", plan_id: "\(self.package_id)", packageID:selectedPlan_id, isInvitationSend: isInstant ) { (success, response, message, statusCode) in
            
            self.hideActivityIndicator()
            if success{
                DispatchQueue.main.async {
                    let dictData = response as? [String:Any] ?? [:]
                    let resDict = dictData
                    var cardDetailsList : EventDetails?
                    cardDetailsList = Mapper<EventDetails>().map(JSON: resDict)
                    self.eventDetailObj = cardDetailsList
                    self.createApiRequestForInitPayment(bodyParameter:
                    self.payFortModel.getBodyParameters())
                    
                    

                }
            }else{
                DispatchQueue.main.async {
                    self.showAlert(message: message, title: kAppName)
                }
            }
        }
    }

    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 131.0
    }
    
    //---------------------------------------------------
    //MARK: WebApi Methods
    //---------------------------------------------------
    func callPlanLIstAPi(_ package_id : Int)  {
         self.showActivityIndicatory()
        controller.getPlanList (param: ["plan_id":"\(self.package_id)"]){ (success, planList, message, statusCode) in
            print(success)
            print(planList)
            print(message)
            print(statusCode)
            if success{
                self.hideActivityIndicator()
                self.planList = planList as? [PlansForEvents] ?? []
                print(self.planList)
                DispatchQueue.main.async {
                    self.tblPlans.reloadData()
                }
            }else{
                self.hideActivityIndicator()
            }
        }
    }
    
    func paymentCompletedSuccessfully(){
        self.delegate?.planUpdateSuccefully()
        
    }
    
    func createApiRequestForInitPayment(bodyParameter: [String: Any]) {
               do {
                   let jsonData = try JSONSerialization.data(withJSONObject: bodyParameter, options: .prettyPrinted)
                   let request = payFortModel.request(body: jsonData, method: "POST")
                   
                   Alamofire.request(request).responseJSON(completionHandler: { (response: DataResponse<Any>) -> () in
                       print("response", response)
                       if response.error?.localizedDescription.isEmpty ?? true {
                           self.paymentApiRequest(response: response.result.value)
                           
                           return
                       }
                       
                       print("Error ", response.error?.localizedDescription ?? "")
                   })
                   
               } catch let error as NSError {
                   print("Error ",error.localizedDescription)
               }
               
           }
           
           // PayFort Request
           func paymentApiRequest(response: Any?) {
                   if (response != nil) {
                      self.view.isUserInteractionEnabled = true
                       guard let responseDict = response as? NSDictionary else { return }
                       let email = GlobalUtils.getInstance().email()
                       let tokenStr = responseDict["sdk_token"] as? String
                       let planObj = self.eventDetailObj?.guest_plans?[0] as! Guest_plans
                       
        
                       let amount = "\((planObj.amount ?? 0) * 100)"
                       self.order_id =  Int(Date().timeIntervalSince1970)
                       let payloadDict = NSMutableDictionary.init()
                       payloadDict.setValue(tokenStr, forKey: "sdk_token")
                       payloadDict.setValue(amount, forKey: "amount")
                       payloadDict.setValue("PURCHASE", forKey: "command")
                       payloadDict.setValue("SAR", forKey: "currency")
                       payloadDict.setValue(email, forKey: "customer_email")
                       payloadDict.setValue(payFortModel.language, forKey: "language")
                       payloadDict.setValue(order_id, forKey: "merchant_reference")
                       
                       var paycontroller: PayFortController?
                       
                      
                           paycontroller = PayFortController.init(enviroment: KPayFortEnviromentSandBox)
                           
           //            }else {
           //                paycontroller = PayFortController.init(enviroment: KPayFortEnviromentProduction)
           //            }
                       
                       paycontroller?.isShowResponsePage = true
                       paycontroller?.callPayFort(withRequest: payloadDict, currentViewController: self, success: { (requestDic, responeDic) in
                        
                            let event_id : Int = self.eventDetailObj?.id ?? 0
                            
                        self.controller.updateEventConfirmPayment(event_id: "\(event_id)",params:[:]) { (success, eventObj, message, statusCode) in
                                
                                if success{
                                    DispatchQueue.main.async {
                                        self.dismiss(animated: true, completion: nil)
                                        self.delegate?.planUpdateSuccefully()
                                    }
                                }else{
                                    DispatchQueue.main.async {
                                        self.showAlert(message: message, title: kAppName)
                                    }
                                }
                                
                            }
                        
                       }, canceled: { (requestDic, responeDic) in
                            self.showAlert(message: "Current payment session has been canceled.", title: kAppName)
                           
                       }, faild: { (requestDic, responeDic, message) in
                         //  self.showAlert(with: "Current payment session has been failure.")
                            self.showAlert(message: "Current payment session has been failure.", title: kAppName)
                       })
                       
                   }
               }
    
}
