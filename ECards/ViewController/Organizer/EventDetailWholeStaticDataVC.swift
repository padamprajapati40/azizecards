//
//  EventDetailWholeStaticDataVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 19/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import ObjectMapper
import PopupDialog
import Alamofire

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

class EventDetailWholeStaticDataVC: AppViewController,UITextFieldDelegate,CoupanListVCDelegate {
    var actualAmount : Double?
    var discount:Double?
    var coupanObj:Coupan?
    var total : Double?
    //MARK: Varibales..............
    var eventObj : EventDetails?
    var isInviteSelecetd = Bool()
     let controller = OrganizerViewModel()
     var eventId : Int = Int()
    var msg : String = String()
    var isOpenFromManageEvents = Bool()
    var planName : String = String()
    var plan_Amount = Int()
 
    //MARK: IBOutlets..............
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var txtDiscount: UITextField!
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var discountView: UIView!
    @IBOutlet weak var discountedView: UIView!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblMessage: PaddingLabel!
    
    @IBOutlet weak var lblToTime: UILabel!
    @IBOutlet weak var lblEventEndTime: UILabel!
    
    @IBOutlet weak var lblFromTime: UILabel!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblEventDate: UILabel!
 
    @IBOutlet weak var btnLanguage: UIButton!
    @IBOutlet weak var lblPickLocation: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnInfo: UIButton!
    
    @IBOutlet weak var lblNumberOfAttendess: UILabel!
  
    @IBOutlet weak var lblMessageTime: UILabel!
    @IBOutlet weak var btnPriceOfPackage: UIButton!
    @IBOutlet weak var lblGuestNumber: UILabel!
    @IBOutlet weak var lblPakgaeName: UILabel!
    
    @IBOutlet weak var lblInstanceMessage: UILabel!
    @IBOutlet weak var lblLanguage: UILabel!
    
    @IBOutlet weak var viewConfirmMessage : UIView!
    
    //-------------------------------------------------------
    //MARK: View Life Cycle
    //-------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtDiscount.delegate = self;
      //  self.eventId = 104
        self.scrollView.isHidden = true
        self.discountView.isHidden = false
      
        self.discountedView.isHidden = true
       // self.btnContinue.font =  Font.regularFont
        self.btnContinue.titleLabel?.font =  Font.tagViewFont
        self.btnEdit.titleLabel?.font =  Font.tagViewFont
        self.title =  AppHelper.localizedtext(key: "EventDetail") 
        if self.isOpenFromManageEvents{
            self.discountView.isHidden = true
            self.discountedView.isHidden = true
            self.btnEdit.isHidden = true
            self.btnContinue.isHidden = true
            self.btnEdit.isEnabled = false
            self.btnContinue.isEnabled = false
            self.lblInstanceMessage.isHidden = true
            self.btnInvite.isHidden = true
            self.btnInvite.isEnabled = false
            self.viewConfirmMessage.isHidden = true
        }else{
            self.btnEdit.isHidden = false
            self.btnContinue.isHidden = false
            self.btnEdit.isEnabled = true
            self.btnContinue.isEnabled = true
            self.lblInstanceMessage.isHidden = false
            self.btnInvite.isHidden = false
            self.btnInvite.isEnabled = true
        }
     //   self.btnEdit.isHidden = false
        self.setTitleView()
        //self.btnEdit.isHidden = false
        let lang = GlobalUtils.getInstance().getApplicationLanguage()
        if(lang == "arabic"){
            self.btnEdit.imageEdgeInsets = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 0)
        }
       
    }
    //---------------------------------------
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.eventObj != nil{
                               
                                 self.callEventDetailApi("\(self.eventId)")
        }
        if isOpenFromManageEvents{
                                 if self.eventId != 0 {
                                         self.callEventDetailApi("\(self.eventId)")
                                 }
        }
       
        
    }
    

    @IBAction func btnInviteAction(_ sender: Any) {
        let tickImage = UIImage(named: "Square_Blue_Tick")
        let unTickImage = UIImage(named: "square")
        if self.isInviteSelecetd{
            self.discountView.isHidden = false
            self.isInviteSelecetd = false
            self.btnInvite.setImage(unTickImage, for: .normal)
            self.btnEdit.isHidden = false
            self.btnEdit.isEnabled = true
            self.navigationItem.setHidesBackButton(false, animated: true)
            
        }else{
            self.discountView.isHidden = true
            let strMessageDate =  self.eventObj?.message_date_time ?? ""
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            if(strMessageDate != ""){
                let eventMsgDate = dateFormatter.date(from:strMessageDate)!
                if(eventMsgDate < Date()){
                    self.showAlertWithCallBack(title: kAppName, message: "Invitatation Message time already passed so please schedule invitation time again.") {
                        let editEvent = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventVC") as! CreateEventVC
                        editEvent.eventObj = self.eventObj
                        editEvent.isOpenFromEdit = true
                        self.navigationController?.pushViewController(editEvent, animated: true)
                    }
                }
            }
            self.isInviteSelecetd = true
            self.btnInvite.setImage(tickImage, for: .normal)
            self.btnEdit.isHidden = true
            self.btnEdit.isEnabled = false
            self.navigationItem.setHidesBackButton(true, animated: true)
        }
        
        
        
    }
    
    @IBAction func btnEditActions(_ sender: Any) {
        
        let editEvent = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventVC") as! CreateEventVC
        editEvent.eventObj = self.eventObj
        editEvent.isOpenFromEdit = true
        self.navigationController?.pushViewController(editEvent, animated: true)
       
    }
    
    @IBAction func btnContinueAction(_ sender: Any) {
        
        if self.isInviteSelecetd{
            let event_id : Int = self.eventObj?.id ?? 0
            self.discountView.isHidden = true
            self.eventConfirmationAction("\(event_id)")
           
        }else{
            self.showAlert(message: ValidationAlert.kEmptyCheckboxTick, title: kAppName)
        }
    }
    
    
   
    
    func setData(_ eventDetaliLits : EventDetails)  {
        
        let tickImage = UIImage(named: "squareWithTick")
        let unTickImage = UIImage(named: "square")
        
        
        let strEventDate : String = eventDetaliLits.event_date ?? ""
        let strMessageDate : String = eventDetaliLits.message_date_time ?? ""
        let strEventEndDate : String = eventDetaliLits.to_time ?? ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
         
        
        let eventDate = dateFormatter.date(from:strEventDate)
        let messageDate = dateFormatter.date(from: strMessageDate)
        dateFormatter.dateFormat = "E, dd MMM yyyy HH:mm:ss Z"
        
        
        let eventEndDate = dateFormatter.date(from: strEventEndDate)
        
        dateFormatter.dateFormat = "dd-MMM-yyyy h:mm a"
        let strEventFinalDate  : String = dateFormatter.string(from: eventDate ?? Date())
        
        dateFormatter.dateFormat = "dd-MMM-yyyy  h:mm a"
        let strEventEndDateTime : String = dateFormatter.string(from: eventEndDate ?? Date())
        
        self.lblEventDate.text = strEventFinalDate
//        self.lblMessageTime.text = strMessageFinalDate
        
         var strMessageFinalDate : String = String()
        if messageDate == nil{
            strMessageFinalDate = ""
            self.lblMessageTime.text = ""
        }else{
            
            let instant_msg : Bool = eventDetaliLits.instant_invitation_sent ?? false
            if instant_msg{
                strMessageFinalDate = ""
                self.lblMessageTime.text = ""
            }else{
                strMessageFinalDate  = dateFormatter.string(from: messageDate ?? Date())
                self.lblMessageTime.text = "\(AppHelper.localizedtext(key: "kScheduleMessage"))\(strMessageFinalDate)"
            }
        }
        self.lblEventName.text = eventDetaliLits.name
      //  self.lblMessageTime.text = "\(AppHelper.localizedtext(key: "kScheduleMessage"))\(strMessageFinalDate)"
        
      //  self.lblMessageTime.text = strMessageFinalDate
        
//        self.lblNumberOfAttendess.text = "\(eventDetaliLits.total_number_of_guests)"
        self.lblPickLocation.text = eventDetaliLits.venue
        let arrMsg = eventDetaliLits.messages ?? []
        if(arrMsg.count > 0){
            let dictData = arrMsg[0]
            let strEnglish = dictData["english_message"] as? String ?? ""
            let strArabic = dictData["arabic_message"] as? String ?? ""
            if(eventDetaliLits.language == "english"){
                self.lblMessage.attributedText = strEnglish.htmlToAttributedString ?? NSAttributedString(string: "")
            }
            else if(eventDetaliLits.language == "arabic"){
                self.lblMessage.attributedText = strArabic.htmlToAttributedString ?? NSAttributedString(string: "")
            }else{
                let msg = strEnglish + "\n\n" + strArabic
                self.lblMessage.attributedText = msg.htmlToAttributedString ?? NSAttributedString(string: "")
            }
        }else{
            self.lblMessage.attributedText = eventDetaliLits.message?.htmlToAttributedString ?? NSAttributedString(string: "")
        }
        
      
        var language : String = eventDetaliLits.language as? String ?? ""
        
        switch language {
        case "both":
            language = AppHelper.localizedtext(key: "kBoth")
            break
        case "english":
            language = AppHelper.localizedtext(key: "kEnglish")
            break
        case "arabic":
            language = AppHelper.localizedtext(key: "kArabic")
            break
        default:
            break
        }
        
        self.lblLanguage.text = language
        self.lblEventEndTime.text = strEventEndDateTime
        
//        self.cardId = eventDetaliLits.card_theme_id ?? 0
        
      //  self.lblNumberOfAttendess.text = "201-500"
//        self.lblToTime.text = "3:33PM"
//        self.lblFromTime.text = "11:33PM"
      //  self.lblMessageTime.text = strMessageFinalDate
        
//        let str_language : String = LocalizationSystem.sharedInstance.getLanguage()
//
//        if str_language == "en"{
//             dateFormatter.dateFormat = "h:mm a"
//        }else{
//             dateFormatter.dateFormat = "a h:mm"
//        }
        
        let to_time : String = eventDetaliLits.to_time ?? ""
        let from_time : String = eventDetaliLits.from_time ?? ""
       
        dateFormatter.dateFormat = "h:mm a"
        let to_Date = dateFormatter.date(from: to_time)
        let from_Date = dateFormatter.date(from: from_time)
        if(to_Date != nil){
            self.lblToTime.text = dateFormatter.string(from:to_Date! )
        }
        if(from_Date != nil){
            self.lblFromTime.text = dateFormatter.string(from:from_Date! )
        }
        
        
        let planObj = eventDetaliLits.plan
        let plan_name : String = planObj?.name ?? ""
        let plan_price : Int = planObj?.price ?? 0
        let plan_description : String = planObj?.description ?? ""
        let arrGuest = eventDetaliLits.guest_plans as? [Guest_plans] ?? []
        
    
        if arrGuest.count != 0 {
            let guest_Plan_Obj = arrGuest[0]
                let guest_name : String = guest_Plan_Obj.name ?? ""
            self.planName = guest_name
              //  self.lblNumberOfAttendess.text = guest_name
              //  self.btnPriceOfPackage.setTitle("\(plan_price)SR", for: .normal)
                self.lblPakgaeName.text = plan_name
                let minimum_attendees : Int = guest_Plan_Obj.minimum_attendees ?? 0
                let maximum_attendees : Int = guest_Plan_Obj.maximum_attendees ?? 0
                let amount : Int = guest_Plan_Obj.amount ?? 0
                self.plan_Amount = amount
               if(self.plan_Amount == 0){
                  self.discountView.isHidden  = true
                  self.discountedView.isHidden = true
               }else{
                    if(self.coupanObj == nil){
                        self.discountedView.isHidden = true
                        self.discountView.isHidden  = false
                    }else{
                        self.discountedView.isHidden = false
                        self.discountView.isHidden  = true
                    }
                   
                }

//               
//            //   if range != ""{
               var strStaticGuest = String()

            strStaticGuest = AppHelper.localizedtext(key: "kGuests")
            
            self.lblNumberOfAttendess.text = "\(minimum_attendees) - \(maximum_attendees) \(strStaticGuest)"
            self.lblAmount.text = "\(amount) SR";
            self.msg = plan_description
            let guest_plan_amount : Int = guest_Plan_Obj.amount ?? 0
            self.btnPriceOfPackage.setTitle("\(guest_plan_amount)SR", for: .normal)
            if(self.coupanObj != nil){
                if(self.plan_Amount != 0){
                     self.applyDiscountCode(coupanID: self.coupanObj?.id ?? 0)
                }
               
            }
            
        }
        
//        if eventDetaliLits.instant_invitation_sent ?? false {
//
//            self.btnInvite.setImage(tickImage, for: .normal)
//            
//        }else{
//            self.btnInvite.setImage(unTickImage, for: .normal)
//        }
    
        
//        if planObj?.id == 1{
//            self.lblInstanceMessage.isHidden = true
//            self.btnInvite.isHidden = true
//        }else{
//            self.lblInstanceMessage.isHidden = false
//            self.btnInvite.isHidden = false
//        }
        self.scrollView.isHidden = false
        
        if(self.isOpenFromManageEvents == true){
            let arrPayments = self.eventObj?.payments ?? []
            if(arrPayments.count>0){
                let dictData = arrPayments[0]
                self.discountedView.isHidden = false
                self.discountView.isHidden = true
                self.lblSubTotal.text = "\(dictData["actual_amount"] as? String ?? "") SR"
                self.lblDiscount.text = "\(dictData["discount"] as? String ?? "") SR"
                let amount = Int(dictData["amount"] as? String ?? "") ?? 0
                let total = amount / 100
                self.lblTotalAmount.text = "\(total) SR"
            }else{
                self.discountedView.isHidden = true
                self.discountView.isHidden = true
            }
        }
        if(GlobalUtils.getInstance().getApplicationLanguage() == "arabic"){
            self.txtDiscount.textAlignment = .right
            self.lblNumberOfAttendess.textAlignment = .right
        }else{
            self.txtDiscount.textAlignment = .left
            self.lblNumberOfAttendess.textAlignment = .left
        }
       
    }
    
    func  callEventDetailApi(_ eventId : String)  {
        
        controller.getEventDetails(cardId: eventId) { (success, eventDetailsList, message, statusCode) in
            if success{
                DispatchQueue.main.async {
                    
                    print(eventDetailsList)
                    print()
                    
                    let eventDetailObj = eventDetailsList as? EventDetails
                    self.eventObj = eventDetailObj
                    self.setData(eventDetailObj!)
                }
            }else{
                DispatchQueue.main.async {
                    self.showAlert(message: message, title: kAppName)
                }
            }
        }
    }
    
    
//     func popupAndsetMessageOninfoClick(title : String, Message : String)  {
//        
//        let popUp = PopupDialog(title: title, message: Message)
//        let btnOk = CancelButton(title: AppHelper.localizedtext(key: "OK")) {
//            print("You okied the car dialog.")
//        }
//        popUp.addButtons([btnOk])
//        
//        // Present dialog
//        self.present(popUp, animated: true, completion: nil)
//    }
    @IBAction func btnInfoAction(_ sender: Any) {
        let message: String = self.msg
        self.popupAndsetMessageOninfoClick(title: kAppName, Message:message)
    }
    
    func eventConfirmationAction(_ event_id : String)  {

        self.showActivityIndicatory()
        self.discountView.isHidden = true
        if self.isOpenFromManageEvents{

        }else{
            controller.event_confirmation(event_id: event_id) { (success, response, message, statusCode) in
                   if success{
                       DispatchQueue.main.async {
                           self.hideActivityIndicator()
                               if self.plan_Amount == 0{
                                    self.completePayment(params: [:]  )
//                                   self.scrollView.isUserInteractionEnabled = true
//                                   let successVC = self.storyboard?.instantiateViewController(withIdentifier: "EventSuccessVC") as! EventSuccessVC
//                                successVC.eventObj = self.eventObj
//                                   self.navigationController?.pushViewController(successVC, animated: true)
                                }else{
                                    self.createApiRequestForInitPayment(bodyParameter:
                                    self.payFortModel.getBodyParameters())
                                }
                          
                       }
                   }else{
                       DispatchQueue.main.async {
                           self.hideActivityIndicator()
                           if self.plan_Amount == 0{
                             
                           }else{
                               self.createApiRequestForInitPayment(bodyParameter:
                               self.payFortModel.getBodyParameters())
                           }
                        
                       }
                   }
               }
        }
    }
    
    //----------------------------------------------------------
    func createApiRequestForInitPayment(bodyParameter: [String: Any]) {
        self.scrollView.isUserInteractionEnabled = false
               do {
                   let jsonData = try JSONSerialization.data(withJSONObject: bodyParameter, options: .prettyPrinted)
                   let request = payFortModel.request(body: jsonData, method: "POST")
                   self.view.isUserInteractionEnabled = false
                   Alamofire.request(request).responseJSON(completionHandler: { (response: DataResponse<Any>) -> () in
                       print("response", response)
                       if response.error?.localizedDescription.isEmpty ?? true {
                           self.paymentApiRequest(response: response.result.value)
                           
                           return
                       }
                       
                       print("Error ", response.error?.localizedDescription ?? "")
                   })
                   
               } catch let error as NSError {
                 self.scrollView.isUserInteractionEnabled = true
                   print("Error ",error.localizedDescription)
               }
               
           }
           
           // PayFort Request
           func paymentApiRequest(response: Any?) {
               if (response != nil) {
                   guard let responseDict = response as? NSDictionary else { return }
                   let email = GlobalUtils.getInstance().email()
                   let tokenStr = responseDict["sdk_token"] as? String
                   let amount = "\(self.plan_Amount * 100)"
                
                   self.order_id =  Int(Date().timeIntervalSince1970)
                   let payloadDict = NSMutableDictionary.init()
                   payloadDict.setValue(tokenStr, forKey: "sdk_token")
                   payloadDict.setValue(amount, forKey: "amount")
                   payloadDict.setValue("PURCHASE", forKey: "command")
                   payloadDict.setValue("SAR", forKey: "currency")
                   payloadDict.setValue(email, forKey: "customer_email")
                   payloadDict.setValue(payFortModel.language, forKey: "language")
                   payloadDict.setValue(order_id, forKey: "merchant_reference")
                   
                   var paycontroller: PayFortController?
                   paycontroller = PayFortController.init(enviroment: KPayFortEnviromentSandBox)
                       
       //            }else {
       //                paycontroller = PayFortController.init(enviroment: KPayFortEnviromentProduction)
       //            }
                   
                   paycontroller?.isShowResponsePage = true
                   paycontroller?.callPayFort(withRequest: payloadDict, currentViewController: self, success: { (requestDic, responeDic) in
                        
                          self.completePayment(params: responeDic as? [String : Any] ?? [:]  )
                          let event_id : Int = self.eventObj?.id ?? 0
                    


                       
                   }, canceled: { (requestDic, responeDic) in
                     //  self.showAlert(with: "Current payment session has been canceled.")
                    self.showAlert(message: "Current payment session has been canceled.", title: kAppName)
                       self.scrollView.isUserInteractionEnabled = true
                   }, faild: { (requestDic, responeDic, message) in
                     //  self.showAlert(with: "Current payment session has been failure.")
                    self.scrollView.isUserInteractionEnabled = true
                    self.showAlert(message: "Current payment session has been failure.", title: kAppName)
                   })
                   
               }
           }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        if(textField == self.txtDiscount){
            let coupanListVC = self.storyboard?.instantiateViewController(withIdentifier: "CoupanListVC") as! CoupanListVC
            coupanListVC.coupanObj = self.coupanObj
            coupanListVC.delegate = self;
            coupanListVC.eventObj = self.eventObj
            self.navigationController?.pushViewController(coupanListVC, animated: true)
            
            return false;
        }
        return true
    }
  
    func coupanAppliedData(_ coupanDict: [String:Any],message:String){
        self.discountView.isHidden = false
        self.discountedView.isHidden = false
        let subAmount = coupanDict["actual_amount"] as? Int ?? 0
        let discount = coupanDict["discount"] as? Double ?? 0.0
        let total = Int(coupanDict["amount_to_be_paid"] as? Double ?? 0.0)
        self.discount = discount;
        self.actualAmount = coupanDict["amount_to_be_paid"] as? Double ?? 0.0;
        self.plan_Amount = total;
        self.lblSubTotal.text = "\(subAmount) SR"
        self.lblDiscount.text = "\(discount) SR"
        self.lblTotalAmount.text = "\(total) SR"
        //self.showAlert(message: message, title: kAppName)
    }
    
    func applyDiscountCode(coupanID:Int)  {
         self.showActivityIndicatory()
        let guestPlanObj = self.eventObj?.guest_plans?[0].id ?? 0
        self.discountedView.isHidden = true
        let params = ["coupon_id":coupanID,"guest_plan_id":guestPlanObj]
        controller.applyDiscountCodes(params:params){ (success, dictRes, message, statusCode) in
            if success{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    self.discountedView.isHidden = false
                    let dictPlan = dictRes as! [String:Any]
                    print(dictPlan)
                    self.coupanAppliedData(dictPlan,message: message)

                }
            }else{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    self.discountedView.isHidden = true
                    self.discountView.isHidden = false
                    self.showAlert(message: message, title: kAppName)
                }
                
            }
        }
    }
    
    func coupanAppliedData(_ coupanObj: Coupan) {
        self.coupanObj = coupanObj;
        //self.callEventDetailApi("\(self.eventObj?.id ?? 0)")
    }
    
    func sendPaymentDetailBackend(){
        let event_id = self.eventObj?.id ?? 0
        
        self.showActivityIndicatory()
        var params : [String:Any] = [:]
            if(self.coupanObj != nil){
                params["coupon_id"] = self.coupanObj?.id ?? 0
        }
        
        controller.updateEventConfirmPayment(event_id: "\(event_id)",params:params) { (success, eventObj, message, statusCode) in
            
            if success{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    let eventObjFinal = eventObj as? EventDetails
                    self.scrollView.isUserInteractionEnabled = true
                    let successVC = self.storyboard?.instantiateViewController(withIdentifier: "EventSuccessVC") as! EventSuccessVC
                    successVC.eventObj = eventObjFinal
                    self.navigationController?.pushViewController(successVC, animated: true)
                   
                }
            }else{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    self.showAlert(message: message, title: kAppName)
                }
            }
            
        }
    }
    
    func completePayment(params:[String:Any]){
        let event_id = self.eventObj?.id ?? 0
        let userId = GlobalUtils.getInstance().userID()
        self.showActivityIndicatory()
        var params : [String:Any] = params
            if(self.coupanObj != nil){
                params["coupon_id"] = self.coupanObj?.id ?? 0
        }
        let guestPlanObj = self.eventObj?.guest_plans?[0].id ?? 0
        let amount = self.eventObj?.guest_plans?[0].amount ?? 0
        params["event_id"] = event_id;
        params["organizer_id"] = userId;
        params["discount"] = self.discount ?? 0.0;
        params["actual_amount"] = amount
        params["guest_plan_id"] = guestPlanObj;
        var dictParam : [String:Any] = ["payment":params];
        
        controller.updateEventConfirmPayment(event_id: "\(event_id)",params:dictParam) { (success, eventObj, message, statusCode) in
            
            if success{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    let eventObjFinal = eventObj as? EventDetails
                    self.scrollView.isUserInteractionEnabled = true
                    let successVC = self.storyboard?.instantiateViewController(withIdentifier: "EventSuccessVC") as! EventSuccessVC
                    successVC.eventObj = eventObjFinal
                    self.navigationController?.pushViewController(successVC, animated: true)
                   
                }
            }else{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    self.showAlert(message: message, title: kAppName)
                }
            }
            
        }
    }
    
}
