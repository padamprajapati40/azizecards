//
//  EventDeatilsVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 02/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import ObjectMapper

class EventDeatilsVC: AppViewController, UICollectionViewDataSource, UICollectionViewDelegate{

    //MARK: Variables.........................
    var eventId : Int = Int()
    var cardDetailsList : CardDetails?;
     let controller = OrganizerViewModel()
    
    //MARK: IBOutlets.........................
    
    @IBOutlet weak var collectionMoreOptions: UICollectionView!
    @IBOutlet weak var collectionTheme: UICollectionView!
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var pgControl: UIPageControl!
    @IBOutlet weak var btnFavourite: UIButton!
    
    @IBOutlet weak var btnSelectCardForEvent: UIButton!
    
    //---------------------------------------------------
    //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        
      print(eventId)
        self.callTempletDetailApi()
       self.setTitleView()
    }
    

    @IBAction func pgControlCalled(_ sender: Any) {
    }
    
    //---------------------------------------------------
    //MARK: UIButton Actions
    //---------------------------------------------------
    
    @IBAction func btnFavouriteAction(_ sender: Any) {
    }
    
    @IBAction func btnSelectCardForEventAction(_ sender: Any) {
        
        let event = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventVC") as! CreateEventVC
        self.navigationController?.pushViewController(event, animated: true)
    }
    
    //---------------------------------------------------
    //MARK: WebApi Methods
    //---------------------------------------------------
    func callTempletDetailApi()  {
        controller.getEventDetails(cardId: "\(self.eventId)") { (success, cardDetailsList, message, statusCode) in
            DispatchQueue.main.async {
                if success{
                    self.cardDetailsList = cardDetailsList as? CardDetails 
                    print(self.cardDetailsList)
                    self.lblTitle.text = self.cardDetailsList?.title  ?? ""
                    self.lblDescription.text = self.cardDetailsList?.body?.withoutHtml  ?? ""
                    self.collectionTheme.reloadData()
                    self.collectionMoreOptions.reloadData()
                }
            }
            
           print(success)
            print(cardDetailsList)
            print(message)
            print(statusCode)
        }
    }
    
    //---------------------------------------------------
    //MARK: UICollectionview Datasource
    //---------------------------------------------------
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        if collectionView.tag == 1001{
            return 1
        }else {
            
           return self.cardDetailsList?.images?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EventCollectionCell", for: indexPath)as! EventCollectionCell
    
        let imageList = self.cardDetailsList?.images as? [CardDetailsImages] ?? []
        let imgObj = imageList.first as? CardDetailsImages
        
        if imageList.count <= 0{
            
        }else{
            if collectionView.tag == 1001{
                let url = URL(string: imgObj?.url  ?? "")!
                cell.imgEventType.setImageFromURL(url: url);
                
            }
            
            if collectionView.tag == 2001{
                let imgUrl = imageList[indexPath.row].url ?? ""
                let url = URL(string: imgUrl)!
                cell.imgEventType.setImageFromURL(url: url)
                //cell.lblEventTypeName.text = ""
                
            }
        }
       
       
        return cell
    }
    
    
    //---------------------------------------------------
    //MARK: UIcollectionview Delegate
    //---------------------------------------------------
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       // let event = self.storyboard?.instantiateViewController(withIdentifier: "SpecificEventVC") as! SpecificEventVC
        //self.navigationController?.pushViewController(event, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 1001{
            return CGSize(width:(collectionView.frame.size.width),height: (collectionView.frame.size.height))
        }else{
            return CGSize(width:(collectionView.frame.size.width-10)/3,height: (collectionView.frame.size.width-10)/3)
        }
        
        
    }

}
