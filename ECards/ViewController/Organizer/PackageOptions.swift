//
//  PackageOptions.swift
//  ECards
//
//  Created by Dhruva Madhani on 03/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

class PackageOptions: AppViewController {
    
    
    @IBOutlet weak var btnDifferenceBetweenPackages: UIButton!
    @IBOutlet weak var btnCreateEvent: UIButton!
    @IBOutlet weak var txtGuardName: BWTextField!
    
    @IBOutlet weak var txtGuardPhoneNumber: BWTextField!
    @IBOutlet weak var txtGuardPhoneCode: BWTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitleView()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnCreateEventAction(_ sender: Any) {
    }
    
   
    @IBAction func btnDifferenceBetweenPackagesActions(_ sender: Any) {
    }
    
}
