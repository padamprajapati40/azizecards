//
//  CustomizeCardAddVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 31/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import Kingfisher
import AVKit
import AVFoundation
import PopupDialog


extension UIStackView {
    func addBackgroundColor(color: UIColor) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
}

class CustomizeCardAddVC: AppViewController , UITableViewDataSource, UITableViewDelegate, CustomizeMessageDelegate,UITextFieldDelegate{
    let demoUrl = "http://qrinvitationcards.com/sample_customize_card"
    var selectedTag = Int()
    var color = String()
    var msg = String()
    var addCounter = Int()
    var eventId : Int = Int()
    var cardId : Int = Int()
    var eventName = String()
    var arrCard :[[String:Any]] = []
     var EventDetailsList : EventDetails?
    let controller = OrganizerViewModel()
   var btnAdd = UIButton()
    var btnBold = UIButton()
    var btnItalic = UIButton()
    var btnFont = UIButton()
    var bgImgUrl = String()
    var txtMessage = UITextField()
    var isFromEdit = Bool()
     var isBoldSelected = Bool()
     var isItalicSelected = Bool()
     var lblAdd = AppHelper.localizedtext(key: "Add")
    var fontSize : CGFloat?
    @IBOutlet weak var tblCard: UITableView!
    @IBOutlet weak var collectionTxt: UICollectionView!
    @IBOutlet weak var bgImageView: UIImageView!
     @IBOutlet weak var btnSampleDemoCardAction : UIButton!
    @IBOutlet weak var lblEventName : UILabel!
    @IBOutlet weak var btnContinue : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addCounter = 0
        self.txtMessage.borderWidth = 1.0
        print(self.EventDetailsList)
        self.title = AppHelper.localizedtext(key: "CustomizeCard") 
       //  self.btnContinue.font =  Font.regularFont
        self.btnContinue.titleLabel?.font = Font.tagViewFont
        self.color = "#000000"
        let url = URL(string: self.bgImgUrl  ?? "")!
        self.bgImageView.setImageFromURL(url: url);

        let done = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(btnDoneAction(_ :)))
       // self.navigationItem.rightBarButtonItem = done
        self.bgImageView.contentMode = .scaleToFill
        
        self.tblCard.isEditing = true
        
        self.txtMessage.delegate = self
        
        let name : String = self.EventDetailsList?.name ?? ""
        self.lblEventName.text = name
        let rightBtn = UIBarButtonItem(title:"Add New", style: .plain, target: self, action: #selector(onClickMethod))//Change your function name and image name here
      //  self.navigationItem.rightBarButtonItem = rightBtn
        self.txtMessage.borderColor = UIColor(named: "lightGray")
        self.txtMessage.borderWidth = 1.0
        self.setTitleView()
        
        
        //self.toolbarItemTintColor = UIColor.red
    }
    
    @objc func onClickMethod() {
//        let addCardVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewCardVC")as! AddNewCardVC
//        self.navigationController?.pushViewController(addCardVC, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         lblAdd = AppHelper.localizedtext(key: "Add")
        self.tblCard.reloadData()
    }
    
    
    
    //-------------------------------------------------------------
    //MARK: UItableview datasource
    //-------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if self.arrCard.count == 0{
//            return 1
//        }
        return self.arrCard.count
    }
    //-------------------------------------------------------------
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomizeCardTableCell", for: indexPath) as! CustomizeCardTableCell
        cell.delegate = self
        cell.txtMessage.delegate = self
        cell.btnEdit.tag = indexPath.row
        cell.btnDelete.tag = indexPath.row
        
        cell.txtMessage.font = UIFont(name: "Avenir-Book", size: self.fontSize ?? 18.0)!
        
        let dicCard : [String:Any] = self.arrCard[indexPath.row]
        let details = dicCard["text"] as? String ?? ""
        let fontchangeStr : String = String(format:"<span style=\"font-family: '-apple-system', 'Avenir-Book'; font-size: 18.0\">%@</span>", details)
        
        let htmlData = NSString(string: fontchangeStr).data(using: String.Encoding.unicode.rawValue)
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType:
                NSAttributedString.DocumentType.html]
        let attributedString = try? NSMutableAttributedString(data: htmlData ?? Data(),
                                                                  options: options,
                                                                  documentAttributes: nil)
        cell.txtMessage.attributedText = attributedString
        let color : String = dicCard["color"] as? String ?? ""
        cell.txtMessage.textColor = hexStringToUIColor(hex: color)
        cell.txtMessage.tag = indexPath.row
      
        cell.txtMessage.isEnabled = false
       
        //self.msg = cell.txtMessage.text ?? ""
        return cell
    }
    //-------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    //-------------------------------------------------------------
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    //-------------------------------------------------------------
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
            let movedObject = self.arrCard[sourceIndexPath.row]
               self.arrCard.remove(at: sourceIndexPath.row)
               self.arrCard.insert(movedObject, at: destinationIndexPath.row)
        self.tblCard.reloadData()
    }
    
    //-------------------------------------------------------------
    @IBAction func btnContinueAction(_ sender: Any) {
      
            if self.arrCard.count == 0{

                self.showAlert(message:AppHelper.localizedtext(key: "kEmptyMessage"), title: kAppName)
            }else{
                
                if GlobalUtils.getInstance().sessionToken() == ""{
                    let welcomeVC = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeVC")as! WelcomeVC
                    welcomeVC.eventObj = self.EventDetailsList;
                    let navVC = UINavigationController(rootViewController:welcomeVC )
                    navVC.modalPresentationStyle = .fullScreen
                    self.present(navVC, animated: true, completion: nil);
                    return;
                }
                else{
                    self.showActivityIndicatory()
                    controller.addMsgAdColorToEventCards(arrCard : arrCard, Card_theme_id: "\(self.cardId)", EventId: "\(self.eventId)") { (success, eventCardList, message, statusCode) in

                        if success{
                            self.hideActivityIndicator()
                            print(success)
                            print(eventCardList)
                            print(message)
                            print(statusCode)

                            let eventCardObj : EventCards = eventCardList as! EventCards

                            let url : String = eventCardObj.preview_url ?? ""
                            
                            DispatchQueue.main.async {
                            
                                    self.showAlertWithCallBack(title: kAppName, message: message, callBack: {
                                        
                                        let webVc = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
                                              //webVc.isSampleExcel = false
                                        webVc.isPreviewUrl = true
                                        webVc.strUrl = url
                                        webVc.eventId = self.eventId
                                        webVc.EventDetailsList = self.EventDetailsList
                                        self.navigationController?.pushViewController(webVc, animated: true)
                                        
//                                        let addGuests = self.storyboard?.instantiateViewController(withIdentifier: "AddGuestsVC")as! AddGuestsVC
//                                        addGuests.isFromCreateEvent = true
//                                        addGuests.eventId  = self.eventId
//                                        addGuests.eventObj = self.EventDetailsList
//                                        //            addGuests.eventName = self.eventName
//                                        self.navigationController?.pushViewController(addGuests, animated: true)
                                    })
                            
                            }


                        }else{
                            DispatchQueue.main.async {
                                self.hideActivityIndicator()
                                self.showAlert(message: message, title: kAppName)
                            }
                        }

                    }
                }
                
                

            }
        }
    //-------------------------------------------------------------
    //MARK: UITableview Delegate
    //-------------------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
      //-------------------------------------------------------------
  
    //-------------------------------------------------------------
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.msg = textField.text as? String ?? ""
        
       

        
        
        
        return true
    }
    
//    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
//        if self.arrCard.count >= 10{
//            self.showAlert(message: ValidationAlert.kMaxLimitOfCard, title: kAppName)
//        }else{
//            if self.txtMessage.text == "" {
//                self.showAlert(message: ValidationAlert.kEmptyMessage, title: kAppName)
//            }else{
//                if isFromEdit{
//                    var  dicText : [String:Any] = ["text" : self.txtMessage.text!, "color" : self.color]
//                    self.arrCard[self.selectedTag] = dicText
//                    self.color = "#000000"
//                    self.txtMessage.resignFirstResponder()
//                    self.tblCard.reloadData()
//                }else{
//                    var  dicText : [String:Any] = ["text" : self.txtMessage.text!, "color" : self.color]
//                    self.arrCard.append(dicText)
//                    self.color = "#000000"
//                    self.txtMessage.resignFirstResponder()
//                    self.tblCard.reloadData()
//                }
//            }
//        }
//    }
    
    
    
    
      //-------------------------------------------------------------
//    func textField(_ textField: UITextField,
//    shouldChangeCharactersIn range: NSRange,
//    replacementString string: String) -> Bool{
//        
//          let currentText = textField.text ?? ""
//
//        // attempt to read the range they are trying to change, or exit if we can't
//        guard let stringRange = Range(range, in: currentText) else { return false }
//
//        // add their new text to the existing text
//        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
//
//        // make sure the result is under 16 characters
//        return updatedText.count <= 144
//    }
      //-------------------------------------------------------------
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView =  UIView(frame: CGRect(x: 0, y: 30, width: self.tblCard.frame.width, height: 85))
        headerView.backgroundColor = UIColor.white
        headerView.layer.cornerRadius = 5.0

        self.txtMessage = UITextField(frame: CGRect(x: 5, y: 10, width: self.tblCard.frame.size.width - 70, height: 40))
        self.txtMessage.borderStyle = .roundedRect
        self.txtMessage.delegate = self
        self.txtMessage.autocapitalizationType = .none
        self.txtMessage.font = UIFont(name: "Avenir-Book", size: self.fontSize ?? 18.0)!
        self.txtMessage.placeholder = AppHelper.localizedtext(key: "kPlaceHolder")
        self.txtMessage.borderColor = UIColor(named:"lightGray");
        self.txtMessage.cornerRadius = 5.0
        self.txtMessage.borderWidth = 1.0
        self.txtMessage.autocapitalizationType = .sentences
        let stackContainer = UIStackView(frame: CGRect(x: 5, y: 60, width: self.tblCard.frame.size.width-10, height: 40))
        let btnColor = UIButton(frame: CGRect(x: self.btnBold.frame.size.width + 10, y: 50, width: 50, height: 30))
        btnColor.setImage(UIImage(named: "Color"), for: .normal)
        stackContainer.addArrangedSubview(btnColor)
        stackContainer.clipsToBounds = true
        stackContainer.distribution = .fillEqually
        stackContainer.layer.cornerRadius = 10.0;
        stackContainer.cornerRadius = 10;
        self.btnAdd = UIButton(frame: CGRect(x: self.txtMessage.frame.size.width + 10, y: 15, width: 50, height: 30))
       // btnAdd.setImage(UIImage(named: "Plus_WithBackground"), for: .normal)

        self.btnAdd.setTitle(lblAdd,for: .normal)
        
        self.btnBold = UIButton(frame: CGRect(x: self.txtMessage.frame.size.width/2 , y: 50, width: 50, height: 30))
        self.btnBold.setImage(UIImage(named: "Bold"), for: .normal)
//        self.btnBold.backgroundColor = .white
        stackContainer.addArrangedSubview(btnBold)
        stackContainer.spacing = 10;
        self.btnItalic = UIButton(frame: CGRect(x: self.btnBold.frame.size.width + 90, y: 50, width: 50, height: 30))
        self.btnItalic.setImage(UIImage(named: "Italic"), for: .normal)
        self.btnFont = UIButton(frame: CGRect(x: self.btnBold.frame.size.width + 90, y: 50, width: 50, height: 30))
        self.btnFont.setImage(UIImage(named: "Font"), for: .normal)
        
        //       self.btnItalic.backgroundColor = .white
        stackContainer.addArrangedSubview(btnItalic)
      //  stackContainer.addArrangedSubview(btnFont)
        self.isFromEdit = false
       // self.btnAdd.backgroundColor = .black
        self.btnAdd.titleLabel?.font = UIFont(name: "Zeit-Light", size: 13.0)
        self.btnAdd.setTitleColor(.black, for: .normal)

        let primaryColor = UIColor(named: "SecondrayColor")?.cgColor

        self.btnAdd.layer.borderColor = primaryColor
        self.btnAdd.layer.borderWidth = 1.0
        self.btnAdd.layer.cornerRadius = 5.0

        btnColor.addTarget(self, action: #selector(chooseColorFromView), for: .touchUpInside)
        btnAdd.addTarget(self, action: #selector(addMessageAndColorFromView), for: .touchUpInside)
        btnBold.addTarget(self, action: #selector(btnBoldAction(_:)), for: .touchUpInside)
        btnItalic.addTarget(self, action: #selector(btnItalicAction(_:)), for: .touchUpInside)
        btnFont.addTarget(self, action: #selector(btnFontAction(_:)), for: .touchUpInside)
        headerView.addSubview(txtMessage)
        headerView.addSubview(stackContainer)
        headerView.addSubview(btnAdd)
        stackContainer.addBackground(color:UIColor(named:"PrimaryColor")!)
        stackContainer.centerXAnchor.constraint(equalTo: headerView.centerXAnchor).isActive = true
        stackContainer.centerYAnchor.constraint(equalTo: headerView .centerYAnchor).isActive = true
//        stackContainer.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
//        stackContainer.centerXAnchor.constraint(equalTo: headerView.centerXAnchor).isActive = true
//        headerView.addSubview(btnBold)
//        headerView.addSubview(btnItalic)
//        self.tblCard.addSubview(headerView)

        return headerView
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100.0
    }
    
   
    //-------------------------------------------------------------
    //MARK: Navigation Methods
    //-------------------------------------------------------------
    @IBAction func btnAddAction(_ sender: AnyObject) {
        
//        if self.addCounter < 10{
//            let addGuests = self.storyboard?.instantiateViewController(withIdentifier: "CustomizeCardVC")as! CustomizeCardVC
//
//
//            self.addCounter = self.addCounter + 1
//                //        addGuests.eventId  = id
//                //        addGuests.cardId = self.cardId
//                //        addGuests.eventName = self.eventName
//                //        addGuests.msg = self.lblMessage.text!
//                addGuests.EventDetailsList = EventDetailsList
//            addGuests.delegate = self
//            self.navigationController?.pushViewController(addGuests, animated: true)
//        }else{
//            self.showAlert(message: "You have reached to maximum limit.", title: kAppName)
//        }
      
    }
//-------------------------------------------------------------
    @objc func btnDoneAction(_ sender: Any) {
  
        if self.arrCard.count == 0{

        }else{
            
//            let addGuests = self.storyboard?.instantiateViewController(withIdentifier: "AddGuestsVC")as! AddGuestsVC
//            addGuests.eventId  = self.eventId
//            addGuests.eventObj = self.EventDetailsList
//            //            addGuests.eventName = self.eventName
//            self.navigationController?.pushViewController(addGuests, animated: true)
            
            self.showActivityIndicatory()
            controller.addMsgAdColorToEventCards(arrCard : arrCard, Card_theme_id: "\(self.cardId)", EventId: "\(self.eventId)") { (success, eventCardList, message, statusCode) in

                if success{
                    self.hideActivityIndicator()
                    print(success)
                    print(eventCardList)
                    print(message)
                    print(statusCode)
                    let eventCardObj : EventCards = eventCardList as! EventCards 

                    let url : String = eventCardObj.preview_url ?? ""
                    
                    DispatchQueue.main.async {
                        if GlobalUtils.getInstance().userLoggedIn(){

                            self.showAlertWithCallBack(title: kAppName, message: message, callBack: {
                                
                                let webVc = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
                                      //webVc.isSampleExcel = false
                                webVc.isPreviewUrl = true
                                webVc.strUrl = url
                                      self.navigationController?.pushViewController(webVc, animated: true)
                                
//                                let addGuests = self.storyboard?.instantiateViewController(withIdentifier: "AddGuestsVC")as! AddGuestsVC
//                                addGuests.eventId  = self.eventId
//                                addGuests.eventObj = self.EventDetailsList
//                                //            addGuests.eventName = self.eventName
//                                self.navigationController?.pushViewController(addGuests, animated: true)
                            })
                        }else{
                            let welcomeVC = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeVC")as! WelcomeVC
                            welcomeVC.eventObj = self.EventDetailsList;
                            let navVC = UINavigationController(rootViewController:welcomeVC )
                            self.present(navVC, animated: true, completion: nil);

                        }

                    }


                }else{
                    DispatchQueue.main.async {
                        self.hideActivityIndicator()
                        self.showAlert(message: message, title: kAppName)
                    }
                }

            }

        }
    }
    //-------------------------------------------------------------
    func setTextAndColor(_ dicMessage: [String : Any]) {
        self.arrCard.append(dicMessage)
        self.collectionTxt.reloadData()
    }
    
    //-------------------------------------------------------------
    //MARK : Custom Delegate method of table cell
    //-------------------------------------------------------------
    func addMessageAndColor(_ tag: Int) {
        print(tag)
        if self.arrCard.count >= 9{
            self.txtMessage.text = ""
            self.showAlert(message: ValidationAlert.kMaxLimitOfCard, title: kAppName)
//            self.showAlert(message: "You have reached to maximum limit.", title: kAppName)
        }else{
            if self.txtMessage.text == "" {
                self.showAlert(message: ValidationAlert.kEmptyMessage, title: kAppName)
            }else{
                if isFromEdit{
                    var  dicText : [String:Any] = ["text" : self.txtMessage.text!, "color" : self.color]
                    self.arrCard[self.selectedTag] = dicText
                    self.txtMessage.text = ""
                    self.tblCard.reloadData()
                }else{
                    var  dicText : [String:Any] = ["text" : self.msg, "color" : self.color]
                    self.arrCard.append(dicText)
                    self.tblCard.reloadData()
                }
               
            }
        }
        
        
    }
    //-------------------------------------------------------------
    func chooseColor(_ tag: Int) {
        print(tag)
        let alert = UIAlertController(style: .actionSheet)
        alert.addColorPicker(color: UIColor.blue) { color in
            // action with selected color
            
            //self.txtView.textColor = color
            self.color = color.hexString
            print(self.color)
        }
        alert.addAction(title: AppHelper.localizedtext(key: "Done"), style: .cancel)
        alert.show()
    }
     //-------------------------------------------------------------
    func EditMessage(_ tag: Int) {
        print(tag)
        let dicCard : [String:Any] = self.arrCard[tag] as? [String:Any] ?? [:]
        let msg : String = dicCard["text"] as? String ?? ""
        if msg.contains("<b>") {
            self.isBoldSelected = true
        }
        if msg.contains("<i>") {
                   self.isItalicSelected = true
               }
        let htmlData = NSString(string: msg).data(using: String.Encoding.unicode.rawValue)
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType:
                NSAttributedString.DocumentType.html]
        let attributedString = try? NSMutableAttributedString(data: htmlData ?? Data(),
                                                                  options: options,
                                                                  documentAttributes: nil)
        print(attributedString)
        self.txtMessage.attributedText = attributedString
        self.applyFontAttributed()
//        let myAttribute = [ NSAttributedString.Key.font: UIFont(name: "Chalkduster", size: 18.0)! ]
//
//        let attrString = NSAttributedString(string: "\(attributedString)", attributes: myAttribute)
//        self.txtMessage.attributedText = attrString
       
        let color : String = dicCard["color"] as? String ?? ""
        self.txtMessage.textColor = hexStringToUIColor(hex: color)
        self.color = color
        self.btnAdd.setTitle(AppHelper.localizedtext(key: "Edit"), for: .normal)
        self.selectedTag = tag
        self.isFromEdit = true
    }
     //-------------------------------------------------------------
    func DeleteMessage(_ tag: Int) {
        print(tag)

       let popUp = PopupDialog(title: kAppName, message: ValidationAlert.kConfirmDeleteMessage)
       
       let btnOk = CancelButton(title: AppHelper.localizedtext(key: "YES")) {
           self.arrCard.remove(at: tag)
           self.tblCard.reloadData()
       }
       let btnCancel = CancelButton(title: AppHelper.localizedtext(key: "NO")) {
           
       }
       popUp.addButtons([btnOk,btnCancel])
       
       // Present dialog
       self.present(popUp, animated: true, completion: nil)

//            let refreshAlert = UIAlertController(title: kAppName, message: ValidationAlert.kConfirmDeleteMessage, preferredStyle: .alert)
//
//            refreshAlert.addAction(UIAlertAction(title: AppHelper.localizedtext(key: "YES"), style: .default, handler: { (action: UIAlertAction!) in
//
//
//            }))
//
//            refreshAlert.addAction(UIAlertAction(title: AppHelper.localizedtext(key: "NO"), style: .destructive, handler: { (action: UIAlertAction!) in
//                print("Handle Cancel Logic here")
//            }));
//
//            self.present(refreshAlert, animated: true, completion: nil)
        }
    
    
     //-------------------------------------------------------------
   @objc func chooseColorFromView()  {
        print("Color")
    let alert = UIAlertController(style: .actionSheet)
    alert.addColorPicker(color: UIColor.blue) { color in
        // action with selected color
        
        //self.txtView.textColor = color
        self.txtMessage.textColor = color
        self.color = color.hexString
        print(self.color)
    }
    alert.addAction(title: AppHelper.localizedtext(key: "kCancel"), style: .destructive)
    alert.show()
    }
    
    //----------------------------
    @IBAction func addMessageAndColorFromView(_ sender: AnyObject) {
      if self.arrCard.count >= 5{
          self.showAlert(message: ValidationAlert.kMaxLimitOfCard, title: kAppName)
      }else{
          if self.txtMessage.text == "" {
              self.showAlert(message: ValidationAlert.kEmptyMessage, title: kAppName)
          }else{
            let textVar = self.txtMessage.attributedText
            
            
            var text : String = textVar?.string ?? ""
            if self.isBoldSelected && self.isItalicSelected{
                text = "<i><b>\(text)</b></i>"
            }
            else if self.isItalicSelected{
                text = "<i>\(text)</i>"
            }
            else if self.isBoldSelected{
                text = "<b>\(text)</b>"
            }
            self.isBoldSelected = false
            self.isItalicSelected = false
            print(text)
              if isFromEdit{
                var  dicText : [String:Any] = ["text" : text, "color" : self.color]
                if self.arrCard.count > 0{
                    self.arrCard[self.selectedTag] = dicText
                }
                  self.color = "#000000"
                  self.txtMessage.resignFirstResponder()
                  self.tblCard.reloadData()
              }else{
                  var  dicText : [String:Any] = ["text" : text, "color" : self.color]
                  self.arrCard.append(dicText)
                  self.color = "#000000"
                  self.txtMessage.resignFirstResponder()
                  self.tblCard.reloadData()
              }
          }
      }
  }
    
    @IBAction func btnBoldAction(_ sender: AnyObject) {
        //if self.txtMessage.text != "" {
            self.isBoldSelected = !self.isBoldSelected
            self.applyFontAttributed()
            
        
    }


    func applyFontAttributed(){
        if self.isBoldSelected &&  self.isItalicSelected{
            let attributedString = NSMutableAttributedString(string: "")
            let boldItalicsFont = UIFont(name: "Avenir-HeavyOblique", size: self.fontSize ?? 18.0)!
            let attrs = [NSAttributedString.Key.font : boldItalicsFont]
            let boldString = NSMutableAttributedString(string: self.txtMessage.text ?? "", attributes:attrs)
            self.txtMessage.attributedText = boldString;
        
            
        
        }else if(self.isBoldSelected){
            let attributedString = NSMutableAttributedString(string: "")
            let boldItalicsFont = UIFont(name: "Avenir-Heavy", size: self.fontSize ?? 18.0)!
            let attrs = [NSAttributedString.Key.font : boldItalicsFont]
            let boldString = NSMutableAttributedString(string: self.txtMessage.text ?? "", attributes:attrs)
            self.txtMessage.attributedText = boldString;
        }
        else if(self.isItalicSelected){
            let attributedString = NSMutableAttributedString(string: "")
            let boldItalicsFont = UIFont(name: "Avenir-Oblique", size: self.fontSize ?? 18.0)!
            let attrs = [NSAttributedString.Key.font : boldItalicsFont]
            let boldString = NSMutableAttributedString(string: self.txtMessage.text ?? "", attributes:attrs)
            self.txtMessage.attributedText = boldString;
        }else {
            let attributedString = NSMutableAttributedString(string: "")
            let boldItalicsFont = UIFont(name: "Avenir-Book", size: self.fontSize ?? 18.0)!
            let attrs = [NSAttributedString.Key.font : boldItalicsFont]
            let boldString = NSMutableAttributedString(string: self.txtMessage.text ?? "", attributes:attrs)
            self.txtMessage.attributedText = boldString;
        }
    }
    
    @IBAction func btnItalicAction(_ sender: AnyObject) {
         self.isItalicSelected = !self.isItalicSelected
        self.applyFontAttributed()
//        self.isItalicSelected = true
//        if self.txtMessage.text != "" {
//            if self.isItalicSelected{
//              self.isItalicSelected = false
//                if self.isBoldSelected{
//                    self.txtMessage.font = UIFont.boldSystemFont(ofSize: 18.0)
//                }else{
//                    self.txtMessage.font = Font.regularFont
//                }
//
//            }else{
//                self.isItalicSelected = true
//
//                if self.isBoldSelected{
//                      //set bold and italic both font here
//                }else{
//                    self.txtMessage.font = UIFont.italicSystemFont(ofSize: 18.0)
//                }
//              }
//           }
       }
    @IBAction func btnFontAction(_ sender: AnyObject) {
        
        let popUp = PopupDialog(title: kAppName, message: "Select Font")
              
        let btn12 = CancelButton(title: "12") {
            self.fontSize = 12.0
        }
        let btn14 = CancelButton(title: "14") {
            self.fontSize = 14.0
        }
        let btn16 = CancelButton(title: "16") {
            self.fontSize = 16.0
        }
        let btn18 = CancelButton(title: "18") {
            self.fontSize = 18.0
        }
        let btn20 = CancelButton(title: "20") {
            self.fontSize = 20.0
        }
        let btn22 = CancelButton(title: "22") {
            self.fontSize = 22.0
        }
        let btn24 = CancelButton(title: "24") {
            self.fontSize = 24.0
        }
        let btn28 = CancelButton(title: "28") {
            self.fontSize = 28.0
        }
        let btn32 = CancelButton(title: "32") {
            self.fontSize = 32.0
        }
       let btnCancel = CancelButton(title: AppHelper.localizedtext(key: "NO")) {
        }
        popUp.addButtons([btn12,btn14,btn16,btn18,btn20,btn22,btn24,btn28,btn32,btnCancel])
              
        self.applyFontAttributed()
        // Present dialog
        self.present(popUp, animated: true, completion: nil)

    }
    //--------------------------------------------
    @IBAction func btnSampleDemoCardAction(_ sender: Any) {
//           let webVc = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVC") as! WebviewVC
//            webVc.isSampleExcel = false
//           self.navigationController?.pushViewController(webVc, animated: true)
        
        let videoURL = URL(string: "http://qrinvitationcards.com/customize_card_demo.mp4")
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
       }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           if(textField == self.txtMessage){
               guard let textFieldText = textField.text,
                   let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                       return false
               }
               let substringToReplace = textFieldText[rangeOfTextToReplace]
               let count = textFieldText.count - substringToReplace.count + string.count
              
               return count <= 50
           }
           return true
       }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "SecondrayColor")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
           textField.bordrColor = UIColor(named: "lightGray")
        self.msg = textField.text as? String ?? ""
    }
    
    
}




