//
//  WeddingCreateEvent.swift
//  ECards
//
//  Created by Dhruva Madhani on 03/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

class WeddingCreateEvent: AppViewController, UITextFieldDelegate {

    //MARK: IBOutlets..............
    @IBOutlet weak var txtGroomName: BWTextField!
    @IBOutlet weak var txtBrideName: BWTextField!
    @IBOutlet weak var txtDay: BWTextField!
    @IBOutlet weak var txtMonth: BWTextField!
    @IBOutlet weak var txtYear: BWTextField!
    @IBOutlet weak var txtLocationManually: BWTextField!
    @IBOutlet weak var txtLocationOnMap: BWTextField!
    @IBOutlet weak var txtStartTime: BWTextField!
    @IBOutlet weak var txtEndTime: BWTextField!
    
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var txtViewCustomMessage: UITextView!
    
    @IBOutlet weak var btnTheme: UIButton!
    @IBOutlet weak var imgTheme: UIImageView!
    @IBOutlet weak var lblThemeName: UILabel!
    
    //-------------------------------------------------------
    //MARK: View Life Cycle
    //-------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //-------------------------------------------------------
    //MARK: UIButton Actions
    //-------------------------------------------------------
    
    @IBAction func btnContinueAction(_ sender: Any) {
        
        let addGuests = self.storyboard?.instantiateViewController(withIdentifier: "AddGuestsVC")as! AddGuestsVC
        self.navigationController?.pushViewController(addGuests, animated: true)
    }
    

    @IBAction func btnThemeAction(_ sender: Any) {
        let addGuests = self.storyboard?.instantiateViewController(withIdentifier: "MapVC")as! MapVC
        self.navigationController?.pushViewController(addGuests, animated: true)
    }
    
    //-------------------------------------------------------
    //MARK: UITextfield delegateMethods
    //-------------------------------------------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "SecondrayColor")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "lightGray")
    }
}
