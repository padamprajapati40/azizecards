//
//  AddContactFormVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 16/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import CountryPickerView

protocol AddContactDelegate {
    func addContactDetails(name: String,phoneCode : String,phoneNumber:String,email : String,country_name : String)
    func editContactDetails(name: String,phoneCode : String,phoneNumber:String, email: String, tag: Int, country_name : String)
  
}

class AddContactFormVC: AppViewController, UITextFieldDelegate {

    //MARK: Variables.........................
    var selected_country_name = String()
    var delegate : AddContactDelegate?
    var name = String()
    var phoneNumber = String()
    var tag : Int = Int()
    var isOpenFromEdit : Bool = Bool()
    var email = String()
    var country_code = String()
    
    //MARK: IBOutlets.........................
    @IBOutlet weak var txtName: BWTextField!
    @IBOutlet weak var txtNumber: BWTextField!
    @IBOutlet weak var txtPhoneCode: BWTextField!
    @IBOutlet weak var txtEmail: BWTextField!
    @IBOutlet weak var lblStaticName : UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var cpvMain: CountryPickerView!
    let cp = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
    //---------------------------------------------------
    //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
         
        self.title = AppHelper.localizedtext(key: "AddInvitees")
        
        
//        self.txtPhoneCode.leftView = cp
//        self.txtPhoneCode.placeholder = ""
//        self.txtPhoneCode.leftViewMode = .always
        self.cp.frame = CGRect(x: 10, y: 0, width: self.txtPhoneCode.frame.size.width, height: self.txtPhoneCode.frame.size.height)
        self.txtPhoneCode.addSubview(self.cp);
        cp.dataSource = self
        cp.delegate = self
       
        
        cp.showCountryCodeInView = false
        
       // self.btnAdd.font =  Font.regularFont
        self.btnAdd.titleLabel?.font =  Font.tagViewFont
        if self.isOpenFromEdit{
            
            self.txtName.text = name
            self.txtNumber.text = phoneNumber
            self.txtEmail.text = email
            cp.setCountryByCode("\(country_code)")
            cp.setCountryByPhoneCode("\(country_code)")
            cp.setCountryByCode(self.country_code)
            cp.setCountryByPhoneCode(self.country_code)
            cp.setCountryByName(self.selected_country_name)
        }else{
            self.txtName.text = ""
            self.txtNumber.text = ""
            self.txtEmail.text = ""
            self.country_code = "+966"
            cp.setCountryByCode("+966")
            cp.setCountryByPhoneCode("+966")
        }
        self.cp.centerXAnchor.constraint(equalTo: self.txtPhoneCode.centerXAnchor).isActive = true
               self.cp.centerYAnchor.constraint(equalTo: self.txtPhoneCode.centerYAnchor).isActive = true

        self.txtNumber.delegate = self;
        self.setTitleView()
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == self.txtNumber){
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            let length = self.country_code.length - 1
            if(length == 3){
                return count <= 9
            }else{
                return count <= 10
            }
            
        }
        return true
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.lblStaticName.text = AppHelper.localizedtext(key: "kName")
    }
    
    //---------------------------------------------------
    //MARK: UItextfield Delegate Methods
    //---------------------------------------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "SecondrayColor")
    }
    
    //---------------------------------------------------
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "lightGray")
        if textField == self.txtEmail{
           let isEmailValid = self.isValidEmail(textStr: textField.text ?? "")
            if isEmailValid{
                             
             }else{
                             
                self.showAlertWithCallBack(title: kAppName, message: ValidationAlert.kEmailInvalidMSG) {
                    self.view.endEditing(true)
                    self.txtEmail.text = ""
                }
            }
       }
        if textField == self.txtPhoneCode{
            self.view.endEditing(true)
        }
    }
    
    //---------------------------------------------------
        //MARK: UIButton Actions
    //---------------------------------------------------
    @IBAction func btnAddAction(_ sender: Any) {
        var phoneCode = 10
        if(country_code.length - 1 == 3){
            phoneCode = 9
        }
        
        if self.txtName.text == "" {
            self.showAlert(message: ValidationAlert.kEmptyFirstName, title: kAppName)
        }else if self.txtNumber.text == ""{
            self.showAlert(message: ValidationAlert.kEmptyPhoneNumber, title: kAppName)
        }else if self.country_code == ""{
            self.showAlert(message: ValidationAlert.kEmptyPhoneCode, title: kAppName)
        }
//        else if self.txtEmail.text == "" {
//            self.showAlert(message: ValidationAlert.kEmptyEmailMSG, title: kAppName)
//        }
        else if self.txtNumber.text?.length != phoneCode {
                self.showAlert(title: kAppName, message: ValidationAlert.kInvalidPhone)
            }
        else{
            self.navigationController?.popViewController(animated: true)
            if let delegate = delegate{
                if isOpenFromEdit{
                    delegate.editContactDetails(name: self.txtName.text!,phoneCode : self.country_code, phoneNumber: self.txtNumber.text!, email: self.txtEmail.text ?? "", tag: self.tag, country_name: self.selected_country_name)
                }else{
                    delegate.addContactDetails(name: self.txtName.text!, phoneCode : self.country_code,phoneNumber: self.txtNumber.text!, email: self.txtEmail.text ?? "", country_name: self.selected_country_name)
                }
               
            }
        }
    }
    //----------------------------------------------------------
    func setMaximumLengthOfNumber(code:String) -> Bool {
        if code == "+966"{
            if self.txtNumber.text!.length != 9 {
              //  self.showAlert(message: "Please enter 9 didgits for saudi", title: kAppName)
                return false
            }
            return true
        }else{
            if self.txtNumber.text!.length != 10 {
              //  self.showAlert(message: "Please enter 10 digits", title: kAppName)
                return false
            }
            return true
        }
    }
}

extension AddContactFormVC : CountryPickerViewDataSource{
    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
     //   if countryPickerView.tag == cpvMain.tag && showPreferredCountries.isOn {
            var countries = [Country]()
            ["IN", "QA"].forEach { code in
                if let country = countryPickerView.getCountryByCode(code) {
                    countries.append(country)
                }
            }
            return countries
      //  }
        return []
    }
    
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
//        if countryPickerView.tag == cpvMain.tag && showPreferredCountries.isOn {
//            return "Preferred title"
//        }
        return nil
    }
    
//    func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool {
//      //  return countryPickerView.tag == cpvMain.tag && showOnlyPreferredCountries.isOn
//    }
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select a Country"
    }
        
//    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
////        if countryPickerView.tag == cpvMain.tag {
////            switch searchBarPosition.selectedSegmentIndex {
////            case 0: return .tableViewHeader
////            case 1: return .navigationBar
////            default: return .hidden
////            }
////        }
////        return .tableViewHeader
//    }
    
//    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
//        return countryPickerView.tag == cpvMain.tag && showPhoneCodeInList.isOn
//    }
//
//    func showCountryCodeInList(in countryPickerView: CountryPickerView) -> Bool {
//       return countryPickerView.tag == cpvMain.tag && showCountryCodeInList.isOn
//    }
}

extension AddContactFormVC : CountryPickerViewDelegate{
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        // Only countryPickerInternal has it's delegate set
        let title = "Selected Country"
        let message = "Name: \(country.name) \nCode: \(country.code) \nPhone: \(country.phoneCode)"
      //  self.txtPhoneCode.text = "\(country.phoneCode)"
        if self.country_code == country.phoneCode{
                   
        }else{
            self.txtNumber.text = ""
        }
        self.country_code = "\(country.phoneCode)"
        self.txtNumber.becomeFirstResponder()
       
        
        self.selected_country_name = country.name
        self.cp.setCountryByName(country.name)
      
    }
}
