//
//  ManageEventsVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 18/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import ObjectMapper
import PopupDialog

private func _swizzling(forClass: AnyClass, originalSelector: Selector, swizzledSelector: Selector) {
    if let originalMethod = class_getInstanceMethod(forClass, originalSelector),
       let swizzledMethod = class_getInstanceMethod(forClass, swizzledSelector) {
        method_exchangeImplementations(originalMethod, swizzledMethod)
    }
}

extension UIViewController {

    static let preventPageSheetPresentation: Void = {
        if #available(iOS 13, *) {
            _swizzling(forClass: UIViewController.self,
                       originalSelector: #selector(present(_: animated: completion:)),
                       swizzledSelector: #selector(_swizzledPresent(_: animated: completion:)))
        }
    }()

    @available(iOS 13.0, *)
    @objc private func _swizzledPresent(_ viewControllerToPresent: UIViewController,
                                        animated flag: Bool,
                                        completion: (() -> Void)? = nil) {
        if viewControllerToPresent.modalPresentationStyle == .pageSheet
                   || viewControllerToPresent.modalPresentationStyle == .automatic {
            viewControllerToPresent.modalPresentationStyle = .fullScreen
        }
        _swizzledPresent(viewControllerToPresent, animated: flag, completion: completion)
    }
}

class ManageEventsVC: AppViewController, UITableViewDelegate, UITableViewDataSource {

     //........MARK: Variables......................
    var arrOngoingEvents = ["Riaz Wedding","ShehnazBirthday Wedding","ShehnazBirthday Wedding","ShehnazBirthday Wedding"]
    var arrCompletedEvents = ["ShehnazBirthday Wedding"]
    var arrTable = [String]()
    let controller = OrganizerViewModel()
    var type = String()
    var completedEventList : [CompletedEvents] = [];
    var ongoingEventList : [CompletedEvents] = [];
    var tblEventList : [CompletedEvents] = [];
    var hasData = Bool()
    var isCallApiFirstTime = Bool()
    //........MARK: IBOutlets......................
    @IBOutlet weak var btnCreateEvent: UIButton!
    @IBOutlet weak var lblCreateEventLabel: UILabel!
    @IBOutlet weak var viewNoEventFound: UIView!
    @IBOutlet weak var btnLoginOrsignup: UIButton!
    @IBOutlet weak var viewSignupOrLogin: UIView!
    @IBOutlet weak var tblEvents: UITableView!
    @IBOutlet weak var btnCompletedEents: UIButton!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var btnOnGoingEvents: UIButton!
    
    @IBOutlet weak var lblNoDataFound: UILabel!
    
  //  @IBOutlet weak var btnOngoingEvents: UIButton!
    @IBOutlet weak var btnCompletedEvents: UIButton!
    @IBOutlet weak var btnCancelledEvents: UIButton!
    //--------------------------------------------------------------------
    //MARK: View life cycle
    //--------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        let language = GlobalUtils.getInstance().getApplicationLanguage()
        self.segmentControl.semanticContentAttribute = .forceLeftToRight
        
        self.DefalutDialoguesettingOfPopup()
        
        let fontAttribute = [NSAttributedString.Key.font: Font.tagViewFont,
                                    NSAttributedString.Key.foregroundColor: UIColor.black]
               segmentControl.setTitleTextAttributes(fontAttribute, for: .normal)
               
               let fontAttribute1 = [NSAttributedString.Key.font: Font.tagViewFont,
                                    NSAttributedString.Key.foregroundColor: UIColor.white]
               segmentControl.setTitleTextAttributes(fontAttribute1, for: .selected)
        if #available(iOS 13.0, *) {
            segmentControl.selectedSegmentTintColor = COLOR.textColor
        } else {
            segmentControl.tintColor = COLOR.textColor
            // Fallback on earlier versions
        }
        self.type = "upcoming"
        self.lblNoDataFound.font = Font.regularFont
        self.navigationItem.title = AppHelper.localizedtext(key: "ManageEvents")
        self.lblNoDataFound.isHidden = true
       // self.lblNoDataFound.text = "No event found."
        self.tblEvents.isHidden = false
        
        self.segmentControl.isHidden = true
        self.segmentControl.isEnabled = false
        let color_primary = UIColor(named: "PrimaryColor")?.cgColor
        self.btnOnGoingEvents.layer.borderColor = color_primary
        self.btnCompletedEvents.layer.borderColor = color_primary
        self.btnCancelledEvents.layer.borderColor = color_primary
      
        let color_primary1 = UIColor(named: "PrimaryColor")
              let color_white = UIColor.white
               
               self.btnOnGoingEvents.setTitleColor(color_white, for: .normal)
               self.btnOnGoingEvents.backgroundColor = color_primary1
               self.btnCompletedEvents.setTitleColor(color_primary1, for: .normal)
               
               self.btnCancelledEvents.setTitleColor(color_primary1, for: .normal)
        self.setTitleView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if GlobalUtils.getInstance().sessionToken() != ""{
            self.segmentControl.isHidden = false
            self.viewSignupOrLogin.isHidden = true
            self.btnOnGoingEvents.isHidden = true
            self.btnCompletedEvents.isHidden = true
            self.btnCancelledEvents.isHidden = true
            self.viewNoEventFound.isHidden = true
            self.tblEvents.isHidden = false
            self.isCallApiFirstTime = true
            self.navigationItem.title = AppHelper.localizedtext(key: "ManageEvents")
            self.callUpcomingEventsApi(self.type)
        }else{
            self.navigationItem.title = AppHelper.localizedtext(key: "QRCodeInvite")
            self.viewSignupOrLogin.isHidden = false
            self.viewNoEventFound.isHidden = true
            self.tblEvents.isHidden = true
            self.segmentControl.isHidden = true
            self.lblNoDataFound.isHidden = true
        }
        //self.segmentControl.isHidden = true
        self.segmentControl.isEnabled = true
        
        
        self.btnOnGoingEvents.setTitle(AppHelper.localizedtext(key: "OngoingEvents"), for: .normal)
        self.btnCompletedEvents.setTitle(AppHelper.localizedtext(key: "CompletedEvents"), for: .normal)
        self.btnCancelledEvents.setTitle(AppHelper.localizedtext(key: "CancelledEvents"), for: .normal)
        
        self.segmentControl.setTitle(AppHelper.localizedtext(key: "OngoingEvents"), forSegmentAt: 0)
        self.segmentControl.setTitle(AppHelper.localizedtext(key: "CompletedEvents"), forSegmentAt: 1)
        self.segmentControl.setTitle(AppHelper.localizedtext(key: "CancelledEvents"), forSegmentAt: 2)
        self.lblCreateEventLabel.text = AppHelper.localizedtext(key: "kNoAvailableEvent")
        self.btnCreateEvent.setTitle(AppHelper.localizedtext(key: "CreateEvent"), for: .normal)
        self.lblNoDataFound.text = AppHelper.localizedtext(key: "kNoEventFound")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        if GlobalUtils.getInstance().sessionToken() != ""{
//            self.segmentControl.isHidden = false
//            self.viewSignupOrLogin.isHidden = true
//            self.viewNoEventFound.isHidden = true
//            self.tblEvents.isHidden = false
//            self.isCallApiFirstTime = true
//            self.callUpcomingEventsApi(self.type)
//        }else{
//            self.viewSignupOrLogin.isHidden = false
//            self.viewNoEventFound.isHidden = true
//            self.tblEvents.isHidden = true
//            self.segmentControl.isHidden = true
//
//        }
        
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
    
    
    //--------------------------------------------------------------------
    //MARK: Uibutton actions
    //--------------------------------------------------------------------
    @IBAction func btnLoginOrsignupAction(_ sender: Any) {
//        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//        self.present(UINavigationController(rootViewController: loginVC), animated: true, completion: nil);
        
        let welcomeVC = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeVC")as! WelcomeVC
        // welcomeVC.eventObj = self.EventDetailsList;
        welcomeVC.isOpenFromManageEvents = true
        let navVC = UINavigationController(rootViewController:welcomeVC )
        navVC.modalPresentationStyle = .fullScreen
        
        navVC.presentationController?.delegate = self
        self.present(navVC, animated: true, completion: nil);
    }
    //--------------------------------------------
    @IBAction func btnCreateEventAction(_ sender: Any) {
        
        self.tabBarController!.selectedIndex=1
//        let tabbarVc = self.storyboard?.instantiateViewController(withIdentifier: "EventTabbarController")as! EventTabbarController
//        tabbarVc.selectedIndex = 1
////        let createEvent = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventVC") as! CreateEventVC
//       self.navigationController?.pushViewController(tabbarVc, animated: true)
    }
    //--------------------------------------------
    @IBAction func btnOnGoingEventsAction(_ sender: Any) {
        let ongoingEvents = self.storyboard?.instantiateViewController(withIdentifier: "OnGoingEventListVC")as! OnGoingEventListVC
        ongoingEvents.isOpenFromOngoing = true
        self.navigationController?.pushViewController(ongoingEvents, animated: true)
    }
    //--------------------------------------------------------------------
    @IBAction func btnCompletedEventsActions(_ sender: Any) {
        let ongoingEvents = self.storyboard?.instantiateViewController(withIdentifier: "OnGoingEventListVC")as! OnGoingEventListVC
        ongoingEvents.isOpenFromOngoing = false
        self.navigationController?.pushViewController(ongoingEvents, animated: true)
    }
    //--------------------------------------------------------------------
    @IBAction func indexChangedSegmentAction(_ sender: Any) {
        switch self.segmentControl.selectedSegmentIndex{
        case 0 :
            self.type = "upcoming"
            self.isCallApiFirstTime = false
            self.callUpcomingEventsApi(self.type)
           
            break
        case 1:
            self.type = "completed"
            self.isCallApiFirstTime = false
            self.callUpcomingEventsApi(self.type)
            break
        case 2:
         self.type = "canceled"
         self.isCallApiFirstTime = false
        self.callUpcomingEventsApi(self.type)
         
         break
            
        default:
            break
        }
    }
    //--------------------------------------------------------------------
    @IBAction func btnOngoingEventsAction(_ sender: Any) {
        self.type = "upcoming"
        self.isCallApiFirstTime = false
        self.changeThemeOfSelectedButtons(isSelected: true, btn: self.btnOnGoingEvents)
        self.changeThemeOfSelectedButtons(isSelected: false, btn: self.btnCompletedEvents)
        self.changeThemeOfSelectedButtons(isSelected: false, btn: self.btnCancelledEvents)
        self.callUpcomingEventsApi(self.type)
    }
    //--------------------------------------------------------------------
    @IBAction func btnCompletedEventsAction(_ sender: Any) {
           self.type = "completed"
            self.isCallApiFirstTime = false
        self.changeThemeOfSelectedButtons(isSelected: true, btn: self.btnCompletedEvents)
        self.changeThemeOfSelectedButtons(isSelected: false, btn: self.btnOnGoingEvents)
        self.changeThemeOfSelectedButtons(isSelected: false, btn: self.btnCancelledEvents)
           self.callUpcomingEventsApi(self.type)
       }
    
    //--------------------------------------------------------------------
    @IBAction func btnCancelledEventsAction(_ sender: Any) {
           self.type = "canceled"
            self.isCallApiFirstTime = false
        self.changeThemeOfSelectedButtons(isSelected: true, btn: self.btnCancelledEvents)
        self.changeThemeOfSelectedButtons(isSelected: false, btn: self.btnOnGoingEvents)
        self.changeThemeOfSelectedButtons(isSelected: false, btn: self.btnCompletedEvents)
           self.callUpcomingEventsApi(self.type)
            
    }
    
    func changeThemeOfSelectedButtons(isSelected : Bool, btn: UIButton)  {
        
        let color_primary = UIColor(named: "PrimaryColor")
        let color_white = UIColor.white
        
        if isSelected{
            
            btn.backgroundColor = color_primary
            btn.setTitleColor(color_white, for: .normal)
        }else{
            btn.backgroundColor = color_white
            btn.setTitleColor(color_primary, for: .normal)
        }
        self.btnOnGoingEvents.layer.borderColor = color_primary?.cgColor
        self.btnCompletedEvents.layer.borderColor = color_primary?.cgColor
        self.btnCancelledEvents.layer.borderColor = color_primary?.cgColor
        
    }
    
    
    //--------------------------------------------------------------------
    
    //--------------------------------------------------------------------
    //MARK: UITableview Datasource
    //--------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return self.ongoingEventList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OngoingOrCompletedTableCell", for: indexPath)as! OngoingOrCompletedTableCell
        let planObj : Plan? = self.ongoingEventList[indexPath.row].plan
        var planName = planObj?.name ?? ""
        cell.lblEventName.text = "\(self.ongoingEventList[indexPath.row].name ?? "") (\(planName))"
       // cell.viewBackground?.backgroundColor = UIColor(named:"PrimaryColor")
        let img_rightArrow : UIImage = UIImage(named:"rightArrow")!
        let tintableImage = img_rightArrow.withRenderingMode(.alwaysTemplate)
        cell.imgRightArrow.image = tintableImage
        cell.imgRightArrow.tintColor = UIColor(named:"SecondrayColor")
        
        if self.type == "upcoming"{
            cell.lblEventActive.isHidden = false
           // cell.lblEventActive.tintColor = UIColor(named: "ActiveColor")
            let isActive : Bool = self.ongoingEventList[indexPath.row].is_active ?? false
           
            cell.imgRightArrow.isHidden = false
            if isActive{
                cell.lblEventActive.layer.cornerRadius = 5.0
            }else{
                 cell.lblEventActive.isHidden = true
            }
            
        }else{
            cell.lblEventActive.isHidden = true
            
             if self.type == "canceled"{
                cell.accessoryType = .none
                cell.imgRightArrow.isHidden = true
             }else{
                cell.imgRightArrow.isHidden = false
            }
        }
        
        return cell
    }
    
    //--------------------------------------------------------------------
    //MARK: UITableview Delegate
    //--------------------------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let eventList = self.storyboard?.instantiateViewController(withIdentifier: "OnGoingEventListVC") as! OnGoingEventListVC
        if self.type == "upcoming"{
            eventList.isOpenFromOngoing = true
            
        }else{
            eventList.isOpenFromOngoing = false
        }
        eventList.eventId = self.ongoingEventList[indexPath.row].id ?? 0
        eventList.eventObj = self.ongoingEventList[indexPath.row]
        
        let isCanceled : Bool = self.ongoingEventList[indexPath.row].is_canceled ?? false
        if isCanceled{
            self.showAlert(message: ValidationAlert.kEventCancel, title: kAppName)
            return
        }
        
        
        self.navigationController?.pushViewController(eventList, animated: true)
    }
    //--------------------------------------------------------------------
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    //--------------------------------------------------------------------

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if self.type == "upcoming"{
            if !(self.ongoingEventList[indexPath.row].invitation_message_deliver ?? false) ?? false{
                return true
            }else{
                return false
            }
        }else{
            return false
        }
    
    }
     //--------------------------------------------------------------------
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
             if (editingStyle == UITableViewCell.EditingStyle.delete) {
                
                let popUp = PopupDialog(title: kAppName, message: ValidationAlert.kDeleteEventMessage)
                
                let btnOk = CancelButton(title: AppHelper.localizedtext(key: "YES")) {
                               DispatchQueue.main.async {
                                                                    self.showActivityIndicatory()
                                                                 let eventId : Int = self.ongoingEventList[indexPath.row].id ?? 0
                                                                 self.ongoingEventList.remove(at: indexPath.row)
                                                                    self.controller.deleteEventApi(eventId: "\(eventId)") { (success, response, message, statusCode) in
                         
                                                                        if success{
                                                                            self.hideActivityIndicator()
                                                                            DispatchQueue.main.async{
                                                                             
                                                                                self.showAlertWithCallBack(title: kAppName, message: message) {
                                                                                 self.tblEvents.reloadData()
                                                                                }
                                                                            }
                                                                        }else{
                                                                            DispatchQueue.main.async {
                                                                                self.hideActivityIndicator()
                                                                                self.showAlert(message: message, title: kAppName)
                                                                            }
                                                                        }
                                                                    }
                         }
                }
                let btnCancel = CancelButton(title: AppHelper.localizedtext(key: "NO")) {
                    
                }
                popUp.addButtons([btnOk,btnCancel])

                self.present(popUp, animated: true, completion: nil)
                
        }
                

     }
    //--------------------------------------------------------------------
    //MARK: Web api methods
    //--------------------------------------------------------------------
    func callUpcomingEventsApi(_ type : String)  {
        self.showActivityIndicatory()
        controller.getUpcomingOrCompletdEvents(type: self.type) { (success, response, message, statusCode) in
            if success{
                DispatchQueue.main.async {
 
                    print(response)
                    self.hideActivityIndicator()
                    
                    self.ongoingEventList = response as? [CompletedEvents] ?? []
                    
                    if self.isCallApiFirstTime{
                        if self.ongoingEventList.count != 0 {
                            self.hasData = true
                        }
                        if self.hasData{
                            if self.ongoingEventList.count == 0{
                                                            if self.type == "upcoming"{
                                                                                            
                                                                                           self.viewNoEventFound.isHidden = false
                                                                                           self.lblNoDataFound.isHidden = true
                                                                }else{
                                                                                           self.lblNoDataFound.isHidden = false
                                                                                           self.viewNoEventFound.isHidden = true
                                                               }
               
                                                            self.tblEvents.isHidden = true
                                                            
                                                        }else{
                                                            self.viewNoEventFound.isHidden = true
                                                        
                                                            self.tblEvents.isHidden = false
                                                        }
                        }else{
                            if self.ongoingEventList.count == 0{
                                if self.type == "upcoming"{
                                                                
                                                               self.viewNoEventFound.isHidden = false
                                                               self.lblNoDataFound.isHidden = true
                                    }else{
                                                               self.lblNoDataFound.isHidden = false
                                                               self.viewNoEventFound.isHidden = true
                                   }
//                                self.viewNoEventFound.isHidden = false
//                                //  self.lblNoDataFound.isHidden = true
                                self.tblEvents.isHidden = true
                                
                            }else{
                                self.viewNoEventFound.isHidden = true
                               // self.lblNoDataFound.isHidden = true
                                self.tblEvents.isHidden = false
                            }
                        }
                    }else{
//                        if self.ongoingEventList.count == 0{
//                            self.viewNoEventFound.isHidden = false
//                            
//                            self.tblEvents.isHidden = true
//                            
//                        }else{
//                            self.viewNoEventFound.isHidden = true
//                            
//                            self.tblEvents.isHidden = false
//                        }
                        
                        
                        if self.ongoingEventList.count == 0{
                            if self.type == "upcoming"{
                                self.viewNoEventFound.isHidden = false
                                self.lblNoDataFound.isHidden = true
                            }else{
                                self.lblNoDataFound.isHidden = false
                                self.viewNoEventFound.isHidden = true
                            }
                           
                          //
                            self.tblEvents.isHidden = true
                        }else{
                          //  self.lblNoDataFound.isHidden = true
                            self.tblEvents.isHidden = false
                        }
                    }
                    
                    
                    
                    
                    self.tblEvents.reloadData()
                }
            }else{
                DispatchQueue.main.async {
                            self.hideActivityIndicator()
                            
                            if statusCode == 422 {
                                self.showAlertWithCallBack(title: kAppName, message: message) {
                                    var sessiontoken : String =  GlobalUtils.getInstance().sessionToken() as? String ?? ""
                                                                             // GlobalUtils.getInstance().sessionToken().removeAll()
                                                       sessiontoken = ""
                                                       GlobalUtils.getInstance().setSessionToken(token: sessiontoken)
                                    let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                    let navVC = UINavigationController(rootViewController:loginVC )
                                           self.present(navVC, animated: true, completion: nil);
                
                                   // self.navigationController?.popToViewController(loginVC, animated: true)
                                }
                            }else{
                                self.showAlert(message: message, title: kAppName)
                            }
                        }
            }
        }
    }
}



extension ManageEventsVC: UIAdaptivePresentationControllerDelegate {
    func presentationControllerDidAttemptToDismiss(_ presentationController: UIPresentationController) {


        print("slide to dismiss stopped")
        self.dismiss(animated: true, completion: nil)
    }
}
