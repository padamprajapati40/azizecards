//
//  ReportsCompletedEventsVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 25/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import PieCharts

class ReportsCompletedEventsVC: AppViewController {

    //........MARK: Variables......................
    let controller = OrganizerViewModel()
    var eventId  = Int()
    
    //........MARK: IBOutlets......................
    
    @IBOutlet weak var chartView: PieChart!
    @IBOutlet weak var btnAttended: UIButton!
    @IBOutlet weak var btnNotAttended: UIButton!
  
    //--------------------------------------------------------------------
    //MARK: View life cycle
    //--------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = AppHelper.localizedtext(key: "Report")
        
        print("Event id is \(self.eventId)")
        
        let imgCirlce = UIImage(named: "Circle_Gray")
        
        self.btnAttended.setImage(imgCirlce?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnAttended.tintColor = UIColor(named: "PrimaryColor")
        
        self.btnNotAttended.setImage(imgCirlce?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnNotAttended.tintColor = UIColor(named: "lightGray")
        
       
        
        self.callRepotsApi("\(self.eventId)")
        self.setTitleView()
    }
    
    //---------------------------------------------------
    //MARK: UIButton Actions
    //---------------------------------------------------
    @IBAction func btnAttendedAction(_ sender: Any) {
    }
    //---------------------------------------------------
    @IBAction func btnNotAttendedAction(_ sender: Any) {
    }

    //---------------------------------------------------
    //MARK: web api methods
    //---------------------------------------------------
    func callRepotsApi(_ eventId : String)  {
        
        controller.getViewReportsOfEvents(eventId: eventId) { (success, response, message, statusCode) in
            if success{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    print(response)
                    print(message)
                    print(statusCode)
                    
                    let dicResponse : [String:Any] = response as? [String:Any] ?? [:]
                    
                    let dicAttendance : [String:Any] = dicResponse["attendence_report"] as? [String:Any] ?? [:]
                    
                    print(dicAttendance)
                    let attended : Int = dicAttendance["attended"] as? Int ?? 0
                    let remaining : Int  = dicAttendance["remaining"] as? Int ?? 0
                    
                    print(attended)
                    print(remaining)
                    
//                   self.chartView.models = [
//                        PieSliceModel(value: Double(attended), color: UIColor(named: "PrimaryColor") ?? .blue),
//                        PieSliceModel(value: Double(remaining), color:UIColor(named: "lightGray") ?? .gray),
//                    ]
                    
                    self.chartView.models = [
                        PieSliceModel(value: 2.0, color: UIColor(named: "PrimaryColor") ?? .blue),
                        PieSliceModel(value: 8.0, color:UIColor(named: "lightGray") ?? .gray),
                    ]

                }
            }else{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    self.showAlert(message: message, title: kAppName)
                }
            }
        }
    }
}
