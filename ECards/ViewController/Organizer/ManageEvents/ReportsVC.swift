//
//  ReportsVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 25/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import PieCharts






class ReportsVC: AppViewController , PieChartDelegate, PieSliceDelegate{
    
    

    
    //MARK: Variables.........................
    let controller = OrganizerViewModel()
    var eventId  = Int()
    var eventCompletedObj : CompletedEvents?
    var dictRSVpReport = [String:Any]()
    var dictAttendanceReport = [String:Any]()
    
    //MARK: IBOutlets.........................
    
    @IBOutlet weak var chartViewEvent: PieChart!
    @IBOutlet weak var chartView: PieChart!
    @IBOutlet weak var btnPending: UIButton!
    
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnYes: UIButton!
    
    @IBOutlet weak var btnAttended: UIButton!
    @IBOutlet weak var btnNotAttended: UIButton!
    @IBOutlet weak var lblNoDataFoundRSVPd : UILabel!
     @IBOutlet weak var lblNoDataFoundAttended: UILabel!
    
    
    //---------------------------------------------------
    //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = AppHelper.localizedtext(key: "Report") 
        
        print("Event id is \(self.eventId)")
        
        self.chartView.delegate = self
        self.chartViewEvent.delegate = self
        
        
        
        let imgCirlce = UIImage(named: "Circle_Gray")

        self.btnYes.setImage(imgCirlce?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnYes.tintColor = UIColor(named: "PrimaryColor")
        
        self.btnNo.setImage(imgCirlce?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnNo.tintColor = UIColor(named: "lightGray")
        
        self.btnPending.setImage(imgCirlce?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnPending.tintColor = UIColor(named: "SecondrayColor")
        
        self.btnAttended.setImage(imgCirlce?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnAttended.tintColor = UIColor(named: "PrimaryColor")
        
        self.btnNotAttended.setImage(imgCirlce?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnNotAttended.tintColor = UIColor(named: "lightGray")
       
//        chartView.models = [
//            PieSliceModel(value: 2.1, color: UIColor(named: "PrimaryColor") ?? .blue),
//            PieSliceModel(value: 3, color:UIColor(named: "lightGray") ?? .gray),
//            PieSliceModel(value: 1, color: UIColor(named: "SecondrayColor") ?? .yellow)
//        ]'
        
        print(self.eventCompletedObj)
        
        self.lblNoDataFoundRSVPd.isHidden = true
        self.lblNoDataFoundAttended.isHidden = true
       self.setTitleView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.chartView.clear()
        self.chartViewEvent.clear()
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
         self.callRepotsApi("\(self.eventId)")
       //  self.setChartData()
    }
    
    //---------------------------------------------------
    //MARK: web api methods
    //---------------------------------------------------
    func callRepotsApi(_ eventId : String)  {
        
        controller.getViewReportsOfEvents(eventId: eventId) { (success, response, message, statusCode) in
            if success{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    print(response)
                    print(message)
                    print(statusCode)
                    self.chartViewEvent.tag = 2
                    self.chartView.tag = 1
                    

                    
                    let dicResponse : [String:Any] = response as? [String:Any] ?? [:]
                    
                    let dicAttendance : [String:Any] = dicResponse["rsvp_report"] as? [String:Any] ?? [:]
                    print(dicAttendance)
                    
                    self.dictRSVpReport = dicAttendance
                    
                    
                    let dictAttending : [String:Any] = dicAttendance["attending"] as? [String:Any] ?? [:]
                    let dictNotAttending : [String:Any] = dicAttendance["not_attending"] as? [String:Any] ?? [:]
                    let dictMayBeAttending : [String:Any] = dicAttendance["may_be_attending"] as? [String:Any] ?? [:]
                    let dictPending : [String:Any] = dicAttendance["pending"] as? [String:Any] ?? [:]
                    
                    
                   // let yes : Int = dicAttendance["attending"] as? Int ?? 0
                    let yes : Int = dictAttending["total_count"] as? Int ?? 0
                    let no : Int  =  dictNotAttending["totel_count"]as? Int ?? 0
                    let maybe : Int =  dictMayBeAttending["total_count"] as? Int ?? 0
                    let pending : Int =  dictPending["total_count"]as? Int ?? 0
                    
                    print(yes)
                    print(no)
                    
                    self.chartView.models = [
                        PieSliceModel(value: Double(yes), color: UIColor(named: "PrimaryColor") ?? .blue,obj:dictAttending),
                        PieSliceModel(value: Double(no), color:UIColor(named: "lightGray") ?? .gray,obj:dictNotAttending),
                        //PieSliceModel(value: Double(maybe), color: UIColor(named: "SecondrayColor") ?? .yellow,obj:dictNotAttending),
                   //     PieSliceModel(value: Double(pending), color:UIColor(named: "lightGray") ?? .gray),
                        PieSliceModel(value: Double(pending), color:UIColor(named: "SecondrayColor") ?? .gray,obj:dictPending),
                    ]
                    let dicAttendanceEvent : [String:Any] = dicResponse["attendence_report"] as? [String:Any] ?? [:]
                    
                    print(dicAttendanceEvent)
                    
                    self.chartView.layers = [PiePlainTextLayer()]
                    
                    self.dictAttendanceReport = dicAttendanceEvent
                    
                    let dictAttended : [String:Any] = dicAttendanceEvent["attended"] as? [String:Any] ?? [:]
                    let dictRemaining : [String:Any] = dicAttendanceEvent["remaining"] as? [String:Any] ?? [:]
                    
                    let attended : Int =  dictAttended["total_count"]as? Int ?? 0
                    let remaining : Int  = dictRemaining["total_count"] as? Int ?? 0
                    
//
                    let textLayerSettings = PiePlainTextLayerSettings()
                                      textLayerSettings.viewRadius = 55
                                      textLayerSettings.hideOnOverflow = true
                                      textLayerSettings.label.font = UIFont(name: "Zeit-Light", size: 8.0) ?? UIFont.systemFont(ofSize: 8.0)
                                      let formatter = NumberFormatter()
                                      formatter.maximumFractionDigits = 1

                                      textLayerSettings.label.textGenerator = {slice in
                                        let stringText : String = formatter.string(from: slice.data.percentage * 1000 as NSNumber).map{"\($0)%"} ?? ""
                                        print(stringText)
                                        if stringText == "0"
                                        {
                                            return ""
                                        }else{
                                            return stringText
                                        }
                                      }
                                      
                    
                    
//                    let textLayerSettings = PiePlainTextLayerSettings()
//                                       textLayerSettings.viewRadius = 55
//                                       textLayerSettings.hideOnOverflow = true
//                                       textLayerSettings.label.font = UIFont(name: "Zeit-Light", size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)
//                                       let formatter = NumberFormatter()
//                                       formatter.maximumFractionDigits = 1
//                                        textLayerSettings.label.textGenerator = {slice in
////                                           return "0"
//                                            return formatter.string(from: slice.data.percentage * 1000 as NSNumber).map{"\($0)%"} ?? ""
//                                       }
                    print(attended)
                    
                    self.chartViewEvent.layers = [PiePlainTextLayer()]
                    self.chartViewEvent.models = [
                        PieSliceModel(value: Double(attended), color: UIColor(named: "PrimaryColor") ?? .blue,obj:dictAttended),
                                    //    PieSliceModel(value: Double(no), color:UIColor(named: "lightGray") ?? .gray),
                                        PieSliceModel(value: Double(remaining), color: UIColor(named: "lightGray") ?? .yellow,obj:dictRemaining),
                                   //     PieSliceModel(value: Double(pending), color:UIColor(named: "lightGray") ?? .gray),
                                     //   PieSliceModel(value: Double(pending), color:UIColor(named: "lightGray") ?? .gray),
                                    ]

                  
//                    
                }
            }else{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    //self.showAlert(message: message, title: kAppName)
                }
            }
        }
    }
    
    
    func setChartData()  {
        let textLayerSettings = PiePlainTextLayerSettings()
        let layer = PiePlainTextLayer()
        layer.settings = textLayerSettings;
       // print(remaining)
        self.chartView.layers = [layer]
        self.chartViewEvent.tag = 2
        self.chartView.tag = 1

        self.chartView.models = [
            PieSliceModel(value: 1, color: UIColor(named: "PrimaryColor") ?? .blue),
            PieSliceModel(value: 1, color:UIColor(named: "lightGray") ?? .gray),
            PieSliceModel(value: 1, color: UIColor(named: "SecondrayColor") ?? .yellow)
        ]
        
        let layer1 = PiePlainTextLayer()
        layer1.settings = textLayerSettings;
       // print(remaining)
        self.chartViewEvent.layers = [layer1]
        self.chartViewEvent.models = [
            PieSliceModel(value: 1, color: UIColor(named: "PrimaryColor") ?? .blue),
            PieSliceModel(value: 1, color:UIColor(named: "lightGray") ?? .gray),
        ]
    }
    
    func onSelected(slice: PieSlice, selected: Bool) {
        print(self.chartView.selectedOffset)
        print("Selected: \(selected), slice: \(slice)")
        if self.chartView.tag == 1{
          
            let dictData = slice.data.model.obj as? [String:Any] ?? [:]
            print("Dict Attending:-\(dictData)");
                let chartListVC = self.storyboard?.instantiateViewController(withIdentifier: "ChartDataListVC")as! ChartDataListVC
                
                chartListVC.dictData = dictData
                self.navigationController?.pushViewController(chartListVC, animated: true)

           
        }else{
          //  if(slice.data.model.value == 3){
                let dictData = slice.data.model.obj as? [String:Any] ?? [:]
                let chartListVC = self.storyboard?.instantiateViewController(withIdentifier: "ChartDataListVC")as! ChartDataListVC
                
                chartListVC.dictData = self.dictAttendanceReport
                self.navigationController?.pushViewController(chartListVC, animated: true)
//            }else{
//                let chartListVC = self.storyboard?.instantiateViewController(withIdentifier: "ChartDataListVC")as! ChartDataListVC
//                 chartListVC.isComing = false
//                self.navigationController?.pushViewController(chartListVC, animated: true)
//            }
            
        }
       
       
    }
    func onGenerateSlice(slice: PieSlice){
        print(slice)
        
        
    }
    func onStartAnimation(slice: PieSlice){
        print(slice)
    }
    func onEndAnimation(slice: PieSlice){
        print(slice)
    }
    
    //---------------------------------------------------
    //MARK: UIButton Actions
    //---------------------------------------------------
    @IBAction func btnYesAction(_ sender: Any) {
    }
    //---------------------------------------------------
    @IBAction func btnNoAction(_ sender: Any) {
    }
    //---------------------------------------------------
    @IBAction func btnPendingAction(_ sender: Any) {
    }
    //---------------------------------------------------
    @IBAction func btnAttendedAction(_ sender: Any) {
    }
    //---------------------------------------------------
    @IBAction func btnNotAttendedAction(_ sender: Any) {
    }
    //---------------------------------------------------
    
}
