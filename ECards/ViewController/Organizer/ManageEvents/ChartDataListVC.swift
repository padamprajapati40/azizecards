//
//  ChartDataListVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 06/01/20.
//  Copyright © 2020 Dhruva Madhani. All rights reserved.
//

import UIKit

class ChartDataListVC: AppViewController, UITableViewDataSource , UITableViewDelegate{
    
    @IBOutlet weak var tblView: UITableView!
    var arrData : [[String:Any]] = []
    var arrNotComing = [String]()
    var isComing : Bool = false
    
    var dictData = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.title = AppHelper.localizedtext(key: "QRCodeInvite")
        self.arrData = dictData["data"] as? [[String:Any]] ?? []
        self.tblView.reloadData()
//        self.arrNames = ["Vandan","Gaurav","Abhishek"]
//        self.arrNotComing = ["Nilofer","Gunjan"]
        self.setTitleView()
        
        print(self.dictData)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count;
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChartListDataTableCell", for: indexPath) as! ChartListDataTableCell
        let dictData = self.arrData[indexPath.row];
        cell.lblName.text = dictData["full_name"] as? String ?? ""
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
}
