//
//  SendCancelationMessageVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 20/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import PopupDialog

class SendCancelationMessageVC: AppViewController, UITextFieldDelegate,UITextViewDelegate {

    //MARK: Variables.........................
    let imgBlueCircle = UIImage(named: "circle")
    let imgGrayCirle = UIImage(named: "Circle_Gray")
    var isEnglishSelected = Bool()
    var isArabicSelected = Bool()
    var isBothLanguageSelected = Bool()
    let controller = OrganizerViewModel()
    //var eventObj : CompletedEvents?
    var eventCompletedObj : CompletedEvents?
    var eventId  = Int()
    var eventDate = Date()
    var msgDate = Date()
    var strEventDate  = String()
    var strMsgDate = String()
    //MARK: IBOutlets.........................
    
    @IBOutlet weak var btnEnglish: UIButton!
    @IBOutlet weak var btnArabic: UIButton!
    
    @IBOutlet weak var btnBoth: UIButton!
    @IBOutlet weak var lblSelectedLanaguage: UILabel!
    
    @IBOutlet weak var txtEnglish: UITextView!
    @IBOutlet weak var txtArabic: UITextView!
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtDate: BWTextField!
    
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var toolbarObj: UIToolbar!
    
    @IBOutlet weak var lblTitle1 : UILabel!
    @IBOutlet weak var lblTitle2 : UILabel!
    //---------------------------------------------------
    //MARK: View Life Cycle
    //---------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = AppHelper.localizedtext(key: "CancellationMessage")
        print(self.eventCompletedObj)
//        
//        print(self.eventCompletedObj?.event_date)
//        print(self.eventCompletedObj?.message_date_time)
//
        self.btnSubmit.titleLabel?.font = Font.tagViewFont
        self.DefalutDialoguesettingOfPopup()
        if self.eventCompletedObj != nil{
            self.setData(self.eventCompletedObj!)
        }
//        
        self.setTitleView()
        
    }
    //---------------------------------------------------
    //MARK: Button Done Toolbar ACtion
    //---------------------------------------------------
    @IBAction func btnDoneAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    //---------------------------------------------------
    //MARK: UIDatePicker ACtion
    //---------------------------------------------------
    @IBAction func datePickerValueChangedAction(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let isDateAllow = datePicker.date.isBetweeen(date: self.eventDate, andDate: self.msgDate)
        print(isDateAllow)
        if isDateAllow{
            let strDate = dateFormatter.string(from: datePicker.date)
                   print(strDate)
                   self.txtDate.text = strDate
        }else{
            self.showAlertWithCallBack(title: kAppName, message: "You can enter date in between \(self.strMsgDate) and \(self.strEventDate)") {
                self.txtDate.text = ""
                }
        }
    }
    
    func setData(_ eventObj:CompletedEvents){
        
        let strEventDate : String = eventObj.event_date ?? ""
        let strMessageDate : String = eventObj.message_date_time ?? ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let eventDate = dateFormatter.date(from:strEventDate)
        
        let messageDate = dateFormatter.date(from: strMessageDate)
        
        self.eventDate = eventDate ?? Date()
        self.msgDate = messageDate ?? Date()
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let strEventFinalDate  : String = dateFormatter.string(from: eventDate ?? Date())
        
        dateFormatter.dateFormat = "dd/MM/yyy, HH:mm a"
        let strMessageFinalDate : String = dateFormatter.string(from: messageDate ?? Date())
        
        self.strEventDate = strEventFinalDate
        self.strMsgDate = strMessageFinalDate
        
        
        if(eventObj.language == "english"){
          //  self.btnEnglishAction(self.btnEnglish)
            self.lblSelectedLanaguage.text = "English"
            self.isEnglishSelected = true
            self.isArabicSelected = false
            self.isBothLanguageSelected = false
            self.btnArabic.isEnabled = false
            self.btnBoth.isEnabled = false
            self.btnEnglish.isEnabled = true
                       
         }else if(eventObj.language == "arabic"){
            self.lblSelectedLanaguage.text = "Arabic"
            // self.btnArabicAction(self.btnArabic)
             self.isEnglishSelected = false
             self.isArabicSelected = true
             self.isBothLanguageSelected = false
            self.btnArabic.isEnabled = true
            self.btnBoth.isEnabled = false
            self.btnEnglish.isEnabled = false
                     
         }else{
            self.lblSelectedLanaguage.text = "Both"
            self.btnBothAction(self.btnBoth)
            self.isEnglishSelected = false
            self.isArabicSelected = false
            self.isBothLanguageSelected = true
            self.btnArabic.isEnabled = false
            self.btnBoth.isEnabled = true
            self.btnEnglish.isEnabled = false
            
         }
        self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
        
       
    }
    //---------------------------------------------------
    //MARK: UIButton Actions
    //---------------------------------------------------
    
    @IBAction func btnEnglishAction(_ sender: Any) {
        self.isEnglishSelected = true
        self.isArabicSelected = false
        self.isBothLanguageSelected = false
        self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
    }
    @IBAction func btnArabicAction(_ sender: Any) {
        self.isEnglishSelected = false
        self.isArabicSelected = true
        self.isBothLanguageSelected = false
        self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
    }
    
    @IBAction func btnBothAction(_ sender: Any) {
        self.isEnglishSelected = false
        self.isArabicSelected = false
        self.isBothLanguageSelected = true
        self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        if !self.isEnglishSelected && !self.isArabicSelected && !self.isBothLanguageSelected {
            self.showAlert(message: ValidationAlert.kChooseLanguage, title: kAppName)
            
        }else if self.txtArabic.text == "" && self.txtEnglish.text == ""{
            self.showAlert(message: ValidationAlert.kEmptyMessage, title: kAppName)
        }else{
            var chosenLanguage = String()
            
            if self.isEnglishSelected{
                chosenLanguage = "english"
            }else if self.isArabicSelected{
                chosenLanguage = "arabic"
            }else{
                chosenLanguage = "both"
            }
            
            
            
            
            let popUp = PopupDialog(title: kAppName, message: ValidationAlert.kConfirmationCancelMessage)
            let btnOk = CancelButton(title: AppHelper.localizedtext(key: "YES")) {
                self.showActivityIndicatory()
                                       self.controller.sendMessage(EventId: "\(self.eventId)", language: chosenLanguage, MessageDateTime: "\(Date())", SendTo: "to_all", MessageType: "cancelation", EnglishMessage: self.txtEnglish.text ?? "", ArabicMessage: self.txtArabic.text ?? "") { (success, response, message, statusCode) in
                                           
                                           print(response)
                                           if success{
                                               self.hideActivityIndicator()
                                               DispatchQueue.main.async {
                                                   print(success)
                                                   let successVc = self.storyboard?.instantiateViewController(withIdentifier: "EventSuccessVC") as! EventSuccessVC
                                                   successVc.isopenFromCancelledMessage = true
                                                   
                                                   self.navigationController?.pushViewController(successVc, animated: true)
                                               }
                                               
                                               
                                           }else{
                                               DispatchQueue.main.async {
                                                   self.hideActivityIndicator()
                                                   self.showAlert(message: message, title: kAppName)
                                               }
                                           }
                                       }
            }
            let btnCancel = CancelButton(title: AppHelper.localizedtext(key: "NO")) {
                
            }
            popUp.addButtons([btnOk,btnCancel])
                 
                 // Present dialog
                 self.present(popUp, animated: true, completion: nil)
               
            
            
//            let alert = UIAlertController(title: kAppName, message: ValidationAlert.kConfirmationCancelMessage, preferredStyle: .alert)
//            let okAction = UIAlertAction(title: AppHelper.localizedtext(key: "YES"), style: .default) { (action) in
//                self.showActivityIndicatory()
//                         self.controller.sendMessage(EventId: "\(self.eventId)", language: chosenLanguage, MessageDateTime: "\(Date())", SendTo: "to_all", MessageType: "cancelation", EnglishMessage: self.txtEnglish.text ?? "", ArabicMessage: self.txtArabic.text ?? "") { (success, response, message, statusCode) in
//
//                             print(response)
//                             if success{
//                                 self.hideActivityIndicator()
//                                 DispatchQueue.main.async {
//                                     print(success)
//                                     let successVc = self.storyboard?.instantiateViewController(withIdentifier: "EventSuccessVC") as! EventSuccessVC
//                                     successVc.isopenFromCancelledMessage = true
//
//                                     self.navigationController?.pushViewController(successVc, animated: true)
//                                 }
//
//
//                             }else{
//                                 DispatchQueue.main.async {
//                                     self.hideActivityIndicator()
//                                     self.showAlert(message: message, title: kAppName)
//                                 }
//                             }
//                         }
//            }
//            let cancelAction = UIAlertAction(title: AppHelper.localizedtext(key: "NO"), style: .destructive, handler: nil)
//            alert.addAction(okAction)
//            alert.addAction(cancelAction)
//            self.present(alert, animated: true, completion: nil)
//
           
        }
    }
    
    //--------------------------------------------------------------------
    //MARK: Custom  Methods
    //--------------------------------------------------------------------
    func languageSelectedAction(_ english:Bool,arabic:Bool,both:Bool)   {
        
        
        if english{
            self.txtArabic.text = ""
            self.txtArabic.isEditable = false
            self.txtEnglish.isEditable = true
            self.btnEnglish.setImage(imgBlueCircle, for: .normal)
            self.btnArabic.setImage(imgGrayCirle, for: .normal)
            self.btnBoth.setImage(imgGrayCirle, for: .normal)
            self.lblTitle1.isHidden = true
            self.lblTitle2.isHidden = false
            self.txtEnglish.isHidden = false
            self.txtArabic.isHidden = true
            
            
        }
        
        if arabic{
            self.txtEnglish.text = ""
            self.txtArabic.isEditable = true
            self.txtEnglish.isEditable = false
            self.btnEnglish.setImage(imgGrayCirle, for: .normal)
            self.btnArabic.setImage(imgBlueCircle, for: .normal)
            self.btnBoth.setImage(imgGrayCirle, for: .normal)
            self.lblTitle1.isHidden = false
            self.lblTitle2.isHidden = true
            self.txtEnglish.isHidden = true
            self.txtArabic.isHidden = false
        }
        
        if both{
            self.txtArabic.isEditable = true
            self.txtEnglish.isEditable = true
            self.btnEnglish.setImage(imgGrayCirle, for: .normal)
            self.btnArabic.setImage(imgGrayCirle, for: .normal)
            self.btnBoth.setImage(imgBlueCircle, for: .normal)
            
            self.lblTitle1.isHidden = false
            self.lblTitle2.isHidden = false
            self.txtEnglish.isHidden = false
            self.txtArabic.isHidden = false
        }
    }
    //---------------------------------------------------
    //MARK: WebApi Methods
    //---------------------------------------------------
   
    
    //---------------------------------------------------
    //MARK: UItextfield Delegate
    //---------------------------------------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "SecondrayColor")
//        if textField == self.txtDate{
//
//            textField.inputView = self.datePicker
//            textField.inputAccessoryView = self.toolbarObj
//        }
    }
    //---------------------------------------------------
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "lightGray")
    }

    
    //---------------------------------------------------
    //MARK: UItextView Delegate
    //---------------------------------------------------
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.bordrColor = UIColor(named: "SecondrayColor")
    }
   
    //---------------------------------------------------
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.bordrColor = UIColor(named: "lightGray")
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        
        return textView.text.count + (text.count - range.length) <= 250
    }
    
}
extension Date {
    func isBetweeen(date date1: Date, andDate date2: Date) -> Bool {
        return date1.compare(self) == self.compare(date2)
    }
}

//extension UITextView{
//
//  //  @IBDesignable class BWTextView : UITextView{
//     //   private var kAssociationKeyMaxLength: Int = 0
//           @IBInspectable var maxLength: Int {
//                    get {
//                        if let length = objc_getAssociatedObject(self, 0) as? Int {
//                            return length
//                        } else {
//                            return Int.max
//                        }
//                    }
//                    set {
//                        objc_setAssociatedObject(self, 0, newValue, .OBJC_ASSOCIATION_RETAIN)
//
//                       // addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
//
//                        addDoneOnKeyboardWithTarget(self, action:  #selector(checkMaxLength), shouldShowPlaceholder: true)
//                    }
//                }
//
//                @objc func checkMaxLength(txtView: UITextView) {
//                    guard let prospectiveText = self.text,
//                        prospectiveText.count > maxLength
//                        else {
//                            return
//                    }
//
//                    let selection = selectedTextRange
//
//                    let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
//                    let substring = prospectiveText[..<indexEndOfText]
//                    text = String(substring)
//
//                    selectedTextRange = selection
//                }
//   // }
//
//
//}
