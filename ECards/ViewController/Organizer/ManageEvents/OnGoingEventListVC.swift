//
//  OnGoingEventListVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 18/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

class OnGoingEventListVC: AppViewController, UITableViewDataSource, UITableViewDelegate {
   
    //........MARK: Variables......................
    var isOpenFromOngoing = Bool()
      let cellSpacingHeight: CGFloat = 15
    var eventId : Int = Int()
//
//    var arrOngoingEvents = ["Add Attendee", "Send reminder message", "Send Cancellation message","Create Guard","Reports"]
    var arrOngoingEvents = [String]()
    var arrCompletedEvents = [AppHelper.localizedtext(key: "ViewEventDetails"),AppHelper.localizedtext(key: "ThankYou"),AppHelper.localizedtext(key: "Report")]
    
    var eventObj : CompletedEvents?
    
    var controller = OrganizerViewModel()
    //........MARK: IBOutlets......................
    @IBOutlet weak var lblScreenTitle: UILabel!
    
    @IBOutlet weak var tblEvents: UITableView!
    
    //--------------------------------------------------------------------
        //MARK: View life cycle
    //--------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblScreenTitle.text = eventObj?.name ?? ""
//        if self.isOpenFromOngoing{
//
//            self.lblScreenTitle.text = AppHelper.localizedtext(key: "OngoingEvents")
//
//
//        }else{
//            self.lblScreenTitle.text = AppHelper.localizedtext(key: "CompletedEvents")
//        }
       // self.title = self.lblScreenTitle.text
        self.title = AppHelper.localizedtext(key: "QRCodeInvite")
        print(self.eventObj)
        
        print(self.eventObj?.plan_id)
        
        if(self.eventObj?.can_send_thankyou_msg == false || self.eventObj?.Thankyou_message_deliver == true){
            arrCompletedEvents.remove(at: 1);
        }
        
        
        if self.eventObj?.plan_id == 2 {
             //self.arrOngoingEvents = ["Add Attendee", "Send Reminder Message", "Send Cancellation Message","Create Guard","Reports ", "View Event Details"]
            let strArabic : String = AppHelper.localizedtext(key: "AddAttendee")
            self.arrOngoingEvents = [strArabic, AppHelper.localizedtext(key: "SendReminderMsg"),AppHelper.localizedtext(key: "SendCancellationMsg"),AppHelper.localizedtext(key: "Report"),AppHelper.localizedtext(key: "ViewEventDetails"),AppHelper.localizedtext(key: "CreateGuard"),AppHelper.localizedtext(key: "ThankYou")]
            let isActive : Bool = self.eventObj?.is_active ?? false
            if(isActive == true){
                self.arrOngoingEvents.remove(at:5)
            }
            
            //self.arrOngoingEvents = ["Add Attendee", "Send reminder message", "Send Cancellation message","Create Guard","Reports "]
//            if !(self.eventObj?.invitation_message_deliver ?? false) ?? false{
//
//                self.arrOngoingEvents.append(AppHelper.localizedtext(key: "DeleteEvents"))
//            }else{
//
//            }
           
            self.tblEvents.reloadData()
        }else{
           // self.arrOngoingEvents = ["Add Attendee", "Send reminder message", "Send Cancellation message","View Event Details"]
          //  self.arrOngoingEvents = ["Add Attendee","View Event Details"]
           self.arrOngoingEvents = [AppHelper.localizedtext(key: "AddAttendee"), AppHelper.localizedtext(key: "ViewEventDetails")]
//            if self.eventObj?.invitation_message_deliver ?? false{
//               
//            }else{
//                 self.arrOngoingEvents.append(AppHelper.localizedtext(key: "DeleteEvents"))
//            }
        
            if self.isOpenFromOngoing{
                
            }else{
                
               if self.eventObj?.plan_id == 2 {
                self.arrCompletedEvents = [AppHelper.localizedtext(key: "ViewEventDetails"),AppHelper.localizedtext(key: "ThankYou"),AppHelper.localizedtext(key: "Report")]
               }else{
                
                    self.arrCompletedEvents = [AppHelper.localizedtext(key: "ViewEventDetails")]
                }
                
            }
            
            self.tblEvents.reloadData()
        }
        self.setTitleView()
    }
    //--------------------------------------------------------------------
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.callEventDetailApi("\(self.eventId)")
    }
    //--------------------------------------------------------------------
    //MARK: UITableview Datasource
    //--------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isOpenFromOngoing{
            return self.arrOngoingEvents.count
        }
        return self.arrCompletedEvents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OngoingOrCompletedTableCell", for: indexPath)as! OngoingOrCompletedTableCell
        
        
            let img_rightArrow : UIImage = UIImage(named:"rightArrow")!
                      let tintableImage = img_rightArrow.withRenderingMode(.alwaysTemplate)
                         cell.imgRightArrow.image = tintableImage
                      cell.imgRightArrow.tintColor = UIColor(named:"SecondrayColor")
        if self.isOpenFromOngoing{
            cell.lblEventName.text = self.arrOngoingEvents[indexPath.row] ?? ""
          
            if (cell.lblEventName.text == AppHelper.localizedtext(key: "ThankYou")){
                cell.viewBackground?.backgroundColor = UIColor(hexString: "#EEEEEF")
            }else{
                cell.viewBackground?.backgroundColor = UIColor(named:"PrimaryColor")
            }
        }else{
                cell.viewBackground?.backgroundColor = UIColor(named:"PrimaryColor")
                 cell.lblEventName.text = self.arrCompletedEvents[indexPath.row] ?? ""
        }
        return cell
    }
    
    //--------------------------------------------------------------------
    //MARK: UITableview Delegate
    //--------------------------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isOpenFromOngoing{
            
            if self.eventObj?.plan_id == 2 {
                switch indexPath.row{
                case 0:
                    let addInvitees = self.storyboard?.instantiateViewController(withIdentifier: "AddGuestsVC") as! AddGuestsVC
                    addInvitees.eventId = self.eventId
                     addInvitees.attendees = self.eventObj?.attendees ?? []
                    addInvitees.isOpenFromManageEvents = true
                    addInvitees.isOpenFromOngoingList = true
                    addInvitees.eventCompletedObj = self.eventObj
                    self.navigationController?.pushViewController(addInvitees, animated: true)
                    break
                case 1:
                    let reminderVC = self.storyboard?.instantiateViewController(withIdentifier: "SendReminderMesageVC") as! SendReminderMesageVC
                    reminderVC.eventId = self.eventId
                    reminderVC.eventCompletedObj = self.eventObj
                    self.navigationController?.pushViewController(reminderVC, animated: true)
                    break
                case 2:
                    let cancelationVC = self.storyboard?.instantiateViewController(withIdentifier: "SendCancelationMessageVC") as! SendCancelationMessageVC
                    cancelationVC.eventId = self.eventId
                    cancelationVC.eventCompletedObj = self.eventObj
                    self.navigationController?.pushViewController(cancelationVC, animated: true)
                    break
                case 5:
                    let isActive : Bool = self.eventObj?.is_active ?? false
                    if(isActive == true){
                        
                    }else{
                        let craeteGuardVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateGuardVC") as! CreateGuardVC
                                            craeteGuardVC.eventId = self.eventId
                                           self.navigationController?.pushViewController(craeteGuardVC, animated: true)
                    }
                   
                    break
                case 3:
                    let reportsVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportsVC") as! ReportsVC
                    reportsVC.eventId = self.eventId
                    self.navigationController?.pushViewController(reportsVC, animated: true)
                    break
                case 4 :
                    let eventDetails = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailWholeStaticDataVC") as! EventDetailWholeStaticDataVC
                    eventDetails.eventId = self.eventId
                    eventDetails.isOpenFromManageEvents = true
                    //eventDetails.eventObj = self.eventObj
                    self.navigationController?.pushViewController(eventDetails, animated: true)
//                    break
                case 7 :
                    let alert = UIAlertController(title: kAppName, message: ValidationAlert.kDeleteEventMessage, preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: AppHelper.localizedtext(key: "YES"), style: .default) { (action) in
                        self.showActivityIndicatory()
                        self.controller.deleteEventApi(eventId: "\(self.eventId)") { (success, response, message, statusCode) in
                            
                            if success{
                                self.hideActivityIndicator()
                                DispatchQueue.main.async{
                                   // self.showAlert(message: message, title: kAppName)
                                    self.showAlertWithCallBack(title: kAppName, message: message) {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }else{
                                DispatchQueue.main.async {
                                    self.hideActivityIndicator()
                                    self.showAlert(message: message, title: kAppName)
                                }
                            }
                        }
                    }
                    
                    let cancelAction = UIAlertAction(title: AppHelper.localizedtext(key: "NO"), style: .destructive) { (action) in
                        
                    }
                    alert.addAction(okAction)
                    alert.addAction(cancelAction)
                    self.present(alert, animated: true, completion: nil)
                    break
                case 6:
                    break
                    
                default:
                    break
                }
            }else{
                switch indexPath.row{
                case 0:
                    let addInvitees = self.storyboard?.instantiateViewController(withIdentifier: "AddGuestsVC") as! AddGuestsVC
                    addInvitees.eventId = self.eventId
                    addInvitees.attendees = self.eventObj?.attendees ?? []
                    addInvitees.isOpenFromManageEvents = true
                    addInvitees.isOpenFromOngoingList = true
                    self.navigationController?.pushViewController(addInvitees, animated: true)
                    break
//                case 1:
//                    let reminderVC = self.storyboard?.instantiateViewController(withIdentifier: "SendReminderMesageVC") as! SendReminderMesageVC
//                    reminderVC.eventId = self.eventId
//                    reminderVC.eventCompletedObj = self.eventObj
//                    self.navigationController?.pushViewController(reminderVC, animated: true)
//                    break
//                case 1:
//                    let cancelationVC = self.storyboard?.instantiateViewController(withIdentifier: "SendCancelationMessageVC") as! SendCancelationMessageVC
//                    cancelationVC.eventId = self.eventId
//                    cancelationVC.eventCompletedObj = self.eventObj
//                    self.navigationController?.pushViewController(cancelationVC, animated: true)
//                    break
                case 1 :
                    let eventDetails = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailWholeStaticDataVC") as! EventDetailWholeStaticDataVC
                    eventDetails.eventId = self.eventId
                    eventDetails.isOpenFromManageEvents = true
                    self.navigationController?.pushViewController(eventDetails, animated: true)
//                case 3:
//                    let craeteGuardVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateGuardVC") as! CreateGuardVC
//                     craeteGuardVC.eventId = self.eventId
//                    self.navigationController?.pushViewController(craeteGuardVC, animated: true)
//                    break
//                case 4:
//                    let reportsVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportsVC") as! ReportsVC
//                    reportsVC.eventId = self.eventId
//                    self.navigationController?.pushViewController(reportsVC, animated: true)
//                    break
                    
                case 2:
                    let alert = UIAlertController(title: kAppName, message: ValidationAlert.kDeleteEventMessage, preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: AppHelper.localizedtext(key: "YES"), style: .default) { (action) in
                        self.showActivityIndicatory()
                        self.controller.deleteEventApi(eventId: "\(self.eventId)") { (success, response, message, statusCode) in
                            
                            if success{
                                self.hideActivityIndicator()
                                DispatchQueue.main.async{
                                   // self.showAlert(message: message, title: kAppName)
                                    
                                    self.showAlertWithCallBack(title: kAppName, message: message) {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }else{
                                DispatchQueue.main.async {
                                    self.hideActivityIndicator()
                                    self.showAlert(message: message, title: kAppName)
                                }
                            }
                        }
                    }
                    
                    let cancelAction = UIAlertAction(title: AppHelper.localizedtext(key: "NO"), style: .destructive) { (action) in
                        
                    }
                    alert.addAction(okAction)
                    alert.addAction(cancelAction)
                    self.present(alert, animated: true, completion: nil)
                    break
                default:
                    break
                }
            }
            
            
        }else{
            switch indexPath.row{
            case 1:
                if(self.eventObj?.can_send_thankyou_msg == false || self.eventObj?.Thankyou_message_deliver == true){
                    let reportsVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportsVC") as! ReportsVC
                    reportsVC.eventId = self.eventId
                    self.navigationController?.pushViewController(reportsVC, animated: true)
                    break
                }else{
                    let thankyouVC = self.storyboard?.instantiateViewController(withIdentifier: "SendThankYouMessageVC") as! SendThankYouMessageVC
                    thankyouVC.eventId = self.eventId
                    thankyouVC.eventCompletedObj = self.eventObj
                    self.navigationController?.pushViewController(thankyouVC, animated: true)
                    break
                }
              
            case 2:
                let reportsVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportsVC") as! ReportsVC
                reportsVC.eventId = self.eventId
                self.navigationController?.pushViewController(reportsVC, animated: true)
                break
            case 0:
                
                let eventDetails = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailWholeStaticDataVC") as! EventDetailWholeStaticDataVC
                eventDetails.eventId = self.eventId
                eventDetails.isOpenFromManageEvents = true
                self.navigationController?.pushViewController(eventDetails, animated: true)
                
                break
                
                
            default:
                break
            }
        }
    }
    //--------------------------------------------------------------------
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
 
   
   
    //--------------------------------------------------------------------
    //MARK: Web api methods
    //--------------------------------------------------------------------
    
    func  callEventDetailApi(_ eventId : String)  {
        
        controller.getEventDetails(cardId: eventId) { (success, eventDetailsList, message, statusCode) in
            if success{
                DispatchQueue.main.async {
                    
                    print(eventDetailsList)
                    print()
                    
                    let eventDetailObj = eventDetailsList as? EventDetails
//                    self.eventObj = eventDetailObj
//                    print(self.eventObj)
                    let id : Int = eventDetailObj?.id ?? 0
                    self.eventId = id
                    self.eventObj?.attendees = eventDetailObj?.attendees
                    
                    
                }
            }else{
                DispatchQueue.main.async {
                    self.showAlert(message: message, title: kAppName)
                }
            }
        }
    }
    
    

}
