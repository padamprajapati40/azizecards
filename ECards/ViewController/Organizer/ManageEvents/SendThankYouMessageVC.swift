
//
//  SendThankYouMessageVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 24/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

class SendThankYouMessageVC: AppViewController , UITextFieldDelegate,UITextViewDelegate{

    
    //MARK: Variables.........................
    
    var isToAllSelected = Bool()
    let imgBlueCircle = UIImage(named: "circle")
    let imgGrayCirle = UIImage(named: "Circle_Gray")
    var isEnglishSelected = Bool()
    var isArabicSelected = Bool()
    var isBothLanguageSelected = Bool()
    let controller = OrganizerViewModel()
    
    var eventId  = Int()
    var strTo = String()
    //var eventObj : CompletedEvents?
    var eventCompletedObj : CompletedEvents?
    var strEventDate = String()
    var msgDate = Date()
    var msgDateafter = Date()
    var strMsgDate = String()
    var strMsgDateAfter = String()
    //MARK: IBOutlets.........................
    @IBOutlet weak var lblSelectedLanaguage: UILabel!
    @IBOutlet weak var btnEnglish: UIButton!
    @IBOutlet weak var btnArabic: UIButton!
    
    @IBOutlet weak var btnBoth: UIButton!
    
    @IBOutlet weak var txtEnglish: UITextView!
    @IBOutlet weak var txtArabic: UITextView!
    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var btnAttended: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtDate: BWTextField!
    
     @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var toolbarObj: UIToolbar!
    var msgObj : Messages?
    
    @IBOutlet weak var lblTitle1 : UILabel!
    @IBOutlet weak var lblTitle2 : UILabel!
    //---------------------------------------------------
    //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnSubmit.titleLabel?.font = Font.tagViewFont
        
        if(self.eventCompletedObj?.language == "english"){
          //  self.btnEnglishAction(self.btnEnglish)
            self.isEnglishSelected = true
            self.lblSelectedLanaguage.text = "English"
            self.isArabicSelected = false
            self.isBothLanguageSelected = false
            self.btnArabic.isEnabled = false
            self.btnBoth.isEnabled = false
            self.btnEnglish.isEnabled = true
                       
         }else if(self.eventCompletedObj?.language == "arabic"){
            // self.btnArabicAction(self.btnArabic)
             self.isEnglishSelected = false
            self.lblSelectedLanaguage.text = "Arabic"
             self.isArabicSelected = true
             self.isBothLanguageSelected = false
            self.btnArabic.isEnabled = true
            self.btnBoth.isEnabled = false
            self.btnEnglish.isEnabled = false
                     
         }else{
            self.lblSelectedLanaguage.text = "Both"
            self.btnBothAction(self.btnBoth)
            self.isEnglishSelected = false
            self.isArabicSelected = false
            self.isBothLanguageSelected = true
            self.btnArabic.isEnabled = false
            self.btnBoth.isEnabled = true
            self.btnEnglish.isEnabled = false
            
         }
        self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
        
        
        
        
        
        
        let msgList = self.eventCompletedObj?.messages ?? []
        let filterMsg = msgList.filter { $0.message_type == "thankyou" }
        if(filterMsg.count > 0){
            self.msgObj = filterMsg[0]
            if(self.msgObj?.language == "english"){
                self.lblSelectedLanaguage.text = "English"
                self.btnEnglishAction(self.btnEnglish)
                self.txtEnglish.text = self.msgObj?.english_message ?? ""
            }
            else if(self.msgObj?.language == "arabic"){
                self.lblSelectedLanaguage.text = "Arabic"
                self.btnArabicAction(self.btnArabic)
                self.txtArabic.text = self.msgObj?.arabic_message ?? ""
            }else{
                self.lblSelectedLanaguage.text = "Both"
                self.btnBothAction(self.btnBoth)
                self.txtArabic.text = self.msgObj?.arabic_message ?? ""
                self.txtEnglish.text = self.msgObj?.english_message ?? ""
            }
        }
        
        if(self.msgObj?.send_to == "attended"){
            
            self.btnAttendedAction(self.btnAttended);
        }
        else{
             
            self.btnAllAction(self.btnAll)
        }
        
        
        self.title = AppHelper.localizedtext(key: "ThankyouMessage")
        print(self.eventCompletedObj)
        
        let strEventDate : String = self.eventCompletedObj?.event_date ?? ""
        let strMessageDate : String = self.eventCompletedObj?.message_date_time ?? ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let eventDate = dateFormatter.date(from:strEventDate)
        
        let messageDate = dateFormatter.date(from: self.msgObj?.message_date_time ?? "")
        
       // self.eventDate = eventDate ?? Date()
     //   self.msgDate = messageDate ?? Date()
        
      //  dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        dateFormatter.dateFormat = "dd-MMM-yyyy  h:mm a"
        let strEventFinalDate  : String = dateFormatter.string(from: eventDate ?? Date())
        if(messageDate != nil){
             self.txtDate.text = dateFormatter.string(from: messageDate!) ?? ""
        }
       
        self.strEventDate = strEventFinalDate
        
        self.msgDate = Calendar.current.date(byAdding: .day, value: 1, to: messageDate ?? Date())!
               print(msgDate)
        
        self.strMsgDate = dateFormatter.string(from: self.msgDate)
        
        let after3DaysDate = Calendar.current.date(byAdding: .day, value: 3, to: self.msgDate ?? Date())!
        print(after3DaysDate)
        
        self.msgDateafter = after3DaysDate
        
        self.strMsgDateAfter = dateFormatter.string(from: self.msgDateafter)
        
        
        
        let maximumDate = Calendar.current.date(byAdding: .day, value: 7, to: eventDate ?? Date())!
        self.datePicker.maximumDate = maximumDate
        
        let minimumDate = Calendar.current.date(byAdding: .day, value: 0, to: eventDate ?? Date())!
        self.datePicker.minimumDate = minimumDate
        self.setTitleView()
    }
    //---------------------------------------------------
    //MARK: Button Done Toolbar ACtion
    //---------------------------------------------------
    @IBAction func btnDoneAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    //---------------------------------------------------
    //MARK: UIDatePicker ACtion
    //---------------------------------------------------
    @IBAction func datePickerValueChangedAction(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        dateFormatter.dateFormat = "dd-MMM-yyyy  h:mm a"
      
        let strDate = dateFormatter.string(from: datePicker.date)
        print(strDate)
         self.txtDate.text = strDate
    // let isDateAllow = self.compareDatesOFEventandInvitationMsg(self.strEventDate, date2: strDate)
//        let isDateAllow = datePicker.date.isBetweeen(date: self.msgDate, andDate: self.msgDateafter)
//        if isDateAllow{
//            self.txtDate.text = strDate
//        }else{
//            
//            self.showAlertWithCallBack(title: kAppName, message: "You can  send thank you message in between \(self.strMsgDate) and \(self.strMsgDateAfter)  dates.") {
//                self.txtDate.text = ""
//            }
//        }
        
    }
    //---------------------------------------------------
    //MARK: UItextfield Delegate
    //---------------------------------------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "SecondrayColor")
        if textField == self.txtDate{
            
            textField.inputView = self.datePicker
            textField.inputAccessoryView = self.toolbarObj
        }
    }
    //---------------------------------------------------
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "lightGray")
    }
    
    //---------------------------------------------------
    //MARK: UItextView Delegate
    //---------------------------------------------------
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.bordrColor = UIColor(named: "SecondrayColor")
    }
    
    //---------------------------------------------------
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.bordrColor = UIColor(named: "lightGray")
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        
        return textView.text.count + (text.count - range.length) <= 250
    }
    //---------------------------------------------------
    //MARK: UIButton Actions
    //---------------------------------------------------
    @IBAction func btnEnglishAction(_ sender: Any) {
        self.isEnglishSelected = true
        self.isArabicSelected = false
        self.isBothLanguageSelected = false
        self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
    }
    
    @IBAction func btnArabicAction(_ sender: Any) {
        self.isEnglishSelected = false
        self.isArabicSelected = true
        self.isBothLanguageSelected = false
        self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
    }
    
    @IBAction func btnBothAction(_ sender: Any) {
        self.isEnglishSelected = false
        self.isArabicSelected = false
        self.isBothLanguageSelected = true
        self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
        
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        let thankYou_msg_deliver : Bool = self.eventCompletedObj?.Thankyou_message_deliver ?? false
        if (thankYou_msg_deliver == true){
            self.showAlert(message: ValidationAlert.kThankYouMsgDeliverError, title: kAppName)
        }else{
            if !self.isEnglishSelected && !self.isArabicSelected && !self.isBothLanguageSelected {
                       self.showAlert(message: ValidationAlert.kChooseLanguage, title: kAppName)
                       
                   }else if self.txtArabic.text == "" && self.txtEnglish.text == ""{
                       self.showAlert(message: ValidationAlert.kEmptyMessage, title: kAppName)
                   }else if self.txtDate.text == "" {
                       self.showAlert(message: ValidationAlert.kEmptyMessageDate, title: kAppName)
                   }else if self.strTo == ""{
                       self.showAlert(message: ValidationAlert.kEmptyToWhom, title: kAppName)
                   }else{
                       var chosenLanguage = String()
                       
                       if self.isEnglishSelected{
                           chosenLanguage = "english"
                       }else if self.isArabicSelected{
                           chosenLanguage = "arabic"
                       }else{
                           chosenLanguage = "both"
                       }
                       
                       self.showActivityIndicatory()
                       controller.sendMessage(EventId: "\(self.eventId)", language: chosenLanguage, MessageDateTime: self.txtDate.text!, SendTo: self.strTo, MessageType: "thankyou", EnglishMessage: self.txtEnglish.text ?? "", ArabicMessage: self.txtArabic.text ?? "") { (success, response, message, statusCode) in
                           
                           print(response)
                           if success{
                               self.hideActivityIndicator()
                               DispatchQueue.main.async {
                                   print(success)
                                   let successVc = self.storyboard?.instantiateViewController(withIdentifier: "EventSuccessVC") as! EventSuccessVC
                                   successVc.isopenFromThankyouMessage = true
                                   successVc.isOpenScheduled = true
                                   self.navigationController?.pushViewController(successVc, animated: true)
                               }
                               
                               
                           }else{
                               DispatchQueue.main.async {
                                   self.hideActivityIndicator()
                                   self.showAlert(message: message, title: kAppName)
                               }
                           }
                       }
                   }
        }
        
    }
    
    @IBAction func btnAllAction(_ sender: Any) {
        
        
        if self.isToAllSelected{
            self.isToAllSelected = false
            //graybutton
            self.strTo = "attended"
            self.btnAll.setImage(imgGrayCirle, for: .normal)
            self.btnAttended.setImage(imgBlueCircle, for: .normal)
        }else{
            self.isToAllSelected = true
            //blue buton
            self.strTo = "to_all"
            self.btnAll.setImage(imgBlueCircle, for: .normal)
            self.btnAttended.setImage(imgGrayCirle, for: .normal)
            
        }
        
    }
    
    @IBAction func btnAttendedAction(_ sender: Any) {
        
        if self.isToAllSelected{
            self.isToAllSelected = false
            //graybutton
            self.strTo = "attended"
            self.btnAll.setImage(imgGrayCirle, for: .normal)
            self.btnAttended.setImage(imgBlueCircle, for: .normal)
        }else{
            self.isToAllSelected = true
            //blue buton
            self.strTo = "to_all"
            self.btnAll.setImage(imgBlueCircle, for: .normal)
            self.btnAttended.setImage(imgGrayCirle, for: .normal)
            
        }
    }
    //--------------------------------------------------------------------
    //MARK: Custom  Methods
    //--------------------------------------------------------------------
    func languageSelectedAction(_ english:Bool,arabic:Bool,both:Bool)   {
        
        
        if english{
            self.txtArabic.text = ""
            self.txtArabic.isEditable = false
            self.txtEnglish.isEditable = true
            self.btnEnglish.setImage(imgBlueCircle, for: .normal)
            self.btnArabic.setImage(imgGrayCirle, for: .normal)
            self.btnBoth.setImage(imgGrayCirle, for: .normal)
            
            self.lblTitle1.isHidden = true
            self.lblTitle2.isHidden = false
            self.txtEnglish.isHidden = false
            self.txtArabic.isHidden = true
            
        }
        
        if arabic{
            self.txtEnglish.text = ""
            self.txtArabic.isEditable = true
            self.txtEnglish.isEditable = false
            self.btnEnglish.setImage(imgGrayCirle, for: .normal)
            self.btnArabic.setImage(imgBlueCircle, for: .normal)
            self.btnBoth.setImage(imgGrayCirle, for: .normal)
            
            self.lblTitle1.isHidden = false
            self.lblTitle2.isHidden = true
            self.txtEnglish.isHidden = true
            self.txtArabic.isHidden = false
        }
        
        if both{
            self.txtArabic.isEditable = true
            self.txtEnglish.isEditable = true
            self.btnEnglish.setImage(imgGrayCirle, for: .normal)
            self.btnArabic.setImage(imgGrayCirle, for: .normal)
            self.btnBoth.setImage(imgBlueCircle, for: .normal)
            
            self.lblTitle1.isHidden = false
            self.lblTitle2.isHidden = false
            self.txtEnglish.isHidden = false
            self.txtArabic.isHidden = false
        }
    }
    //---------------------------------------------------
    func compareDatesOFEventandInvitationMsg(_ date1 : String, date2 : String) -> Bool{
           
            let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "dd/MM/yyyy"
           
           let dateFormatter1 = DateFormatter()
           dateFormatter1.dateFormat = "dd-MMM-yyyy  h:mm a"
           
           let event_date : Date = dateFormatter.date(from: date1) as? Date ?? Date()
           let invitation_date : Date = dateFormatter1.date(from: date2) as? Date ?? Date()
           
           let result : ComparisonResult = event_date.compare(invitation_date)
           print(result)
           if result == ComparisonResult.orderedSame{
            
           return false
           }else if result == ComparisonResult.orderedDescending{
            
            
               return false
           }else if result == ComparisonResult.orderedAscending{
               return true
           }
           return false
       }
    //---------------------------------------------------
    //MARK: WebApi Methods
    //---------------------------------------------------
  

}
