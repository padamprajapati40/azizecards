//
//  SendReminderMesageVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 20/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit


extension ISO8601DateFormatter {
    convenience init(_ formatOptions: Options, timeZone: TimeZone = TimeZone(secondsFromGMT: 0)!) {
        self.init()
        self.formatOptions = formatOptions
        self.timeZone = timeZone
    }
}

extension Formatter {
    static let iso8601 = ISO8601DateFormatter([.withInternetDateTime, .withFractionalSeconds])
}

extension Date {
    var iso8601: String {
        return Formatter.iso8601.string(from: self)
    }
}

extension String {
    var iso8601: Date? {
        return Formatter.iso8601.date(from: self)
    }
}

class SendReminderMesageVC: AppViewController,UITextFieldDelegate,UITextViewDelegate {

    //MARK: Variables.........................
    @IBOutlet weak var lblSelectedLanaguage: UILabel!
    var isToAllSelected = Bool()
    let imgBlueCircle = UIImage(named: "circle")
    let imgGrayCirle = UIImage(named: "Circle_Gray")
    var isEnglishSelected = Bool()
    var isArabicSelected = Bool()
    var isBothLanguageSelected = Bool()
    var eventId  = Int()
    var strTo = String()
    let controller = OrganizerViewModel()
    //var eventObj : CompletedEvents?
    var eventCompletedObj : CompletedEvents?
    var eventDate = Date()
       var msgDate = Date()
       var strEventDate  = String()
       var strMsgDate = String()
    var msgObj : Messages?
    //MARK: IBOutlets.........................
    
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet weak var btnEnglish: UIButton!
    @IBOutlet weak var btnArabic: UIButton!
    
    @IBOutlet var toolbarObj: UIToolbar!
    @IBOutlet weak var btnBoth: UIButton!
    
    @IBOutlet weak var txtEnglish: UITextView!
    @IBOutlet weak var txtArabic: UITextView!
    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var btnNotRSVpdYet: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtDate: BWTextField!
    
    //---------------------------------------------------
    //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnAll.setImage(imgGrayCirle, for: .normal)
        self.btnNotRSVpdYet.setImage(imgGrayCirle, for: .normal)
        self.btnAll.setImage(imgBlueCircle, for: .selected)
        self.btnNotRSVpdYet.setImage(imgBlueCircle, for: .selected)
        self.title = AppHelper.localizedtext(key: "ReminderMessage")
        self.btnSubmit.titleLabel?.font = Font.tagViewFont
        
        print(self.eventCompletedObj)
        if self.eventCompletedObj != nil{
            self.setData(self.eventCompletedObj!)
        }
        self.setTitleView()
    }
    func setData(_ eventObj:CompletedEvents){
         
         let strEventDate : String = eventObj.event_date ?? ""
         let strMessageDate : String = eventObj.message_date_time ?? ""
         let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
       
        if(strMessageDate == ""){
            self.datePicker.minimumDate = Date();
        }else{
            var invitationDate = strMessageDate.iso8601
            self.datePicker.minimumDate = invitationDate;
        }
       dateFormatter.timeZone = TimeZone(abbreviation: "UTC");
        
      //  dateFormatter.dateFormat = "dd/MM/yyyy"
        let edate =  dateFormatter.date(from: strEventDate)
        let time = self.eventCompletedObj?.from_time ?? ""
        
        self.datePicker.maximumDate = edate;
        if(time != "" && edate != nil){
                        let dateFormatter = DateFormatter()
                        dateFormatter.timeStyle = DateFormatter.Style.short
                        dateFormatter.dateFormat = "h:mm a"
                        let date = dateFormatter.date(from: time);
                        var component = Calendar.current.dateComponents(in: TimeZone.current, from: edate!)
                        var component1 = Calendar.current.dateComponents(in: TimeZone.current, from: date!)
                        component.hour = (component1.hour!)-1;
                        component.minute = component1.minute;
                        component.second = component1.second;
                        let maximumDate = Calendar.current.date(from: component)
                        self.datePicker.maximumDate = maximumDate;
        }
        
        
         
         let messageDate = dateFormatter.date(from: strMessageDate)
         
         self.eventDate = eventDate ?? Date()
         self.msgDate = messageDate ?? Date()
        
        self.msgDate = Calendar.current.date(byAdding: .day, value: 1, to: messageDate ?? Date())! 
        print(msgDate)
         
         dateFormatter.dateFormat = "dd/MM/yyyy"
         let strEventFinalDate  : String = dateFormatter.string(from: eventDate ?? Date())
         
         dateFormatter.dateFormat = "dd/MM/yyy, h:mm a"
         let strMessageFinalDate : String = dateFormatter.string(from: messageDate ?? Date())
         
         self.strEventDate = strEventFinalDate
         self.strMsgDate = strMessageFinalDate
        
        if(eventObj.language == "english"){
            self.lblSelectedLanaguage.text = "English"
          //  self.btnEnglishAction(self.btnEnglish)
            self.isEnglishSelected = true
            self.isArabicSelected = false
            self.isBothLanguageSelected = false
            self.btnArabic.isEnabled = false
            self.btnBoth.isEnabled = false
            self.btnEnglish.isEnabled = true
                       
         }else if(eventObj.language == "arabic"){
            self.lblSelectedLanaguage.text = "Arabic"
            // self.btnArabicAction(self.btnArabic)
             self.isEnglishSelected = false
             self.isArabicSelected = true
             self.isBothLanguageSelected = false
            self.btnArabic.isEnabled = true
            self.btnBoth.isEnabled = false
            self.btnEnglish.isEnabled = false
                     
         }else{
            self.lblSelectedLanaguage.text = "Both"
            self.btnBothAction(self.btnBoth)
            self.isEnglishSelected = false
            self.isArabicSelected = false
            self.isBothLanguageSelected = true
            self.btnArabic.isEnabled = false
            self.btnBoth.isEnabled = true
            self.btnEnglish.isEnabled = false
            
         }
        self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
        
        //....
        
        let msgList = eventObj.messages ?? []
        let filterMsg = msgList.filter { $0.message_type == "reminder" }
        if(filterMsg.count > 0){
              self.msgObj = filterMsg[0]
              if(self.msgObj?.language == "english"){
                self.btnEnglishAction(self.btnEnglish)
                self.isEnglishSelected = true
                self.isArabicSelected = false
                self.isBothLanguageSelected = false
                  
              }
              else if(self.msgObj?.language == "arabic"){
                self.btnArabicAction(self.btnArabic)
                self.isEnglishSelected = false
                self.isArabicSelected = true
                self.isBothLanguageSelected = false
                
              }else{
                 self.btnBothAction(self.btnBoth)
                 self.isEnglishSelected = false
                 self.isArabicSelected = false
                 self.isBothLanguageSelected = true
             }
            self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            
              let messageDate = dateFormatter.date(from: self.msgObj?.message_date_time ?? "")
              dateFormatter.dateFormat = "dd-MMM-yyyy  h:mm a"
            
            if(messageDate != nil){
                self.txtDate.text = dateFormatter.string(from: messageDate!) ?? ""
            }
            
            let to_whom : String = self.msgObj?.send_to ?? ""
            
            if to_whom == "to_all"{
                
                self.isToAllSelected = true
                //blue buton
                self.btnAll.setImage(imgBlueCircle, for: .normal)
                self.btnNotRSVpdYet.setImage(imgGrayCirle, for: .normal)
                self.strTo = "to_all"
                
            }else{
                self.isToAllSelected = false
                //graybutton
                self.btnAll.setImage(imgGrayCirle, for: .normal)
                self.btnNotRSVpdYet.setImage(imgBlueCircle, for: .normal)
                self.strTo = "not_rsvp"
            }
        }
        
        
        
     }
    //---------------------------------------------------
    //MARK: Button Done Toolbar ACtion
    //---------------------------------------------------
    @IBAction func btnDoneAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    //---------------------------------------------------
    //MARK: UIDatePicker ACtion
    //---------------------------------------------------
    @IBAction func datePickerValueChangedAction(_ sender: Any) {
         let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        dateFormatter.dateFormat = "dd-MMM-yyyy  h:mm a"
        let strDate = dateFormatter.string(from: datePicker.date)
         print(strDate)
         self.txtDate.text = strDate
//
//        let isDateAllow = datePicker.date.isBetweeen(date: msgDate, andDate: eventDate)
//
//        let strEventDate : String = self.eventCompletedObj?.event_date ?? ""
//        let strMessageDate : String = self.eventCompletedObj?.message_date_time ?? ""
////
////        let result = compareDates(strMessageDate, time2: strMessageDate)
////        print(result)
//        let strDate = dateFormatter.string(from: datePicker.date)
//                   print(strDate)
//         self.txtDate.text = strDate
//        if isDateAllow{
//            let strDate = dateFormatter.string(from: datePicker.date)
//            print(strDate)
//             self.txtDate.text = strDate
//        }else{
//            self.showAlertWithCallBack(title: kAppName, message: "You can enter date in between \(self.strMsgDate) and \(self.strEventDate)") {
//            self.txtDate.text = ""
//            }
       // }
        
       
    }
    
    func compareDates(_ time1 : String, time2 : String) -> Bool {
           
           let dateFormatter = DateFormatter()
           dateFormatter.timeStyle = DateFormatter.Style.short
           dateFormatter.dateFormat = "h:mm a"
           dateFormatter.timeZone = .current
           let date1 = self.eventCompletedObj?.message_date_time  as? String ?? ""
           let eventDate : Date = dateFormatter.date(from: date1) as? Date ?? Date()
           let date2 =  eventDate.timeIntervalSince1970
           let timezoneOffset =  TimeZone.current.secondsFromGMT()
           let timezoneEpochOffset = (date2 + Double(timezoneOffset))
           let timeZoneOffsetDate = Date(timeIntervalSince1970: timezoneEpochOffset)
           
           let dateTime1 : Date = dateFormatter.date(from: time1) as? Date ?? Date()
           let dateTime2 : Date = dateFormatter.date(from: time2) as? Date ?? Date()
           
           let result : ComparisonResult = dateTime1.compare(dateTime2)
           print(result)
           if result == ComparisonResult.orderedSame{
              return true
           }else if result == ComparisonResult.orderedDescending{
               return false
           }else if result == ComparisonResult.orderedAscending{
               return true
           }else{
               return false
           }
       }
    //---------------------------------------------------
    //MARK: UItextfield Delegate
    //---------------------------------------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "SecondrayColor")
        
        if textField == self.txtDate{
          
            textField.inputView = self.datePicker
            textField.inputAccessoryView = self.toolbarObj
        }
    }
    //---------------------------------------------------
    func textFieldDidEndEditing(_ textField: UITextField) {
         textField.bordrColor = UIColor(named: "lightGray")
    }
    
    //---------------------------------------------------
    //MARK: UItextView Delegate
    //---------------------------------------------------
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.bordrColor = UIColor(named: "SecondrayColor")
        
        
    }
    
    //---------------------------------------------------
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.bordrColor = UIColor(named: "lightGray")
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        
        return textView.text.count + (text.count - range.length) <= 250
    }
    //---------------------------------------------------
    //MARK: UIButton Actions
    //---------------------------------------------------
    @IBAction func btnEnglishAction(_ sender: Any) {
        self.isEnglishSelected = true
        self.isArabicSelected = false
        self.isBothLanguageSelected = false
        self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
    }
    
    @IBAction func btnArabicAction(_ sender: Any) {
        self.isEnglishSelected = false
        self.isArabicSelected = true
        self.isBothLanguageSelected = false
        self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
    }
    
    @IBAction func btnBothAction(_ sender: Any) {
        self.isEnglishSelected = false
        self.isArabicSelected = false
        self.isBothLanguageSelected = true
        self.languageSelectedAction(self.isEnglishSelected, arabic: self.isArabicSelected, both: self.isBothLanguageSelected)
        
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        //ValidationAlert.kReminderMsgDeliverError
        
        let reminder_msg_deliver : Bool = self.eventCompletedObj?.Reminder_message_deliver ?? false
        if (reminder_msg_deliver == true){
            self.showAlert(message: ValidationAlert.kReminderMsgDeliverError, title: kAppName)
        }else{
                  if !self.isEnglishSelected && !self.isArabicSelected && !self.isBothLanguageSelected {
                        self.showAlert(message: ValidationAlert.kChooseLanguage, title: kAppName)
                        
            //        }else if self.txtArabic.text == "" && self.txtEnglish.text == ""{
            //            self.showAlert(message: "Please enter message", title: kAppName)
                    }else if self.txtDate.text == "" {
                        self.showAlert(message: ValidationAlert.kEmptyMessageDate, title: kAppName)
                    }else if self.strTo == ""{
                        self.showAlert(message: ValidationAlert.kEmptyToWhom, title: kAppName)
                    }else{
                        var chosenLanguage = String()
                        
                        if self.isEnglishSelected{
                            chosenLanguage = "english"
                        }else if self.isArabicSelected{
                            chosenLanguage = "arabic"
                        }else{
                            chosenLanguage = "both"
                        }
                        
                        self.showActivityIndicatory()
                        controller.sendMessage(EventId: "\(self.eventId)", language: chosenLanguage, MessageDateTime: self.txtDate.text!, SendTo: self.strTo, MessageType: "reminder", EnglishMessage: "", ArabicMessage: "") { (success, response, message, statusCode) in
                            
                            print(response)
                            if success{
                                self.hideActivityIndicator()
                                DispatchQueue.main.async {
                                    print(success)
                                    let successVc = self.storyboard?.instantiateViewController(withIdentifier: "EventSuccessVC") as! EventSuccessVC
                                    successVc.isopenFromThankyouMessage = true
                                    successVc.isOpenScheduled = true
                                    self.navigationController?.pushViewController(successVc, animated: true)
                                }
                                
                                
                            }else{
                                DispatchQueue.main.async {
                                    self.hideActivityIndicator()
                                    self.showAlert(message: message, title: kAppName)
                                }
                            }
                        }
                    }
        }
      
    }
    
    @IBAction func btnAllAction(_ sender: Any) {
        let btn = sender as!  UIButton
        if(btn == self.btnAll && btn.isSelected == true){
            return;
        }
        self.btnAll.isSelected = !self.btnAll.isSelected
        if(self.btnAll.isSelected == true){
            self.btnAll.isSelected = true
            self.btnNotRSVpdYet.isSelected = false
             self.strTo = "to_all"
        }else{
            self.btnAll.isSelected = false
            self.btnNotRSVpdYet.isSelected = true
            self.strTo = "not_rsvp"
        }
       
        
//        if self.isToAllSelected{
//            //self.isToAllSelected = false
//            //graybutton
//            self.btnAll.isSelected = true
//            self.btnNotRSVpdYet.isSelected = false
//            self.strTo = "not_rsvp"
//        }else{
//            //self.isToAllSelected = true
//            //blue buton
//            self.btnAll.isSelected = true
//            self.btnNotRSVpdYet.isSelected = false
//            self.strTo = "to_all"
//
//        }
        
    }
    
    @IBAction func btnNotRSVPdyetAction(_ sender: Any) {
        let btn = sender as!  UIButton
        
        if(btn == self.btnNotRSVpdYet && btn.isSelected == true){
            return;
        }
              
               self.btnNotRSVpdYet.isSelected = !self.btnNotRSVpdYet.isSelected
               if(self.btnNotRSVpdYet.isSelected == true){
                   self.btnAll.isSelected = false
                   self.btnNotRSVpdYet.isSelected = true
                    self.strTo = "not_rsvp"
               }else{
                   self.btnAll.isSelected = true
                   self.btnNotRSVpdYet.isSelected = false
                   self.strTo = "to_all"
               }
    }
    //--------------------------------------------------------------------
    //MARK: Custom  Methods
    //--------------------------------------------------------------------
    func languageSelectedAction(_ english:Bool,arabic:Bool,both:Bool)   {
        
        
        if english{
//            self.txtArabic.text = ""
//            self.txtArabic.isEditable = false
//            self.txtEnglish.isEditable = true
            self.btnEnglish.setImage(imgBlueCircle, for: .normal)
            self.btnArabic.setImage(imgGrayCirle, for: .normal)
            self.btnBoth.setImage(imgGrayCirle, for: .normal)
            
        }
        
        if arabic{
//            self.txtEnglish.text = ""
//            self.txtArabic.isEditable = true
//            self.txtEnglish.isEditable = false
            self.btnEnglish.setImage(imgGrayCirle, for: .normal)
            self.btnArabic.setImage(imgBlueCircle, for: .normal)
            self.btnBoth.setImage(imgGrayCirle, for: .normal)
        }
        
        if both{
//            self.txtArabic.isEditable = true
//            self.txtEnglish.isEditable = true
            self.btnEnglish.setImage(imgGrayCirle, for: .normal)
            self.btnArabic.setImage(imgGrayCirle, for: .normal)
            self.btnBoth.setImage(imgBlueCircle, for: .normal)
        }
    }
    //---------------------------------------------------
    //MARK: WebApi Methods
    //---------------------------------------------------
}
