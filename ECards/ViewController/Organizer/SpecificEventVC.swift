//
//  SpecificEventVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 02/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

class SpecificEventVC: AppViewController ,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
  
    
       //MARK: Variables.........................
    var eventid: Int = Int()
    var cardList : [Card] = [];
    let controller = OrganizerViewModel()
    @IBOutlet weak var collectionView: UICollectionView!
    var bgImgUrl = String()
     //MARK: IBOutlets.........................

    
    
    //---------------------------------------------------
        //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = AppHelper.localizedtext(key: "QRCodeInvite")
     //   self.tabBarController?.tabBar.isHidden = false
        print("event id is \(self.eventid)")
        
        self.setTitleView()
       
//        UserDefaults.standard.set(eventid, forKey: UserdefaultsKeyName.kEventCardID)
//        UserDefaults.standard.synchronize()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.callGetCardsApi(id: self.eventid)
    }
    

    //---------------------------------------------------
        //MARK: UICollectionview Datasource
    //---------------------------------------------------
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cardList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EventCollectionCell", for: indexPath)as! EventCollectionCell
        cell.lblEventTypeName.text = self.cardList[indexPath.row].title ?? ""
        let imageList = self.cardList[indexPath.row].image as? CardTypesmages
        
//            let imgObj = imageList.first as? CardTypesmages
        
        if imageList?.url == nil{
            
        }else{
            let url = URL(string: imageList?.url  ?? "")!
            cell.imgEventType.setImageFromURL(url: url);
            
        }
        
       
        
        return cell
    }
    
    //---------------------------------------------------
    //MARK: UIcollectionview Delegate
    //---------------------------------------------------
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:(collectionView.frame.size.width-10)/2,height: (collectionView.frame.size.width+60)/2)
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EventCollectionCell", for: indexPath)as! EventCollectionCell
         let imageList = self.cardList[indexPath.row].image as? CardTypesmages
       self.bgImgUrl = imageList?.url ?? ""
//
         let event = self.storyboard?.instantiateViewController(withIdentifier: "CreateEventVC") as! CreateEventVC
        event.hidesBottomBarWhenPushed = false;
        event.bgImgUrl = self.bgImgUrl
        event.cardId = self.cardList[indexPath.row].id ?? 0
        event.eventId = self.eventid ?? 0
        event.eventName = self.cardList[indexPath.row].title ?? ""
        self.navigationController?.pushViewController(event, animated: true)
//
//        let event = self.storyboard?.instantiateViewController(withIdentifier: "CustomizeCardAddVC") as! CustomizeCardAddVC
//
//        event.cardId = self.cardList[indexPath.row].id ?? 0
//        event.eventId = self.eventid ?? 0
//        event.bgImgUrl = imageList?.url ?? ""
//        self.navigationController?.pushViewController(event, animated: true)
        
    }
    
    
    
    //---------------------------------------------------
    //MARK: WebApi Methods
    //---------------------------------------------------
    
    func callGetCardsApi(id : Int) {
        //if(self.cardList.count <= 0){
            self.showActivityIndicatory()
        //}
        controller.getCards(cardId: "\(id)") { (success, data, message, statusCode) in
          DispatchQueue.main.async {
            if success{
                self.hideActivityIndicator()
                self.cardList = data as? [Card] ?? []
                self.collectionView.reloadData()
                print(message)
                print(statusCode)
            }else{
                self.hideActivityIndicator()
                print(message)
                print(statusCode)
            }
            }
        }
    }
}
