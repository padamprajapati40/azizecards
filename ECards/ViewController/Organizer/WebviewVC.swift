//
//  WebviewVC.swift
//  AzizECards
//
//  Created by Dhruva Madhani on 24/01/20.
//  Copyright © 2020 Dhruva Madhani. All rights reserved.
//

import UIKit
import WebKit
import AVKit
import AVFoundation

class WebviewVC: AppViewController , WKNavigationDelegate{

    @IBOutlet weak var btnContinue : UIButton!
    
    var isSampleExcel = Bool()
    var isPreviewUrl = Bool()
    var isOpenForTutorials = Bool()
    var strUrl : String?
    @IBOutlet var webView : WKWebView!
    var EventDetailsList : EventDetails?
    var eventId : Int = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnContinue.titleLabel?.font = Font.tagViewFont

        if self.isSampleExcel{
            self.title = AppHelper.localizedtext(key: "SampleExcel")
            webView.navigationDelegate = self
            let url = URL(string: "http://3.6.10.190/sample_contact_file")!
            webView.load(URLRequest(url: url))
            webView.allowsBackForwardNavigationGestures = true
            
            self.btnContinue.isHidden = true
            
        }else if self.isPreviewUrl{
            self.title = AppHelper.localizedtext(key: "kEventCard")
            webView.navigationDelegate = self
                
             let url = URL(string: strUrl ?? "http://3.6.10.190/sample_contact_file")!
            webView.load(URLRequest(url: url))
            webView.allowsBackForwardNavigationGestures = true
            
            self.btnContinue.isHidden = false
           // let btnContinue =  UIBarButtonItem(title: "Continue", style: .plain, target: self, action: #selector(btnContinueAction(_:)))/
           // self.navigationItem.rightBarButtonItem = btnContinue
        }else if self.isOpenForTutorials{
            
            self.title = AppHelper.localizedtext(key: "kTutorialList")
            webView.navigationDelegate = self
            let url = URL(string: "https://www.youtube.com/watch?v=y9EAD2bXoxA")!
            webView.load(URLRequest(url: url))
            webView.allowsBackForwardNavigationGestures = true
            self.btnContinue.isHidden = true
            
        } else{
            self.title = AppHelper.localizedtext(key: "SampleCard")
            webView.navigationDelegate = self
            let url = URL(string: "http://qrinvitationcards.com/customize_card_demo.mp4")!
            webView.load(URLRequest(url: url))
            webView.allowsBackForwardNavigationGestures = true
            
            self.btnContinue.isHidden = true
        }
        self.setTitleView()
     
    }
    

//    @objc func btnContinueAction(_ sender: Any) {
//        let addGuests = self.storyboard?.instantiateViewController(withIdentifier: "AddGuestsVC")as! AddGuestsVC
//        addGuests.isFromCreateEvent = true
//        addGuests.eventId  = self.eventId
//        addGuests.eventObj = self.EventDetailsList
//        //            addGuests.eventName = self.eventName
//        self.navigationController?.pushViewController(addGuests, animated: true)
//    }
    
    @IBAction func btnContinueAction(_ sender: Any) {
        let addGuests = self.storyboard?.instantiateViewController(withIdentifier: "AddGuestsVC")as! AddGuestsVC
               addGuests.isFromCreateEvent = true
               addGuests.eventId  = self.eventId
               addGuests.eventObj = self.EventDetailsList
               //            addGuests.eventName = self.eventName
               self.navigationController?.pushViewController(addGuests, animated: true)
    }
   
}
