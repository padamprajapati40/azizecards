//
//  PaymentDetailsVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 15/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//





//Display list of users click on link the graph
//Discuss animation updation with @abhishek

import UIKit
import CardScan

protocol PaymentDelegate {
    func paymentCompletedSuccessfully()
}

class PaymentDetailsVC: AppViewController , UITextFieldDelegate{

 //MARK: Variables.........................
    var isFromSummary : Bool = false
    var isRememberCardSelected = Bool()
     var eventObj : EventDetails?
    var isOpenFromManageEventsAddGuest = Bool()
    var delegate:PaymentDelegate?
     let controller = OrganizerViewModel()
    //MARK: IBOutlets.........................
    
    @IBOutlet weak var txtNameOnCard: BWTextField!
    @IBOutlet weak var txtCardNumber: BWTextField!
    @IBOutlet weak var txtMonth: BWTextField!
    @IBOutlet weak var txtYear: BWTextField!
    @IBOutlet weak var txtCVV: BWTextField!
    
     var pickerView: UIPickerView = UIPickerView()
    
    @IBOutlet var toolbarObj: UIToolbar!
    
    @IBOutlet weak var btnPay: UIButton!
    var monthList = ["01","02","03","04","05","06","07","08","09","10","11","12"]
    var yearList = ["2020","2021","2022","2023","2024","2025","2026","2027","2028","2029","2030","2031","2032","2034","2035","2036","2037","2039","2040"]
    @IBOutlet weak var btnRememberCard: UIButton!
    
    //---------------------------------------------------
    //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        if(self.isFromSummary == true){
             self.navigationItem.setHidesBackButton(true, animated: true);
        }
        self.isRememberCardSelected = false
        self.title = AppHelper.localizedtext(key: "PaymentDetails")
        self.txtMonth.inputView = pickerView;
        self.txtYear.inputView = pickerView;
        pickerView.dataSource = self;
        pickerView.delegate = self;
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.setTitleView()
    }
    

    //---------------------------------------------------
    //MARK: WebApi Methods
    //---------------------------------------------------
    

    
    
    //---------------------------------------------------
    //MARK: UIButton Methods
    //---------------------------------------------------
   
    @IBAction func btnScanCardAction(_ sender:Any){
        guard let vc = ScanViewController.createViewController(withDelegate: self) else {
            print("scan view controller not supported on this hardware")
            return
        }
        vc.includeCardImage = true

        self.present(vc, animated: true)
    }
    
    
    @IBAction func btnRememberCardAction(_ sender: Any) {
        
        let imgUnSelected = UIImage(named: "square")
        let imgSelected = UIImage(named: "squareWithTick")
        
        if self.isRememberCardSelected{
            self.isRememberCardSelected = false
            self.btnRememberCard.setImage(imgUnSelected, for: .normal)
        }else{
            self.isRememberCardSelected = true
            self.btnRememberCard.setImage(imgSelected, for: .normal)
        }
    }
    //---------------------------------------------------
    @IBAction func btnPayAction(_ sender: Any) {
        if self.txtNameOnCard.text == ""{
            self.showAlert(message: ErrorMessage.kEmptyNameOnCard, title: kAppName)
        }else if self.txtCardNumber.text == "" {
            self.showAlert(message: ErrorMessage.kEmptyCardNumber, title: kAppName)
        }else if self.txtMonth.text == "" {
            self.showAlert(message: ErrorMessage.kEmptyMonth, title: kAppName)
        }else if self.txtYear.text == "" {
            self.showAlert(message: ErrorMessage.kEmptyYear, title: kAppName)
        }else if self.txtCVV.text == "" {
            self.showAlert(message: ErrorMessage.kEmptyCVV, title: kAppName)
        }else{
              self.navigationItem.setHidesBackButton(true, animated: true)
             let event_id : Int = self.eventObj?.id ?? 0
                        
            controller.updateEventConfirmPayment(event_id: "\(event_id)", params: [:]) { (success, eventObj, message, statusCode) in
                    
                    if success{
                        DispatchQueue.main.async {
                            if self.isOpenFromManageEventsAddGuest == true{
                                          self.showAlertWithCallBack(title: kAppName, message: "Now, you can add new contacts...") {
                                              self.delegate?.paymentCompletedSuccessfully()
                                              self.dismiss(animated: true, completion: nil)
                                            return
                                          }
                                        
                                }
                            let eventObjFinal = eventObj as? EventDetails
                            self.showAlertWithCallBack(title: kAppName, message: message, callBack: {

                                let successVC = self.storyboard?.instantiateViewController(withIdentifier: "EventSuccessVC") as! EventSuccessVC
                                successVC.eventObj = eventObjFinal
                                self.navigationController?.pushViewController(successVC, animated: true)
                            })
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.showAlert(message: message, title: kAppName)
                        }
                    }
                    
                }
                
           // }
            
            
        }
    }
    //--------------------------------------------------------------------
    //MARK: Toolbar actions
    //--------------------------------------------------------------------
    
    @IBAction func btnDoneToolbarAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    //---------------------------------------------------
    //MARK: UItextfield Delegate Methods
    //---------------------------------------------------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "SecondrayColor")
       
        
        if (textField == self.txtMonth){
            self.pickerView.tag = 2000;
            self.pickerView.reloadAllComponents()
        }
        else if(textField == self.txtYear){
            self.pickerView.tag = 2001;
            self.pickerView.reloadAllComponents()
            
        }
    }
    
    //---------------------------------------------------
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "lightGray")
    }
}

extension PaymentDetailsVC : UIPickerViewDelegate,UIPickerViewDataSource{
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       if(pickerView.tag == 2000){
            return monthList.count
        }else{
            return yearList.count
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1;
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        if(pickerView.tag == 2000){
            return monthList[row];
        }else{
            return yearList[row];
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if(pickerView.tag == 2000){
            self.txtMonth.text =  monthList[row];
        }else{
            self.txtYear.text =  yearList[row];
        }
    }
}


extension PaymentDetailsVC : ScanDelegate{
    func userDidSkip(_ scanViewController: ScanViewController) {
        self.dismiss(animated: true)
    }
    
    func userDidCancel(_ scanViewController: ScanViewController) {
        self.dismiss(animated: true)
    }
    
    func userDidScanCard(_ scanViewController: ScanViewController, creditCard: CreditCard) {
        let number = creditCard.number
        let expiryMonth = creditCard.expiryMonth
        let expiryYear = creditCard.expiryYear
        self.txtYear.text = expiryYear;
        self.txtMonth.text = expiryMonth
        self.txtCardNumber.text = number;
    // If you're using Stripe and you include the CardScan/Stripe pod, you
      // can get `STPCardParams` directly from CardScan `CreditCard` objects,
    // which you can use with Stripe's APIs
        //let cardParams = creditCard.cardParams()

    // At this point you have the credit card number and optionally the expiry.
    // You can either tokenize the number or prompt the user for more
    // information (e.g., CVV) before tokenizing.

        self.dismiss(animated: true)
    }
}
