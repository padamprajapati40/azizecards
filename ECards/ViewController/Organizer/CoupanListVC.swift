//
//  CoupanListVC.swift
//  AzizECards
//
//  Created by Padam on 03/04/20.
//  Copyright © 2020 Dhruva Madhani. All rights reserved.
//

import UIKit

protocol CoupanListVCDelegate: class {
    func coupanAppliedData(_ coupanObj:Coupan)
}

class CoupanListCell : UITableViewCell{
    @IBOutlet weak var lblCoupanName: UILabel!
    @IBOutlet weak var lblPercentage: UILabel!
    @IBOutlet weak var lblExpiryDate: UILabel!
}

class CoupanListVC: AppViewController {
    var delegate:CoupanListVCDelegate?
    @IBOutlet weak var tblCoupanList: UITableView!
    @IBOutlet weak var noDataView: UIView!
    var controller = OrganizerViewModel()
    var coupanList : [Coupan] = []
    var eventObj : EventDetails?
    var coupanObj : Coupan?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.callPlanLIstAPi()
        self.noDataView.isHidden = true
       // self.title = "Coupan List"
        self.setTitleView()
        // Do any additional setup after loading the view.
    }
    
    func callPlanLIstAPi()  {
         self.showActivityIndicatory()
        controller.getCoupanCodes(){ (success, planList, message, statusCode) in
            DispatchQueue.main.async {
            if success{
                self.coupanList = planList as? [Coupan] ?? []
                if(self.coupanList.count <= 0){
                    self.noDataView.isHidden = false
                    self.tblCoupanList.isHidden = true
                }else{
                    self.noDataView.isHidden = true
                    self.tblCoupanList.isHidden = false
                }
                print(self.coupanList)
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    self.tblCoupanList.reloadData()
                }
            }else{
                self.noDataView.isHidden = false
                self.tblCoupanList.isHidden = true
                self.hideActivityIndicator()
            }
            }
        }
    }
    
    
}


extension CoupanListVC : UITableViewDataSource,UITableViewDelegate{
    
    //MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.coupanList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CoupanListCell", for: indexPath) as! CoupanListCell
        let coupanObj = self.coupanList[indexPath.row]
        cell.lblCoupanName.text = coupanObj.title ?? ""
        cell.lblPercentage.text = "\(coupanObj.discount ?? 0)%"
        let strMessageDate =  coupanObj.expiry_time ?? ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if(coupanObj.id == self.coupanObj?.id){
            cell.accessoryType = .checkmark;
        }else{
            cell.accessoryType = .none;
        }
        let expiryDate = dateFormatter.date(from: strMessageDate)
        dateFormatter.dateFormat = "dd-MM-YYYY";
        cell.lblExpiryDate.text = dateFormatter.string(from: expiryDate ?? Date())
        
        configureCell(cell: cell, forRowAt: indexPath)
        return cell
    }
    
    func configureCell(cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    //MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let coupanCode = self.coupanList[indexPath.row];
        self.coupanObj = coupanCode
        self.tblCoupanList.reloadData()
        self.navigationController?.popViewController(animated: true);
        self.delegate?.coupanAppliedData(coupanCode)
        //self.applyDiscountCode(coupanID: coupanCode.id ?? 0)
        //self.navigationController?.popViewController(animated: true);
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100;
    }
}
