//
//  MapVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 11/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import GooglePlaces
import GoogleMaps

extension GMSCoordinateBounds {
    convenience init(location: CLLocationCoordinate2D, radiusMeters: CLLocationDistance) {
        let region = MKCoordinateRegion(center: location, latitudinalMeters: radiusMeters, longitudinalMeters: radiusMeters)
        self.init(coordinate: region.northWest, coordinate: region.southEast)
    }
}

extension MKCoordinateRegion {
    var northWest: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: center.latitude + span.latitudeDelta / 2, longitude: center.longitude - span.longitudeDelta / 2)
    }

    var southEast: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: center.latitude - span.latitudeDelta / 2, longitude: center.longitude + span.longitudeDelta / 2)
    }
}

protocol MapVCDelegate: class {
    func sendLocationToCreateEventVC(_ address: String?,latitute:Double,longitude:Double)
}

class MapVC: AppViewController , MKMapViewDelegate, CLLocationManagerDelegate, GMSAutocompleteResultsViewControllerDelegate{

    @IBOutlet weak var mapView: MKMapView!
    
    let locationManager = CLLocationManager()
    let newPin = MKPointAnnotation()
    weak var delegate: MapVCDelegate?
    var address:String = ""
    var latitude:Double = 0.0
    var longitude:Double = 0.0
    
   // var placesClient: GMSPlacesClient!
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?

    func isLocationAccessEnabled() {
        let appLocationMSG = AppHelper.localizedtext(key: "kLocationMsg")
       if CLLocationManager.locationServicesEnabled() {
          switch CLLocationManager.authorizationStatus() {
             case .notDetermined, .restricted, .denied:
                self.showAlertWithCallBack(title: kAppName, message: appLocationMSG) {
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                               return
                           }
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                   
            }
             case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
          }
       } else {
          print("Location services not enabled")
       }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title =  AppHelper.localizedtext(key: "PickLocation") 
      //  self.locationManager.requestAlwaysAuthorization()
        
        
        
      //  placesClient = GMSPlacesClient.shared()
       
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        // Set bounds to map viewable region
        
               
               // New Bounds now set to User Location so choose closest destination to user.
//        let predictBounds =  GMSCoordinateBounds(coordinate: userLocation.coordinate, coordinate: userLocation.coordinate)
        
      
        // Set autocomplete filter to no filter to include all types of destinations.
        let addressFilter = GMSAutocompleteFilter()
        addressFilter.type = .noFilter
               
        //resultsViewController.autocompleteFilter = addressFilter
        
        
        
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        let subView = UIView(frame: CGRect(x: 0, y: 85.0, width: 350.0, height: 45.0))
        
        subView.addSubview((searchController?.searchBar)!)
        view.addSubview(subView)
        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = false
        searchController?.searchBar.placeholder = AppHelper.localizedtext(key: "kSearch")
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        navigationController?.navigationBar.isTranslucent = false
        searchController?.hidesNavigationBarDuringPresentation = false
        self.extendedLayoutIncludesOpaqueBars = true
        self.edgesForExtendedLayout = .top
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        mapView.delegate = self
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        if let coor = mapView.userLocation.location?.coordinate{
            mapView.setCenter(coor, animated: true)
        }
        
        var str_title_save : String = String()
        str_title_save = AppHelper.localizedtext(key: "kSave")
        
        let rightButtonItem = UIBarButtonItem.init(
              title: "\(str_title_save)",
              style: .done,
              target: self,
              action: #selector(rightButtonAction(sender:))
        )

        self.navigationItem.rightBarButtonItem = rightButtonItem
        self.setTitleView()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.isLocationAccessEnabled()
    }
    
    
    @objc func rightButtonAction(sender: UIBarButtonItem){
        self.delegate?.sendLocationToCreateEventVC(self.address, latitute: self.latitude, longitude: self.longitude);
        self.navigationController?.popViewController(animated: true);
    }
    
    
    
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationManager.stopUpdatingLocation()
        let locValue:CLLocationCoordinate2D = locations[locations.count-1].coordinate
        let bounds = GMSCoordinateBounds(location: locations[locations.count-1].coordinate, radiusMeters: 20000)
        resultsViewController?.autocompleteBounds = bounds
        mapView.removeAnnotations(self.mapView.annotations);
        mapView.mapType = MKMapType.standard
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: locValue, span: span)
        mapView.setRegion(region, animated: true)
        self.getLocationAddress(location: CLLocation(latitude: locValue.latitude, longitude:  locValue.longitude))
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {

        if annotation is MKUserLocation  {
            return nil
        }

        let reuseId = "pin"
        var pav:MKPinAnnotationView?
        if (pav == nil)
        {
            pav = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pav?.isDraggable = true
            pav?.canShowCallout = true;
        }
        else
        {
            pav?.annotation = annotation;
        }

        return pav;
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationView.DragState, fromOldState oldState: MKAnnotationView.DragState)
    {
        if (newState == MKAnnotationView.DragState.ending)
        {
            let droppedAt = view.annotation?.coordinate
            print("dropped at : ", droppedAt?.latitude ?? 0.0, droppedAt?.longitude ?? 0.0);
            let selectedLocation = CLLocation(latitude: droppedAt?.latitude ?? 0.0, longitude: droppedAt?.longitude ?? 0.0)
            self.getLocationAddress(location: selectedLocation)
            view.setDragState(.none, animated: true)
        }
        if (newState == .canceling )
        {
            view.setDragState(.none, animated: true)
        }
    }
    
    
    
    func getLocationAddress(location:CLLocation) {
        var geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error)->Void in
            var placemark:CLPlacemark!

            if error == nil && placemarks?.count ?? 0 > 0 {
                placemark = placemarks?[0] as! CLPlacemark

                var addressString : String = ""
                if placemark.isoCountryCode == "TW" /*Address Format in Chinese*/ {
                    if placemark.country != nil {
                        addressString = placemark.country ?? ""
                    }
                    if placemark.subAdministrativeArea != nil {
                        addressString = addressString + (placemark.subAdministrativeArea ?? "") + ", "
                    }
                    if placemark.postalCode != nil {
                        addressString = addressString + (placemark.postalCode ?? "") + " "
                    }
                    if placemark.locality != nil {
                        addressString = addressString + (placemark.locality ?? "")
                    }
                    if placemark.thoroughfare != nil {
                        addressString = addressString + (placemark.thoroughfare ?? "")
                    }
                    if placemark.subThoroughfare != nil {
                        addressString = addressString + (placemark.subThoroughfare ?? "")
                    }
                } else {
                    if placemark.subThoroughfare != nil {
                        addressString = (placemark.subThoroughfare ?? "") + " "
                    }
                    if placemark.thoroughfare != nil {
                        addressString = addressString + (placemark.thoroughfare ?? "") + ", "
                    }
                    if placemark.postalCode != nil {
                        addressString = addressString + (placemark.postalCode ?? "") + " "
                    }
                    if placemark.locality != nil {
                        addressString = addressString + (placemark.locality ?? "") + ", "
                    }
                    if placemark.administrativeArea != nil {
                        addressString = addressString + (placemark.administrativeArea ?? "") + " "
                    }
                    if placemark.country != nil {
                        addressString = addressString + (placemark.country ?? "")
                    }
                }
                self.mapView.removeAnnotations(self.mapView.annotations)
                let annotation = MKPointAnnotation()
                annotation.coordinate = location.coordinate
                annotation.title = addressString
                self.mapView.addAnnotation(annotation)
                self.address = addressString
                self.latitude = location.coordinate.latitude
                self.longitude = location.coordinate.longitude
                
                print(addressString)
            }
        })
    }
    
   //Delegate Methods of autoComplete
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        // Do something with the selected place.
        print("Place name: \(String(describing: place.name))")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        let coordinate = place.coordinate;
        self.latitude = coordinate.latitude;
        self.longitude = coordinate.longitude;
        self.address = place.formattedAddress ?? place.name ?? "";
        self.mapView.removeAnnotations(self.mapView.annotations)
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        annotation.title = self.address
        self.mapView.addAnnotation(annotation)
        self.searchController?.dismiss(animated: true, completion: nil);
        self.mapView.showAnnotations(self.mapView?.annotations ?? [], animated: true);
//        self.delegate?.sendLocationToCreateEventVC(self.address, latitute: self.latitude, longitude: self.longitude);
//        self.navigationController?.popViewController(animated: true);
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        self.showAlert(message: error.localizedDescription, title: kAppName)
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

