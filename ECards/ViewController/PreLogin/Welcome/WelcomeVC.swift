
//
//  WelcomeVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 30/11/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

class WelcomeVC: AppViewController {

     //MARK: IBOutlets.........................
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnCreateAccount: UIButton!
    var isOpenFromManageEvents = Bool()
    
    //MARK: Variables.........................
     let mainStoryboard = storyBoard.kMainStoryboard;
     var eventObj : EventDetails?
    
    //---------------------------------------------------
    //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnLogin.titleLabel?.font = Font.tagViewFont
        self.btnCreateAccount.titleLabel?.font = Font.tagViewFont
        
     //   if(self.isOpenFromManageEvents == true){
            let testUIBarButtonItem = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(WelcomeVC.clickButton))
            self.navigationItem.rightBarButtonItem  = testUIBarButtonItem
      //  }
        self.title = AppHelper.localizedtext(key: "QRCodeInvite")
        self.setTitleView()
    }
    @objc func clickButton(){
        self.dismiss(animated: true, completion: nil)
    }
    //---------------------------------------------------
    //MARK: UIButton Actions
    //---------------------------------------------------
    @IBAction func btnLoginAction(_ sender: Any) {
        let login = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
        login.eventObj = self.eventObj
        login.isOpenFromManageEvents = self.isOpenFromManageEvents
        self.navigationController?.pushViewController(login, animated: true)
    }
    
    @IBAction func btnCreateAccountAction(_ sender: Any) {
        
        let createAccount = mainStoryboard.instantiateViewController(withIdentifier: "CreateAccountVC")as! CreateAccountVC
        createAccount.eventObj = self.eventObj
        self.navigationController?.pushViewController(createAccount, animated: true)
    }
}
