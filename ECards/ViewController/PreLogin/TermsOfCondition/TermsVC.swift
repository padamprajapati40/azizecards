//
//  TermsVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 28/11/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import WebKit

class TermsVC: AppViewController, WKNavigationDelegate {

    
    //MARK: Variable...........................
    
    //MARK: IBOutlets...........................
    @IBOutlet weak var webview: WKWebView!
    
    //---------------------------------------------------
        //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = AppHelper.localizedtext(key: "TermsAndConditions")  
        
        refreshControl.addTarget(self, action: #selector(reloadWebView), for: .valueChanged)
        self.webview.scrollView.addSubview(refreshControl)
        self.navigationController?.navigationBar.tintColor = COLOR.textColor
        self.webview.navigationDelegate = self
        self.navigationItem.hidesBackButton = false
        
        self.loadwebView()
        self.setTitleView()
        
    }
    //---------------------------------------------------
        //MARK: Custom Functions
    //---------------------------------------------------
    func loadwebView() {
        let url = URL(string: StaticPages.kTermsURL)!
        self.webview.load(URLRequest(url: url))
        self.webview.allowsBackForwardNavigationGestures = true
    }
    //---------------------------------------------------
    @objc func reloadWebView()
    {
        refreshControl.endRefreshing()
        //  self.webView1.reload()
        self.webview.reload()
    }

}
