//
//  VerifyOTPAfterForgotPwdVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 18/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

class VerifyOTPAfterForgotPwdVC: AppViewController {

    //MARK: Variables.........................
    
    let controller = AccountViewModel()
    var otp = String()
    
    //MARK: IBOutlets.........................
    
   
    @IBOutlet weak var pinView: SVPinView!
    @IBOutlet weak var btnDone: UIButton!
    
    
    //---------------------------------------------------
    //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = AppHelper.localizedtext(key: "QRCodeInvite")
        self.configurePinView()
        self.btnDone.titleLabel?.font = Font.tagViewFont
        self.setTitleView()
    }
    
    //-------------------------------------------------------
    //MARK: UITextfield delegateMethods
    //-------------------------------------------------------
    
    func configurePinView() {
        
        
        pinView.pinLength = 6
        pinView.secureCharacter = "\u{25CF}"
        pinView.interSpace = 5
//        pinView.textColor = UIColor.white
//        pinView.borderLineColor = UIColor.white
//        pinView.activeBorderLineColor = UIColor.white
        pinView.borderLineThickness = 1
        pinView.shouldSecureText = true
        pinView.allowsWhitespaces = false
        pinView.style = .none
//        pinView.fieldBackgroundColor = UIColor.white.withAlphaComponent(0.3)
//        pinView.activeFieldBackgroundColor = UIColor.white.withAlphaComponent(0.5)
        pinView.fieldCornerRadius = 5
        pinView.activeFieldCornerRadius = 5
        pinView.placeholder = "******"
        //pinView.becomeFirstResponderAtIndex = 0
        
        pinView.font = UIFont.systemFont(ofSize: 15)
        pinView.keyboardType = .phonePad
        pinView.isContentTypeOneTimeCode = true

        pinView.didFinishCallback = { [weak self] pin in
              print("The pin entered is \(pin)")
            self?.pinView.shouldDismissKeyboardOnEmptyFirstField = true
            
            
          }
          
          
    }
    
    
    func didFinishEnteringPin(pin:String) {
        showAlert(title: "Success", message: "The Pin entered is \(pin)")
    }
    
  
    //-------------------------------------------------------
    //MARK: UIbutton Actions
    //-------------------------------------------------------

    @IBAction func btnDoneAction(_ sender: Any) {
        
            let otp = self.pinView.getPin()
            self.showActivityIndicatory()
            
            controller.VerifyForgotOTP(Otp: otp) { (success, response, message, statusCode) in
                print(success)
                print(response)
                print(message)
                print(statusCode)
                DispatchQueue.main.async {
                    if success{
                        self.hideActivityIndicator()
                        let reset = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordVC")as! ResetPasswordVC
                        self.navigationController?.pushViewController(reset, animated: true)

                    }else{
                        self.hideActivityIndicator()
                        self.showAlert(message: message, title: kAppName)
                    }
                }
            }
//            self.hideActivityIndicator()
//            let reset = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordVC")as! ResetPasswordVC
//            self.navigationController?.pushViewController(reset, animated: true)
        }
    }

