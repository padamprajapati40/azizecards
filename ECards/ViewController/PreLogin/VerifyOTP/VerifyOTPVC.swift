//
//  VerifyOTPVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 01/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

protocol VerifyOtpDelegate {
    func verifyPhoneNumber(isVerified : Bool)
}

class VerifyOTPVC:AppViewController , UITextFieldDelegate{

    //MARK: Variables.........................
    var phoneNumber = String()
    var code = String()
    var delegate : VerifyOtpDelegate?
    let controller = AccountViewModel()
    var otp = String()
    
     //MARK: IBOutlets.........................
    @IBOutlet weak var pinView: SVPinView!
    @IBOutlet weak var txt2: BWTextField!
    @IBOutlet weak var txt3: BWTextField!
    @IBOutlet weak var txt4: BWTextField!
    @IBOutlet weak var txt1: BWTextField!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var lblVerification: UILabel!
    @IBOutlet weak var btnResendOTP: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    
    //---------------------------------------------------
    //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnVerify.titleLabel?.font = Font.tagViewFont
        self.configurePinView()
        print(self.phoneNumber)
        print(otp)
        
        self.title = AppHelper.localizedtext(key: "VerifyOTP")  
        
      //  self.showAlert(message: "otp is \(otp)", title: kAppName)
        
//        let close = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(btnCloseAction(_:))) // action:#selector(Class.MethodName) for swift 3
//        self.navigationItem.rightBarButtonItem  = close
//
        self.lblVerification.text = " \(AppHelper.localizedtext(key: "PhoneNumberSentText"))  \(self.phoneNumber)"
        self.setTitleView()
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
       
    }
    //-------------------------------------------------------
    //MARK: UITextfield delegateMethods
    //-------------------------------------------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "SecondrayColor")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "lightGray")
    }
    
     func configurePinView() {
            
            pinView.pinLength = 6
            pinView.secureCharacter = "\u{25CF}"
            pinView.interSpace = 5
    //        pinView.textColor = UIColor.white
    //        pinView.borderLineColor = UIColor.white
    //        pinView.activeBorderLineColor = UIColor.white
            pinView.borderLineThickness = 1
        
            pinView.shouldSecureText = true
            pinView.allowsWhitespaces = false
            pinView.style = .none
    //        pinView.fieldBackgroundColor = UIColor.white.withAlphaComponent(0.3)
    //        pinView.activeFieldBackgroundColor = UIColor.white.withAlphaComponent(0.5)
            pinView.fieldCornerRadius = 5
            pinView.activeFieldCornerRadius = 5
            pinView.placeholder = "******"
            pinView.becomeFirstResponderAtIndex = 0
            pinView.isContentTypeOneTimeCode = true
            pinView.font = UIFont.systemFont(ofSize: 15)
            pinView.keyboardType = .phonePad
//            pinView.pinInputAccessoryView = { () -> UIView in
//                let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
//                doneToolbar.barStyle = UIBarStyle.default
//                let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
//                let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(dismissKeyboard))
//                
//                var items = [UIBarButtonItem]()
//                items.append(flexSpace)
//                items.append(done)
//                
//                doneToolbar.items = items
//                doneToolbar.sizeToFit()
//                return doneToolbar
//            }()
//            
//            pinView.didFinishCallback = didFinishEnteringPin(pin:)
//            pinView.didChangeCallback = { pin in
//                print("The entered pin is \(pin)")
//            }
        }
        
        
        func didFinishEnteringPin(pin:String) {
            showAlert(title: "Success", message: "The Pin entered is \(pin)")
        }
    
    //-------------------------------------------------------
        //MARK: UIbutton Actions
    //-------------------------------------------------------
    @IBAction func btnResendOTPAction(_ sender: Any) {
        self.showActivityIndicatory()
        controller.resendOTP(phoneNumber: self.phoneNumber, countryCode: self.code) { (success, response, message, statusCode) in
            print(success)
            print(response)
            print(message)
            print(statusCode)
            
            DispatchQueue.main.async {
                self.hideActivityIndicator()
                self.showAlert(message: message, title: kAppName)
                let cell  = self.pinView.collectionView.cellForItem(at: IndexPath(row: 0, section: 0))
                let textField = cell?.viewWithTag(101) as? SVPinField
                if(textField != nil){
                    textField?.becomeFirstResponder()
                }
                
                
            }
            
        }
    }
    @IBAction func btnVerifyAction(_ sender: Any) {
     
        let otp = self.pinView.getPin()
            self.showActivityIndicatory()
            controller.VerifyOTP(Otp: otp) { (success, response, message, statusCode) in
                print(success)
                print(response)
                print(message)
                print(statusCode)
                DispatchQueue.main.async {
                    if success{
                        self.hideActivityIndicator()
                       
                        self.showAlertWithCallBack(title: kAppName, message: message) {
                            self.dismiss(animated: true, completion: nil)
                            if let delegate = self.delegate{
                                    delegate.verifyPhoneNumber(isVerified: success)
                            }
                        }
                        
                       // self.navigationController?.popViewController(animated: true)
                       
                    }else{
                        self.hideActivityIndicator()
                        self.showAlert(message: message, title: kAppName)
                    }
                }
            }
//            self.hideActivityIndicator()
//            if let delegate = delegate{
//                delegate.verifyPhoneNumber(isVerified: true)
//                //self.navigationController?.popViewController(animated: true)
//                self.dismiss(animated: true, completion:    nil)
//            }
        }

    
    @objc func btnCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
