//
//  ResetPasswordVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 01/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

class ResetPasswordVC: AppViewController, UITextFieldDelegate {

    //MARK: Variables.........................
    
    let controller = AccountViewModel()
    
    
    //MARK: IBOutlets..................
     @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    
    //-------------------------------------------------------
    //MARK: View Life Cycle
    //-------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnChangePassword.titleLabel?.font = Font.tagViewFont
        self.title = AppHelper.localizedtext(key: "QRCodeInvite")
        //ResetPassword
        self.setTitleView()
    }
    
    //-------------------------------------------------------
    //MARK: UITextfield delegateMethods
    //-------------------------------------------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "SecondrayColor")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "lightGray")
    }
    
    //-------------------------------------------------------
    //MARK: UIbutton Actions
    //-------------------------------------------------------

    @IBAction func btnChangePassword(_ sender: Any) {
        
        if self.txtPassword.text == ""{
             self.showAlert(title: kAppName, message: ValidationAlert.kEmptyPwd)
        }else if (self.txtPassword.text?.length)! < 8{
            self.showAlert(message: ValidationAlert.kMinimumPassword, title: kAppName)
        }else if self.txtConfirmPassword.text == ""{
             self.showAlert(title: kAppName, message: ValidationAlert.kEmptyConfirmPwd)
        }else if self.txtPassword.text != self.txtConfirmPassword.text{
            self.showAlert(message: ValidationAlert.kMatchPwd, title: kAppName)
        }else{
            self.showActivityIndicatory()
            controller.updatePassword(password: self.txtPassword.text!, confirmPassword: self.txtConfirmPassword.text!) { (success, response, message, statusCode) in
                if success{
                    self.hideActivityIndicator()
                    DispatchQueue.main.async {
                        self.showAlertWithCallBack(title: kAppName, message: message) {
                            let vc = self.navigationController?.viewControllers[1] as! LoginVC
                           self.navigationController?.popToViewController(vc, animated: true);
                        }
                       
                    }
                }else{
                    self.hideActivityIndicator()
                    DispatchQueue.main.async {
                        self.showAlert(message: message, title: kAppName)
                    }
                }
            }
            
            self.hideActivityIndicator()
//            let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//            self.navigationController?.pushViewController(login, animated: true)
        }
    }
    @IBAction func btnCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
