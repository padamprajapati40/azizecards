//
//  ForgotPasswordVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 27/11/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import Alamofire

class ForgotPasswordVC: AppViewController , UITextFieldDelegate{

    let controller = AccountViewModel()
    
    //MARK: IBOutlets..................
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblStaticTitle: UILabel!
    @IBOutlet weak var lblStaticSubText: UILabel!
    @IBOutlet weak var txtfldEmailAddress: UITextField!
    
    //-------------------------------------------------------
        //MARK: View Life Cycle
    //-------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = AppHelper.localizedtext(key: "QRCodeInvite")
        self.btnNext.titleLabel?.font = Font.tagViewFont
        self.setTitleView()
    }
    
    //-------------------------------------------------------
    //MARK: UITextfield delegateMethods
    //-------------------------------------------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "SecondrayColor")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "lightGray")
        if textField == self.txtfldEmailAddress{
            let isEmailValid = self.isValidEmail(textStr: textField.text ?? "")
            if isEmailValid{
                      
             }else{
                      
                self.showAlertWithCallBack(title: kAppName, message: ValidationAlert.kEmailInvalidMSG) {
                    self.view.endEditing(true)
                    self.txtfldEmailAddress.text = ""
                }
                    
            }
        }
    }
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        
//        var maxLength = 40
//        var currentString = textField.text
//        return currentString!.length < maxLength
//
//    }
    //-------------------------------------------------------
        //MARK: UIbutton Actions
    //-------------------------------------------------------
    @IBAction func btnNextAction(_ sender: Any) {
        
        if self.txtfldEmailAddress.text == "" {
            self.showAlert(title: kAppName, message: ValidationAlert.kEmptyEmailMSG)
        }else{
           
            self.showActivityIndicatory()
            controller.forgotPassword(email: self.txtfldEmailAddress.text!) { (success, data, message, statusCode) in
                
                if success{
                    
                    print(data)
                    let message : String = message as? String ?? ""
                    
                    DispatchQueue.main.async {
                        self.hideActivityIndicator()
                        self.showAlertWithCallBack(title: kAppName, message: message, callBack: {
                            let verify = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOTPAfterForgotPwdVC")as! VerifyOTPAfterForgotPwdVC
                            self.navigationController?.pushViewController(verify, animated: true)
                        })
                        
                       
                    }
                    
                }else{
                    DispatchQueue.main.async {
                        self.hideActivityIndicator()
                        self.showAlert(message: message, title:kAppName)
                    }
                }
            }
        }
    }
    
    @IBAction func btnCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
