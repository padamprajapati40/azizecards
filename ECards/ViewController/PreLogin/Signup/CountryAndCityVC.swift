//
//  CountryAndCityVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 29/11/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView


protocol CountryOrCityDelegate {
    func countrySelected(_ countryName : String,selectedIndex:Int)
    func citySelected(_ cityName : String,selectedIndex:Int)
    func addCity(cityName : String)
    
}


class CountryAndCityVC: AppViewController , NVActivityIndicatorViewable, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate,AddCityDelegate, UITextFieldDelegate{
   
    
   
    
    //MARK: Variables.........................
    var arrCountry = [[String:Any]]()
    var strCityName = String()
    var isopenFromCity : Bool = Bool()
    var arrList : [String]  = []
    var delegate : CountryOrCityDelegate?
    var countryName : String = ""
    var isSearch = Bool()
    var arrFilter = [String]()
    var arrFilterCountry = [String:Any]()
    var arrData : [String] = []
    var selectedTag = Int()
    var isSelected = Bool()
    //MARK: IBOutlets.........................
    @IBOutlet weak var tblCountryAndCity: UITableView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var searchbarObj: UISearchBar!
    
    //---------------------------------------------------
        //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.arrData = arrList;
        self.searchbarObj.delegate = self
        print(self.strCityName)
        self.tblCountryAndCity.tableFooterView = UIView()
        if isopenFromCity{
            self.searchbarObj.placeholder = AppHelper.localizedtext(key: "CityList")
            self.title = AppHelper.localizedtext(key: "SelectCity")
            
            let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(btnAddAction(_:)))
            self.navigationItem.leftBarButtonItem  = add
            
        }else{
            self.searchbarObj.placeholder = AppHelper.localizedtext(key: "CountryList")
            self.title =  AppHelper.localizedtext(key: "SelectCountry") 
        }
        let selectedLanguage : String = GlobalUtils.getInstance().getApplicationLanguage()
        if selectedLanguage == "arabic"{
            self.searchbarObj.semanticContentAttribute = .forceRightToLeft
            self.searchbarObj.searchTextField.textAlignment = .right
        }else{
            self.searchbarObj.semanticContentAttribute = .forceLeftToRight
            self.searchbarObj.searchTextField.textAlignment = .left
        }
        let close = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(btnCloseAction(_:))) // action:#selector(Class.MethodName) for swift 3
        self.navigationItem.rightBarButtonItem  = close
        self.setTitleView()
      
    }
     //---------------------------------------------------
    //MARK: Tableview Datasource
    //---------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrData.count
       
    }
    //---------------------------------------------------
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryAndCityTableCell", for: indexPath) as! CountryAndCityTableCell
            let name : String  = self.arrData[indexPath.row] as? String ?? ""
        if name == ""{
            self.arrData.remove(at: indexPath.row)
            self.tblCountryAndCity.reloadData()
        }
         cell.lblName.text = name
        
        if self.countryName == name{
                       cell.accessoryType = .checkmark
        }else{
                       cell.accessoryType = .none
        }
      
        return cell
    }
    
    //---------------------------------------------------
    //MARK: Tableview Delegate
    //---------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isopenFromCity{
    
            if let delegate = delegate {
                delegate.citySelected(self.arrData[indexPath.row],selectedIndex: indexPath.row)
            }
           
        }else{
            if let delegate = delegate {
                delegate.countrySelected(self.arrData[indexPath.row], selectedIndex: indexPath.row)
            }
        }
        self.dismiss(animated: true, completion: nil)
        
    }
    //---------------------------------------------------
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    //---------------------------------------------------
    //MARK: UIsearchbar  Delegate
    //---------------------------------------------------
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearch = true;
        let selectedLanguage : String = GlobalUtils.getInstance().getApplicationLanguage()
               if selectedLanguage == "arabic"{
                   self.searchbarObj.semanticContentAttribute = .forceRightToLeft
                self.searchbarObj.searchTextField.textAlignment = .right
               }else{
                   self.searchbarObj.semanticContentAttribute = .forceLeftToRight
                self.searchbarObj.searchTextField.textAlignment = .left
               }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.characters.count == 0 {
            isSearch = false;
            self.arrData = self.arrList
            print(self.arrData)
            self.tblCountryAndCity.reloadData()
        } else {
            print(self.arrData)
            //change below line from self.arrData to self.arrList
            self.arrData = self.arrList.filter { $0.hasPrefix(searchText) }
            print(self.arrData)
            self.tblCountryAndCity.reloadData()
        }
    }
    
    //---------------------------------------------------
    //MARK: API
    //---------------------------------------------------

   @objc func btnCloseAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    //---------------------------------------------------
    @objc func btnAddAction(_ sender: Any) {
           
        self.promptForAnswer()
        self.tblCountryAndCity.isHidden = true
//        let addCity = self.storyboard?.instantiateViewController(withIdentifier: "AddCityVC")as! AddCityVC
//        addCity.delegate = self
//        let navVC = UINavigationController(rootViewController:addCity )
//        self.present(navVC, animated: true, completion: nil);
    }
    //---------------------------------------------------
    //MARK: Custom Delegate
     //---------------------------------------------------
    
    func addCity(cityName: String) {
           
        print(cityName)
        delegate?.addCity(cityName: cityName)
     //   self.dismiss(animated: true, completion: nil)
        
        self.dismiss(animated: true) {
             self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField.text?.length ?? 0 <= 20{
            return true
        }else{
            textField.text == ""
            return false
        }
    }
    
    func promptForAnswer() {
        
        
        
        let ac = UIAlertController(title: "Enter City Name", message: nil, preferredStyle: .alert)
      //  ac.addTextField()

        ac.addTextField { (textfield) in
            textfield.placeholder = "City"
            textfield.autocapitalizationType = .sentences
            textfield.delegate = self
        }
        let submitAction = UIAlertAction(title: "Add", style: .default) { [unowned ac] _ in
            let answer = ac.textFields![0]
            let cityName = answer.text ?? ""
            if(cityName.count > 50){
                self.showAlertWithCallBack(title: kAppName, message: "City name should be maximum 50 character length.") {
                    
                }
            }else{
                self.delegate?.addCity(cityName: answer.text ?? "")
                           self.dismiss(animated: true, completion: nil)
            }
           
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { (action) in
            self.tblCountryAndCity.isHidden = false
        }

        ac.addAction(submitAction)
        ac.addAction(cancelAction)

        present(ac, animated: true)
    }
}
