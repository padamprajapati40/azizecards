//
//  AddCityVC.swift
//  AzizECards
//
//  Created by Dhruva Madhani on 07/02/20.
//  Copyright © 2020 Dhruva Madhani. All rights reserved.
//

import UIKit

protocol AddCityDelegate {
    func addCity(cityName : String)
}

class AddCityVC: AppViewController {

    //..................MARK: Variables.........................
    var delegate : AddCityDelegate?
    
    
    
    //..................MARK: IBOutlets.........................
    @IBOutlet weak var txtCity : UITextField!
    @IBOutlet weak var btnAddCity : UIButton!
    
    
    
    //----------------------------------------------------
        //MARK : view life cycle
    //----------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = AppHelper.localizedtext(key: "QRCodeInvite")
        self.btnAddCity.titleLabel?.font = Font.tagViewFont
        
        let close = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(btnCloseAction(_:)))
        self.navigationItem.rightBarButtonItem  = close
        self.setTitleView()
    }
    
    //----------------------------------------------------
        //MARK: UIButton Action
    //----------------------------------------------------
    @IBAction func btnAddCityAction(_ sender: Any) {
        if self.txtCity.text == "" {
            self.showAlert(message: ValidationAlert.kEmptyCity, title: kAppName)
        }else{
            if let delegate = delegate{
                delegate.addCity(cityName: self.txtCity.text!)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    //----------------------------------------------------
        //MARK: Custom  Methods
    //----------------------------------------------------
    @objc func btnCloseAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
}
