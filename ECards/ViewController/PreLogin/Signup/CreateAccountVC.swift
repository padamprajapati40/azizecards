//
//  CreateAccountVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 27/11/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import CountryPickerView

class CreateAccountVC: AppViewController,NVActivityIndicatorViewable, UITextFieldDelegate , CountryOrCityDelegate, VerifyOtpDelegate{
    var phoneLength : Int = 0
    var photoManager : BWPhotoManager!
    //MARK: Variables.........................
    let controller = AccountViewModel()
    var countryCode = String()
    var arrGender = [AppHelper.localizedtext(key: "kMale"),AppHelper.localizedtext(key: "kFemale")]
    var eventObj : EventDetails?
    var arrayCountry : [[String:Any]] = []
    var selectedTag = Int()
    var isOpenFromEvents = Bool()
    var country_code = String()
     let cp = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
    //MARK: IBOutlets.........................
    
    @IBOutlet weak var viewNumberVerified: UIView!
    @IBOutlet weak var lblPhoneLabel: UILabel!
    @IBOutlet var toolbarObj: UIToolbar!
    @IBOutlet var pickerGender: UIPickerView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var imgTermsClick: UIImageView!
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblStaticCreateAccount: UILabel!
    @IBOutlet weak var lblStaticTerms: UILabel!
    @IBOutlet weak var checkBox: UIButton!
    @IBOutlet weak var txtName: BWTextField!
     @IBOutlet weak var txtLastName: BWTextField!
     @IBOutlet weak var txtEmail: BWTextField!
     @IBOutlet weak var txtPassword: BWTextField!
     @IBOutlet weak var txtRetypePassword: BWTextField!
     @IBOutlet weak var txtCountry: BWTextField!
     @IBOutlet weak var txtCity: BWTextField!
     @IBOutlet weak var txtGender: BWTextField!
     @IBOutlet weak var txtNumber: BWTextField!
     @IBOutlet weak var txtPhoneCode: BWTextField!
    
    @IBOutlet weak var btnTerms: UIButton!
      @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnChooseImg: UIButton!
    @IBOutlet weak var btnCreateAccount: UIButton!
    //---------------------------------------------------
        //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: true);

        self.btnCreateAccount.titleLabel?.font = Font.tagViewFont
        self.txtNumber.smartInsertDeleteType = UITextSmartInsertDeleteType.yes
       // self.txtNumber.maxLength = 0
        self.title = AppHelper.localizedtext(key: "QRCodeInvite")
        self.viewNumberVerified.isHidden = true
        self.pickerGender.backgroundColor = UIColor.white
        self.RoundImage(img: self.imgUser)
        self.txtCountry.text = "Saudi Arabia"
        self.countryCode = "SA"
        //for country code
//        let cp = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
//               self.txtPhoneCode.leftView = cp
//               self.txtPhoneCode.leftViewMode = .always
               cp.dataSource = self
               cp.delegate = self
              self.cp.frame = CGRect(x: 10, y: 0, width: self.txtPhoneCode.frame.size.width, height: self.txtPhoneCode.frame.size.height)
               self.txtPhoneCode.addSubview(self.cp);
               cp.setCountryByCode("+966")
               cp.setCountryByPhoneCode("+966")
               self.country_code = "+966"
        self.cp.centerXAnchor.constraint(equalTo: self.txtPhoneCode.centerXAnchor).isActive = true
               self.cp.centerYAnchor.constraint(equalTo: self.txtPhoneCode.centerYAnchor).isActive = true

               cp.showCountryCodeInView = false
       
          
        let testUIBarButtonItem = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(WelcomeVC.clickButton))
        self.navigationItem.rightBarButtonItem  = testUIBarButtonItem
        self.setTitleView()
        let langName = GlobalUtils.getInstance().getApplicationLanguage()
        if(langName == "arabic"){
            let myString = "بالنقر فوق إنشاء حساب فأنت توافق على موقعنا "
            let myAttribute = [ NSAttributedString.Key.foregroundColor: UIColor.lightGray,NSAttributedString.Key.font:Font.btnFont ]
            var myAttrString = NSMutableAttributedString(string: myString, attributes: myAttribute)
            let myString1 = "الأحكام والشروط";
            let myAttribute1 = [ NSAttributedString.Key.foregroundColor: COLOR.textColor,NSAttributedString.Key.font:Font.btnFont,NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue ] as [NSAttributedString.Key : Any]
            var myAttrString1 = NSAttributedString(string: myString1, attributes: myAttribute1)
            let combination = NSMutableAttributedString()
            combination.append(myAttrString)
            combination.append(myAttrString1)
            self.lblStaticTerms.attributedText = combination
            self.lblStaticTerms.textAlignment = .right
            self.lblStaticTerms.semanticContentAttribute = .forceRightToLeft
            self.lblPhoneLabel.textAlignment = .right
                       self.lblPhoneLabel.semanticContentAttribute = .forceRightToLeft
        }else{
                       let myString = "By Clicking on Create Account you are agreeing to our "
                       let myAttribute = [ NSAttributedString.Key.foregroundColor: UIColor.lightGray,NSAttributedString.Key.font:Font.btnFont ]
                       var myAttrString = NSMutableAttributedString(string: myString, attributes: myAttribute)
                       let myString1 = "Terms and conditions";
                       let myAttribute1 = [ NSAttributedString.Key.foregroundColor: COLOR.textColor,NSAttributedString.Key.font:Font.btnFont,NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue ] as [NSAttributedString.Key : Any]
                       var myAttrString1 = NSAttributedString(string: myString1, attributes: myAttribute1)
                       let combination = NSMutableAttributedString()
                       combination.append(myAttrString)
                       combination.append(myAttrString1)
                       self.lblStaticTerms.semanticContentAttribute = .forceLeftToRight
                       self.lblStaticTerms.textAlignment = .left
                       self.lblStaticTerms.attributedText = combination
                       self.lblPhoneLabel.textAlignment = .left
                       self.lblPhoneLabel.semanticContentAttribute = .forceLeftToRight
        }
        
        
        
    }
    
    func textFieldDesignWithLeftPadding(textField:UITextField, image:UIImage)
    {
    
        
        let btnView = UIButton(frame: CGRect(x: 0, y: 0, width: ((textField.frame.size.height) * 0.7), height: (((textField.frame.size.height)) * 0.7)))
           btnView.setImage(image, for: .normal)
           btnView.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 30)
           textField.rightViewMode = .always
           textField.rightView = btnView
        
    }
    
    @objc func clickButton(){
           self.dismiss(animated: true, completion: nil)
       }
    

    
    
    
    //---------------------------------------------------
        //MARK: UIButton Actions
    //---------------------------------------------------
    @IBAction func btnCreateAccountAction(_ sender: Any) {
        if self.txtName.text == ""{
            self.showAlert(title: kAppName, message: ValidationAlert.kEmptyFirstName)
        }else if self.txtLastName.text == ""{
            self.showAlert(title: kAppName, message: ValidationAlert.kEmptyLastName)
        }else if self.txtEmail.text == ""{
            self.showAlert(title: kAppName, message: ValidationAlert.kEmptyEmailMSG)
        }else if self.txtPassword.text == "" {
            self.showAlert(title: kAppName, message: ValidationAlert.kEmptyPwd)
        }else if (self.txtPassword.text?.length)! < 8{
            self.showAlert(message: ValidationAlert.kMinimumPassword, title: kAppName)
        } else if self.txtRetypePassword.text == "" {
            self.showAlert(title: kAppName, message: ValidationAlert.kEmptyConfirmPwd)
        }else if self.txtPassword.text != self.txtRetypePassword.text{
            self.showAlert(message: ValidationAlert.kMatchPwd, title: kAppName)
        }else if self.txtCountry.text == "" {
            self.showAlert(title: kAppName, message: ValidationAlert.kEmptyCountry)
        }else if self.txtCity.text == ""{
            self.showAlert(title: kAppName, message: ValidationAlert.kEmptyCity)
        }else if self.txtGender.text  == ""{
            self.showAlert(title: kAppName, message: ValidationAlert.kEmptyGender)
        }else if self.country_code == "" {
            self.showAlert(title: kAppName, message: ValidationAlert.kEmptyPhoneCode)
        }else if self.txtNumber.text == "" {
            self.showAlert(title: kAppName, message: ValidationAlert.kEmptyPhoneNumber)
        }else if self.txtNumber.text?.length != phoneLength {
            self.showAlert(title: kAppName, message: ValidationAlert.kInvalidPhone)
        }
        
        else if self.checkBox.isSelected == false {
            self.showAlert(title: kAppName, message: ValidationAlert.kAcceptTermsCondition)
        }else{
            
            var phoneNumber : String = self.txtNumber.text ?? ""
            if self.viewNumberVerified.isHidden{
                self.showActivityIndicatory()
                controller.doSignUp(Name: self.txtName.text!, LastName: self.txtLastName.text!, Email: self.txtEmail.text!, password: self.txtPassword.text!, confirmpassword: self.txtRetypePassword.text!, Country: self.txtCountry.text!, City: self.txtCity.text!, Gender: self.txtGender.text!, PhoneNumber: phoneNumber, isAccepted: true, event_id: self.eventObj?.id, country_code : self.country_code,profilePic:self.imgUser.image) { (success, data, message, statusCode) in
                    
                    DispatchQueue.main.async {
                        if success{
                            self.hideActivityIndicator()
                            let dicData : [String:Any] = data as? [String:Any] ?? [:]
                            DispatchQueue.main.async {
                                let mainStoryboard = storyBoard.kMainStoryboard;
                                let verifyNumber = mainStoryboard.instantiateViewController(withIdentifier: "VerifyOTPVC")as! VerifyOTPVC
                                verifyNumber.delegate = self
                                verifyNumber.phoneNumber = self.txtNumber.text!
                                verifyNumber.code = self.country_code
                                verifyNumber.otp = dicData["otp"] as? String ?? ""
                            
                                self.navigationController?.pushViewController(verifyNumber, animated: true)
                            }
                            
                            
                        }else{
                            self.hideActivityIndicator()
                            DispatchQueue.main.async {
                                self.showAlert(message: message, title: kAppName)
                            }
                            
                        }
                    }
                    
                    
                    
                }
            }else{
                self.dismiss(animated: true, completion: nil)
//                let details = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailWholeStaticDataVC") as! EventDetailWholeStaticDataVC
//                details.eventObj = self.eventObj
//                self.navigationController?.pushViewController(details, animated: true)
            }
           
            
            
        }
    }
    //---------------------------------------------------
    @IBAction func btnTermsAction(_ sender: Any) {
        let terms = self.storyboard?.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
//        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
//        self.navigationItem.backBarButtonItem?.tintColor = .white
        self.navigationController?.pushViewController(terms, animated: true)
    }
    @IBAction func btnLoginAction(_ sender: Any) {
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(loginVC, animated: true)
        
    }
    //---------------------------------------------------
    
    @IBAction func btnCheckBoxAction(_ sender: Any) {
        self.checkBox.isSelected = !self.checkBox.isSelected
    }
    //---------------------------------------------------
    @IBAction func btnChooseImageAction(_ sender: Any) {
        
        photoManager = BWPhotoManager(navigationController: self.navigationController ?? UINavigationController(), mediaType: .image, allowEditing: true, isFromEdit: false, callback: { (chooseImage) in
            self.imgUser.image = chooseImage as! UIImage;
        });
        
    }
    //---------------------------------------------------
    @IBAction func btnDoneAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    //---------------------------------------------------
    //MARK: Custom Delegate Actions
    //---------------------------------------------------
    func countrySelected(_ countryName : String,selectedIndex:Int){
        let filterArray = arrayCountry.filter { $0["name"] as? String ?? "" == countryName } as? [[String:Any]] ?? []
        if(filterArray.count > 0)
        {
            let dicCountry = filterArray[0]
            let name : String = dicCountry["name"] as? String ?? ""
            self.txtCountry.text = name
            let code : String = dicCountry["country_code"] as? String ?? ""
         //   self.txtPhoneCode.text = code
            let countryCode : String = dicCountry["code"] as? String ?? ""
            self.countryCode = countryCode
            
            self.country_code = code
            cp.setCountryByCode("\(country_code)")
            cp.setCountryByPhoneCode("\(country_code)")
            self.txtNumber.delegate = self;
//            if self.country_code.length == 3{
//                self.txtNumber.maxLength = 9
//            }else{
//                self.txtNumber.maxLength = 10
//            }
        }
        let length = self.country_code.length-1
        if(length == 3){
            phoneLength = 9
        }else{
            phoneLength = 10
        }
        self.txtNumber.text = ""
        self.selectedTag = selectedIndex
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == self.txtNumber){
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            let length = self.country_code.length - 1
            return count <= phoneLength
        }
        return true
    }
//---------------------------------------------------
    func citySelected(_ cityName: String,selectedIndex:Int) {
        print(cityName)
        
        self.txtCity.text = cityName
        self.selectedTag = selectedIndex
    }
    //---------------------------------------------------
    func verifyPhoneNumber(isVerified: Bool) {
        
        if isVerified{
            self.viewNumberVerified.isHidden = false
            
         //   self.dismiss(animated: true, completion: nil)
            self.showAlertWithCallBack(title: kAppName, message: ErrorMessage.kPhoneNumberVerify) {
                
                if self.isOpenFromEvents{
                    self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                }else{
                    
                                  //self.dismiss(animated: true, completion: {
                                      let welcomeVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
                                      //  welcomeVC.eventObj = self.EventDetailsList;
                                      let navVC = UINavigationController(rootViewController:welcomeVC )
                                      self.present(navVC, animated: true, completion: nil);
                                //  })
                }
        
            }
        }
    }
    //---------------------------------------------------
    func addCity(cityName : String){
        print(cityName)
        self.txtCity.text = cityName
        
    }
    //---------------------------------------------------
        //MARK: UItextfield Delegate Methods
    //---------------------------------------------------
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        let length = self.country_code.length - 1
        if(length == 3){
             phoneLength = 9
        }else{
             phoneLength = 10
        }
        if textField == self.txtCity{
            if self.txtCountry.text == ""{
                self.showAlert(title: kAppName, message: ValidationAlert.kEmptyCountry)
            }else{
                self.callCityAPI()
            }
            return false
        }
        
        if textField == self.txtCountry{
            self.txtCity.text = ""
            self.callCountryApi()
            return false
        }
        
        if textField == self.txtGender{
            textField.inputView = self.pickerGender
            textField.inputAccessoryView = self.toolbarObj
        }
        return true;
    }
    //-------------------------------------------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "SecondrayColor")
    }
    //-------------------------------------------------------
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "lightGray")
        if textField == self.txtEmail{
            let isEmailValid = self.isValidEmail(textStr: textField.text ?? "")
            if isEmailValid{
                      
             }else{
                      
                self.showAlertWithCallBack(title: kAppName, message: ValidationAlert.kEmailInvalidMSG) {
                    self.view.endEditing(true)
                    self.txtEmail.text = ""
                }
                    
            }
        }
    }
    //-------------------------------------------------------

    //---------------------------------------------------
        //MARK: WebApi Methods
    //---------------------------------------------------
    func callCountryApi()  {
        controller.getCountries { (success, data, message, statusCode) in
            if success{
                print(data)
                let arrCountry = data as? [[String:Any]] ?? []
                
                self.arrayCountry = arrCountry;

                DispatchQueue.main.async {
                    let mainStoryboard = storyBoard.kMainStoryboard;
                    let country = mainStoryboard.instantiateViewController(withIdentifier: "CountryAndCityVC")as! CountryAndCityVC
                    var countryList : [String] = arrCountry.map { (dictionary) -> String in
                        return dictionary["name"] as? String ?? ""
                    }
                    countryList = countryList.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }

                    country.arrList = countryList;
                    country.delegate = self
                    country.countryName = self.txtCountry.text ?? "";
                    if self.txtCountry.text == ""{
                        
                    }else{
                        country.isSelected = true
                        country.selectedTag = self.selectedTag
                    }
                    self.present(UINavigationController(rootViewController: country), animated:true,completion: nil)
                   // self.navigationController?.pushViewController(country, animated: true)
                }

            }
        }
    }
    //---------------------------------------------------
    func callCityAPI()  {
     
        
        controller.getCities(param: ["country":"\(self.countryCode)"]) { (success, data, message, statusCode) in
            
            print(success)
            print(data)
            
            DispatchQueue.main.async {
                let mainStoryboard = storyBoard.kMainStoryboard;
                let city = mainStoryboard.instantiateViewController(withIdentifier: "CountryAndCityVC")as! CountryAndCityVC
                city.delegate = self
                
                city.countryName = self.txtCity.text ?? "";
                city.isopenFromCity = true
                var arrdata = data as? [String] ?? []
                arrdata = arrdata.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
                city.arrList = arrdata
                if self.txtCity.text == ""{
                                       
                }else{
                        city.isSelected = true
                        city.selectedTag = self.selectedTag
                        city.strCityName = self.txtCity.text!
                }
                  self.present(UINavigationController(rootViewController: city), animated:true,completion: nil)
            }
        }
        
    }
}

extension CreateAccountVC: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrGender.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.txtGender.text = self.arrGender[row]
    }
    

    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return self.arrGender[row]
    }
}
extension String {
    var length: Int { return self.count }
}

extension CreateAccountVC : CountryPickerViewDelegate{
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        // Only countryPickerInternal has it's delegate set
        let title = "Selected Country"
        let message = "Name: \(country.name) \nCode: \(country.code) \nPhone: \(country.phoneCode)"
      //  self.txtPhoneCode.text = "\(country.phoneCode)"
        self.country_code = "\(country.phoneCode)"
        self.cp.setCountryByName(country.name)
        self.txtNumber.becomeFirstResponder()
        
      
    }
}

extension CreateAccountVC : CountryPickerViewDataSource{
    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
     //   if countryPickerView.tag == cpvMain.tag && showPreferredCountries.isOn {
            var countries = [Country]()
            ["IN", "QA"].forEach { code in
                if let country = countryPickerView.getCountryByCode(code) {
                    countries.append(country)
                }
            }
            return countries
      //  }
        return []
    }
    
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
//        if countryPickerView.tag == cpvMain.tag && showPreferredCountries.isOn {
//            return "Preferred title"
//        }
        return nil
    }
    
//    func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool {
//      //  return countryPickerView.tag == cpvMain.tag && showOnlyPreferredCountries.isOn
//    }
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select a Country"
    }
        
//    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
////        if countryPickerView.tag == cpvMain.tag {
////            switch searchBarPosition.selectedSegmentIndex {
////            case 0: return .tableViewHeader
////            case 1: return .navigationBar
////            default: return .hidden
////            }
////        }
////        return .tableViewHeader
//    }
    
//    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
//        return countryPickerView.tag == cpvMain.tag && showPhoneCodeInList.isOn
//    }
//
//    func showCountryCodeInList(in countryPickerView: CountryPickerView) -> Bool {
//       return countryPickerView.tag == cpvMain.tag && showCountryCodeInList.isOn
//    }
}
