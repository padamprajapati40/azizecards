//
//  LoginVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 27/11/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import ObjectMapper

class LoginVC: AppViewController, NVActivityIndicatorViewable , UITextFieldDelegate{

    //MARK: Variables..............
    var user = [User]()
    let controller = AccountViewModel()
    var eventObj : EventDetails?
    var isOpenFromManageEvents = Bool()
    
    //MARK: IBOutlets..............
    
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var lblStaticLoginWithEmail: UILabel!
    @IBOutlet weak var lblStaticDontHaveAccount: UILabel!
    
    @IBOutlet weak var txtEmail: BWTextField!
    @IBOutlet weak var txtPassword: BWTextField!
    
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    
    
    
    //-------------------------------------------------------
        //MARK: View Life Cycle
    //-------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnLogin.titleLabel?.font = Font.tagViewFont
      //  self.navigationItem.setHidesBackButton(true, animated: false)
//        self.txtEmail.text = "gunjan.ranparia@gmail.com"
//        self.txtPassword.text = "12345678"
        let testUIBarButtonItem = UIBarButtonItem(image: UIImage(named: "close"), style: .plain, target: self, action: #selector(WelcomeVC.clickButton))
        self.navigationItem.rightBarButtonItem  = testUIBarButtonItem
        self.title = AppHelper.localizedtext(key: "QRCodeInvite")
        self.setTitleView()
        self.navigationItem.setHidesBackButton(true, animated: true);

    }
    
    @objc func clickButton(){
           self.dismiss(animated: true, completion: nil)
       }
    //-------------------------------------------------------
        //MARK: UIButton Actions
    //-------------------------------------------------------
    @IBAction func btnForgotPasswordAction(_ sender: Any) {
        let forgotPwd = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC")as! ForgotPasswordVC
        self.navigationController?.pushViewController(forgotPwd, animated: true)
       // self.present(forgotPwd, animated:true,completion: nil)
    }
    //-------------------------------------------------------
    @IBAction func btnSignupAction(_ sender: Any) {
        
        let signup = self.storyboard?.instantiateViewController(withIdentifier: "CreateAccountVC")as! CreateAccountVC
        signup.isOpenFromEvents = true
        self.navigationController?.pushViewController(signup, animated: true)
    }
    
    //-------------------------------------------------------
    @IBAction func btnLoginAction(_ sender: Any) {
        
          self.view.endEditing(true);
          if(self.txtEmail.text == ""){
            self.showAlert(title: kAppName, message: ValidationAlert.kEmptyEmailMSG)
          }
          else if(!self.isValidEmail(textStr: self.txtEmail?.text ?? "")){
            self.showAlert(title: kAppName, message: ValidationAlert.kEmailInvalidMSG)
          }
          
          else if(self.txtPassword?.text == ""){
            self.showAlert(title: kAppName, message: ValidationAlert.kEmptyPwd)
          }
          else if (self.txtPassword.text?.length)! < 8{
                  self.showAlert(message: ValidationAlert.kMinimumPassword, title: kAppName)
            }
          else{
            let userName = self.txtEmail?.text ?? ""
            let password = self.txtPassword?.text ?? ""
            let userType = "Organizer"
            let eventId = self.eventObj?.id ?? 0
            self.showActivityIndicatory()
            controller.doLogin(userName: userName, password:password,userType:userType,event_id: self.eventObj?.id ) { (success, userObj, message, Int) in
                   if(success == true){
                       DispatchQueue.main.async {
                          self.hideActivityIndicator()
                             if GlobalUtils.getInstance().sessionToken() == ""{
                                          
                                          self.tabBarController?.tabBar.items?[0].title = AppHelper.localizedtext(key: "ManageEvents")
                                          self.tabBarController?.tabBar.items?[1].title = AppHelper.localizedtext(key: "QRCodeInvite")
                                          self.tabBarController?.tabBar.items?[2].title = AppHelper.localizedtext(key: "Profile")
                                          
                               }else{
                                           
                                           self.tabBarController?.tabBar.items?[0].title = AppHelper.localizedtext(key: "ManageEvents")
                                           self.tabBarController?.tabBar.items?[1].title = AppHelper.localizedtext(key: "Home")
                                           self.tabBarController?.tabBar.items?[2].title = AppHelper.localizedtext(key: "Profile")
                                          
                               }
                          self.dismiss(animated: true, completion: nil);
                        
                       }
                   }else{
                       DispatchQueue.main.async {
                           self.hideActivityIndicator()
                           self.showAlert(title: kAppName, message: message)
                       }
                   }
               }
          }

    }
    
    //-------------------------------------------------------
        //MARK: Custom Actions
    //-------------------------------------------------------

    
    //-------------------------------------------------------
    //MARK: UITextfield delegateMethods
    //-------------------------------------------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "SecondrayColor")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.bordrColor = UIColor(named: "lightGray")
    }
    
   
    //-------------------------------------------------------
        //MARK: Api Actions
    //-------------------------------------------------------
    
    func callLoginApi(apiName: String, params: Parameters)  {
        let url = WebServiceUrl.kBASEURL + apiName
        print("\(url)")
        
        let size = CGSize(width: 50, height: 30)//For Activity indicator
        
        startAnimating(size, message: "Please Wait", messageFont: UIFont.systemFont(ofSize: 12.0), type: .ballSpinFadeLoader, color: .black
            , padding: 1.0, displayTimeThreshold: 1, minimumDisplayTime: 0, backgroundColor: .clear, textColor: .black)//For Activity indicator
        
        
        let header : HTTPHeaders = [
            "Content-Type" : "application/x-www-form-urlencoded"
        ]
        
        
        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: header).responseString(completionHandler: { (response:DataResponse<String>) in
            print("\(response.response?.statusCode)")
            switch(response.result) {
                
            case .success(_):
                
                self.stopAnimating()//For Activity indicator
                
                switch response.response!.statusCode{
                case 200:
                    print("Success")
                    
                    let strResponse = response.result.value as? String ?? ""
                    print(strResponse)
                    
                    if strResponse != ""{
                        
                        
                        var dicResponse = self.convertToDictionary(text: strResponse) as? [String:Any] ?? [:]
                        
                        print(dicResponse)
                        if !dicResponse.isEmpty{
                            print("Dictionary is not empty")
                            print(dicResponse["message"])
                            
                            print(dicResponse["data"])
                            
                        }
                    }
                
                    break
                case 400:
                    print("Bad Request")
                   
                    break
                case 401:
                    print("Invalid Token")
                    
                    break
                case 404:
                    print("Data not found")
                   
                    break
                default:
                    print("Default")
                    break
                }
                break
                
            case .failure(_):
                self.stopAnimating()//For Activity indicator
                print(response.result.error)
                break
            }
        })
        
    }
}

