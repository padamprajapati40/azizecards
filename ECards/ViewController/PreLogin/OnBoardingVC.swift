//
//  OnBoardingVC.swift
//  ECards
//
//  Created by Dhruva Madhani on 30/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import Kingfisher
import AVKit
import AVFoundation


class OnBoardingVC: AppViewController,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
   
    //..........MARK: Variables..............
    var controller = AccountViewModel()
    var tutorialList : [Tutorial] = []

    
    var arrImages = [UIImage(named: "onBoarding_1"),UIImage(named: "onBoarding_2"),UIImage(named: "onBoarding_3")]
    var arrTitle = ["Create an event","Invite","Enjoy your event"]
    var arrSubTitle = ["event Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.","Invite Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.","Enjoy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."]

    //..........MARK: IBOutlets..............
    @IBOutlet weak var collectionOnBoarding: UICollectionView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var pgControl: UIPageControl!
    @IBOutlet weak var btnPrevious: UIButton!
    
    
    //---------------------------------------------------
        //MARK: View Life Cycle
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pgControl.numberOfPages = 0
        self.btnNext.tintColor = COLOR.textColor
        self.btnPrevious.tintColor = COLOR.textColor
        self.btnNext.setTitleColor(COLOR.textColor, for: .normal)
        self.btnPrevious.setTitleColor(COLOR.textColor, for: .normal)
        self.collectionOnBoarding.semanticContentAttribute = .forceLeftToRight
      //  self.navigationItem.setHidesBackButton(true, animated: true);
        if(self.pgControl.currentPage == 0){
            self.btnPrevious.isHidden = true;
        }
        else{
            self.btnPrevious.isHidden = false
        }
        self.setTitleView()
        let barButtonItem = UIBarButtonItem(title:AppHelper.localizedtext(key: "kSkip"),
                                                    style: .plain,
                                                    target: self,
                                                    action: #selector(menuButtonTapped))
        barButtonItem.setTitleTextAttributes([NSAttributedString.Key.font: Font.barButtonFont], for: .normal)
         barButtonItem.setTitleTextAttributes([NSAttributedString.Key.font: Font.barButtonFont], for: .selected)
        barButtonItem.setTitleTextAttributes([NSAttributedString.Key.font: Font.barButtonFont], for: .highlighted)
        // Adding button to navigation bar (rightBarButtonItem or leftBarButtonItem)
        self.navigationItem.rightBarButtonItem = barButtonItem
        
    }
    
    @objc fileprivate func menuButtonTapped() {
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        
        let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "EventTabbarController") as? EventTabbarController
        initialViewController?.tabBarItem.title = kAppName
        initialViewController?.selectedIndex = 1
        appDelegate.window?.rootViewController = initialViewController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        
        self.getTutorialApi()
        if(self.pgControl.currentPage == 0){
            self.btnPrevious.isHidden = true;
        }
        else{
            self.btnPrevious.isHidden = false
        }
        
        let str_language : String = GlobalUtils.getInstance().getApplicationLanguage()
        if str_language == "english"{
            self.btnNext.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
            self.btnPrevious.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        }else{
//            self.btnNext.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
//            self.btnPrevious.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
            
             self.btnPrevious.contentHorizontalAlignment = .right
            self.btnNext.contentHorizontalAlignment = .left
        }
       
        
    }
    //---------------------------------------------------
        //MARK: Datasource UICollectionview
    //---------------------------------------------------
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       // return self.arrTitle.count
        return self.tutorialList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OnBoardingCollectionCell", for: indexPath) as! OnBoardingCollectionCell
//        cell.imgBoarding.image = self.arrImages[indexPath.row]
//        cell.lblTitle.text = self.arrTitle[indexPath.row]
//        cell.lblSubTitle.text = self.arrSubTitle[indexPath.row]
        
        cell.lblSubTitle.text = self.tutorialList[indexPath.row].description ?? ""
        cell.lblTitle.text = self.tutorialList[indexPath.row].title ?? ""
        
        let strUrl : String = self.tutorialList[indexPath.row].file?.url ?? ""
        print(strUrl)
        let url = URL(string: strUrl)
      //  print(url!.pathExtension)
        if strUrl != ""{
            
            cell.imgBoarding.isHidden = false
            cell.viewVideo.isHidden = true
            
            if  url?.pathExtension == "png"{
                let url = URL(string: strUrl)
                    cell.imgBoarding.contentMode = .scaleAspectFit
                    cell.imgBoarding.kf.setImage(with: url ,
                    placeholder: nil,
                    options: [.transition(ImageTransition.fade(1))],
                    progressBlock: { receivedSize, totalSize in
                                                            
                        },
                        completionHandler: { image, error, cacheType, imageURL in
                 })
            }else{
                
                cell.imgBoarding.isHidden = true
                cell.viewVideo.isHidden = false
                          
                let videoURL = URL(string: strUrl)
              
                let player = AVPlayer(url: videoURL!) // your video url
                 let playerLayer = AVPlayerLayer(player: player)
                playerLayer.frame = cell.viewVideo.bounds
                cell.viewVideo.layer.addSublayer(playerLayer)
                 player.play()
            }
        }else{
            cell.imgBoarding.isHidden = true
        }
        
        
        self.btnNext.tag = indexPath.row
        self.btnPrevious.tag = indexPath.row
        return cell
    }
    
    //---------------------------------------------------
        //MARK: Delegate UICollectionview
    //---------------------------------------------------
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.collectionOnBoarding.frame.width, height: self.collectionOnBoarding.frame.height)
    }
    
    //---------------------------------------------------
    //MARK: UIbutton actions
    //---------------------------------------------------
    @IBAction func btnSkipAction(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        
        let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "EventTabbarController") as? EventTabbarController
        initialViewController?.tabBarItem.title = kAppName
        initialViewController?.selectedIndex = 1
        appDelegate.window?.rootViewController = initialViewController
        appDelegate.window?.makeKeyAndVisible()
    }
    //---------------------------------------------------
    @IBAction func btnNextAction(_ sender: UIButton) {
        
        print(sender.tag)
        if(self.pgControl.currentPage == self.tutorialList.count - 1){
                let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                
                let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "EventTabbarController") as? EventTabbarController
                initialViewController?.tabBarItem.title = kAppName
                initialViewController?.selectedIndex = 1
                appDelegate.window?.rootViewController = initialViewController
                appDelegate.window?.makeKeyAndVisible()
        }else{
            let collectionBounds = self.collectionOnBoarding.bounds
            let contentOffset = CGFloat(floor(self.collectionOnBoarding.contentOffset.x + collectionOnBoarding.frame.size.width))
            self.moveCollectionToFrame(contentOffset: contentOffset)
        }
        
        
    }
    //------------------------------------------------------
    @IBAction func btnPreviousAction(_ sender: UIButton) {
        
        print(sender.tag)
        let collectionBounds = self.collectionOnBoarding.bounds
        let contentOffset = CGFloat(floor(self.collectionOnBoarding.contentOffset.x - collectionOnBoarding.frame.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
        return;
    }
    
    
    func moveCollectionToFrame(contentOffset : CGFloat) {

        let frame: CGRect = CGRect(x : contentOffset ,y : self.collectionOnBoarding.contentOffset.y ,width : self.collectionOnBoarding.frame.width,height : self.collectionOnBoarding.frame.height)
        self.pgControl.currentPage = Int(contentOffset / self.collectionOnBoarding.frame.size.width)
        self.collectionOnBoarding.scrollRectToVisible(frame, animated: true)
        if(self.pgControl.currentPage == 0){
            self.btnPrevious.isHidden = true;
        }
        
        else{
            self.btnPrevious.isHidden = false
        }
    }
    //---------------------------------------------------------------------
    //MARK: Webapi method
    //---------------------------------------------------------------------
    
    func getTutorialApi()  {
        self.showActivityIndicatory()
        controller.getTutorial { (success, tutorialList, message, statusCode) in
//            self.arrTitle.removeAll()
//            self.arrSubTitle.removeAll()
//            self.arrImages.removeAll()
            
            if success{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    print(tutorialList)
                    self.tutorialList = tutorialList as? [Tutorial] ?? []
                    print(self.tutorialList)
                    self.pgControl.numberOfPages = self.tutorialList.count
                    if self.tutorialList.isEmpty{
                        
                    }else{
                        self.collectionOnBoarding.reloadData()
                    }
                }
            }else{
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                }
            }
            
        }
    }
    
    
    
    //------------------------------------------------------
    //MARK: Vlaue changed action of UIPagecontrol
    //------------------------------------------------------
    @IBAction func pgControlAction(_ sender: Any) {
        let width: Int = Int(self.collectionOnBoarding.frame.size.width)
        let scrollTo = CGPoint(x: width * pgControl.currentPage, y: 0)
        self.collectionOnBoarding.setContentOffset(scrollTo, animated: true)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        
        let pageWidth: CGFloat = self.collectionOnBoarding.frame.size.width
        
        self.pgControl.currentPage = Int(self.collectionOnBoarding.contentOffset.x / pageWidth)
        if(self.pgControl.currentPage == 0){
            self.btnPrevious.isHidden = true;
        }
        else{
            self.btnPrevious.isHidden = false
        }
    }
}
