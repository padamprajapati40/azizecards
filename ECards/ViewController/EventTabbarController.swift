//
//  EventTabbarController.swift
//  ECards
//
//  Created by Dhruva Madhani on 02/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

class EventTabbarController: UITabBarController,UITabBarControllerDelegate {
    override func viewDidLoad() {
        self.delegate = self
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController){
        if(self.selectedIndex == 1){
            let navVC = self.viewControllers?[1] as! UINavigationController;
            navVC.popToRootViewController(animated: false);
        }
        if(self.selectedIndex == 0){
            let navVC = self.viewControllers?[0] as! UINavigationController;
            navVC.popToRootViewController(animated: false);
        }
        if(self.selectedIndex == 2){
            let navVC = self.viewControllers?[2] as! UINavigationController;
            navVC.popToRootViewController(animated: false);
        }
    }
}
