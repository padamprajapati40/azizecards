//
//  UIAlert.swift
//  UNMASK
//
//  Created by Reactive Space on 2/27/18.
//  Copyright © 2018 Owais Akram. All rights reserved.
//

import Foundation
import UIKit
import PopupDialog
// for showing alert

@IBDesignable class PaddingLabel: UILabel {

    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 0.0
    @IBInspectable var leftInset: CGFloat = 7.0
    @IBInspectable var rightInset: CGFloat = 7.0

    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }

    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }
}

extension UIViewController{
    
    func showAlert(message: String, title: String)
    {
       let popUp = PopupDialog(title: title, message: message)
    
        let btnOk = CancelButton(title: AppHelper.localizedtext(key: "OK")) {
            print("You okied the car dialog.")
        }
        popUp.addButtons([btnOk])
        
        // Present dialog
        self.present(popUp, animated: true, completion: nil)
    }
    
    
    
    func showErrorMsg(message:String)
    {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
//        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
//        loadingIndicator.hidesWhenStopped = true
//        loadingIndicator.style = .gray
//        loadingIndicator.startAnimating();
//
//        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        // set the timer
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.hideIndicator), userInfo: nil, repeats: false)
    }
    
    
    
    func showIndicator(message:String)
    {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
                let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                loadingIndicator.hidesWhenStopped = true
                loadingIndicator.style = .gray
                loadingIndicator.startAnimating();
        
               alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        // set the timer
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.hideIndicator), userInfo: nil, repeats: false)
    }
    
    @objc func hideIndicator()
    {
        dismiss(animated: false, completion: nil)
        
    }
}
