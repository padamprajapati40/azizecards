 //
//  WSHandler.swift
//
//
//  Created by Bluewhale Apps on 26/06/19.
//  Copyright © 2019 Bluewhale Apps. All rights reserved.

import Alamofire
import Foundation
 
 extension Date {
     func currentTimeMillis() -> Int64 {
         return Int64(self.timeIntervalSince1970 * 1000)
     }
 }
 
 extension Data {
    
    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
 }
 
extension Dictionary {
    func merge(_ dict: Dictionary<Key,Value>) -> Dictionary<Key,Value> {
        var mutableCopy = self
        for (key, value) in dict {
            mutableCopy[key] = value
        }
        return mutableCopy
    }
    mutating func lowercaseKeys() {
           for key in self.keys {
               let str = (key as! String).capitalizingFirstLetter()
               self[str as! Key] = self.removeValue(forKey:key)
           }
    }
}
 
extension String {
    
    
    func getCountryPhonceCode () -> String
    {
        var countryDictionary  = ["AF":"93",
                                  "AL":"355",
                                  "DZ":"213",
                                  "AS":"1",
                                  "AD":"376",
                                  "AO":"244",
                                  "AI":"1",
                                  "AG":"1",
                                  "AR":"54",
                                  "AM":"374",
                                  "AW":"297",
                                  "AU":"61",
                                  "AT":"43",
                                  "AZ":"994",
                                  "BS":"1",
                                  "BH":"973",
                                  "BD":"880",
                                  "BB":"1",
                                  "BY":"375",
                                  "BE":"32",
                                  "BZ":"501",
                                  "BJ":"229",
                                  "BM":"1",
                                  "BT":"975",
                                  "BA":"387",
                                  "BW":"267",
                                  "BR":"55",
                                  "IO":"246",
                                  "BG":"359",
                                  "BF":"226",
                                  "BI":"257",
                                  "KH":"855",
                                  "CM":"237",
                                  "CA":"1",
                                  "CV":"238",
                                  "KY":"345",
                                  "CF":"236",
                                  "TD":"235",
                                  "CL":"56",
                                  "CN":"86",
                                  "CX":"61",
                                  "CO":"57",
                                  "KM":"269",
                                  "CG":"242",
                                  "CK":"682",
                                  "CR":"506",
                                  "HR":"385",
                                  "CU":"53",
                                  "CY":"537",
                                  "CZ":"420",
                                  "DK":"45",
                                  "DJ":"253",
                                  "DM":"1",
                                  "DO":"1",
                                  "EC":"593",
                                  "EG":"20",
                                  "SV":"503",
                                  "GQ":"240",
                                  "ER":"291",
                                  "EE":"372",
                                  "ET":"251",
                                  "FO":"298",
                                  "FJ":"679",
                                  "FI":"358",
                                  "FR":"33",
                                  "GF":"594",
                                  "PF":"689",
                                  "GA":"241",
                                  "GM":"220",
                                  "GE":"995",
                                  "DE":"49",
                                  "GH":"233",
                                  "GI":"350",
                                  "GR":"30",
                                  "GL":"299",
                                  "GD":"1",
                                  "GP":"590",
                                  "GU":"1",
                                  "GT":"502",
                                  "GN":"224",
                                  "GW":"245",
                                  "GY":"595",
                                  "HT":"509",
                                  "HN":"504",
                                  "HU":"36",
                                  "IS":"354",
                                  "IN":"91",
                                  "ID":"62",
                                  "IQ":"964",
                                  "IE":"353",
                                  "IL":"972",
                                  "IT":"39",
                                  "JM":"1",
                                  "JP":"81",
                                  "JO":"962",
                                  "KZ":"77",
                                  "KE":"254",
                                  "KI":"686",
                                  "KW":"965",
                                  "KG":"996",
                                  "LV":"371",
                                  "LB":"961",
                                  "LS":"266",
                                  "LR":"231",
                                  "LI":"423",
                                  "LT":"370",
                                  "LU":"352",
                                  "MG":"261",
                                  "MW":"265",
                                  "MY":"60",
                                  "MV":"960",
                                  "ML":"223",
                                  "MT":"356",
                                  "MH":"692",
                                  "MQ":"596",
                                  "MR":"222",
                                  "MU":"230",
                                  "YT":"262",
                                  "MX":"52",
                                  "MC":"377",
                                  "MN":"976",
                                  "ME":"382",
                                  "MS":"1",
                                  "MA":"212",
                                  "MM":"95",
                                  "NA":"264",
                                  "NR":"674",
                                  "NP":"977",
                                  "NL":"31",
                                  "AN":"599",
                                  "NC":"687",
                                  "NZ":"64",
                                  "NI":"505",
                                  "NE":"227",
                                  "NG":"234",
                                  "NU":"683",
                                  "NF":"672",
                                  "MP":"1",
                                  "NO":"47",
                                  "OM":"968",
                                  "PK":"92",
                                  "PW":"680",
                                  "PA":"507",
                                  "PG":"675",
                                  "PY":"595",
                                  "PE":"51",
                                  "PH":"63",
                                  "PL":"48",
                                  "PT":"351",
                                  "PR":"1",
                                  "QA":"974",
                                  "RO":"40",
                                  "RW":"250",
                                  "WS":"685",
                                  "SM":"378",
                                  "SA":"966",
                                  "SN":"221",
                                  "RS":"381",
                                  "SC":"248",
                                  "SL":"232",
                                  "SG":"65",
                                  "SK":"421",
                                  "SI":"386",
                                  "SB":"677",
                                  "ZA":"27",
                                  "GS":"500",
                                  "ES":"34",
                                  "LK":"94",
                                  "SD":"249",
                                  "SR":"597",
                                  "SZ":"268",
                                  "SE":"46",
                                  "CH":"41",
                                  "TJ":"992",
                                  "TH":"66",
                                  "TG":"228",
                                  "TK":"690",
                                  "TO":"676",
                                  "TT":"1",
                                  "TN":"216",
                                  "TR":"90",
                                  "TM":"993",
                                  "TC":"1",
                                  "TV":"688",
                                  "UG":"256",
                                  "UA":"380",
                                  "AE":"971",
                                  "GB":"44",
                                  "US":"1",
                                  "UY":"598",
                                  "UZ":"998",
                                  "VU":"678",
                                  "WF":"681",
                                  "YE":"967",
                                  "ZM":"260",
                                  "ZW":"263",
                                  "BO":"591",
                                  "BN":"673",
                                  "CC":"61",
                                  "CD":"243",
                                  "CI":"225",
                                  "FK":"500",
                                  "GG":"44",
                                  "VA":"379",
                                  "HK":"852",
                                  "IR":"98",
                                  "IM":"44",
                                  "JE":"44",
                                  "KP":"850",
                                  "KR":"82",
                                  "LA":"856",
                                  "LY":"218",
                                  "MO":"853",
                                  "MK":"389",
                                  "FM":"691",
                                  "MD":"373",
                                  "MZ":"258",
                                  "PS":"970",
                                  "PN":"872",
                                  "RE":"262",
                                  "RU":"7",
                                  "BL":"590",
                                  "SH":"290",
                                  "KN":"1",
                                  "LC":"1",
                                  "MF":"590",
                                  "PM":"508",
                                  "VC":"1",
                                  "ST":"239",
                                  "SO":"252",
                                  "SJ":"47",
                                  "SY":"963",
                                  "TW":"886",
                                  "TZ":"255",
                                  "TL":"670",
                                  "VE":"58",
                                  "VN":"84",
                                  "VG":"284",
                                  "VI":"340"]
        if countryDictionary[self] != nil {
            return countryDictionary[self]!
        }

        else {
            return ""
        }

    }
    
     
    func capitalizingFirstLetter() -> String {
           return prefix(1).uppercased() + self.lowercased().dropFirst()
    }

    mutating func capitalizeFirstLetter() {
           self = self.capitalizingFirstLetter()
    }
    
    func dateString() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .full
        dateFormatter.dateFormat = DateFormat.dateFormat;
        //dateFormatter.dateStyle = style
        return dateFormatter.date(from: self) ?? Date()
        
    }
 }
//---------------------------------------------------------------------
// MARK: PSRequest
//---------------------------------------------------------------------
class BWRequest: NSObject {
    
    var reqUrlComponent:String?
    var reqParam:Any?
    var reqParamArr:Array<Dictionary<String, Any>> = []
    var headerParam:Dictionary<String,String> = [:]
    var data: Data?
    var method:String?
    init(reqUrlComponent:String) {
        self.reqUrlComponent = reqUrlComponent
    }
}

class BWResponse: NSObject {
    var response:URLResponse?
    var resData:Data?
    var error:Error?
    var jsonType:Int?
}

//---------------------------------------------------------------------
// MARK: WebService Handler
//---------------------------------------------------------------------
class BWHandler: NSObject,URLSessionDelegate {
    fileprivate static var obj:BWHandler?
    let kBASEURL  = WebServiceUrl.kBASEURL
    let kTimeOutValue = 60
   
   let sessionToken:String? = nil
    var apiUrl:URL?
    
    static func create() -> BWHandler{
        if(obj == nil){
            obj = BWHandler()
        }
        return obj!
    }
    
    static func sharedInstance() -> BWHandler{
        if(obj == nil){
            return create()
        }
        else{
            return obj!
        }
    }
    
    //---------------------------------------------------------------------
    // MARK: appDefaultHedaer
    //---------------------------------------------------------------------
    func appDefaultHedaer() -> Dictionary<String,String>{
        let dic:Dictionary<String,String> = ["Content-Type":"application/json"]
        return dic
    }
    
    //---------------------------------------------------------------------
    // MARK: POST Web Service methods
    //---------------------------------------------------------------------
    
    func post(_ bwRequest:BWRequest,completionHandler:@escaping (BWResponse?) -> Void) -> Void {
        post(bwRequest,true, completionHandler: completionHandler )
    }
    
    func post(_ bwRequest:BWRequest,_ isShowDialog:Bool,completionHandler:@escaping (BWResponse?) -> Void) -> Void {
        let urlComponent = bwRequest.reqUrlComponent
        if urlComponent == nil {return}
        
        var urlSTR = ""
        
       urlSTR = kBASEURL + urlComponent!
        urlSTR = urlSTR.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)!
        self.apiUrl = URL(string: urlSTR)
        var request = URLRequest(url: self.apiUrl!)
        request.httpMethod =  bwRequest.method ?? "POST"
        request.timeoutInterval =  TimeInterval(kTimeOutValue);
        request.allHTTPHeaderFields = bwRequest.headerParam
        if(bwRequest.method == "PUT"){
            request.setValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        }
        // Set request param
        let reqParam = bwRequest.reqParam
        if(reqParam != nil){
            do{
                let postData = try JSONSerialization.data(withJSONObject: reqParam, options:.prettyPrinted)
                let decoded = try JSONSerialization.jsonObject(with: postData, options: [])
                // here "decoded" is of type `Any`, decoded from JSON data
                let jsonString = String(data: postData, encoding: .utf8)
                print(jsonString!)
                // you can now cast it with the right type
                if decoded is [String:AnyObject] {
                    // use dictFromJSON
                }
                request.httpBody = postData
            }catch {
                let res = BWResponse()
                res.error = nil
                completionHandler(nil)
            }
        }
        
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
       
        let dataTask = session.dataTask(with: request, completionHandler: {(data, response, error) -> Void in
            
            if data != nil{
                let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("@API: RESPONCE:\(datastring!)")
                print("\n@API: -----------------------------------------------")
            }
            
            let res = BWResponse()
            res.response = response
            
            if let resError = error {
                res.error = resError
            }
            
            if let resData = data {
                res.resData = resData
            }
             completionHandler(res)
        })
        print("@API: -----------------------------------------------\n")
        print("@API: URL:\(urlSTR)\n")
        print("@API: PARAM:\(reqParam)\n")
        
        dataTask.resume()
    }
    
    
    //---------------------------------------------------------------------
    // MARK: GET Web Service methods
    //---------------------------------------------------------------------
    
    //==============================================================
    public func makeQueryString(values: Dictionary<String,Any>) -> String {
        var querySTR = ""
        if values.count > 0 {
            querySTR = "?"
            for item in values {
                let key = item.key
                var value = item.value as? String ?? ""
                var keyValue = ""
                if(value == ""){
                    let value = item.value as? Int
                     keyValue = key + "=" + "\(value!)" + "&"
                }else{
                     keyValue = key + "=" + value + "&"
                }
                
                querySTR = querySTR.appending(keyValue)
            }
            querySTR.removeLast()
        }
        return querySTR
    }
    
    func get(_ psRequest:BWRequest,_ isShowDialog:Bool,completionHandler:@escaping (BWResponse?) -> Void) -> Void {
        
        let urlComponent = psRequest.reqUrlComponent
        
        if urlComponent == nil {return}
        
        let dictData = psRequest.reqParam
        let param = dictData as? Dictionary<String,Any> ?? [:]
        let querySTR = makeQueryString(values: param)
    
        var urlSTR = kBASEURL + urlComponent! + querySTR
        
        urlSTR = urlSTR.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)!
        let url = URL(string: urlSTR)
        var request = URLRequest(url: url!)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = psRequest.headerParam
        request.timeoutInterval =  180;

        
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
        let dataTask = session.dataTask(with: request, completionHandler: {(data, response, error) -> Void in
            
            if data != nil{
                let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("@API: RESPONCE:\(datastring!)")
                print("\n@API: -----------------------------------------------\n")
            }
            
            let res = BWResponse()
            res.response = response
            
            if let resError = error {
                res.error = resError
            }
            
            if let resData = data {
                res.resData = resData
            }
            
            //TODO: Send response back in BG thread and shift to main therad in controller only
            //DispatchQueue.main.async {
                completionHandler(res)
            //}
        })
        
        print("@API: -----------------------------------------------\n")
        print("@API: URL:\(urlSTR)\n")
        //print("@API: PARAM:\(params!)\n")
        dataTask.resume()
    }
    
    
    
 

    private func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
    
    
    func updateProfilePicture(_ bwRequest:BWRequest,videoData:Data?,_ isShowDialog:Bool,completionHandler:@escaping (BWResponse?) -> Void) -> Void {
    //        if(videoData == nil){
    //             completionHandler(nil)
    //            return;
    //        }
            let urlComponent = bwRequest.reqUrlComponent
            
            if urlComponent == nil {return}
            
            var urlSTR = ""
            
            urlSTR =  urlComponent!
            urlSTR = urlSTR.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)!
            self.apiUrl = URL(string: urlSTR)
        
            var headerParam = GlobalUtils.getInstance().getHeaderParam()
            headerParam["Content-Type"] = "multipart/form-data"
            let reqParam = bwRequest.reqParam as! [String:Any]
       
            var body = Data()
            
            
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in reqParam {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                let profileName  = "\(Date().currentTimeMillis()).png"

                
                if let data = videoData{
                    multipartFormData.append(data, withName: "user[picture]", fileName: profileName, mimeType: "image/png")
                }
                
            }, usingThreshold: UInt64.init(), to:self.apiUrl!, method: .put, headers: headerParam) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print("Succesfully uploaded")
                        let res = BWResponse()
                        if let resError = response.error {
                            res.error = resError
                        }
                        if let resData = response.data {
                            res.resData = resData
                        }
                       completionHandler(res)
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    let res = BWResponse()

                        res.error = error
                    completionHandler(res)
                }
            }
    }
    
    
    
    func uploadVideoFile(_ bwRequest:BWRequest,videoData:Data?,_ isShowDialog:Bool,completionHandler:@escaping (BWResponse?) -> Void) -> Void {
 
            let urlComponent = bwRequest.reqUrlComponent
            
            if urlComponent == nil {return}
            
            var urlSTR = ""
            
            urlSTR = kBASEURL + urlComponent!
            urlSTR = urlSTR.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)!
            self.apiUrl = URL(string: urlSTR)
            let headerParam = ["Content-Type":"multipart/form-data"]
            let reqParam = bwRequest.reqParam as! [String:Any]
            let params = reqParam as? [String:Any] ?? [:]
            var body = Data()
            
            
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in reqParam {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                if let data = videoData{
                    multipartFormData.append(data, withName: "user[picture]", fileName: "image.png", mimeType: "image/png")
                }
                
            }, usingThreshold: UInt64.init(), to:self.apiUrl!, method: .post, headers: headerParam) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print("Succesfully uploaded")
                        let res = BWResponse()
                        
                        
                        if let resError = response.error {
                            res.error = resError
                        }
                        if let resData = response.data {
                            res.resData = resData
                        }
                       completionHandler(res)
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    let res = BWResponse()

                        res.error = error
                    completionHandler(res)
                }
            }
    }
 }
