//
//  GuestListTableCell.swift
//  ECards
//
//  Created by Dhruva Madhani on 09/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

class GuestListTableCell: UITableViewCell {

    @IBOutlet weak var lblGuestName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgSucess: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
