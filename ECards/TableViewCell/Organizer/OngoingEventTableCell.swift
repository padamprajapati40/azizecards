//
//  OngoingEventTableCell.swift
//  ECards
//
//  Created by Dhruva Madhani on 02/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

class OngoingEventTableCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var imgEvent: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
