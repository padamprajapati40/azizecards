//
//  CustomizeCardTableCell.swift
//  ECards
//
//  Created by Dhruva Madhani on 03/01/20.
//  Copyright © 2020 Dhruva Madhani. All rights reserved.
//

import UIKit

protocol CustomizeMessageDelegate {
    func addMessageAndColor(_ tag : Int)
    func chooseColor(_ tag : Int)
    func EditMessage(_ tag : Int)
    func DeleteMessage(_ tag : Int)
}

class CustomizeCardTableCell: UITableViewCell, UITextFieldDelegate {

    var delegate : CustomizeMessageDelegate?
    
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var txtMessage: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btnColorAction(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.chooseColor(sender.tag)
        }
    }
    
    @IBAction func btnDeleteAction(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.DeleteMessage(sender.tag)
        }
    }
    
    @IBAction func btnEditAction(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.EditMessage(sender.tag)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Textfield called.....")
    }
    
}
