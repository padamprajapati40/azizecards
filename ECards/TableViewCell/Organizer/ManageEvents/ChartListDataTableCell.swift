//
//  ChartListDataTableCell.swift
//  ECards
//
//  Created by Dhruva Madhani on 06/01/20.
//  Copyright © 2020 Dhruva Madhani. All rights reserved.
//

import UIKit

class ChartListDataTableCell: UITableViewCell {

    
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
