//
//  ContactTableCell.swift
//  ECards
//
//  Created by Dhruva Madhani on 16/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

protocol ContactEditAndDeleteDelegate {
    func editContact(_ tag:Int)
    func deleteContact(_ tag:Int)
}

class ContactTableCell: UITableViewCell {

    var delegate: ContactEditAndDeleteDelegate?
    
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSrNumber: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btnEditAction(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.editContact(sender.tag)
        }
        
    }
    
    @IBAction func btnDeleteAction(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.deleteContact(sender.tag)
        }
    }
}
