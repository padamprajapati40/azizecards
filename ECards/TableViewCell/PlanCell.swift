//
//  PlanCell.swift
//  AzizECards
//
//  Created by Dhruva Madhani on 22/01/20.
//  Copyright © 2020 Dhruva Madhani. All rights reserved.
//

import UIKit

protocol planMessageDelegate{
    func mainPlanMethodForMessage(_  tag : Int)
    func mainPlanSeletionMethod (_ tag : Int, isSelectedPlan : Bool, indexPath : IndexPath)
}

class PlanCell: UITableViewCell {

    var isSelectedPlan = Bool()
    var delegate : planMessageDelegate?
    var indexPath : IndexPath?
    
    @IBOutlet weak var btnPlan: UIButton!
    @IBOutlet weak var btnInfo: UIButton!
    @IBOutlet weak var lblPlan: UILabel!
    @IBOutlet weak var viewPlan: UIView!
    
    
    @IBAction func btnInfoAction(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.mainPlanMethodForMessage(sender.tag)
        }
    }
    
    @IBAction func btnPlanAction(_ sender: UIButton) {
        let btn = sender as! UIButton
      //  btn.isSelected = !btn.isSelected;
        if let delegate = delegate{
            delegate.mainPlanSeletionMethod(btn.tag, isSelectedPlan: btn.isSelected ?? false, indexPath: IndexPath(row: btn.tag, section: 0) )
        }
       }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
