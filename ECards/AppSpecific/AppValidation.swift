//
//  Validation.swift
//  ShortTrip
//
//  Created by prompt on 6/20/17.
//  Copyright © 2017 Prompt Softech. All rights reserved.
//

import UIKit

class AppValidation: NSObject {

    // MARK: For Email -----------------------------------------
    static func isValidEmail(textStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+    -]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: textStr)
    }
}
