//
//  Dialogs.swift
//  MuApp_Web_Api_Test
//
//  Created by macmini2 on 10/20/16.
//  Copyright © 2016 Prompt Softech. All rights reserved.
//

import UIKit
class Dialogs {
    var lodingFrame:UIView?
    var isLodingDisplay = false
    var isTostDisplay = false
    var superView:UIView?
    static let instance:Dialogs = Dialogs()
    
    /*static let sharedInstance:Dialogs = {
        return instance
    }()*/
    
    static func getInstance() -> Dialogs{
        return instance;
    }
    
    
    //---------------------------------------------------------------------
    // MARK: Show Alert
    //---------------------------------------------------------------------
    open func showAlert(_ viewController:UIViewController,_ msg:String){
        DispatchQueue.main.async {
        let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
        }
    }
    
    open func showAlert(_ viewController:UIViewController,_ msg:String,_ callback:@escaping ()->Void){
        DispatchQueue.main.async {
        let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {
            action in
            callback()
        }))
        viewController.present(alert, animated: true, completion: nil)
        }
    }
    
    open func showAlert(_ viewController:UIViewController,_ buttonText:String,_ msg:String,_ callback:@escaping ()->Void){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: buttonText, style: .default, handler: {
                action in
                callback()
            }))
            viewController.present(alert, animated: true, completion: nil)
        }
    }
}
