//
//  PlanTableCell.swift
//  ECards
//
//  Created by Dhruva Madhani on 17/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

class PlanTableCell: UITableViewCell {

    @IBOutlet weak var lblRange: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
