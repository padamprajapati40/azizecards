/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper



struct Messages : Mappable {
    var id : Int?
    var message_text : String?
    var message_date_time : String?
    var message_type : String?
    var created_at : String?
    var updated_at : String?
    var event_id : Int?
    var english_message : String?
    var arabic_message : String?
    var send_to : String?
    var language : String?
    var deliver : Bool?
    
    
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        id <- map["id"]
        message_text <- map["message_text"]
        message_date_time <- map["message_date_time"]
        message_type <- map["message_type"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        event_id <- map["event_id"]
        english_message <- map["english_message"]
        arabic_message <- map["arabic_message"]
        send_to <- map["send_to"]
        language <- map["language"]
        deliver <- map["deliver"]
    }
}

struct CompletedEvents : Mappable {
	var id : Int?
    var name : String?
    var message : String?
    var messages : [Messages]?
    var venue : String?
    var event_date : String?
    var card_theme_id : Int?
    var organizer_id : String?
    var additional_attributes : String?
    var created_at : String?
    var updated_at : String?
    var latitude : Int?
    var longitude : Int?
    var language : String?
    var message_date_time : String?
    var state : Int?
    var plan_id : Int?
    var guest_plan_id : Int?
    var total_number_of_guests : Int?
    var guests_attended : Int?
    var guest_yet_to_attend : Int?
    var plan : Plan?
    var guest_plans : [Guest_plans]?
    var attendees : [Attendees]?
    var from_time : String?
    var to_time : String?
    var invitation_message_deliver : Bool?
    var Thankyou_message_deliver : Bool?
    var can_add_attendees : Bool?
    var is_canceled : Bool?
    var instant_invitation_sent : Bool?
    var confirm_event_details : Bool?
       var selected_language : String?
       var can_send_thankyou_msg : Bool?
       var Cancelation_message_deliver : Bool?
       var Reminder_message_deliver : Bool?
       var confirm : Bool?
    var is_active : Bool?
    var is_paid : Bool?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		id <- map["id"]
        name <- map["name"]
        messages <- map["messages"]
        message <- map["message"]
        venue <- map["venue"]
        event_date <- map["event_date"]
        card_theme_id <- map["card_theme_id"]
        organizer_id <- map["organizer_id"]
        additional_attributes <- map["additional_attributes"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        language <- map["language"]
        message_date_time <- map["message_date_time"]
        state <- map["state"]
        plan_id <- map["plan_id"]
        guest_plan_id <- map["guest_plan_id"]
        total_number_of_guests <- map["total_number_of_guests"]
        guests_attended <- map["guests_attended"]
        guest_yet_to_attend <- map["guest_yet_to_attend"]
        plan <- map["plan"]
        guest_plans <- map["guest_plans"]
        from_time <- map["from_time"]
        to_time <- map["to_time"]
        invitation_message_deliver <- map["invitation_message_deliver"]
        Thankyou_message_deliver <- map["Thankyou_message_deliver"]
        can_add_attendees <- map["can_add_attendees"]
        attendees <- map["attendees"]
        is_canceled <- map["is_canceled"]
        instant_invitation_sent <- map["instant_invitation_sent"]
        confirm_event_details <- map["confirm_event_details"]
               selected_language <- map["selected_language"]
               can_send_thankyou_msg <- map["can_send_thankyou_msg"]
               Cancelation_message_deliver <- map["Cancelation_message_deliver"]
               Reminder_message_deliver <- map["Reminder_message_deliver"]
               confirm <- map["confirm"]
        is_active <- map["is_active"]
        is_paid <- map["is_paid"]
	}

}
