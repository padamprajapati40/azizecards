/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct Guard : Mappable {
	var password : String?
	var id : Int?
	var current_sign_in_at : String?
	var auth_token : String?
	var last_sign_in_at : String?
	var expiry_date : String?
	var organizer_id : Int?
	var event_id : Int?
	var country_code : String?
	var name : String?
	var phone_number : String?
	var created_at : String?
	var updated_at : String?
	var instruction : String?
	var token_expiration : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		password <- map["password"]
		id <- map["id"]
		current_sign_in_at <- map["current_sign_in_at"]
		auth_token <- map["auth_token"]
		last_sign_in_at <- map["last_sign_in_at"]
		expiry_date <- map["expiry_date"]
		organizer_id <- map["organizer_id"]
		event_id <- map["event_id"]
		country_code <- map["country_code"]
		name <- map["name"]
		phone_number <- map["phone_number"]
		created_at <- map["created_at"]
		updated_at <- map["updated_at"]
		instruction <- map["instruction"]
		token_expiration <- map["token_expiration"]
	}

}
