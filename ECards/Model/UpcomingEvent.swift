/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct UpcomingEvent : Mappable {
    var id : Int?
    var name : String?
    var message : String?
    var venue : String?
    var event_date : String?
    var card_id : Int?
    var organizer_id : Int?
    var additional_attributes : String?
    var created_at : String?
    var updated_at : String?
    var latitude : Int?
    var longitude : Int?
    var total_number_of_guests : Int?
    var guests_attended : Int?
    var guest_yet_to_attend : Int?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		id <- map["id"]
		name <- map["name"]
		message <- map["message"]
		venue <- map["venue"]
        event_date <- map["event_date"]
        card_id <- map["card_id"]
        organizer_id <- map["organizer_id"]
        additional_attributes <- map["additional_attributes"]
		created_at <- map["created_at"]
		updated_at <- map["updated_at"]
		latitude <- map["latitude"]
        longitude <- map["longitude"]
        total_number_of_guests <- map["total_number_of_guests"]
        guests_attended <- map["guests_attended"]
        guest_yet_to_attend <- map["guest_yet_to_attend"]
		
	}

}
