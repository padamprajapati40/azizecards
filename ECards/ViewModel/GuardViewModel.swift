//
//  GuardViewModel.swift
//  ECards
//
//  Created by Dhruva Madhani on 25/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import Foundation
import ObjectMapper

class GuardViewModel: NSObject {
    
    let apiClient = BWHandler.sharedInstance()
    let currentVC = UIApplication.topViewController() as? AppViewController
    let helper = AppHelper.init()
    
    
    func guardLogin ( phoneNumer : String, countryCode : String , password : String, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let fcmToken = GlobalUtils.getInstance().getDeviceToken();
            let dictData = ["phone_number":phoneNumer,"country_code":countryCode,"password":password,"type":"guard"];
            // let params = ["event_card":dictData];
            WebApi.sharedInstance.guardLogin(param: dictData) { (success, response, message, statusCode) in
                if(success == true){
                    let dictData = response as? [String:Any] ?? [:]
                    
                    let resDict = dictData
                    
                    var guardObj : Guard?
                    guardObj = Mapper<Guard>().map(JSON: dictData)
                    
                    
                    callback(success,guardObj,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }

}
