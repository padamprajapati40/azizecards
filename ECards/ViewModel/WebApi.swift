//
//  TestVC.swift
//  Nathdwara_Swift
//
//  Created by prompt on 13/08/16.
//  Copyright © 2016 prompt. All rights reserved.
//


import Foundation
import UIKit
import ObjectMapper
import Alamofire
class WebApi {
    
    static let sharedInstance: WebApi = {
        let instance = WebApi()
        return instance
    }()
    
    fileprivate func getDefaultErrorMsg() -> String{
        return NSLocalizedString("ERROR_MSG_1", comment: "")
    }
    
    fileprivate func getInternateErrorMsg() -> String{
        return NSLocalizedString("ERROR_MSG_INTERNET", comment: "")
    }
    
    fileprivate func callBackError(_ error:Error) -> Void{
        print(getDefaultErrorMsg())
    }
    
    
    func doLogin(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = AuthApiMethodName.kSignIn;
        let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = param
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
        ws.post(request) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    func get_countries( callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = AuthApiMethodName.kGet_countries;
        let request = BWRequest(reqUrlComponent: methodName)
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
        ws.get(request, true) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    //get_cities
    func get_cities( param : [String:Any] , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = AuthApiMethodName.kGet_cities;
        let request = BWRequest(reqUrlComponent: methodName)
        request.headerParam = header
        request.reqParam = param
        
        let ws = BWHandler.sharedInstance()
        ws.makeQueryString(values: param)
        ws.get(request, true) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    //get CardTypes
    func card_types( callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = OrganizerApiName.kCardTypes;
        let request = BWRequest(reqUrlComponent: methodName)
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
        ws.get(request, true) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    
    func getCards( cardID : String , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = OrganizerApiName.kCard + "\(cardID)/cards";
        let request = BWRequest(reqUrlComponent: methodName)
        request.headerParam = header
        request.reqParam = [:]
        
        let ws = BWHandler.sharedInstance()
        
        ws.get(request, true) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    
    func plansOfEvents(param: [String:Any],  callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = OrganizerApiName.kPlanList ;
        let request = BWRequest(reqUrlComponent: methodName)
        request.headerParam = header
        request.reqParam = param
        
        let ws = BWHandler.sharedInstance()
        ws.makeQueryString(values: param)
        ws.get(request, true) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    func plansOfPackageOptions(  callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = OrganizerApiName.kPlanOfPackageOptions ;
        let request = BWRequest(reqUrlComponent: methodName)
        request.headerParam = header
        request.reqParam = [:]
        
        let ws = BWHandler.sharedInstance()
        
        ws.get(request, true) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    func addInvitees(event_id : String, param:[String:Any], callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = OrganizerApiName.kAddInvitees + "\(event_id)/attendees";
        let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = param
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
        ws.post(request) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    func addMsgAndColorTOEevntCards( param:[String:Any], callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = OrganizerApiName.kAddMsgAndColorToEventCards;
        let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = param
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
        ws.post(request) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    func createEvent(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = OrganizerApiName.kCreateEvent;
        let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = param
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
        ws.post(request) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    //For Get Guards
    func getGuardList( event_id : String , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
           let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = OrganizerApiName.kAddInvitees + "\(event_id)/security_guards"  ;
           let request = BWRequest(reqUrlComponent: methodName)
           request.headerParam = header
            request.reqParam = [:]
           
           let ws = BWHandler.sharedInstance()
          // ws.makeQueryString(values: param)
           ws.get(request, true) { (response) in
               self.checkResponse(response, callback)
           }
       }
    
    func template_details( cardID : String , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = OrganizerApiName.kEventDetails + "\(cardID)";
        let request = BWRequest(reqUrlComponent: methodName)
        request.headerParam = header
        request.reqParam = [:]
        
        let ws = BWHandler.sharedInstance()
       // ws.makeQueryString(values: param)
        ws.get(request, true) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    func doSignUp(param:Dictionary<String, Any>,profilePic:UIImage!, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = AuthApiMethodName.kSignUp;
        let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = param
        request.headerParam = header
        let imageData = profilePic.pngData();
        let ws = BWHandler.sharedInstance()
         ws.uploadVideoFile(request, videoData: imageData, false) { (response) in
             self.checkResponse(response, callback)
         }
    }
    
    
    func sendMessage(param:Dictionary<String, Any>, event_id: String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = OrganizerApiName.kAddInvitees + "\(event_id)/messages";
        let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = param
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
        ws.post(request) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    func sendInstantMessage(param:Dictionary<String, Any>, event_id: String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
         let header = GlobalUtils.getInstance().getHeaderParam();
        
         let methodName = OrganizerApiName.kAddInvitees + "\(event_id)/attendees/send_invitation";
         let request = BWRequest(reqUrlComponent: methodName)
         request.reqParam = param
         request.headerParam = header
         let ws = BWHandler.sharedInstance()
         ws.post(request) { (response) in
             self.checkResponse(response, callback)
         }
     }
    //View Reports
    func getViewReports( eventId : String , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = OrganizerApiName.kAddInvitees + "\(eventId)/view_reports";
        let request = BWRequest(reqUrlComponent: methodName)
        request.headerParam = header
        request.reqParam = [:]
        
        let ws = BWHandler.sharedInstance()
        
        ws.get(request, true) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    //Get Event details
    func getEventDetails( eventID : String , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = OrganizerApiName.kAddInvitees + "\(eventID)";
        let request = BWRequest(reqUrlComponent: methodName)
        request.headerParam = header
        request.reqParam = [:]
        
        let ws = BWHandler.sharedInstance()
        
        ws.get(request, true) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    //Get profile Details of user
    func getProfileDetails(  callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = UserApiName.kProfile;
        let request = BWRequest(reqUrlComponent: methodName)
        request.headerParam = header
        request.reqParam = [:]
        
        let ws = BWHandler.sharedInstance()
        ws.get(request, true) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    //Get Tutorial
    func getTutorialDetails(  callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = AuthApiMethodName.kTutorial;
        let request = BWRequest(reqUrlComponent: methodName)
        request.headerParam = header
        request.reqParam = [:]
        
        let ws = BWHandler.sharedInstance()
        ws.get(request, true) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    
    
    //get user inquiry subjects
    func getUserInquirySubjects(  callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = UserApiName.kUserInquirySubjects;
        let request = BWRequest(reqUrlComponent: methodName)
        request.headerParam = header
        request.reqParam = [:]
        
        let ws = BWHandler.sharedInstance()
        ws.get(request, true) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    //Get open/Closed ticket status as user inquiry status
    func getUserInquiryStatus( param : [String:Any] , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = UserApiName.kUserInquiryStatus ;
        let request = BWRequest(reqUrlComponent: methodName)
        request.headerParam = header
        request.reqParam = param
        
        let ws = BWHandler.sharedInstance()
        ws.makeQueryString(values: param)
        ws.get(request, true) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    //Post raise ticket
    func raiseTicket(param:Dictionary<String, Any>,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = UserApiName.kUserInquiryStatus;
        let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = param
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
        ws.post(request) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    func createGuard(param:Dictionary<String, Any>, event_id: String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = OrganizerApiName.kAddInvitees + "\(event_id)/security_guards";
        let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = param
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
        ws.post(request) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    
    func createBulkAttendee(param:Dictionary<String, Any>, event_id: String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = OrganizerApiName.kAddInvitees + "\(event_id)/attendees/create_in_bulk";
        let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = param
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
        ws.post(request) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    func update_password(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        let header = GlobalUtils.getInstance().getHeaderParam();
        let url = WebServiceUrl.kBASEURL + "organizers/update_password"
        
        Alamofire.request(url, method: .put, parameters: param, encoding: JSONEncoding.default,headers:header )
        .responseJSON { response in
            print(response.result)
            let bwresponse = BWResponse()
            bwresponse.error = response.error;
            bwresponse.resData = response.data
            self.checkResponse(bwresponse, callback)
        }
    }
    
    func update_profile_Pic(param:Dictionary<String, Any>, profilePic:UIImage!,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
            let header = GlobalUtils.getInstance().getHeaderParam();
            let url = WebServiceUrl.kBASEURL + "organizers/update_profile"
            let request = BWRequest(reqUrlComponent: url)
            let imageData =  profilePic!.jpegData(compressionQuality: 0.5)
            request.reqParam = param;
            let ws = BWHandler.sharedInstance()
            ws.updateProfilePicture(request, videoData: imageData, false) { (response) in
                       self.checkResponse(response, callback)
                   }
       
    }
    
    func event_confirmation( event_id: String, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
           
           let header = GlobalUtils.getInstance().getHeaderParam();
           let url = WebServiceUrl.kBASEURL + "organizers/events/\(event_id)/confirm"
           
           Alamofire.request(url, method: .put, parameters: nil, encoding: JSONEncoding.default,headers:header )
           .responseJSON { response in
               print(response.result)
               let bwresponse = BWResponse()
               bwresponse.error = response.error;
               bwresponse.resData = response.data
               self.checkResponse(bwresponse, callback)
           }
       }
    
    func doSignOut(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = AuthApiMethodName.kSignOut;
        let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = param
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
        ws.post(request) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    func forgotPassword(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = AuthApiMethodName.kForgetPassword;
        let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = param
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
        ws.post(request) { (response) in
            self.checkResponse(response, callback)
        }
    }
    func verifyOTP(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = AuthApiMethodName.kVerifyOTP;
        let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = param
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
        ws.post(request) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    func verifyForgotOTP(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
         let header = GlobalUtils.getInstance().getHeaderParam();
        let url = WebServiceUrl.kBASEURL + "organizers/verify_password_code"
        Alamofire.request(url, method: .put, parameters: param, encoding: JSONEncoding.default,headers: header)
        .responseJSON { response in
            print(response.result)
            let bwresponse = BWResponse()
            bwresponse.error = response.error;
            bwresponse.resData = response.data
            self.checkResponse(bwresponse, callback)
        }
    }
    
    func resendOTP(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = AuthApiMethodName.kResendOTP;
        let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = param
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
        ws.post(request) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    
    func getTermsandConditions(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = AuthApiMethodName.kGetTermsAndConditions;
        let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = param
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
        ws.get(request, false) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    func getDiscounts( callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
          let header = GlobalUtils.getInstance().getHeaderParam();
          let methodName = OrganizerApiName.kDisCount;
          let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = [:]
          request.headerParam = header
          let ws = BWHandler.sharedInstance()
          ws.get(request, false) { (response) in
              self.checkResponse(response, callback)
          }
      }
    
//    func getNotifications(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        let header = GlobalUtils.getInstance().getHeaderParam();
//        let methodName = AuthApiMethodName.kGetNotifications;
//        let request = BWRequest(reqUrlComponent: methodName)
//        request.reqParam = param
//        request.headerParam = header
//        let ws = BWHandler.sharedInstance()
//        ws.get(request, false) { (response) in
//            self.checkResponse(response, callback)
//        }
//    }

    
    
    func guardLogin(param:Dictionary<String, Any>, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = GuardApiName.kLoginGuard;
        let request = BWRequest(reqUrlComponent: methodName)
        request.reqParam = param
        request.headerParam = header
        let ws = BWHandler.sharedInstance()
        ws.post(request) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    func updateEventDetails(param:Dictionary<String, Any>, event_id: String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
           
           let header = GlobalUtils.getInstance().getHeaderParam();
           let url = WebServiceUrl.kBASEURL + "organizers/events/\(event_id)"
           
           Alamofire.request(url, method: .put, parameters: param, encoding: JSONEncoding.default,headers:header )
           .responseJSON { response in
               print(response.result)
               let bwresponse = BWResponse()
               bwresponse.error = response.error;
               bwresponse.resData = response.data
               self.checkResponse(bwresponse, callback)
           }
       }
    
    //organizers/events/385/security_guards/37
    func updateEventGuardDetails(param:Dictionary<String, Any>, event_id: String, Guard_id : String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
              
              let header = GlobalUtils.getInstance().getHeaderParam();
              let url = WebServiceUrl.kBASEURL + "organizers/events/\(event_id)/security_guards/\(Guard_id)"
              
              Alamofire.request(url, method: .put, parameters: param, encoding: JSONEncoding.default,headers:header )
              .responseJSON { response in
                  print(response.result)
                  let bwresponse = BWResponse()
                  bwresponse.error = response.error;
                  bwresponse.resData = response.data
                  self.checkResponse(bwresponse, callback)
              }
          }
    
    
    func updateEventConfirmPayment( event_id: String,params:[String:Any],callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
           
           let header = GlobalUtils.getInstance().getHeaderParam();
           let url = WebServiceUrl.kBASEURL + "organizers/events/confirm_payment"
           print("url:\(url)")
           print("body:\(params)")
           Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default,headers:header )
           .responseJSON { response in
               print(response.result)
               let bwresponse = BWResponse()
               bwresponse.error = response.error;
               bwresponse.resData = response.data
               self.checkResponse(bwresponse, callback)
           }
       }
    
    
    
    func logoutApi( callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        let header = GlobalUtils.getInstance().getHeaderParam();
        let url = WebServiceUrl.kBASEURL + "organizers/logout"
        
        Alamofire.request(url, method: .delete, parameters: nil, encoding: JSONEncoding.default,headers:header )
        .responseJSON { response in
            print(response.result)
            let bwresponse = BWResponse()
            bwresponse.error = response.error;
            bwresponse.resData = response.data
            self.checkResponse(bwresponse, callback)
        }
    }
    
    
    func deleteEventApi( eventId : String ,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        let header = GlobalUtils.getInstance().getHeaderParam();
        let url = WebServiceUrl.kBASEURL + "organizers/events/\(eventId)"
        
        Alamofire.request(url, method: .delete, parameters: nil, encoding: JSONEncoding.default,headers:header )
        .responseJSON { response in
            print(response.result)
            let bwresponse = BWResponse()
            bwresponse.error = response.error;
            bwresponse.resData = response.data
            self.checkResponse(bwresponse, callback)
        }
    }
    
    
    func deleteEventGuardApi( eventId : String , guardId : String, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        let header = GlobalUtils.getInstance().getHeaderParam();
        let url = WebServiceUrl.kBASEURL + "organizers/events/\(eventId)/security_guards/\(guardId)/remove_guard"
        
        Alamofire.request(url, method: .delete, parameters: nil, encoding: JSONEncoding.default,headers:header )
        .responseJSON { response in
            print(response.result)
            let bwresponse = BWResponse()
            bwresponse.error = response.error;
            bwresponse.resData = response.data
            self.checkResponse(bwresponse, callback)
        }
    }
    
    func deleteAttendeeApi( eventId : String , Attendee_Id : String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
           
           let header = GlobalUtils.getInstance().getHeaderParam();
           let url = WebServiceUrl.kBASEURL + "organizers/events/\(eventId)/attendees/\(Attendee_Id)/remove_attendee"
           
           Alamofire.request(url, method: .delete, parameters: nil, encoding: JSONEncoding.default,headers:header )
           .responseJSON { response in
               print(response.result)
               let bwresponse = BWResponse()
               bwresponse.error = response.error;
               bwresponse.resData = response.data
               self.checkResponse(bwresponse, callback)
           }
       }
    
    //---------------------------------------------------------------------
    // MARK: Check Response
    //---------------------------------------------------------------------
    func checkResponse(_ response:BWResponse?,_ callback:(Bool,Any?,String,Int) -> Void){
        
        // stage 1
        let error = response?.error
        if (error != nil ){
            
            callback(false,nil,"Something went wrong",400)
        }
        
        // stage 2
        let resData = response?.resData
        let response = response?.response as? HTTPURLResponse
        let statusCode = response?.statusCode ?? 400
        if resData != nil {
            do {
                let dic:Dictionary<String,Any> = try JSONSerialization.jsonObject(with: resData!, options: JSONSerialization.ReadingOptions.allowFragments) as! Dictionary<String,Any>
                let isSuccess =  dic["success"] as? Bool ?? false;
                let message = dic["message"] as? String ?? "";
                if(isSuccess){
                    var data = dic["data"]
                    if(data == nil){
                        data = dic["date"]
                    }
                    callback(isSuccess,data,message,statusCode)
                }else{
                    
                    callback(isSuccess,nil,message,statusCode)
                }
                
            } catch {
                
                callback(false,nil,"No Data Found",statusCode)
            }
        }
    }
    
    
    func upcomingEventsAndOngoingEvents( param : [String:Any]  , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
         let header = GlobalUtils.getInstance().getHeaderParam();
         let methodName = OrganizerApiName.kUpcomingEvents
         let request = BWRequest(reqUrlComponent: methodName)
         request.headerParam = header
         request.reqParam = param
         
         let ws = BWHandler.sharedInstance()
         ws.makeQueryString(values: param)
         ws.get(request, true) { (response) in
             self.checkResponse(response, callback)
         }
     }
    
    func getCoupanCodes( param : [String:Any]  , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = AuthApiMethodName.kGetCoupanCode
        let request = BWRequest(reqUrlComponent: methodName)
        request.headerParam = header
        request.reqParam = param
        
        let ws = BWHandler.sharedInstance()
        ws.makeQueryString(values: param)
        ws.get(request, true) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    func applyDiscountCodes( param : [String:Any]  , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        let header = GlobalUtils.getInstance().getHeaderParam();
        let methodName = AuthApiMethodName.kApplyDiscount
        let request = BWRequest(reqUrlComponent: methodName)
        request.headerParam = header
        request.reqParam = param
        let ws = BWHandler.sharedInstance()
        ws.makeQueryString(values: param)
        ws.get(request, true) { (response) in
            self.checkResponse(response, callback)
        }
    }
    
    
}

