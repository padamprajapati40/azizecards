//
//  UserViewModel.swift
//  ECards
//
//  Created by Dhruva Madhani on 02/01/20.
//  Copyright © 2020 Dhruva Madhani. All rights reserved.
//

import UIKit
import Foundation
import ObjectMapper

class UserViewModel: NSObject {

    let apiClient = BWHandler.sharedInstance()
    let currentVC = UIApplication.topViewController() as? AppViewController
    let helper = AppHelper.init()
    
    //get profile data
    
    func getProfileDetails (callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            //let params : String = param
            WebApi.sharedInstance.getProfileDetails() { (success, response, message, statusCode) in
                if(success){
                    let dictData = response as? [String:Any] ?? [:]
                    let resDict = dictData
                    
                    var userObj : User?
                    userObj = Mapper<User>().map(JSON: resDict)
                   
                    callback(true,userObj,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    //get user inquiry subjects
    
    func getUserInquirySubjects (callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            //let params : String = param
            WebApi.sharedInstance.getUserInquirySubjects() { (success, response, message, statusCode) in
                if(success){
                    let dictData = response as? [String:Any] ?? [:]
                    callback(true,response,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    //get closed or open status as user inquiry status
    func getUserInquiryStatus (type: String , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let params : [String:Any] = ["type":type]
            WebApi.sharedInstance.getUserInquiryStatus(param: params) { (success, response, message, statusCode) in
                if(success){
                    let arrData = response as? [[String:Any]] ?? []
                  //  let resDict = dictData
                    //let arrEvents = dictData["data"] as? [[String:Any]] ?? []
                    let userInquiryList = Mapper<UserInquiryStatus>().mapArray(JSONArray: arrData)
                    
                    callback(true,userInquiryList,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    //post raise ticket
    func raiseTicket (Name:String, Email: String, message:String, PhoneNumber:String, user_inquiry_subject_id : Int,requestCall:Bool ,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
      
            let fcmToken = GlobalUtils.getInstance().getDeviceToken();
        let dictData : [String:Any] = ["name":Name,"email":Email, "message":message ,"phone_number":PhoneNumber,"ticket_type_id":user_inquiry_subject_id,"request_a_call":requestCall];
            let params = ["ticket":dictData];
            WebApi.sharedInstance.raiseTicket(param: params) { (success, response, message, statusCode) in
                if(success == true){
                    let dictData = response as! [String:Any]
                    
                    
                    callback(success,dictData,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
   
    //Logout
    
    func logOutApi(callback:@escaping (Bool,Any?,String,Int) -> Void)  {
        
        WebApi.sharedInstance.logoutApi() { (success, response, message, statuscode) in
            
            print(success)
            print(response)
            print(message)
            print(statuscode)
            if(success == true){
                let dictData = response as? [String:Any] ?? [:]
                               
                               
                               callback(success,dictData,message,statuscode);
                           }else{
                               callback(success,response,message,statuscode);
                           }
            
        }
    }
    
    
    //update profile pic
    func updateProfile(picture : UIImage, FirstName : String, LastName: String, Email : String, PhoneNumber : String,Country_code:String, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
         
         if(!AppHelper.isInternetConnected()){
             return callback(false,nil,InternetAlert.kInternetMessage,400)
         }else{
             let fcmToken = GlobalUtils.getInstance().getDeviceToken()
            let dictData = [ "user[first_name]":FirstName,"user[last_name]":LastName,"user[email]": Email,"user[phone_number]":PhoneNumber, "user[country_code]":Country_code] as [String : Any];
            
            WebApi.sharedInstance.update_profile_Pic(param: dictData, profilePic: picture) { (success, response, message, statusCode) in
                 if(success){
                     let dictData = response as! [String:Any]
                     let resDict = dictData
                     callback(true,resDict,message,statusCode);
                 }else{
                     callback(success,response,message,statusCode);
                 }
                 
             }
         }
     }
    
}
