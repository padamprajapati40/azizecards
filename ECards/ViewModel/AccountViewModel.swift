//
//  AccountController.swift
//  GreetClub
//
//  Created by Padam on 01/07/19.
//  Copyright © 2019 Padam. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class AccountViewModel: NSObject {
    let apiClient = BWHandler.sharedInstance()
    let currentVC = UIApplication.topViewController() as? AppViewController
    let helper = AppHelper.init()
    
    
    func doLogin (userName:String,password:String,userType:String,event_id:Int?,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
             return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let fcmToken = GlobalUtils.getInstance().getDeviceToken()
            var params : [String:Any] = ["email":userName,"password":password,"event_id":event_id];
           
            if(event_id != nil){
                 params["user[event_id]"] = event_id!
            }
            WebApi.sharedInstance.doLogin(param: params) { (success, response, message, statusCode) in
                if(success){
                        let dictData = response as! [String:Any]
                        let resDict = dictData
                        let authToken = dictData["auth_token"] as? String ?? ""
                        let userID = resDict["id"] as? Int ?? 0
                        if(authToken != ""){
                            GlobalUtils.getInstance().setSessionToken(token: authToken)
                            GlobalUtils.getInstance().setUserID(userID: userID);
                            GlobalUtils.getInstance().setLogin(islogin: true)
                            GlobalUtils.getInstance().setifUserLoggedIn(islogin: true)
                            let email : String = resDict["email"] as? String ?? ""
                            GlobalUtils.getInstance().setEmail(email: email)
                        }
                        callback(true,nil,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    func getCountries (callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
             return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let params : [String:Any] = [:]
            WebApi.sharedInstance.get_countries() { (success, response, message, statusCode) in
                if(success){
                        let dictData = response as! [[String:Any]]
                        let resDict = dictData
                    
                        callback(true,resDict,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    func getCities (param: [String:Any] , callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            //let params : String = param
            WebApi.sharedInstance.get_cities(param: param) { (success, response, message, statusCode) in
                if(success){
                    let dictData = response as! [String]
                    let resDict = dictData
                    
                    callback(true,resDict,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    func doSignUp (Name:String,LastName: String, Email: String, password:String,confirmpassword:String, Country:String, City:String , Gender: String,  PhoneNumber:String, isAccepted:Bool,event_id:Int? ,country_code : String,profilePic:UIImage?,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        if(isAccepted == false){
            let acceptTerms = AppHelper.localizedtext(key: "kSelectPolicy")
            return callback(false,nil,acceptTerms,400)
        }
        else if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let fcmToken = GlobalUtils.getInstance().getDeviceToken();
            var eventId : Int? = event_id
            var dictData : [String:Any] = ["user[first_name]":Name,"user[last_name]":LastName,"user[email]":Email, "user[password]":password,  "user[password_confirmation]":confirmpassword , "user[gender]" :Gender, "user[city]":City, "user[country]":Country, "user[phone_number]":PhoneNumber,"user[event_id]":eventId,"user[country_code]": country_code];
            if(event_id != nil){
                 dictData["user[event_id]"] = event_id!
            }
          
            let params = dictData
            WebApi.sharedInstance.doSignUp(param: params,profilePic:profilePic) { (success, response, message, statusCode) in
                if(success == true){
                    let dictData = response as! [String:Any]
                    
                    let authToken = dictData["auth_token"] as? String ?? ""
                    let userID = dictData["id"] as? Int ?? 0
                   
                    if(authToken != ""){
                        GlobalUtils.getInstance().setSessionToken(token: authToken)
                        GlobalUtils.getInstance().setUserID(userID: userID);
                        GlobalUtils.getInstance().setLogin(islogin: true)
                        GlobalUtils.getInstance().setifUserLoggedIn(islogin: true)
                    }
                    callback(success,dictData,message,statusCode);
                }else{
                        callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    
    
    
    func forgotPassword (email:String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        if(email == ""){
            return callback(false,nil,"Please enter email address.",400)
        }else if(!(currentVC?.isValidEmail(textStr: email) ?? false)){
            return callback(false,nil,"Please enter valid email address.",400)
        }
        else if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let params : [String:Any] = ["user":["email":email]]
            WebApi.sharedInstance.forgotPassword(param: params) { (success, response, message, statusCode) in
            if(success == true){
                let dictRes = response as? [String:Any]
                callback(success,dictRes,message,statusCode);
            }else{
                
                    callback(success,response,message,statusCode)
        
            }
            }
        }
    }
    
    func VerifyOTP (Otp:String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
//        if(Otp == ""){
//            return callback(false,nil,"Please enter OTP ",400)
//        }else if(!(currentVC?.isValidEmail(textStr: Otp) ?? false)){
//            return callback(false,nil,"Please enter valid email address.",400)
//        }else
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let params : [String:Any] = ["user":["otp":Otp]]
            WebApi.sharedInstance.verifyOTP(param: params) { (success, response, message, statusCode) in
                if(success == true){
                    let dictRes = response as? [String:Any]
                    callback(success,dictRes,message,statusCode);
                }else{
                    
                    callback(success,response,message,statusCode)
                    
                }
            }
        }
    }
    
    func VerifyForgotOTP (Otp:String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
 
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let params : [String:Any] = ["token": Otp]
            WebApi.sharedInstance.verifyForgotOTP(param: params) { (success, response, message, statusCode) in
                if(success == true){
                    let dictData = response as! [String:Any]
                     let authToken = dictData["auth_token"] as? String ?? ""
                     let userID = dictData["id"] as? Int ?? 0
                     if(authToken != ""){
                         GlobalUtils.getInstance().setSessionToken(token: authToken)
                         GlobalUtils.getInstance().setUserID(userID: userID);
                         GlobalUtils.getInstance().setLogin(islogin: true)
                     }
                    callback(success,dictData,message,statusCode);
                }else{
                    
                    callback(success,response,message,statusCode)
                    
                }
            }
        }
    }
    
    func resendOTP (phoneNumber:String,countryCode: String, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
  
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let params : [String:Any] = ["phone_number":phoneNumber, "country_code":countryCode]
            let dict = ["user":params]
            WebApi.sharedInstance.resendOTP(param: dict) { (success, response, message, statusCode) in
                if(success == true){
                    let dictRes = response as? [String:Any]
                    callback(success,dictRes,message,statusCode);
                }else{
                    
                    callback(success,response,message,statusCode)
                    
                }
            }
        }
    }
    
    
    func updatePassword(password:String,confirmPassword:String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let fcmToken = GlobalUtils.getInstance().getDeviceToken()
            let dictData = ["password":password,"password_confirmation":confirmPassword];
            let dic = ["user":dictData]
            WebApi.sharedInstance.update_password(param: dic) { (success, response, message, statusCode) in
                if(success){
                    let dictData = response as! [String:Any]
                    let resDict = dictData
                    callback(true,resDict,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    
    func getTermsandConditions (callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
            let params : [String:Any] = [:]
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            WebApi.sharedInstance.getTermsandConditions(param: params) { (success, response, message, statusCode) in
               if(success == true){
                   let termsString = response as? String ?? ""
                    callback(success,termsString,message,statusCode);
                }else{
                        callback(success,response,message,statusCode);
                
                }
                
            }
        }
    }
    
    
    //Tutorial
    
    func getTutorial (callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
             return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let params : [String:Any] = [:]
            WebApi.sharedInstance.getTutorialDetails() { (success, response, message, statusCode) in
                if(success){
                        let arrData = response as? [[String:Any]] ?? []
                       // let resDict = dictData
                        var tutorialList : [Tutorial] = [];
                        tutorialList = Mapper<Tutorial>().mapArray(JSONArray: arrData)
                        callback(true,tutorialList,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    
}
