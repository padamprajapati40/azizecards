//
//  OrganizerViewModel.swift
//  ECards
//
//  Created by Dhruva Madhani on 04/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit
import Foundation
import ObjectMapper
import Alamofire
class OrganizerViewModel: NSObject {

    let apiClient = BWHandler.sharedInstance()
    let currentVC = UIApplication.topViewController() as? AppViewController
    let helper = AppHelper.init()
    
    
     func getCardTypes (callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            //let params : String = param
            WebApi.sharedInstance.card_types() { (success, response, message, statusCode) in
                if(success){
                    let dictData = response as! [String:Any]
                    let resDict = dictData
                    let arrBrowseByEvents = resDict["event_types"] as? [[String:Any]] ?? [[:]]
                    
                  
                    var cardTypeList : [CardTypes] = [];
                    cardTypeList = Mapper<CardTypes>().mapArray(JSONArray: arrBrowseByEvents)
                    cardTypeList = cardTypeList.sorted(by: { $0.name ?? "" < $1.name ?? "" })
                    
                    callback(true,cardTypeList,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    
    
     func getCoupanCodes (callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            //let params : String = param
            WebApi.sharedInstance.getCoupanCodes(param: [:]) { (success, response, message, statusCode) in
                if(success){
                    let dictData = response as? [[String:Any]] ?? []
                    let resDict = dictData
                   
                    var coupanList : [Coupan] = [];
                    coupanList = Mapper<Coupan>().mapArray(JSONArray: resDict)
                  //  cardTypeList = Coupan.sorted(by: { $0.name ?? "" < $1.name ?? "" })
                    
                    callback(true,coupanList,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    func applyDiscountCodes (params : [String:Any],callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            //let params : String = param
            WebApi.sharedInstance.applyDiscountCodes(param: params) { (success, response, message, statusCode) in
                if(success){
                    let dictData = response as? [String:Any] ?? [:]
                    let resDict = dictData
                   
                   
                  //  cardTypeList = Coupan.sorted(by: { $0.name ?? "" < $1.name ?? "" })
                    
                    callback(true,dictData,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    
    
    
    
    func getUpcomingEvents (callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            //let params : String = param
            WebApi.sharedInstance.card_types() { (success, response, message, statusCode) in
                if(success){
                    let dictData = response as! [String:Any]
                    let resDict = dictData
                    let dicUpcomingEvents = resDict["upcoming_event"] as? [String:Any] ?? [:]
                    
                    
                    var upcomingEventList : UpcomingEvent?
                    upcomingEventList = Mapper<UpcomingEvent>().map(JSON: dicUpcomingEvents)
//                    var cardTypeList : [CardTypes
//                    cardTypeList = Mapper<CardTypes>().mapArray(JSONArray: arrBrowseByEvents)
                    
                    
                    callback(true,upcomingEventList,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    func getCards (cardId: String, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let params : [String:Any] = [:]
            
            WebApi.sharedInstance.getCards(cardID:cardId) { (success, response, message, statusCode) in
                if(success){
                    let arrCards = response as? [[String:Any]] ?? []
                    var cardTypeList : [Card] = [];
                    cardTypeList = Mapper<Card>().mapArray(JSONArray: arrCards)
                    callback(true,cardTypeList,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
            }
        }
    }
    func getEventDetails (cardId : String, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
       //     let params : [String:Any] = param
            
            WebApi.sharedInstance.getEventDetails(eventID: cardId) { (success, response, message, statusCode) in
                if(success){
                    let dictData = response as? [String:Any] ?? [:]
                    let resDict = dictData
                    var cardDetailsList : EventDetails?
                    cardDetailsList = Mapper<EventDetails>().map(JSON: resDict)
                    callback(true,cardDetailsList,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
            }
        }
    }
    
    func createEvents (Name:String,Date: String, PickLocation: String, ChooseLanguage:String,fromDate:String,ToTime:String, numberOfAttendes:String, messageDateTime:String , cardId: String,  EventCardId:String,message: String,latitude: String ,longitude:String, plan_id : String,packageID:Int,isInvitationSend:Bool,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
       if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
        let fcmToken = GlobalUtils.getInstance().getDeviceToken();
        var msgDateTime = messageDateTime
        if(isInvitationSend == true){
            msgDateTime = ""
        }
        let dictData = ["card_theme_id":cardId,"from_time":fromDate,"to_time":ToTime,"name":Name,"message":message, "venue":PickLocation, "event_date":Date , "latitude" :latitude, "longitude":longitude, "language":ChooseLanguage,"message_date_time":msgDateTime, "event_card_id":EventCardId,"plan_id":packageID,"organizer_id":GlobalUtils.getInstance().userID(),"instant_invitation_sent":isInvitationSend] as [String : Any];
        let params = ["event":dictData,"guest_plan_id": plan_id] as [String : Any];
        WebApi.sharedInstance.createEvent(param: params) { (success, response, message, statusCode) in
                if(success == true){
                    let dictData = response as! [String:Any]
                    let id = dictData["id"] as? String ?? ""
                    print(id)
                    print(dictData)
                  
                    callback(success,dictData,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    
    func getGuardList (event_id : String, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
           
           if(!AppHelper.isInternetConnected()){
               return callback(false,nil,InternetAlert.kInternetMessage,400)
           }else{
          //     let params : [String:Any] = param
               
               WebApi.sharedInstance.getGuardList(event_id: event_id) { (success, response, message, statusCode) in
                   if(success){
                       let arrData = response as? [[String:Any]] ?? []
                       //let resDict = dictData
                      var guardList : [Guard] = [];
                       guardList = Mapper<Guard>().mapArray(JSONArray: arrData)
                       callback(true,guardList,message,statusCode);
                   }else{
                       callback(success,response,message,statusCode);
                   }
               }
           }
       }
    
    func addAttendees (event_id : String , arrAttendees: [[String:Any]],callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let fcmToken = GlobalUtils.getInstance().getDeviceToken();
           
            let params = ["attendees":arrAttendees];
            WebApi.sharedInstance.addInvitees(event_id: event_id, param: params) { (success, response, message, statusCode) in
                if(success == true){
                    let arrData = response as? [[String:Any]] ?? []
//                    let id = dictData["id"] as? String ?? ""
//                    print(id)
//                    print(dictData)
                    
                    callback(success,arrData,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
            }
        }
    }
    
    
    func getPlanList (param: [String:Any],callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let params : [String:Any] = [:]
            WebApi.sharedInstance.plansOfEvents(param: param) { (success, response, message, statusCode) in
                if(success){
                    let arrData = response as? [[String:Any]] ?? [[:]]
                 //   let arrPlans = dictData[0]["data"] as? [[String:Any]] ?? [[:]]
                    var planList : [PlansForEvents] = [];
                    planList = Mapper<PlansForEvents>().mapArray(JSONArray: arrData)
                    
                    callback(true,planList,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    func getPlanListOfPackageOptions (callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let params : [String:Any] = [:]
            WebApi.sharedInstance.plansOfPackageOptions() { (success, response, message, statusCode) in
                if(success){
                    let dictData = response as? [String:Any] ?? [:]
//                    let arrPlans = dictData["plans"] as? [[String:Any]] ?? [[:]]
//                    var planList : [PlanOfPackage] = [];
//                    planList = Mapper<PlanOfPackage>().mapArray(JSONArray: arrPlans)
                    
                    callback(true,response,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
     func getDiscount (callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
            
            if(!AppHelper.isInternetConnected()){
                return callback(false,nil,InternetAlert.kInternetMessage,400)
            }else{
                let params : [String:Any] = [:]
                WebApi.sharedInstance.getDiscounts() { (success, response, message, statusCode) in
                    if(success){
                        let dictData = response as? [String:Any] ?? [:]
    //                    let arrPlans = dictData["plans"] as? [[String:Any]] ?? [[:]]
    //                    var planList : [PlanOfPackage] = [];
    //                    planList = Mapper<PlanOfPackage>().mapArray(JSONArray: arrPlans)
                        
                        callback(true,response,message,statusCode);
                    }else{
                        callback(success,response,message,statusCode);
                    }
                    
                }
            }
        }
    
    
    
    //Delete Event Api
    func deleteEventApi(eventId : String, callback:@escaping (Bool,Any?,String,Int) -> Void)  {
           if(!AppHelper.isInternetConnected()){
                      return callback(false,nil,InternetAlert.kInternetMessage,400)
           }else{
            WebApi.sharedInstance.deleteEventApi(eventId: eventId) { (success, response, message, statusCode) in
                print(response)
                print(message)
                print(statusCode)
                if success{
                     callback(success,response,message,statusCode);
                }else{
                     callback(success,response,message,statusCode);
                }
            }
        }
        
    }
    //Delete Guard Api
    func deleteGuardApi(eventId : String, guardId: String,callback:@escaping (Bool,Any?,String,Int) -> Void)  {
              if(!AppHelper.isInternetConnected()){
                         return callback(false,nil,InternetAlert.kInternetMessage,400)
              }else{
                
                WebApi.sharedInstance.deleteEventGuardApi(eventId: eventId, guardId: guardId) { (success, response, message, statusCode) in
                    print(response)
                    print(message)
                    print(statusCode)
                    if success{
                        callback(success,response,message,statusCode);
                    }else{
                        callback(success,response,message,statusCode);
                    }
                }
 
           }
           
       }
    
    
    
    //Delete Attendee Api
    func deleteAttendeeApi(eventId : String, Attendee_ID: String,callback:@escaping (Bool,Any?,String,Int) -> Void)  {
              if(!AppHelper.isInternetConnected()){
                         return callback(false,nil,InternetAlert.kInternetMessage,400)
              }else{
               WebApi.sharedInstance.deleteAttendeeApi(eventId: eventId, Attendee_Id: Attendee_ID) { (success, response, message, statusCode) in
                   print(response)
                   print(message)
                   print(statusCode)
                   if success{
                        callback(success,response,message,statusCode);
                   }else{
                        callback(success,response,message,statusCode);
                   }
               }
           }
           
       }
    
    //Event_Cards
    
    func addMsgAdColorToEventCards (arrCard : [[String:Any]], Card_theme_id : String , EventId : String ,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let fcmToken = GlobalUtils.getInstance().getDeviceToken();
            let dictData : [String:Any] = ["card_text":arrCard,"card_theme_id":Card_theme_id, "event_id":EventId] as? [String : Any] ?? [:];
            let params = ["event_card":dictData];
            WebApi.sharedInstance.addMsgAndColorTOEevntCards(param: params) { (success, response, message, statusCode) in
                if(success == true){
                    let dictData = response as? [String:Any] ?? [:]
                   
                    let resDict = dictData
                    var eventCardList : EventCards?
                    eventCardList = Mapper<EventCards>().map(JSON: resDict)
                    
                    callback(success,eventCardList,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    func getUpcomingOrCompletdEvents (type: String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
            
            if(!AppHelper.isInternetConnected()){
                return callback(false,nil,InternetAlert.kInternetMessage,400)
            }else{
                let params : [String:Any] = ["type" : type]
                WebApi.sharedInstance.upcomingEventsAndOngoingEvents(param: params) { (success, response, message, statusCode) in
                    if(success){
                        let dictData = response as? [String:Any] ?? [:]
                        let arrEvents = dictData["events"] as? [[String:Any]] ?? []
                        let eventCardList = Mapper<CompletedEvents>().mapArray(JSONArray: arrEvents)
                        callback(true,eventCardList,message,statusCode);
                    }else{
                        callback(success,response,message,statusCode);
                    }
                    
                }
            }
        }
    
    
    func getViewReportsOfEvents (eventId: String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let params : [String:Any] = [:]
            WebApi.sharedInstance.getViewReports(eventId: eventId) { (success, response, message, statusCode) in
                if(success){
                    let dictData = response as? [String:Any] ?? [:]
//                    let arrEvents = dictData["events"] as? [[String:Any]] ?? []
//                    let eventCardList = Mapper<CompletedEvents>().mapArray(JSONArray: arrEvents)
                    callback(true,response,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    func sendMessage ( EventId : String ,language : String,MessageDateTime : String,SendTo : String,MessageType : String,  EnglishMessage : String , ArabicMessage : String, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let fcmToken = GlobalUtils.getInstance().getDeviceToken();
            let dictData = ["language":language,"message_date_time":MessageDateTime,"send_to":SendTo, "message_type":MessageType,"english_message": EnglishMessage,"arabic_message":ArabicMessage];
           // let params = ["event_card":dictData];
            WebApi.sharedInstance.sendMessage(param: dictData, event_id: EventId) { (success, response, message, statusCode) in
                if(success == true){
                    let dictData = response as? [String:Any] ?? [:]
                    
                    let resDict = dictData
                    
                    
                    callback(success,response,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    
    func sendInstantMessage ( EventId : String , type: String, send_Message : String ,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let fcmToken = GlobalUtils.getInstance().getDeviceToken();
            let dictData = ["type":type,"send_message":send_Message];
           // let params = ["event_card":dictData];
            WebApi.sharedInstance.sendInstantMessage(param: dictData, event_id: EventId) { (success, response, message, statusCode) in                if(success == true){
                    let dictData = response as? [String:Any] ?? [:]
                    
                    let resDict = dictData
                    
                    
                    callback(success,response,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    
    func createGuard ( EventId : String ,arrGuard : [[String:Any]], callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let fcmToken = GlobalUtils.getInstance().getDeviceToken();
          
             let params = ["guards":arrGuard];
            WebApi.sharedInstance.createGuard(param: params, event_id: EventId) { (success, response, message, statusCode) in
                if(success == true){
                    let dictData = response as? [String:Any] ?? [:]
                    
                    let resDict = dictData
                    
                    
                    callback(success,response,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    
    func createBulkAttendee ( EventId : String ,file : [[String:Any]], callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let fcmToken = GlobalUtils.getInstance().getDeviceToken();
          
             let params = ["file":file];
             let headers: HTTPHeaders = [
                 "Content-type": "multipart/form-data"
             ]

            
            
            
//            WebApi.sharedInstance.createGuard(param: params, event_id: EventId) { (success, response, message, statusCode) in
//                if(success == true){
//                    let dictData = response as? [String:Any] ?? [:]
//
//                    let resDict = dictData
//
//
//                    callback(success,response,message,statusCode);
//                }else{
//                    callback(success,response,message,statusCode);
//                }
//
//            }
        }
    }
    
    //get event details......
    func getEventDetails (eventId: String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let params : [String:Any] = [:]
            WebApi.sharedInstance.getEventDetails(eventID: eventId) { (success, response, message, statusCode) in
                if(success){
                //    let dictData = response as? [String:Any] ?? [:]
                    let dictData = response as? [String:Any] ?? [:]
                    
                    let resDict = dictData
                    var eventDetailsList : EventDetails?
                    eventDetailsList = Mapper<EventDetails>().map(JSON: resDict)
                    callback(true,eventDetailsList,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
    
    
    func updateEventDetails(event_id : String, Name:String,Date: String, PickLocation: String, ChooseLanguage:String,fromDate:String,ToTime:String, numberOfAttendes:String, messageDateTime:String , cardId: String,  EventCardId:String,message: String,latitude: String ,longitude:String, plan_id : String,packageID:Int,isInvitationSend:Bool,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
          
          if(!AppHelper.isInternetConnected()){
              return callback(false,nil,InternetAlert.kInternetMessage,400)
          }else{
              let fcmToken = GlobalUtils.getInstance().getDeviceToken()
            let dictData = ["instant_invitation_sent":isInvitationSend,"card_theme_id":cardId,"from_time":fromDate,"to_time":ToTime,"name":Name,"message":message, "venue":PickLocation, "event_date":Date , "latitude" :latitude, "longitude":longitude, "language":ChooseLanguage,"message_date_time":messageDateTime, "event_card_id":EventCardId,"plan_id":plan_id,"organizer_id":GlobalUtils.getInstance().userID()] as [String : Any];
            let params = ["event":dictData,"guest_plan_id": packageID] as [String : Any];
            WebApi.sharedInstance.updateEventDetails(param: params, event_id:event_id) { (success, response, message, statusCode) in
                  if(success){
                      let dictData = response as! [String:Any]
                      let resDict = dictData
                      callback(true,resDict,message,statusCode);
                  }else{
                      callback(success,response,message,statusCode);
                  }
                  
              }
          }
      }
    
    func updateEventGuardDetails(phone_number:String,name:String, event_id : String, guard_id : String, callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
             
             if(!AppHelper.isInternetConnected()){
                 return callback(false,nil,InternetAlert.kInternetMessage,400)
             }else{
                 let fcmToken = GlobalUtils.getInstance().getDeviceToken()
                let dictData = ["phone_number":phone_number,"name":name] as [String:Any];
               let params = ["guard":dictData] as [String : Any];
                WebApi.sharedInstance.updateEventGuardDetails(param: params, event_id: event_id, Guard_id: guard_id) { (success, response, message, statusCode) in
                    
                    if (success){
                          let dictData = response as? [String:Any]
                        let resDict = dictData
                         callback(success,response,message,statusCode);
                    }else{
                         callback(success,response,message,statusCode);
                    }
                }
                
//               WebApi.sharedInstance.updateEventConfirmPayment(event_id:event_id) { (success, response, message, statusCode) in
//                     if(success){
//                         let dictData = response as! [String:Any]
//                         let resDict = dictData
//                         var eventDetailsList : EventDetails?
//                         eventDetailsList = Mapper<EventDetails>().map(JSON: resDict)
//                         callback(true,eventDetailsList,message,statusCode);
//                     }else{
//                         callback(success,response,message,statusCode);
//                     }
//
//                 }
             }
         }
    
    
    func updateEventConfirmPayment(event_id : String,params:[String:Any], callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
          
          if(!AppHelper.isInternetConnected()){
              return callback(false,nil,InternetAlert.kInternetMessage,400)
          }else{
              let fcmToken = GlobalUtils.getInstance().getDeviceToken()
           
            WebApi.sharedInstance.updateEventConfirmPayment(event_id:event_id,params:params) { (success, response, message, statusCode) in
                  if(success){
                      let dictData = response as! [String:Any]
                      let resDict = dictData
                      var eventDetailsList : EventDetails?
                      eventDetailsList = Mapper<EventDetails>().map(JSON: resDict)
                      callback(true,eventDetailsList,message,statusCode);
                  }else{
                      callback(success,response,message,statusCode);
                  }
                  
              }
          }
      }
    
    
    func event_confirmation(event_id : String,callback:@escaping (Bool,Any?,String,Int) -> Void) -> Void {
        
        if(!AppHelper.isInternetConnected()){
            return callback(false,nil,InternetAlert.kInternetMessage,400)
        }else{
            let fcmToken = GlobalUtils.getInstance().getDeviceToken()
            
          WebApi.sharedInstance.event_confirmation(event_id:event_id) { (success, response, message, statusCode) in
                if(success){
                    let dictData = response as? [String:Any] ?? [:]
                    let resDict = dictData
                    callback(true,resDict,message,statusCode);
                }else{
                    callback(success,response,message,statusCode);
                }
                
            }
        }
    }
}


