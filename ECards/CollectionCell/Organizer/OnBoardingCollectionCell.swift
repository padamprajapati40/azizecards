//
//  OnBoardingCollectionCell.swift
//  ECards
//
//  Created by Dhruva Madhani on 30/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

class OnBoardingCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgBoarding: UIImageView!
   @IBOutlet weak var viewVideo : UIView!
   
}
