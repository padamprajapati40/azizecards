//
//  EventCollectionCell.swift
//  ECards
//
//  Created by Dhruva Madhani on 02/12/19.
//  Copyright © 2019 Dhruva Madhani. All rights reserved.
//

import UIKit

class EventCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgEventType: UIImageView!
    @IBOutlet weak var lblEventTypeName: UILabel!
}
